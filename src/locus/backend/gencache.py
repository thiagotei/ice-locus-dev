'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, os

from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX,\
ICE_CODEFILE_F, ICE_CACHE_DIR_NAME
from locus.backend.genlocus import GenLocus

log = logging.getLogger("GenCache")

class GenCache(object):
    """ Generate hash and print out code regions in cache directory. """

    cachedirname = None
    _hashprologue_F = "!hash:"
    _hashepilogue_F = "\n"
    _hashprologue_C = "//hash:"
    _hashepilogue_C = "\n"

    def __init__(self):
        pass

    @staticmethod
    def creathashsyntax(hex_dig, lang):
        synt = ""
        if lang == ICE_CODEFILE_C or\
            lang == ICE_CODEFILE_CXX:
            synt = GenCache._hashprologue_C+hex_dig+GenCache._hashepilogue_C

        elif lang == ICE_CODEFILE_F:
            synt= GenCache._hashepilogue_F+hex_dig+GenCache._hashepilogue_F

        else:
            raise ValueError("Does not recgonize this filetype"
                            " %s.", newsrccode.filetype)
        return synt
    #enddef

    @staticmethod
    def fillcache(origsrccode, newsrccode, args):
        """ Fill the cache with the generated code. """

        log.info("start")

        dicthash = {}

        # Get and check all the hashes from the code regions
        for origcoreg in origsrccode.annotated():
            if origcoreg.label not in dicthash:
                dicthash[origcoreg.label] = origcoreg.hashkey
            else:
                if dicthash[origcoreg.label] != origcoreg.hashkey:
                    raise RuntimeError("Code region with same label but "
                            "different hash keys.")
            #endif
        #endfor

        # save infor about code regs to be used when printing out the Locus
        # optimization file.
        dictcoderegswout = {}

        for coreg in newsrccode.annotated():
            # Controls whether to print or not the annotations
            old_print = coreg.printannot
            coreg.printannot = False

            # print out to the cache dir the code region.
            buf = coreg.getCodeString(newsrccode.filetype)

            # Return to old value
            coreg.printannot = old_print

            # Path to save the output 
            dirtgt = GenCache.cachedirname+"/"+coreg.label+".txt"

            dictcoderegswout[coreg.label] = dirtgt

            with open(dirtgt, 'w') as f:
                # Get the hash of the golden copy version.
                hex_dig = dicthash[coreg.label]

                # Print hash
                hashstr = GenCache.creathashsyntax(hex_dig, newsrccode.filetype)
                f.write(hashstr)

                # Print code
                f.write(buf)
        #endfor

        # Write out the optimization file to use the cache generated.
        GenLocus.cacheoption(dictcoderegswout,
                GenCache.cachedirname+"/"+"cache_"+args.optfile)

        log.info("end")
    #enddef

    @staticmethod
    def createcachedir(optfile):
        if not os.path.isdir("./"+ICE_CACHE_DIR_NAME):
            os.mkdir("./"+ICE_CACHE_DIR_NAME)
            log.info("Created cache dir.")
        else:
            log.info("Cache dir existed.")
        #endif

        # Create a dir for the current opt file.
        GenCache.cachedirname = "./"+ICE_CACHE_DIR_NAME+"/"+optfile
        if os.path.isdir(GenCache.cachedirname):
            import shutil
            shutil.rmtree(GenCache.cachedirname)
        #endif

        os.mkdir(GenCache.cachedirname)
        log.info("Created cache dir for opt file "
                "{}.".format(optfile))
    #enddef

    @staticmethod
    def createhash(buf, filetype):
        import hashlib, re

        # This might be different accoring to the 
        # filetype.
        newbuf = re.sub('\s+', '', buf)
        hash_object = hashlib.sha1(newbuf.encode('utf-8'))
#        print "buf",buf,hashlib.sha1(buf).hexdigest()
#        print "newbuf",newbuf,hashlib.sha1(newbuf).hexdigest()
        hex_dig = hash_object.hexdigest()
        return hex_dig
    #enddef

    @staticmethod
    def striphashkey(buf, filetype):
        """ Get the hash key """

        hashline = None
        stripedbuf = ""
        found = False

        if filetype == ICE_CODEFILE_C or\
           filetype == ICE_CODEFILE_CXX:
            prologue = GenCache._hashprologue_C
        elif filetype == ICE_CODEFILE_F:
            prologue = GenCache._hashprologue_F

        # get the 1st line with the hash
        for line in buf.splitlines():
            if not found and line.startswith(prologue):
                hashline = line
                found = True
            else:
                stripedbuf += line+"\n"
            #endif
        #endfor

        if not hashline:
            raise RuntimeError("Could not find hash in the file.")

        # get the hash
        key = hashline.split(":", 1)
        return stripedbuf, key[1]

