'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, os
import string

from abc import ABCMeta, abstractmethod
from locus.util.constants import stringCodeType, ICE_OUTPUT_MODE_STDOUT,\
ICE_OUTPUT_MODE_INPLACE, ICE_OUTPUT_MODE_SUFFIX

log = logging.getLogger('Writer')

class Writer(object, metaclass=ABCMeta):
    """Write a src file to be optimized """

    BACKUP_SUFFIX = '.bkp'

    def __init__(self):
        pass

    def writeoutput(self, args):
        """ Write the final result according to the options passed by the user."""

        if args.output == 'stdout':
            self.unparseandwrite(sys.stdout.write)

        else:
            outputname = "undefined"
            undoBackup = None

            if args.output == ICE_OUTPUT_MODE_SUFFIX:
                brokenstr = self.srcfilename.rsplit('.', 1)
                if len(brokenstr) == 2:
                    outputname = brokenstr[0] + args.suffix + "." + brokenstr[1]
                else:
                    outputname = self.srcfilename + args.suffix

            elif args.output == ICE_OUTPUT_MODE_INPLACE:
                bkpname = self.srcfilename+Writer.BACKUP_SUFFIX
                os.rename(self.srcfilename, bkpname)
                outputname = self.srcfilename
                # Only used if output mode is inplace that needs to undo file
                # to original version.
                undoBackup = (self.srcfilename, bkpname)

            self.writetofile(outputname)

            return undoBackup

    def writetofile(self,  outputname):
        """Write to file in any way."""
        with open(outputname, 'w') as f:
            self.unparseandwrite(f.write)

    @abstractmethod
    def unparseandwrite(self, writefunc):
        raise NotImplementedError()
