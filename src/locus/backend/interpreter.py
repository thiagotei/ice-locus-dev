'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:

import logging

import locus.frontend.optlang.locus.locusast as lcast
from locus.frontend.optlang.locus.locusvisitor import GenericVisitor

log = logging.getLogger("Interpreter")

def interpreter(optspace, srccode=None, orighashes=None, args=None, combid=0):
    """Interprets each code region, the code region is given by the optspace
    parameter.
    """
    lcast.LocusNode.iceargs = args
    lcast.LocusNode.combid = combid
    log.info("comb "+str(lcast.LocusNode.combid))
    lcast.LocusNode.optid = 0
    lcast.LocusNode.codeseqname = optspace.id.name
    lcast.LocusNode.srccode = srccode
    lcast.LocusNode.orighashes = orighashes
    optspace.execute(optspace.scope)

    #print the resulting values for scope
    if log.isEnabledFor(logging.DEBUG):
        log.debug("Globe scope: {} .".format(optspace.scope.parent))
        visitor = GenericVisitor(lcast.Block)
        visitor.visit(optspace)
        for _, blk in visitor.values:
            currscope = blk.scope
            log.debug("{} - scope {} .".format(repr(blk),currscope))

    return lcast.LocusNode.srccode


def topblkinterpreter(topblk):
    """The global scope is not suppose to call any trasnformation. So, less
    parameters are required.
    Flow-control and search variables can exist on the global scope and used
    on the search def block.
    """
    log.debug("Global scope being interpreted...")
    topblk.execute(topblk.scope, inplace=True)
    #raise NotImplementedError("global scope Interpreter need to be done. change "
    #"grammar and run on the statements!.")

