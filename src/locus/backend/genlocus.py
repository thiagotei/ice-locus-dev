'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, os

from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX,\
ICE_CODEFILE_F, ICE_CACHE_DIR_NAME
from locus.frontend.optlang.locus.locusast import CodeRegDef, OptSeqDef

log = logging.getLogger("GenLocus")

class GenLocus(object):
    """ Generate locus file used specially by search mode with the best variant
    and cache mode with the saved optimizations. """

    @staticmethod
    def cacheoption(dictcoderegs, filename):
        log.debug("start")

        with open(filename, 'w') as fout:
            for label, altdescpath in dictcoderegs.items():
                buf ="CodeReg "+label+" {\n"
                buf+="\tBuiltins.Altdesc(source=\""+altdescpath+"\");\n"
                buf+="}\n"
                fout.write(buf)

        log.debug("end")

    @staticmethod
    def searchmode(optuni, filename):
        log.debug("start")

        if optuni is None:
            log.warning("Gen Locus not successful! Optuni is None.")
            return False

        with open(filename, 'w') as fout:
            optuni.topblk.showBlkStmts(buf=fout)
            if optuni.search is not None:
                optuni.search.showCode(buf=fout)
            #
            for ent in optuni.entities():
                ent.showCode(buf=fout)
            #endfor
        #endwith

        log.debug("end")
        return True
