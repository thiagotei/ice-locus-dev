'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.util.misc import unwindBackup
from locus.backend.interpreter import interpreter, topblkinterpreter
from locus.backend.gencache import GenCache
from locus.util.exceptions import OptimizationError
from locus.backend.genlocus import GenLocus

log = logging.getLogger("CodeGen")

class CodeGen(object):
    """ . """

    def __init__(self):
        pass

    @staticmethod
    def genCode(parsedsrcfiles, orighashes, optuni, args, combid, fillcache=False):
        """ Go through the files with annotations and apply the curren sequence. """
        undoBackup = []

        log.info("start")

        #if log.isEnabledFor(logging.DEBUG):
        if args.debug:
            tmpoptunipath = args.rundirpath+'/optspace-comb-'+str(combid)+'.locus'
            log.debug("Saving optspace in {} ...".format(tmpoptunipath))
            GenLocus.searchmode(optuni, tmpoptunipath)
        #endif

#        log.info(optuni)
       # optuni.show()
#        for ot in optuni.entities():
#            ot.show()

        # Global scope interpretation before code regions and Optseqs are
        # interpreted.
        topblkinterpreter(optuni.topblk)

        #for files
        # Go through each file that has an annotation.
        for filename, srccode in sorted(parsedsrcfiles.items(), key=lambda x: x[0]):
            # Go through each code region in that file.

            # Save the original version to be used by future 
            # code variants.
            newsrccode = copy.deepcopy(srccode)

            #FIXED We should get annotation in file and loop over
            #that. Not loop over codereg. if there are the same annotation with
            #in different code regions this will apply all the tranformations n
            #times on the same file.
            allannots = {}
            for coreg in srccode.annotated():
                try:
                    space = optuni.getoptspace(coreg.label)
                    if coreg.label not in allannots:
                        allannots[coreg.label] = space
                except KeyError as e:
                    log.debug("space {}. {}".format(coreg.label, e))
                    continue
                except Exception as e:
                    log.debug("looking for space {}. "
                            "{}".format(coreg.label,e))
                    raise

                log.debug("found space {} in {}.".format(space.name,\
                    filename))
            #endif srccode

            # Apply optimizations in all annotations
            # Different code regions with the same name will be optimized at the
            # same time.
            # Sorted items in dict so we can have determinism while generating
            # code.
            for annotname, space in sorted(allannots.items(), key=lambda x: x[0]):
                optid = 0
                log.info("=== optimizing space {} in {}.".format(
                        space.name, filename))
                #name = "bufspace_"+str(combid)+".txt"
                #with open(name,  'w') as bufspace:
                    #bufspace.write("###  Ast fro comb {}".format(combid))
                    #space.show(buf=bufspace)

                try:
                    newsrccode = interpreter(space, newsrccode,
                            orighashes[filename], args, combid)

                except Exception as e:
                    if isinstance(e, OptimizationError):
                        msg = str(e)
                    else:
                        msg = "Failed opt on"
                        log.exception(e)
                        #log.error(e)
                    #endif

                    log.warning("{} file: {} at "
                            "annotation: {}.".format(msg,\
                                filename, annotname))

                    for ud in undoBackup:
                        unwindBackup(ud[1], ud[0])

                    raise
            #endif allannotss

            # In cache mode print out the code regions in the cache dir.
            if fillcache:
               GenCache.fillcache(srccode, newsrccode, args)
            #endif

            if newsrccode is None:
                log.warning("Error optimizing file {}.".format(filename))
            else:
                log.info("Finished optimizing file {}.".format(filename))
                filetuple = newsrccode.writeoutput(args)
                if filetuple:
                    undoBackup.append(filetuple)
            #endif
        #endfor parsedsrcfiles

        log.info("end")
        return undoBackup
