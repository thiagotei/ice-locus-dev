'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys
from abc import ABCMeta, abstractmethod

from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX,\
ICE_CODEFILE_F, stringCodeType

log = logging.getLogger("CodeRegionInterface")

class CodeRegionInterface(object, metaclass=ABCMeta):
    """This represents all the annotated code regions in the application optimization process"""
    __index = 0

    LOOP = 0
    BLOCK = 1

    def __init__(self, label=None, hashkey=None):
        self.label = label
        self.index = CodeRegionInterface.__index
        self.hashkey = hashkey
        CodeRegionInterface.__index += 1

    @staticmethod
    def typeString(val):
        if val == CodeRegionInterface.LOOP:
            return "LOOP"
        elif val == CodeRegionInterface.BLOCK:
            return "BLOCK"
        else:
            raise ValueError("No other types accepted.")
