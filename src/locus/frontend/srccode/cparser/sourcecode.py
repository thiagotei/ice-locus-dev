'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import re, logging, sys, os, itertools, argparse

try:
    from pycparser import c_parser, c_ast, parse_file, c_generator,\
                            preprocess_file
#    from pycparser import c_ast, parse_file, preprocess_file
#    from pycparserext.ext_c_parser import GnuCParser
#    from pycparserext.ext_c_generator import GnuCGenerator

except ImportError as e:
    print("You need to install pycparser (I've used pip for it)!")
    raise

from locus.frontend.srccode.srcinterface import SourceCodeInterface
from locus.frontend.srccode.cparser.coderegion import CodeRegion
from locus.backend.srccode.writer import Writer
from locus.util.constants import stringCodeType, ICE_CODEFILE_C

log = logging.getLogger("SourceCodeCParser")

ENDBLOCK = 1003 # this to help the prama parser

class SourceCode(SourceCodeInterface, Writer):
    ''' This represents source code that can have places for optimizations.
        It only keeps the annotated code regions.
    '''
    _funcinitmark = "void dummyinitmark(void *a);\n" \

    def __init__(self, srcname=None, ft=None, preprocargs=""):
        if ft != ICE_CODEFILE_C:
            raise RuntimeError("Only implemented for C files. Received "
                    "{} of type {}.".format(srcname, stringCodeType(ft)))

        SourceCodeInterface.__init__(self, srcname, ft)
        # Represents a list of code regions
        self.__code_regions = []
        self.ast = None
        self.includes = []
        self.preprocargs = []
        if preprocargs:
            for pr in preprocargs:
                self.preprocargs.append(r'-I'+pr)
            #endfor
        #endfor
        log.debug("Preprocargs: {}".format(self.preprocargs))


    def readandparse(self, includeargs=None):
        # Gen the AST
#        self.ast = parse_file(self.srcfilename, use_cpp=True,
#                     cpp_args=r'-I./fake_libc_include')
        text = self.readandpreprocess(includeargs)
        self.parseandtraverse(text)

        return self

    def readandpreprocess(self, includeargs=None):
        # save the includes and put a function as a mark.
        basefilename, extfilename = os.path.splitext(self.srcfilename)
        tmpfilename = basefilename + "_ice_Xfha_" + extfilename

        filetouse = self.saveincludes(self.srcfilename, tmpfilename,
                self.includes)

        path = os.path.abspath(__file__)

        extraincs = []
        if includeargs is not None:
            for inc in includeargs:
                extraincs.append(r'-I'+inc)
            #endfor
        #endif

        dir_path = os.path.dirname(path)
        cpp_args = [r'-E',r'-std=c99',r'-I'+dir_path+'/fake_libc_include',r'-I.']+self.preprocargs+extraincs
        log.debug("cpp args: {}".format(cpp_args))

        preprocres = preprocess_file(filetouse,
                cpp_path='cpp',
                cpp_args=cpp_args)

        # remove the file created$
        if os.path.isfile(tmpfilename):
            os.remove(tmpfilename)

        #with open("debug_XFFS.c",'w') as f:
        #    f.write(preprocres)

        return preprocres

    def gethashes(self):
        hashes = []
        log.warning("ATTENTION!!! Hashes are not supported for SourceCodeCParser instances.")
        return hashes

    @classmethod
    def saveincludes(cls, srcfilename, outputfilename, includes):
        #log.debug("start")

        incl = '^[ \t]*(#\w+[ \t]*(?:<.*>|\".*\"))[ \t]*\n'
        include_re = re.compile(incl, flags=re.MULTILINE)
        # return the file to use
        retname = srcfilename
        with open(srcfilename, 'r') as f:
            buf1 =  f.read()
            buf = buf1.replace("__restrict__","restrict")
            matchobj = include_re.findall(buf)
            if matchobj:
                # save the includes
                includes.extend(matchobj)

                # find the position of the last match
                pos = buf.rfind(matchobj[-1])
                if pos >= 0:
                    #find the first newline after the 
                    posslashn = buf[pos:].find('\n')
                    if posslashn >= 0:
                        placetoput = pos+posslashn+1
                        newbuf = buf[:placetoput] \
                                + cls._funcinitmark \
                                + buf[placetoput:]
                        with open(outputfilename, 'w') as f:
                            f.write(newbuf)

                        retname = outputfilename
                   #     log.debug("Written changes to file "
                   #             "{}.".format(outputfilename))
                    else:
                        log.error("Could not find newline after last "
                                "include at file {}.".format(srcfilename))
                    #endif posslashn
                else:
                    log.error("Could not find the last "
                    "match position file {}.".format(srcfilename))
                #endif pos
            else:
                log.warning("No includes found at {}.".format(srcfilename))
            #endif matchobj

        return retname

    def parseandtraverse(self, text):
        #log.debug("start")
        self.ast = c_parser.CParser().parse(text, self.srcfilename)
        #self.ast = GnuCParser().parse(text, self.srcfilename)

        v = PragmaVisitor(self.__code_regions)
        v.visit(self.ast)
        #log.debug("end")

    def unparseandwrite(self, writefunc):
        generator = c_generator.CGenerator()
        #generator = GnuCGenerator()
        buf = generator.visit(self.ast)

        # remove funcmark and add back the includes
        pos = buf.find(self._funcinitmark)
        if pos >= 0:
            buf = "\n".join(self.includes) + "\n" + \
                    buf[pos+len(self._funcinitmark):]
        else:
            log.debug("could not find the func mark!")

        return writefunc(buf)

    def __iter__(self):
        return self.__code_regions.__iter__()

    def __len__(self):
        return len(self.__code_regions)

    def annotated(self):
        # This is only to keep coherent with the regex parser. 
        # Keep only the annotated regions so no need for filter
        return self.__iter__()

    def cleanUpSingleCmpd(self):
        """
        Find all the Compound statemtnes that have a single statement that is another Compound stmt.
        Then I remove it. RoseLori adds a lot of them, that's I try to clean them up.
        """

        v = GetSingleCmpdVisitor()
        v.visit(self.ast)

        for parent, cmpd in v.singlecmpds:
            if isinstance(parent, c_ast.Compound):
                # Remove the pragma from current compound block.
                posindex = _findingCompoundChild(parent, cmpd)
                if posindex:
                    if len(posindex) > 1: log.warning("Found multiple objects on"
                            "the list! Using the 1st one.")
                    index = posindex[0]
                    parent.block_items[index] = cmpd.block_items[0]
                else:
                    log.warning("Could not find tgt pragma {} in parent "
                        "{}!".format(cmpd, parent))
                ###
            elif isinstance(parent, c_ast.For):
                parent.stmt = cmpd.block_items[0]

            #endif


    def adjustLoopCoreg(self,tgtpragma):
        """Adjust all the loop code regions with the label as tgtpragma to be before the 1st loop
        in the code region.
        """
        #print "adjustLoopCoreg ================="
        for cr in self:
            #print cr.label, cr.type, CodeRegion.LOOP
            if cr.label == tgtpragma and cr.type == CodeRegion.LOOP:
                #check if the following statement is a Loop. if not find the 1st loop
                # and change the pragma.
                if not isinstance(cr.startnode, c_ast.For):
                    if isinstance(cr.startnode, c_ast.Compound):
                        #print cr
                        v = FindFirstLoopVisitor()
                        v.visit(cr.startnode)
                        #print "Aft Search",v.forloopparent
                        #print "Aft loop",v.forloop

                        # Remove the pragma from current compound block.
                        posindex = _findingCompoundChild(cr.parent, cr.pragmabegin)
                        if posindex:
                            if len(posindex) > 1: log.warning("Found multiple objects on"
                                    "the list! Using the 1st one.")
                            index = posindex[0]
                            cr.parent.block_items.pop(index)
                        else:
                            raise RuntimeError("Could not find tgt pragma {} in parent "
                                "{}!".format(cr.pragmabegin, cr.parent))
                        ###

                        forloopparent = v.forloopparent

                        #v.forloop.show()
                        # Add the pragma to the block inside
                        posindex = _findingCompoundChild(forloopparent, v.forloop)
                        if posindex:
                            if len(posindex) > 1: log.warning("Found multiple objects on"
                                    "the list! Using the 1st one.")
                            index = posindex[0]
                            #print "before ########", index
                            #v.forloopparent.show()
                            forloopparent.block_items.insert(index, cr.pragmabegin)
                            #print "after ########"
                            #forloopparent.show()
                            cr.parent = forloopparent
                            cr.startnode = v.forloop
                        else:
                            raise RuntimeError("Could not find tgt loop {} in parent "
                                "{}!".format(v.forloop, forloopparent))
                        ###

                    elif isinstance(cr.startnode, c_ast.Decl) or \
                           isinstance(cr.startnode, c_ast.Assignment) :

                        if not isinstance(cr.parent, c_ast.Compound):
                            raise RuntimeError("Execting a Compound parent, {}.".format(type(cr.parent)))

                        # Find the pragma position current compound block.
                        posindex = _findingCompoundChild(cr.parent, cr.pragmabegin)
                        if posindex:
                            if len(posindex) > 1: log.warning("Found multiple objects on"
                                    "the list! Using the 1st one.")
                            pragmaindex = posindex[0]
                        else:
                            raise RuntimeError("Could not find tgt pragma {} in parent "
                                "{}!".format(cr.pragmabegin, cr.parent))
 
                        forloop = None
                        forindex = pragmaindex+1
                        if len(cr.parent.block_items) <= forindex:
                            raise RuntimeError("Compound too short, {} items, for index {}.".format(len(cr.parent.block_items), forindex))

                        #cr.parent.show()
                        #print pragmaindex
                        # Find the 1st loop after the pragma
                        for ch in cr.parent.block_items[forindex:]:
                            #print type(ch), forindex
                            if isinstance(ch, c_ast.For):
                                foloop = ch
                                break
                            forindex += 1

                        # Remove the pragma from compound
                        cr.parent.block_items.pop(pragmaindex)
                        ###

                        #forloop.show()
                        # Add the pragma to the block inside
                        if forindex:
                            #print "before ########", index
                            #forloopparent.show()
                            cr.parent.block_items.insert(forindex-1, cr.pragmabegin)
                            #print "after ########"
                            #forloopparent.show()
                            #cr.parent = forloopparent
                            cr.startnode = forloop
                        else:
                            raise RuntimeError("Could not find tgt loop {} in parent "
                                "{}!".format(forloop, forloopparent))

                    else:
                        raise RuntimeError("Only prepared for fixing pragmas "
                                "before Compound, Decl, Assignment nodes. {}".format(type(cr.startnode)))
                #print "afterwards!!!"
                #print cr


    def removePragmas(self, tgtpragmalbl):
        """Remove  all pragmas with the given base string."""
        v = BaseStrPragmaVisitor(tgtpragmalbl)
        v.visit(self.ast)

        for par, node in v.pragfound:
            # remove tgtloop from parent and label in its place
            if isinstance(par, c_ast.Compound):
                posindex = _findingCompoundChild(par, node)
                if posindex:
                    if len(posindex) > 1: log.warning("Found multiple objects on"
                            "the list! Using the 1st one.")
                    index = posindex[0]
                    par.block_items.pop(index)
                else:
                    log.error("Could not find tgt loop {} in parent "
                            "{}!".format(tgtloop, tgtloopparent))
                    raise

            #elif isinstance(par, c_ast.For):
            else:
                raise NotImplementedError()

    def replacePragmaWithCoreg(self, tgtpragmalbl, newpragma):
        """Look for a specific pragma and the ICE one to denote a code region. Then traverse the ast again
           creating the structures representing the new code regions.
           ONLY TESTED with loop annotations!!!
        """
        v = ChangeStrPragmaVisitor(tgtpragmalbl, newpragma)
        v.visit(self.ast)

        new_code_regions = []
        v = PragmaVisitor(new_code_regions)
        v.visit(self.ast)

        self.__code_regions = new_code_regions

    def replaceCoregRefWithPragma(self, coregref, tgtref, newpragma):
        """ This receives a reference to the code region. 
        ATTENTION: In this case only the reference is changed. If there are any other code region with the same label
        it will not be changed in this version.
        ---
        It tgtref can be any node under the coregref. So, remove the ICE pragma or not? It depends.
        """

        tgtloopparent, tgtloop = tgtref

        # if the target loop is the 1st. Then the pragma is replaced.
        if coregref.startnode == tgtloop:
            coregref.pragmabegin.string = newpragma

        else:
            #Create the label to tgt loop
            c_ast_pragma = c_ast.Pragma(newpragma, tgtloop.coord)

            # remove tgtloop from parent and label in its place
            if isinstance(tgtloopparent, c_ast.Compound):
                indextgtloop = _findingCompoundChild(tgtloopparent, tgtloop)
                if indextgtloop:
                    tgtloopparent.block_items.insert(indextgtloop[0],
                            c_ast_pragma)
                else:
                    log.error("Could not find tgt loop {} in parent "
                            "{}!".format(tgtloop, tgtloopparent))
                    return False

            elif isinstance(tgtloopparent, c_ast.For):
                # In this case needs to create a Compound.
                block_items = [c_ast_pragma, tgtloop]
                tgtloopparent.stmt = c_ast.Compound(block_items,
                        tgtloop.coord)
            else:
                raise NotImplementedError()
            #endif
        #endif

        # Needs to remove the pragma for endblock
        if coregref.type == CodeRegion.BLOCK:
            # needs to remove the pragma from the end block
            raise NotImplementedError()

    # Replace the code region with Pragma
    def replaceCoregWithPragma(self, tgtpragmalbl, newpragma):
        for cr in self:
            if cr.label == tgtpragmalbl:
                cr.pragmabegin.string = newpragma

                # Needs to remove the pragma for endblock
                if cr.type == CodeRegion.BLOCK:
                    if isinstance(cr.parent, c_ast.Compound):

                        posindex = _findingCompoundChild(cr.parent, cr.pragmaend)

                        # Get all indexes of all nodes referring the pragma with same
                        # annotations. Just change the 1st one.
                        if posindex:
                            if len(posindex) > 1: log.warning("Found multiple objects on"
                                    "the list! Using the 1st one.")
                            index = posindex[0]
                            cr.parent.block_items.pop(index)
                        else:
                            log.error("Could not find node on parent's children.")

                    else:
                        #TODO implement for other type when the block is around
                        #a function.
                        raise NotImplementedError("need to implement for kind "
                                "of parent node %s.",cr.parent)
                       #endif isinstance
                #endif cr.type
            #endif cr.label
        #endfor self

    def replaceCodeRegion(self, tgtpragmalbl, newcoreg):
        raise NotImplementedError

    # Replace the labels found by Loop CodeRegion.
    def replaceLabelWithCoreg(self, oldlabel, tgtpragmalbl):

        # This is a sanity check of the parsing.
        for cr in self:
            if cr.label == tgtpragmalbl:
                log.error("This is a bad sign, another code region has the same"
                        " label - %s -  as the one you're trying to replace the"
                        " label.", tgtpragmalbl)
                return False
        #endfor

        lv = LabelVisitor(oldlabel)
        lv.visit(self.ast)
        if lv.alllabels:
            for lbl in lv.alllabels:
                lblparent, lblnode = lbl

                c_ast_pragma = c_ast.Pragma("@ICE loop="+tgtpragmalbl, lblnode.coord)

                pragmaparent = None
                if isinstance(lblparent, c_ast.Compound):
                    indexlblnode = _findingCompoundChild(lblparent, lblnode)
                    if indexlblnode:
                        # Remove the node
                        lblparent.block_items.pop(indexlblnode[0])

                        lblparent.block_items.insert(indexlblnode[0],
                            c_ast_pragma)
                        lblparent.block_items.insert(indexlblnode[0]+1,
                            lblnode.stmt)
                        pragmaparent = lblparent

                    else:
                        log.error("Could not find tgt loop {} in parent "
                                    "{}!".format(tgtloop, tgtloopparent))
                        return False

                elif isinstance(lblparent, c_ast.For):
                    block_items = [c_ast_pragma, lblnode.stmt]
                    lblparent.stmt = c_ast.Compound(block_items,
                                lblnode.coord)
                    pragmaparent = lblparent.stmt

                else:
                    raise NotImplementedError()

                # Remove label node from AST and replace with a Pragma Node.
                newcr = CodeRegion(tgtpragmalbl, c_ast_pragma, None, lblnode.stmt,
                        None, pragmaparent, CodeRegion.LOOP)

                # Find the correct position to insert the new CodeRegion in the
                # list.
                indexcr = None
                for i, cr in enumerate(self.__code_regions):
                    if cr.pragmabegin \
                        and cr.pragmabegin.coord \
                        and cr.pragmabegin.coord.line > c_ast_pragma.coord.line:
                            log.debug("Found a cr later in the code. l%s (%s) > l%s "
                                    "(addedpragma).", cr.pragmabegin.coord.line,
                                    cr.label,
                                    c_ast_pragma.coord.line)
                            indexcr = i
                            break
                    #endif
                #endif

                if indexcr is not None:
                    self.__code_regions.insert(indexcr, newcr)
                else:
                    self.__code_regions.append(newcr)
        return True

    # Replace the target code region with label using relative position.
    # This can be used to put label on inner loops.
    # If pos references  inner loops the ICE pragma is kept there.
    # Thge initmark and finalmark are here for coherence with regex version. 
    # The cparser version only use the initial mark.
    def replaceCoregWithLabel(self, tgtpragmalbl, newlabelname, pos="0",
            initmark="begin", finalmark="end"):
        ret = self.addLabelLoop(tgtpragmalbl, newlabelname+initmark, pos)

        # Needs to remove the pragma.
        if pos == "0":
            for cr in self:
                if cr.label == tgtpragmalbl:
                    if cr.type != CodeRegion.LOOP:
                        raise NotImplementedError("For now this is just for loops.")
                    #endif cr.type

                    if isinstance(cr.parent, c_ast.Compound):
                        indextgtloop = _findingCompoundChild(cr.parent, cr.pragmabegin)
                        if indextgtloop:
                            cr.parent.block_items.pop(indextgtloop[0])
                            cr.pragmabegin = None
                        else:
                            log.error("Could not find the pragma statement to remove."
                                    "node %s, parent %s .",cr.pragmabegin, cr.parent)

                    else:
                        raise NotImplementedError()
                    #endif isinstance
                #endif cr.label
            #endfor
        #endif
        return ret

    # Used by OpenMP tool to add pragmas in any loop level.
    def addPragmaLoop(self, tgtpragmalbl, newpragma, pos="0"):
        for cr in self:
            if cr.label == tgtpragmalbl:
                if cr.type != CodeRegion.LOOP:
                    raise NotImplementedError("For now this is just for loops.")
                #endif cr.type

                tgtanswer = self._getTargetLoop(cr, pos)
                if tgtanswer:
                    tgtloop, tgtloopparent = tgtanswer
                    log.debug("pos %s - loop %s - parent %s",pos,tgtloop,
                            tgtloopparent)

                    #Create the label to tgt loop
                    c_ast_pragma = c_ast.Pragma(newpragma, tgtloop.coord)

                    # remove tgtloop from parent and label in its place
                    if isinstance(tgtloopparent, c_ast.Compound):
                        indextgtloop = _findingCompoundChild(tgtloopparent, tgtloop)
                        if indextgtloop:
                            tgtloopparent.block_items.insert(indextgtloop[0],
                                    c_ast_pragma)
                        else:
                            log.error("Could not find tgt loop {} in parent "
                                    "{}!".format(tgtloop, tgtloopparent))
                            return False

                    elif isinstance(tgtloopparent, c_ast.For):
                        # In this case needs to create a Compound.
                        block_items = [c_ast_pragma, tgtloop]
                        tgtloopparent.stmt = c_ast.Compound(block_items,
                                tgtloop.coord)
                    else:
                        raise NotImplementedError()
                else:
                    return False

            #endif cr.label
        #endfor self
        return True

    def printouthash(self):
        raise NotImplementedError()

    # Add label to a specific position relative to the target pragma.
    # The pos is specified by number with dots. each . specifies a level and the
    # number the for loop in that level if the notation is  LOOP.
    # It is relative to the pragma position and start from 0!!
    # Example: 0.1.0 denotes 1st loop on the current level, 2 loop on the next
    # nest, and the 1st loop on the last nest.
    # if it is a block it considers any statement.
    # THIS IS ONLY FOR LOOPS FOR NOW.
    def addLabelLoop(self, tgtpragmalbl, newlabelname, pos="0"):
        labelindex = 0
        for cr in self:
            if cr.label == tgtpragmalbl:
                if cr.type != CodeRegion.LOOP:
                    raise NotImplementedError("For now this is just for loops.")
                #endif cr.type

                tgtanswer = self._getTargetLoop(cr, pos)
                if tgtanswer:
                    tgtloop, tgtloopparent = tgtanswer
                    #log.debug("pos %s - loop %s - parent %s",pos,tgtloop,
                    #        tgtloopparent)

                    #Add the label to tgt loop
                    c_ast_label = c_ast.Label(newlabelname + str(labelindex),
                                                tgtloop, tgtloop.coord)
                    labelindex += 1

                    # remove tgtloop from parent and label in its place
                    if isinstance(tgtloopparent, c_ast.Compound):
                        indextgtloop = _findingCompoundChild(tgtloopparent, tgtloop)
                        if indextgtloop:
                            tgtloopparent.block_items[indextgtloop[0]] = c_ast_label
                        else:
                            log.error("Could not find tgt loop {} in parent "
                                    "{}!".format(tgtloop, tgtloopparent))
                            return 0

                    elif isinstance(tgtloopparent, c_ast.For):
                        tgtloopparent.stmt = c_ast_label
                    else:
                        raise NotImplementedError()
                else:
                    return 0

            #endif cr.label
        #endfor self
        return labelindex

    def _getTargetLoop(self, cr, pos):

        listpos = pos.split(".")
        tgtloop = None
        tgtloopparent = None
        if listpos:
            curindex = int(listpos[0])
            if curindex == 0:
                    tgtloop, tgtloopparent = self._findloop(cr.parent, cr.startnode, listpos)

            elif curindex > 0:
                if isinstance(cr.parent, c_ast.Compound):
                    # index of the startnode of current loop code region
                    # in the compound list.
                    indexpragma = _findingCompoundChild(cr.parent, cr.startnode)
                    if indexpragma:
                        tgtloop, tgtloopparent = self._findloop(None, cr.parent,
                            listpos, indexpragma[0])
                    else:
                        log.error("Could not find tgt loop {} in parent "
                                    "{}!".format(cr.parent, cr.startnode))
                        return False
                else:
                    log.error("For curindex > 0 parent got be Compound "
                            ": %s recved.",curindex)
                    return False
                #endif isinstance
            #endif curindex

            if tgtloop is None:
                log.error("Could not find loop on position - {} -.".format(pos))
                return False
        else:
            log.error("Position - {} - received is not "
                    "conforming.".format(pos))
            return False
        #endif listpos

        return tgtloop, tgtloopparent

    # Find a loop recursively
    @classmethod
    def _findloop(cls, parent, node, listpos, relativepos=0):
        curlist = list(listpos)

        if not curlist:
            log.error("curlist is empty. weird!")
            return None, None

        curindex = int(curlist[0])

        child = None
        if isinstance(node, c_ast.Compound):
            if len(node.block_items) < relativepos+curindex+1:
                log.error("Compound did not have the proper number of children. "
                        "Expected {} at least but found"
                        " {}.".format(relativepos+curindex+1,
                            len(node.block_items)))
                return None, None

            else:
                loopindexsearch = 0
                foundaloop = False
                # Find 1st For after the initial thought value.
                for bkn in node.block_items[relativepos+curindex:]:
                    if isinstance(bkn, c_ast.For):
                        foundaloop = True
                        break
                    else:
                        loopindexsearch += 1

                if not foundaloop:
                    log.error("Could not find loop on Compound after index "
                            "{}".format(relativepos+curindex))
                    return None, None

                child = node.block_items[relativepos+curindex+loopindexsearch]
                return cls._findloop(node, child, curlist)
            #endif len

        elif isinstance(node, c_ast.Label):
            child = node.stmt
            return cls._findloop(node, child, curlist)

        elif isinstance(node, c_ast.For):
            if curindex+relativepos > 0:
                log.error("Node For only can have one loop in the body and index "
                    "is {}.".format(relativepos+curindex))
                return None, None
            else:
                curlist.pop(0)
                if curlist:
                    child = node.stmt
                    return cls._findloop(node, child, curlist)
                else:
                    return node, parent
            #endif curindex
        else:
            raise NotImplementedError("this is not recognized {}".format(node))
        #endif isinstance

def _findingCompoundChild(parent, child):
    if parent is None:
        log.error("parent is none!")
        return None

    if child is None:
        log.error("child is none!")
        return None

    posindex = []
    if isinstance(parent, c_ast.Compound):
        try:
#            log.debug("All elements in {}".format(parent))
#            for i in parent.block_items:
#                log.debug("{} ".format(i))

            index = parent.block_items.index(child)
            while index >= 0:
                posindex.append(index)
                index = parent.block_items.index(child, index+1)

        except ValueError as e:
            pass
        except Exception as e:
            log.error("Error looking for item {}.".format(child))
    else:
        log.error("Not received a Compound type!")
    #endif isinstance

#    log.debug("{}.".format(posindex))
    return posindex

class LabelVisitor(c_ast.NodeVisitor):
    """ Get all the labels that starts with tgtlabel and return in a list. """
    def __init__(self, tgtlabel):
        self.alllabels = []
        self.baselabel = tgtlabel
        self.current_parent = None

#    def visit_Label(self, node):
#        if node.name.startswith(self.baselabel):
#            self.alllabels.append(node)

    def generic_visit(self, node):
        if isinstance(node, c_ast.Label):
            self.alllabels.append((self.current_parent, node))

        oldparent = self.current_parent
        self.current_parent = node
        for name, c in node.children():
            ret = self.visit(c)
        self.current_parent = oldparent

class ForLoopDepthVisitor(c_ast.NodeVisitor):

    def __init__(self):
        self.ldepth = 0

    def generic_visit(self, node):
        childrenldepth = []
#        if isinstance(node, c_ast.For):
#            curldepth = self.ldepth
        self.ldepth = 0

        if node is not None:
            for name, c in node.children():
                ret = self.visit(c)
                childrenldepth.append(self.ldepth)

        if childrenldepth:
            maxdep = max(childrenldepth)
            self.ldepth = maxdep

        if isinstance(node, c_ast.For):
            self.ldepth += 1

class ListInnerLoopsVisitor(c_ast.NodeVisitor):

    def __init__(self):
        self.innerloops = []
        self.current_parent = None
#        self.foundloop = False

    def generic_visit(self, node):

        oldparent = self.current_parent
        self.current_parent = node
        chret = []
        if node is not None:
            for name, c in node.children():
                ret = self.visit(c)
                chret.append(ret)

        self.current_parent = oldparent

        foundfor = False
        if chret:
            for v in chret:
                if v == True:
                    foundfor = True
                    break

        if isinstance(node, c_ast.For):
            if not foundfor:
                self.innerloops.append((self.current_parent,node))

            foundfor = True

        #print node, foundfor
        return foundfor

class IsPerfectLoopNest(c_ast.NodeVisitor):
    """ Checks if the loop is perfectly nested by
    seeing the compound has more than one statament and 
    at least on For.
    """

    def __init__(self):
        pass

    def generic_visit(self, node):
        #print "IsPerfectLoopNest",type(node)
        if isinstance(node, c_ast.Compound):
            foundFor = False
            #print "block", type(node.block_items)
            if node.block_items is not None:
                for c in node.block_items:
                    if isinstance(c, c_ast.For):
                        foundFor = True
                        break

                if len(node.block_items) > 1 and foundFor:
                    return False
                ##
            ##
        ##

        for name, c in node.children():
            ret = self.visit(c)
            if not ret:
                return ret
        return True

#class FindParentVisitor(c_ast.NodeVisitor):
#    """Given a node returns its parent."""
#
#    def __init__(self, tgtnode):
#        print "PLEASE $%#", tgtnode
#        self.tgtnode = tgtnode
#        self.tgtparent = None
#        self.current_parent = None
#
#    def generic_visit(self, node):
#        print "node ", node, "tgtnode",self.tgtnode
#        if node == self.tgtnode:
#            print "FindParentVisitor FOund!", self.current_parent
#            self.tgtparent = self.current_parent
#
#        oldparent = self.current_parent
#        self.current_parent = node
##        print(node)
#        for name, c in node.children():
#            self.visit(c)
#        self.current_parent = oldparent

class FindFirstLoopVisitor(c_ast.NodeVisitor):
    """ Find the 1st loop in the Ast and returns. """

    def __init__(self):
        self.current_parent = None
        self.forloopparent = None
        self.forloop = None

    def generic_visit(self, node):
        if isinstance(node, c_ast.For):
            self.forloop = node
            self.forloopparent = self.current_parent
            #found it and finish visitor.
            return True

        oldparent = self.current_parent
        self.current_parent = node
#        print(node)
        for name, c in node.children():
#            print(name)
            ret = self.visit(c)
            # if found the loop stop searching the other children.
            if ret:
                return ret
        self.current_parent = oldparent
        return False

class ChangeStrPragmaVisitor(c_ast.NodeVisitor):
    """ Change the string of the target pragma. Useful for changing pragmas added for rose back to a code region.
        Another traversal must be called to actually create the code regions.
    """
    def __init__(self, tgtpragmastr, replstr):
        self.basepragmastr = tgtpragmastr
        self.replstr = replstr

    def visit_Pragma(self, node):
        if node.string.startswith(self.basepragmastr):
            node.string = self.replstr

class GetSingleCmpdVisitor(c_ast.NodeVisitor):
    """ Finds all compound blocks that have single stmt that is a compound stmt. Then clean them all!"""

    def __init__(self):
        self.singlecmpds = []
        self.current_parent = None

    def generic_visitor(self, node):
        if isinstance(node, c_ast.Compound):
            if len(node.block_items) == 1 and \
                    isinstance(node.block_items[0], c_ast.Compound):
                self.singlecmpds.append((self.current_parent, node))

        oldparent = self.current_parent
        self.current_parent = node
#        print(node)
        for name, c in node.children():
            self.visit(c)
        self.current_parent = oldparent


class BaseStrPragmaVisitor(c_ast.NodeVisitor):
    """ Find all pragmas that have a base string given. """

    def __init__(self, basestr):
        self.pragfound = []
        self.basepragmastr = basestr
        self.current_parent = None

    def generic_visit(self, node):
        if isinstance(node, c_ast.Pragma):
            if node.string.startswith(self.basepragmastr):
                self.pragfound.append((self.current_parent,node))
        #endif

        oldparent = self.current_parent
        self.current_parent = node
#        print(node)
        for name, c in node.children():
            ret = self.visit(c)
        self.current_parent = oldparent

class PragmaVisitor(c_ast.NodeVisitor):

    def __init__(self, code_regions):
        #log.debug("start")
        self.__code_regions = code_regions
        self.current_parent = None
        self.blockstack = [] # To know which block code region is ending

    def generic_visit(self, node):
        if isinstance(node, c_ast.Pragma):
            #node.show()
            pragmainfo = parsePragma(node.string)
            if pragmainfo:
                ptype, plabel = pragmainfo
                # self.current_parent.show()
                if isinstance(self.current_parent, c_ast.Compound):

                    posindex = _findingCompoundChild(self.current_parent, node)

                    # Get all indexes of all nodes referring the pragma with same
                    # annotations. Just change the 1st one.
                    if posindex:
                        if len(posindex) > 1: log.warning("Found multiple objects on"
                                "the list! Using the 1st one.")
                        index = posindex[0]

                        if ptype == CodeRegion.LOOP or \
                            ptype == CodeRegion.BLOCK:

                            if index < len(self.current_parent.block_items)-1:
                                startstmt= self.current_parent.block_items[index+1]

                                cr = CodeRegion(plabel, node, None, startstmt,
                                        None, self.current_parent, ptype)
                                self.__code_regions.append(cr)

                                # Add to the stack waiting to update the endnode
                                if ptype == CodeRegion.BLOCK:
                                    self.blockstack.append(cr)
                            else:
                                log.error("Pragma being the last in this"
                                        " Compound is Weird.")
                        elif ptype == ENDBLOCK:
                            if index > 0:
                                endstmt= self.current_parent.block_items[index-1]
                                cr = self.blockstack.pop()
                                cr.endnode = endstmt
                                cr.pragmaend = node

                        #endif ptype
                    else:
                        log.error("Could not find node on parent's children.")
                    # endif posindex
                else:
                    raise NotImplementedError()


        oldparent = self.current_parent
        self.current_parent = node
#        print(node)
        for name, c in node.children():
#            print(name)
            ret = self.visit(c)
        self.current_parent = oldparent
        #log.debug("code_regions size {}".format(len(self.__code_regions)))

def parsePragma(string):
    import re
    __annot_mod_block = '(block)[ \t]*=[ \t]*(\w+)'
    __annot_mod_endblock = '(endblock)'
    __annot_mod_loop = '(loop)[ \t]*=[ \t]*(\w+)'
    #__annot_begin_c = r'^[ \t]*#pragma[ \t]+@ICE[ \t]*('\
    __annot_begin_c = r'^[ \t]*@ICE[ \t]*('\
    +__annot_mod_block+'|'\
    +__annot_mod_endblock+'|'\
    +__annot_mod_loop+')[ \t]*'
#    +__annot_mod_loop+')[ \t]*\n'

    __annot_begin_c_re = re.compile(__annot_begin_c, flags=re.MULTILINE)

#    log.debug("init {}".format(string))
    match = __annot_begin_c_re.search(string)
    if not match:
#        print "No match"
        return None

#    log.debug("{}".format(match.groups()))

    # Regex Group | Mean
    #       2     | block
    #       3     | block label
    #       4     | endblock
    #       5     | loop 
    #       6     | loop label
    if match.group(2) and match.group(3):
        return (CodeRegion.BLOCK, match.group(3))
    elif match.group(4):
        return (ENDBLOCK, None)
    elif match.group(5) and match.group(6):
        return (CodeRegion.LOOP, match.group(6))
    else:
        log.error("Not found the correct matches!")
        return None
