'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys

from locus.frontend.srccode.coderegioninterface import CodeRegionInterface

log = logging.getLogger("CodeRegionInterface")

class CodeRegion(CodeRegionInterface):
    """ This represents all the code region annotated use when parsing with
    pycparser.
    """

    def __init__(self, label=None, pragmabegin=None, pragmaend=None,
            startnode=None, endnode=None, parent=None, atype=None, keys={}):
        super(CodeRegion, self).__init__(label)

        # if loop the coords of the loopi,
        # if a block a tuple of begin and end of coords
        self.pragmabegin = pragmabegin
        self.pragmaend = pragmaend
        self.startnode = startnode
        self.endnode = endnode
        self.parent = parent # parent node on the AST
        self.type = atype
        self.keys = keys

    def __str__(self):
        epilog = '\n'
        prolog = '\t'
        ret = []
        #ret.append("CodeRegion:\n")
        for key, var in self.__dict__.items():
            if key == 'type':
                var = CodeRegion.typeString(var)
            ret.append(prolog + str(key) + ": " + str(var) + epilog)

        return " ".join(ret)



