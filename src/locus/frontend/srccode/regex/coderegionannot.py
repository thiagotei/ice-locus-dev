'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:
import logging, sys

from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX,\
                                    ICE_CODEFILE_F, stringCodeType
from locus.frontend.srccode.coderegioninterface import CodeRegionInterface as CRI

log = logging.getLogger("CodeRegionAnnot")

class CodeRegionAnnot(CRI):
    ''' This represents source code that can have annotations to express places for optimizations.'''

    def __init__(self, buf=None, annted=False, lstart=None, lend=None,\
            annotinfo=None, hashkey=None):
        super(CodeRegionAnnot, self).__init__(annotinfo.label if annotinfo
                else None, hashkey)
        self.buffer = buf
        self.linestart = lstart
        self.lineend = lend
        self.annotated = annted
        self._annotinfo = annotinfo
        self.printannot = True

    def __str__(self):
        ret = "===\n"
        for key, var in self.__dict__.items():
            ret = ret + str(key) + ": " + str(var) + "\n"
        return ret

    def getCodeString(self, typefile):
    #   log.debug("filetype {}".format(stringCodeType(typefile)))
        if self.annotated and self.printannot:
            annotid  = "undef"
            endannot = "undef"
            if self._annotinfo.type == CodeRegionAnnot.BLOCK:
                annotid = "block="+self._annotinfo.label
                endannot = "endblock"
            elif self._annotinfo.type == CodeRegionAnnot.LOOP:
                annotid = "loop="+self._annotinfo.label
                endannot = "endloop"
            else:
                raise ValueError("Annotation type unrecognized.")

            precode = "undef"
            poscode = "undef"
            if typefile == ICE_CODEFILE_C or \
                typefile == ICE_CODEFILE_CXX:
                precode = "#pragma @ICE "+ annotid  +'\n'
                poscode = "#pragma @ICE "+ endannot +'\n'
            elif typefile == ICE_CODEFILE_F:
                precode = "! @ICE " + annotid  + "\n"
                poscode = "! @ICE " + endannot + "\n"
            else:
                raise ValueError("Typefile unrecognized.")

            return precode + self.buffer + poscode

        else:
            return self.buffer

    def check(self, other):
        res = True
        if self.annotated != other.annotated:
            log.warning("annotated attr is different! {} vs "
                    "{}".format(self.annotated,other.annotated))
            res = False
#           print self
#           print "***"
#           print other
#           print "==="
#           return False
        if self._annotinfo:
            res = self._annotinfo.check(other._annotinfo)
            if res == False:
                log.warning("annotinfo is different! {} vs "
                    "{}".format(self._annotinfo,other._annotinfo))
        return res

    @property
    def type(self):
        return self._annotinfo.type

    @property
    def keys(self):
        return self._annotinfo.keys

class Annot(object):
    '''This represents the annotation on source code 
        Ident is the name of the annotation and the type could be loop or block.
        The annotation might accept other info that will be put in a dict.
    '''

    def __init__(self, label, itype, keys):
        self.label = label
        self.type = itype
        self.keys = keys

    def check(self, other):
        res = True
        if other is None:
            log.error("the other is None.")
            return False
        if self.label != other.label:
            log.warning("annot id different! {} vs "
                    "{}".format(self.label,other.label))
            res = False
        if self.type != other.type:
            log.warning("annot type different! {} vs "
                    "{}".format(Annot.getTypeStr(self.type),Annot.getTypeStr(other.type)))
            res = False
        return res

    @staticmethod
    def getTypeStr(itype):
        vartmp = "undef"
        if itype == CodeRegionAnnot.LOOP:
            vartmp = "LOOP"
        elif itype == CodeRegionAnnot.BLOCK:
            vartmp = "BLOCK"
        return vartmp

    def __str__(self):
        trail = " " #"\n"
        lead = "" #"\t"
        ret = trail
        for key, var in self.__dict__.items():
            vartmp = var
            if key == "type":
                if self.type == CodeRegionAnnot.LOOP:
                    vartmp = "LOOP"
                elif self.type == CodeRegionAnnot.BLOCK:
                    vartmp = "BLOCK"
            ret = ret + lead + str(key) + ": " + str(vartmp) + trail
        return ret
