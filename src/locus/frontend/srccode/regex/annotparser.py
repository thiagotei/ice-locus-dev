'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent

import re, sys, copy
import logging

from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX, ICE_CODEFILE_F
from .coderegionannot import CodeRegionAnnot, Annot
from locus.backend.gencache import GenCache

log = logging.getLogger('AnnotParser')

class AnnotParser(object):
    ''' THe parser used to extract annotations '''
    
    #get the XPP annotation until the end of the line

    __annot_mod_block = 'block=\w+'
    __annot_mod_endblock = 'endblock'
    __annot_mod_loop = 'loop=\w+'
    __annot_mod_endloop = 'endloop'
    __annot_mod_keys = '\w+=[\w./*]+'
    __annot_begin_f = r'^[ \t]*![ \t]*@ICE[ \t]*('\
    +__annot_mod_block+'|'\
    +__annot_mod_endblock+'|'\
    +__annot_mod_loop+'|'\
    +__annot_mod_endloop+')('\
    +__annot_mod_keys+'| |\t)*\n'

    __annot_begin_c = r'^[ \t]*#pragma[ \t]+@ICE[ \t]*('\
    +__annot_mod_block+'|'\
    +__annot_mod_endblock+'|'\
    +__annot_mod_loop+'|'\
    +__annot_mod_endloop+')('\
    +__annot_mod_keys+'| |\t)*\n'

    __annot_begin_cxx = __annot_begin_c

    __annot_begin_f_re = re.compile(__annot_begin_f, flags=re.MULTILINE)    
    __annot_begin_c_re = re.compile(__annot_begin_c, flags=re.MULTILINE)    
    __annot_begin_cxx_re = re.compile(__annot_begin_cxx, flags=re.MULTILINE)    
    __annot_block_re = re.compile(__annot_mod_block, flags=re.MULTILINE)
    __annot_endblock_re = re.compile(__annot_mod_endblock, flags=re.MULTILINE)
    __annot_loop_re = re.compile(__annot_mod_loop, flags=re.MULTILINE)
    __annot_endloop_re = re.compile(__annot_mod_endloop, flags=re.MULTILINE)
    __annot_keys_re = re.compile(__annot_mod_keys, flags=re.MULTILINE) 
    
    def __init__(self):
        '''To instatiate the annotation parser '''
        pass
    #---------------------------------------------
    
    # Check if there is any annotation on the current code
    def hasAnnot(code):
        return AnnotParser.getAnnotRE().search(code)
    hasAnnot = staticmethod(hasAnnot)
    
    def getAnnotRE():
        return AnnotParser.__annot_begin_re
    getAnnotRE = staticmethod(getAnnotRE)
    
#    def getAnnotModule(code):
#        debug("getAnnotModule code : %s " % code, name="AnnotParser")
#        module_id = XPP_NO_OPT
#        if(AnnotParser.__annot_vec_re.search(code)):
#            debug("Found OPT_VECT", name="AnnotParser")
#            module_id = XPP_OPT_VECT
#        elif(AnnotParser.__annot_vec_lbreak.search(code)):
#            debug("Found OPT_LBREAK", name="AnnotParser")
#            module_id = XPP_OPT_LBREAK
#        #else ... put all the modules here
#        return module_id
#    getAnnotModule = staticmethod(getAnnotModule)

    def getAnnotVecRE():
        return AnnotParser.__annot_vec_re
    getAnnotVecRE = staticmethod(getAnnotVecRE)
    #---------------------------------------------

    @staticmethod
    def parse(code, typefile, parsedcode, line_no = 1):
#        code_seq = []

        FOUND_BLOCK = 1
        FOUND_ENDBLOCK = 2
        FOUND_LOOP = 3
        FOUND_ENDLOOP = 4
      #  FOUND_KEY = 5
        last_ann_found = 0

        while True:
            if typefile == ICE_CODEFILE_C:
                match_obj = AnnotParser.__annot_begin_c_re.search(code)
            elif typefile == ICE_CODEFILE_CXX:
                match_obj = AnnotParser.__annot_begin_cxx_re.search(code)
            elif typefile == ICE_CODEFILE_F:
                match_obj = AnnotParser.__annot_begin_f_re.search(code)

            # If could not find the annotation stop parsing this file.
            if not match_obj:
                log.debug("Couldnt find anything else ")
                if code != '':
#                    code_seq.append((code, line_no, False, None))
                    parsedcode.addCodeRegion(CodeRegionAnnot(code, False, line_no, 0, None ))
                break


            index_newl = 0
            # index_newl = code[match_obj.end():].find('\n')
            # print "index %d " % index_newl
            ann = code[match_obj.start():match_obj.end()+index_newl]
            ann_line_no = line_no + code[:match_obj.start()].count('\n')

            #parse the annotation  
            if(AnnotParser.__annot_block_re.search(ann)):
                log.debug("Found block annotation!" ) 
                ann_found = FOUND_BLOCK
                annot_keys = AnnotParser.parserAnnot(ann)
                log.debug("Keys {}".format(annot_keys)) 
            elif(AnnotParser.__annot_loop_re.search(ann)): 
                log.debug("Found loop annotation!")
                ann_found = FOUND_LOOP
                annot_keys = AnnotParser.parserAnnot(ann)
            elif(AnnotParser.__annot_endblock_re.search(ann)):
                log.debug("Found endblock annotation")
                ann_found = FOUND_ENDBLOCK
                if last_ann_found != FOUND_BLOCK:
                    raise ValueError("Found an endblock without an block annotation!")
            elif(AnnotParser.__annot_endloop_re.search(ann)):
                log.debug("Found endloop annotation")
                ann_found = FOUND_ENDLOOP
                if last_ann_found != FOUND_LOOP:
                    raise ValueError("Found an endloop without an loop anno!")
 
         #   elif(AnnotParser.__annot_keys_re.search(ann)):
         #       debug("Found a key annotation")
         #       ann_found = FOUND_KEY
            else:
                raise ValueError("Did not find any annotation!")

            #get the leading non-annotation if that inside a opening block
            if ann_found == FOUND_BLOCK or ann_found == FOUND_LOOP:
                non_ann = code[:match_obj.start()]
                non_annot_line_no = line_no

                if non_ann != '':
                    #code_seq.append((non_ann, non_annot_line_no, False, None))
                    parsedcode.addCodeRegion(CodeRegionAnnot(non_ann, False, non_annot_line_no, 0, None))

                #code_ann = ann
                code_ann = ""
            elif ann_found == FOUND_ENDBLOCK or ann_found == FOUND_ENDLOOP:
                code_ann += code[:match_obj.start()]
               # code_seq.append((code_ann, ann_line_no, True, annot_keys))
                parsedcode.addCodeRegion(CodeRegionAnnot(code_ann, True,
                    ann_line_no, 0, annot_keys,
                    GenCache.createhash(code_ann, typefile)))


            line_no += code[:match_obj.end()].count('\n')
            code = code[match_obj.end()+index_newl:]

            last_ann_found = ann_found

        # This checks if the loop and block annotation have fininished without a end annotation.
        # The cparser for C does not require end loop annotations but this one does. So because of confusion when using
        # the parsers (this one is used for C++ and Fortran, cparser is used for C).
        if last_ann_found == FOUND_BLOCK or last_ann_found == FOUND_LOOP:
            raise RuntimeError("THIS parser requires the block end or loop end annotation!.")

        return parsedcode

    # get the attributes of the annotation found and form a dictionary
    def parserAnnot(annotcode):        
        annotAttr = {} 
        matches = AnnotParser.__annot_keys_re.findall(annotcode)
#        print annotcode
        for x in matches:
#            print "Uow %s " % x
            if x != '':
                split2 = re.split('=', x)
                key = split2[0]
                value = split2[1]
                annotAttr[key] = value
        log.debug("%s", str(annotAttr))
        ident = None
        itype = None
        if 'loop' in annotAttr:
            ident = annotAttr['loop']
            itype = CodeRegionAnnot.LOOP
            del annotAttr['loop']
        elif 'block' in annotAttr:
            ident = annotAttr['block']
            itype = CodeRegionAnnot.BLOCK
            del annotAttr['block']
        else:
            log.warning("could not find block or loop on the\
                    annotation.",)
        return Annot(ident, itype, annotAttr)
    parserAnnot = staticmethod(parserAnnot)

class LabelParser(object):
    """  Parse the result generated from transformations from Label. """
    import re


    def __init__(self):
        pass

    @staticmethod
    def parse(srccode, regiongoalname, base_label_mark, 
            initial="begin",
            final="end"):
        """ This assumes parsing back the results of a one or more
        nonoverlapping code regions with the same id. This means that the
        srccode is already in the ICE srccode data structure."""

        # loop_marks + id number + :
        __annot_transf = base_label_mark + initial + '[\d]*:'
        __annot_transf_end = base_label_mark + final + '[\d]*:[ \t]*;'

        __annot_transf_begin_c = r'^[ \t]*'+__annot_transf+'[ \t]*\n'
        __annot_transf_end_c = r'^[ \t]*'+__annot_transf_end+'[ \t]*\n'

        # Not tested with Fortran
        __annot_transf_begin_f = r'^\$[ \t]+'+__annot_transf+'[ \t]*'
        __annot_transf_end_f = r'^\$[ \t]+'+__annot_transf_end+'[ \t]*'

        __annot_begin_f = r'('\
        +__annot_transf_begin_f+'|'\
        +__annot_transf_end_f+')\n'

        __annot_begin_c = r'('\
        +__annot_transf_begin_c+'|'\
        +__annot_transf_end_c+')'

        __annot_transf_re = re.compile(__annot_transf, flags=re.MULTILINE)
        __annot_transf_end_re = re.compile(__annot_transf_end, flags=re.MULTILINE)

        __annot_begin_f_re = re.compile(__annot_begin_f, flags=re.MULTILINE)
        __annot_begin_c_re = re.compile(__annot_begin_c, flags=re.MULTILINE)

        #newsrccode = copy.deepcopy(srccode)
        newsrccode = srccode

        lineno = 1
        FOUND_NOTHING = 0
        FOUND_TRANSF_BLOCK = 1
        FOUND_TRANSF_ENDBLOCK = 2
        lastannfound = FOUND_NOTHING

        replmntDict = {}

        for coreg in newsrccode:
            codebuf = copy.deepcopy(coreg.buffer) # copying the buf for safety reasons
            log.debug("coreg:\n{}".format(codebuf[-200:]))
            while True:
                if newsrccode.filetype == ICE_CODEFILE_C or \
                    newsrccode.filetype == ICE_CODEFILE_CXX:
                    matchobj = __annot_begin_c_re.search(codebuf)
                elif newsrccode.filetype == ICE_CODEFILE_F:
                    matchobj = __annot_begin_f_re.search(codebuf)

                if not matchobj:
                    log.debug("Couldnt find anything!")
                    if codebuf != '' and coreg in replmntDict:
                        replmntDict[coreg].append(CodeRegionAnnot(codebuf, False, lineno, 0, None))
                    break

                indexnewl = 0
                # index_newl = code[match_obj.end():].find('\n')
                # print "index %d " % index_newl
                ann = codebuf[matchobj.start():matchobj.end()+indexnewl]
                annlineno = lineno + codebuf[:matchobj.start()].count('\n')
                log.debug("Ann found {} line num {}".format(ann, annlineno))

                #parse the annotation  
                if(__annot_transf_re.search(ann)):
                    log.debug("Found label open annotation!")
                    annfound = FOUND_TRANSF_BLOCK

                elif(__annot_transf_end_re.search(ann)):
                    log.debug("Found label close annotation!")
                    annfound = FOUND_TRANSF_ENDBLOCK
                    if lastannfound != FOUND_TRANSF_BLOCK:
                        log.error("Found an endtransf without an transf annotation!")

                #get the leading non-annotation if that inside a opening block
                if annfound == FOUND_TRANSF_BLOCK:
                    non_ann = codebuf[:matchobj.start()]
                    non_annot_line_no = lineno
                    if coreg not in replmntDict:
                        replmntDict[coreg] = []

                    if non_ann != '':
                        #code_seq.append((non_ann, non_annot_line_no, False, None))
                        try:
                            replmntDict[coreg].append(CodeRegionAnnot(non_ann, False, non_annot_line_no, 0, None))
                        except KeyError as ke:
                            log.error("replmntDict do not contain:\n{}.".format(coreg))
                            raise
                        except Exception as e:
                            _, _, exc_tb = sys.exc_info()
                            log.error("{} - line {}".format(e,exc_tb.tb_lineno))
                            raise
                        else:
                            log.debug("found_transf_block non_ann: {}".format( non_ann))


                    #codeann = ann
                    codeann = ""

                elif annfound == FOUND_TRANSF_ENDBLOCK:
                    codeann += codebuf[:matchobj.start()]
                    try:
                        replmntDict[coreg].append(CodeRegionAnnot(codeann, True,
                            annlineno, 0, 
                            Annot(regiongoalname, CodeRegionAnnot.LOOP, {}),
                            GenCache.createhash(codeann, newsrccode.filetype)))
                    except KeyError as ke:
                        log.error("replmntDict do not contain:\n{}.".format(coreg))
                        raise
                    except Exception as e:
                        _, _, exc_tb = sys.exc_info()
                        log.error("{} - line {}".format(e,exc_tb.tb_lineno))
                        raise
                    else:
                        log.debug("found_transf_endblock codeann: {}".format( codeann))

                lineno += codebuf[:matchobj.end()].count('\n')
                codebuf = codebuf[matchobj.end()+indexnewl:]

                lastannfound = annfound
            #end while true
        #end for codereg

        # Add new parsed code regions to the newsrccode
        for origcoreg, newcoregs in replmntDict.items():
            # newcoderegs are in a list
            for newcrg in newcoregs:
                newsrccode.addBeforeCodeRegion(newcrg, origcoreg)
            # endfor
            newsrccode.deleteCodeRegion(origcoreg)
        # endfor

        return newsrccode

