'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, itertools

from locus.frontend.srccode.srcinterface import SourceCodeInterface
from locus.frontend.srccode.regex.annotparser import AnnotParser, LabelParser
from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
from locus.backend.srccode.writer import Writer
from locus.util.constants import stringCodeType, ICE_CODEFILE_C, ICE_CODEFILE_CXX, ICE_CODEFILE_F, ICE_ROSE_END_MARK

log = logging.getLogger("SourceCode")

class SourceCode(SourceCodeInterface, Writer):
    ''' This represents source code that can have places for optimizations.'''

    def __init__(self, srcfilename=None, ft=None, preprocargs=""):
        SourceCodeInterface.__init__(self, srcfilename, ft)
        self.__code_seq = []

    def addCodeRegion(self, cr, index=None):
        ''' If no index provided is equivalent to append. '''
        pos = len(self.__code_seq) if index is None else index
        self.__code_seq.insert(pos, cr)

#   def addCodeRegionIndex(self,cr,pos):
#       self.__code_seq.insert(pos,cr)

    def replaceCodeRegion(self, oldcr, newcr):
        index = self.getCodeRegionIndex(oldcr)
        self.__code_seq[index] = newcr

    def deleteCodeRegion(self, cr):
        index = self.getCodeRegionIndex(cr)
        del self.__code_seq[index]

    def addBeforeCodeRegion(self,newcr,oldcr):
        index = self.getCodeRegionIndex(oldcr)
        self.addCodeRegion(newcr, index)

    def addAfterCodeRegion(self,newcr,oldcr):
        index = self.getCodeRegionIndex(oldcr)
        self.addCodeRegion(newcr, index+1)

    def getCodeRegionIndex(self,cr):
        ret = [i for i,x in enumerate(self.__code_seq) if cr == x]
        length = len(ret)
        if length > 1:
            log.warning("found multiple - {} - coderegions equal.".format(len(ret)))
            raise LookupError
        elif length < 1:
            raise IndexError
        return ret[0]
#       tmp = self.__code_seq.index(cr)
#       while tmp:
#           ret.append(tmp)
#           tmp = 
#       return self.__code_seq.index(cr)

    #FIXED This does not work when they have different number of coderegions. 
    # Very often code regions are added to srccode as optimizations occur.
    # Reimplement this by checking only the annotated regions.
    def makeMetadataCoherent(self, other):
        if self.srcfilename != other.srcfilename:
            self.srcfilename = other.srcfilename
        if self.filetype != other.filetype:
            self.filetype = other.filetype

        selfcrannoted = []
        for secr in self.__code_seq:
            if secr.annotated:
                selfcrannoted.append(secr)

        othercrannoted = []
        for otcr in other.__code_seq:
            if otcr.annotated:
                othercrannoted.append(otcr)

        # Just check metadata of the code regions
        for secr, otcr in zip(selfcrannoted, othercrannoted):
            if not secr.check(otcr):
                log.warning("coderegions are different!")
#               return False

    def dumpAll(self):
        for c in self.__code_seq:
            print(c)

    def __iter__(self):
        return self.__code_seq.__iter__()

    def __str__(self):
        ret = ""
        for c in self.__code_seq:
            ret = ret + str(c)
        return ret
#       return "SourceCode being called!"

    def __getitem__(self, key):
        return self.__code_seq[key]

    def annotated(self):
        return filter(lambda cr: cr.annotated, self.__code_seq)

    def readandparse(self, includeargs=None):
        # reading the file
        with open(self.srcfilename, 'r') as f:
            srccodebuf = f.read()

        try:
            AnnotParser.parse(srccodebuf, self.filetype, self)
        except Exception as e:
            log.error("Problem parsing the file %s - %s .", self.srcfilename, e )
            raise
        else:
            log.debug("{} is of filetype {}.".format(self.srcfilename,
                stringCodeType(self.filetype)))

        return self

    def unparseandwrite(self, writefunc):
        log.debug("{} is of filetype {}.".format(self.srcfilename,
            stringCodeType(self.filetype)))
        buf = ""
        for coreg in self:
            buf += coreg.getCodeString(self.filetype)

        return writefunc(buf)

    def replaceCoregWithPragma(self, tgtpragmalbl, newpragma, finalmark=ICE_ROSE_END_MARK):
        addpragmabefore = []
        for coreg in self.annotated():
            if coreg.label == tgtpragmalbl:
                addpragmabefore.append(coreg)

        if self.filetype == ICE_CODEFILE_C or \
                self.filetype== ICE_CODEFILE_CXX:
            annotbefore = "#pragma " + newpragma + "\n"
            annotafter = "// " + finalmark + "\n"
        elif self.filetype == ICE_CODEFILE_F:
            annotbefore = "!$ " + newpragma + "\n"
            annotafter = "! " + finalmark + "\n"
        else:
            log.error("Typefile unrecognized - {}.".format(stringCodeType(self.filetype)))
            return 0

        for coreg in addpragmabefore:
            newcoreg = CodeRegionAnnot(annotbefore, False, None, 0, None)
            self.addBeforeCodeRegion(newcoreg, coreg)

            if finalmark:
                after = CodeRegionAnnot(annotafter, False, None, 0, None)
                self.addAfterCodeRegion(after, coreg)
            coreg.printannot = False

    # This adds a label mark in the beggining and in the end, 
    # so we can recognize the code region again.
    def replaceCoregWithLabel(self, tgtpragmalbl, newlabel, pos="0", initmark="begin",
            finalmark="end"):

        if pos != "0":
            raise NotImplementedError("For regex pos cannot be different than"
                    " \"0\".")

        if self.filetype == ICE_CODEFILE_C or \
                self.typefile == ICE_CODEFILE_CXX:

            annotbefore = newlabel + initmark
            annotafter = newlabel + finalmark
        elif self.filetype == ICE_CODEFILE_F:
            raise NotImplementedError()
            #annotbefore = regiongoalname + ""$
            #annotbefore = "! " + Pips._initial_loop_mark + "\n\n"+ regiongoalname + ":\n"$
            #annotafter = "! " + Pips._final_loop_mark + "\n"$
            #initialMark = "! "+ Pips._initial_file_mark + "\n"$
        else:
            log.error("Typefile unrecognized - {}.".format(stringCodeType(self.filetype)))
            return 0

        addlabelaround = []
        for coreg in self.annotated():
            if coreg.label == tgtpragmalbl:
                addlabelaround.append(coreg)

        lblindex = 0
        for coreg in addlabelaround:
            newlabelindex = annotbefore + str(lblindex) + ":\n"
            newcoreg = CodeRegionAnnot(newlabelindex, False, None, 0, None)
            self.addBeforeCodeRegion(newcoreg, coreg)

            newlabelindex = annotafter + str(lblindex) + ":;\n"
            newcoreg = CodeRegionAnnot(newlabelindex, False, None, 0, None)
            self.addAfterCodeRegion(newcoreg, coreg)
            coreg.printannot = False

            lblindex += 1

        return lblindex

    # Remove the labels and add a coderegion.
    def replaceLabelWithCoreg(self, oldlabel, tgtpragmalbl):
        try:
            LabelParser.parse(self, tgtpragmalbl, oldlabel)
        except Exception as e:
            log.error("Problem parsing the file %s - %s .", self.srcfilename, e )
            raise
        else:
            log.debug("{} is of filetype {}.".format(self.srcfilename,
                stringCodeType(self.filetype)))

        return self

    def printouthash(self, writefunc=sys.stdout.write):
        for coreg in self.annotated():
            writefunc(coreg.label+": "+coreg.hashkey+"\n")

    def gethashes(self):
        hashes = []
        for coreg in self.annotated():
            hashes.append((coreg.label, coreg.hashkey))
        return hashes

    def replaceCodeRegion(self, codereg, newcoreg):
        codereg.buffer = newcoreg
        return self

#    def replaceCodeRegion(self, tgtpragmalbl, newcoreg, hashkey):
#
#        for coreg in self.annotated():
#            if coreg.label == tgtpragmalbl:
#                if coreg.hashkey == hashkey:
#                    coreg.buffer = newcoreg
#                else:
#                    raise RuntimeError("Hash keys do not match label: {}\nsourcecode: "
#                            "{}\naltdesc: {}\n".format(tgtpragmalbl, coreg.hashkey, hashkey))
#        return self

    # Add lable and keep ICE pragma
    def addLabelLoop(self, tgtpragmalbl, newlabelname, pos="0"):
        """Not impossible to implement this using the regex for pos different
        than "0", or the outermost loop.
        """
        if pos == "0":
            counter = self._addCore(tgtpragmalbl, newlabelname, counter=0, epilog=":\n")
        else:
            raise NotImplementedError

        return counter

    # Add pragma and keep ICE pragma
    def addPragmaLoop(self, tgtpragmalbl, newpragma, pos="0"):
        """Not impossible to implement this using the regex for pos different
        than "0", or the outermost loop.
        """
        if pos == "0":
            self._addCore(tgtpragmalbl, newpragma)
        else:
            raise NotImplementedError

    def _addCore(self, tgtpragmalbl, newinfo, counter=None, prolog="", epilog="\n"):

        log.debug("Begin%% Counter %s",counter)
        for coreg in self.annotated():
            if coreg.label == tgtpragmalbl:
                if self.filetype == ICE_CODEFILE_C or \
                    self.typefile == ICE_CODEFILE_CXX:
                    log.debug("%% Counter UOW %s",counter)
                    if counter is not None:
                        finalinfo = newinfo + str(counter)
                        counter += 1
                        log.debug("%% Counter %s",counter)
                    else:
                        finalinfo = newinfo
                    coreg.buffer = prolog + finalinfo + epilog + coreg.buffer
                else:
                    raise NotImplementedError(
                        "Not implemented for this filetype.".format(
                           stringCodeType(self.filetype)))
                #endif filetype
            #endif core.label 
        #endfor coreg
        return counter

