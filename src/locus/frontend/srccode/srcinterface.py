'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, itertools
from abc import ABCMeta, abstractmethod

log = logging.getLogger("SourceCodeInterface")

class SourceCodeInterface(object, metaclass=ABCMeta):
    ''' This represents source code that can have places for optimizations.'''

    def __init__(self, srcname=None, ft=None):
        self.srcfilename = srcname
        self.filetype = ft

    @abstractmethod
    def readandparse(self):
        pass

    @abstractmethod
    def unparseandwrite(self):
        pass

    @abstractmethod
    def annotated(self):
        """This should return only the coderegions annotated. This a backward
        compatibility for the codegen to work sourcecode from regex and cparser.
        Remember that regex/sourcecode also includes coderegions that are not
        annoatated, whereas cparsers/sourcecode only has pointers to the
        annotated coderegions on the AST tree."""
        pass

    @abstractmethod
    #def replacePragmaWithLabel(self, tgtpragmalbl, newlabel):
    def replaceCoregWithLabel(self, tgtpragmalbl, newlabel):
        """This puts label on top of ICE's pragma."""
        pass

    @abstractmethod
    def replaceLabelWithCoreg(self, oldlabel, tgtpragmalbl):
        """This puts back the ICE pragma on the place of label."""
        pass

    @abstractmethod
    def replaceCoregWithPragma(self, tgtpragmalbl, newpragma):
        """This puts another pragma on top of ICE's pragma."""
        pass

#    @abstractmethod
#    def replacePragmaWithCoreg(self, tgtpragmalbl, newpragma):
#        """This puts another pragma on top of ICE's pragma."""
#        pass

    @abstractmethod
    def addLabelLoop(self, tgtpragmalbl, newlabelname, pos="0"):
        """"This puts label in a loop with pos relative to pragma @ICE.
            This does NOT remove pragma @ICE.
        """
        pass

    @abstractmethod
    def addPragmaLoop(self, tgtpragmalbl, newpragma, pos="0"):
        """Add a pragma to a loop with pos relative to pragma @ICE.
            This does NOT remove pragma @ICE.
        """
        pass

    @abstractmethod
    def replaceCodeRegion(self, tgtpragmalbl, newcoreg, hashkey):
        """ Replace a code region with another code region.
        """
        pass

    @abstractmethod
    def printouthash(self):
        """ Print out the hash for all code regions. """
        pass

    @abstractmethod
    def gethashes(self):
        """ Return a list with the hash for all code regions. """
        pass

