'''

@author: thiago
'''
import logging, sys

from .locusast import ID, Block, IfStmt, AssignStmt, PrintStmt, \
        Constant, BinaryOp, LocusNode, FuncCall, IntConvCall, LenCall, \
        StrCall
from .locusvisitor import GenericVisitor
from .locuscfg import LocusCFG, CFGBB
from .optimizer import exprHasOnlyConsts, DeadCodeEliminationConsts

log = logging.getLogger("DataflowOpts")

class ReachingDefs():

    def __init__(self):
        pass

    @classmethod
    def exec(cls, root, bblst=None, preds=None, alldefs=None, entryout=None):
        """Input is the entry node of cfg.
           Saves IN for each instruction on each basic block.
           Saves OUT for each block.
           The entryout is the OUT set of previous procedure. It can be used
           to pass the OUT of the topblk.
        """

        # create a list of blocks
        if bblst is None:
            bblst = LocusCFG.listfycfg(root)
        #

        if not bblst:
            log.warning("CFG basic block list is empty.")
            return None
        #

        IN = {}
        OUT = {}
        entrynd = bblst[0]
        if not entrynd.isentry():
            log.warning("Something is weird! First node should be the "
                        "entry.Interrupted analysis.")
            return None
        #endif

        # empty OUT for all basic blocks
        for bb in bblst:
            OUT[bb.unqid] = set()
        ##

        if entryout is not None:
            OUT[entrynd.unqid] = entryout
        #

        if preds is None:
            preds = LocusCFG.predecessors(entrynd)

        if alldefs is None:
            alldefs = LocusCFG.getdefs(entrynd)

        # entry node is not invoked on the transfFunc
        entrynd.stmtsIN = []

        changed = True # changes in OUT
        while changed:
            changed = False
            for bb in bblst:
                if not bb.isentry():
                    #
                    # IN[bb] = U OUT[all predecessors]
                    #
                    IN = set()
                    for p in preds[bb.unqid]:
                        IN |= OUT[p.unqid]
                    ##

                    prevOUT = OUT[bb.unqid]

                    #
                    # OUT[bb] = gen(bb) U (IN[bb] - kill(bb))
                    #
                    OUT[bb.unqid] = cls.transfFuncBB(bb, IN, alldefs)

                    if prevOUT != OUT[bb.unqid]:
                        changed = True
                    #
                #
            ##
        #endwhile

        cls.dictfyReachDefs(entrynd)

        return entrynd
    ###

    @classmethod
    def transfFuncBB(cls, bb, bbIN, alldefs):
        """ The gen part of the transfer function of reaching definitions.
            Expect reference to bb, IN of the basic block, and dict with all the
            definitions in the program.
            Return bbOUT and save instOUT for each instruction in the bb.
        """
        # list of IN for each instruction
        stmtsINlst = []
        curIN = bbIN
        for inst in bb.stmts:
            gen_i = set()
            kill_i = set()
            stmtsINlst.append(curIN)

            if isinstance(inst, AssignStmt):
                varname = inst.lvalue.name

                # calc gen_i
                gen_i.add(inst)

                # calc kill_i
                kill_i = set(alldefs[varname].values())
            #

            instOUT = gen_i | (curIN - kill_i)
            curIN = instOUT
        ##

        # bbOUT is the OUT of the last instruction
        # save this on the bb
        bb.OUT = curIN
        bb.stmtsIN = stmtsINlst
        return bb.OUT
    ###

    @classmethod
    def dictfyReachDefs(cls, root):
        """ Make the reaching definitions of each statement that is a set
            to become a dictionary indexed by var name.
            And add that as a field to the node.
        """
        def dictfy(node):
            newfield = []
            for stmt, IN in zip(node.stmts, node.stmtsIN):
                dicIN = {}
                for defn in IN:
                    name = defn.lvalue.name
                    if name not in dicIN:
                        dicIN[name] = []
                    #
                    dicIN[name].append(defn)
                ##
                newfield.append(dicIN)
            ##
            node.dictsIN = newfield
        ##

        LocusCFG.traverse(root, dictfy)
    ###

    @classmethod
    def write(cls, root, buf=sys.stdout):
        def writeFunc(node):
            buf.write("Node "+CFGBB.strType(node.type)+" "+str(node.unqid)+":\n")
            for i, inst in enumerate(node.stmts):
                buf.write("  in: {"+", ".join([str(x.unqid) for x in
                    node.stmtsIN[i]])+"}\n")
                buf.write("  dictin: "+str(node.dictsIN[i])+"\n")
                buf.write("  inst "+ str(inst.unqid)+": "+str(inst)+"\n")
                buf.write("  ==\n")
            ##

            for e, desc in node.edges:
                buf.write("  "+desc+": "+str(e.unqid)+"\n")
            ##
        ###

        LocusCFG.traverse(root, writeFunc)
    #enddef
#endclass

class ConstantProp():

    def __init__():
        pass

    @classmethod
    def exec(cls, root, bblst=None):
        """Expect CFG with reaching definitions on each statement for each bb node.
           Return CFG with constants replacing a variable as long as all the reching definitions
           for a variable were defined by the same constant value.
           Useful apply constant folding along with constant propagation.
           This does not change the structure of the CFG.
        """

        replcnt = 0

        # create a list of blocks
        if bblst is None:
            bblst = LocusCFG.listfycfg(root)
        #

        # go through basic blocks
        for bb in bblst:
            # got through statements
            for stmt, IN in zip(bb.stmts, bb.dictsIN):
                #print("stmt: ", stmt.unqid, type(stmt))
                visIDs = GenericVisitor(ID)
                idlist = []
                if isinstance(stmt, AssignStmt):
                    # is there IDs in statement's rhs?
                    visIDs.visit(stmt.rvalue)
                    idlist = visIDs.values

                elif isinstance(stmt, FuncCall):
                    # despite constant prop, the id never goes away of an argument
                    # in current implementation. So this runs forever :(
                    # fix is to remove the ID vars where id is on id field of an argument.
                    #stmt.show()
                    for arg in stmt.arglist:
                        if arg.expr is not None:
                            visIDs.visit(arg.expr)
                            #print(">>"+" ".join([str(p)+"_"+str(a.unqid) for p,a in  visIDs.values]))
                            # when parent of node from visitor is none, it is arg
                            idlist += [(arg, chi) if par is None else (par, chi) for par, chi in visIDs.values]
                            #print(">>"+" ".join([str(p.unqid)+"_"+str(a.unqid) for p,a in idlist]))
                            visIDs.values=[]
                        elif arg.id is not None:
                            idlist.append((arg, arg.id))
                        #
                    ##
                    #print(" ".join([str(p.unqid)+"_"+str(a.unqid) for p,a in  idlist]))

                else:
                    visIDs.visit(stmt)
                    idlist = visIDs.values
                #

                # go throught the ids of current stmt
                for n_pa_id, n_id in reversed(idlist):
                    #log.debug(f"{n_pa_id} {n_id} - {type(n_pa_id)} {type(n_id)}")

                    # get the reaching defs for the id
                    if n_id.name in IN:
                        val = None
                        replace = True

                        # go through all reching defs and 
                        # check if they have the same constant
                        # value of their RHS.
                        for defn in IN[n_id.name]:
                            rval = defn.rvalue
                            #print("bb ",bb.unqid," stmt: ",stmt, " id: ",n_id.name," defn: ",defn, " rval: ", rval)
                            # get the 1st rhs
                            if val is None:
                                val = rval
                            #
                            if not isinstance(rval, Constant) or\
                                    val != rval:
                                replace = False
                                #print("break?!",isinstance(rval, Constant),
                                #        rval,type(rval), val, type(val),val!=rval)
                                break
                            #

                            val = rval
                        ##

                        # do they have same value? 
                        # yes, replace! no, forget it
                        if replace:
                            #print("Replace! ", n_id," -> " ,  rval)
                            #
                            # In the logic here when parent node is None,
                            # I assume the replacement is the whole rhs of the
                            # assignment.
                            #
                            if n_pa_id is not None:
                                n_pa_id.replace(n_id, rval)
                            else:
                                stmt.rvalue = rval
                            #
                            replcnt += 1
                        #
                    #
                ##
            ##
        ##
        return replcnt
    ###

class ConstantFold():

    def __init__():
        pass
    ###

    @classmethod
    def exec(cls, root, bblst=None):
        """  Folds binary expressions that have all const
        """
        # create a list of blocks
        if bblst is None:
            bblst = LocusCFG.listfycfg(root)
        #

        nfolds = 0
        for bb in bblst:
            for i, stmt in enumerate(bb.stmts):
                visExpr = GenericVisitor(BinaryOp)
                visExpr.visit(stmt)

                #
                # Do it in reverse because children expressions 
                # should be solved first
                #
                for n_pa, n_expr in reversed(visExpr.values):
                    #print(type(n_pa),n_expr, type(n_expr))
                    if exprHasOnlyConsts(n_expr):
                        #if not isinstance(n_pa, IfStmt):
                        val = n_expr.execute(None)
                        #print("ConstantFold: ",val, type(val))
                        if n_pa is not None:
                            n_pa.replace(n_expr, val)
                        else:
                            # replace expresion,
                            # the ast that the cfg came from
                            # is no longer valid
                            bb.stmts[i] = val
                        #
                        nfolds += 1
                    #
                ##

                # built-in functions that input is constant are also folded
                visBuiltin = GenericVisitor((IntConvCall, StrCall, LenCall))
                visBuiltin.visit(stmt)
                for n_pa, n_bltin in reversed(visBuiltin.values):
                    if exprHasOnlyConsts(n_bltin.inp): 
                        val = n_bltin.execute(None)
                        if n_pa is not None:
                            n_pa.replace(n_bltin, val)
                        else:
                            bb.stmts[i] = val
                        #
                        nfolds += 1
                    #
                ##
            #
        ##

        return nfolds
    ###
#endclass

class DeadCodeElim():
    def __init__(self):
        pass

    @classmethod
    def exec(cls, root, bblst=None, preds=None):
        """ Changes the structure of the CFG.
            Conditionals nodes are removed if they are constant booleans.
            Needs another optimizations to be effective.

            Reimplement this. get the predecessors, go throuhg the list. remove nodes. start again.
        """
        deaths = 0

        if bblst is None:
            bblst = LocusCFG.listfycfg(root)
        #

        if preds is None:
            preds = LocusCFG.predecessors(root)
        #

        for tgt in bblst:
            if tgt.iscond() and\
                isinstance(tgt.stmts[0], Constant):

                # if it is true modify all edges of predecessors
                # to the true edge, otherwise do to false edge      
                if tgt.stmts[0]:
                    newedge = CFGBB.getAEdge(tgt.edges, "true")
                else:
                    newedge = CFGBB.getAEdge(tgt.edges, "false")
                #

                deaths += 1

                # goes through all predecessors
                for p in preds[tgt.unqid]:
                    updates = []

                    # goes through all predecessors edges
                    for e, desc in p.edges:

                        # edge point to the node to be removed?
                        if e == tgt: 
                            ne, ndesc = newedge
                            updates.append(((e, desc), (ne, desc)))
                        #
                    ## edges

                    #log.debug("Cutting edges: "+len(updates))
                    for old, new in updates:
                        p.edges.remove(old)
                        p.edges.add(new)
                    ##
                ## preds
            #
        ## bblst
        return deaths
    ###
####

def combinedCfgAstOpts(astbody, entryout=None):
    """ Do some opts in CFG level and some on AST.
        The ones that change the structute of the program
        is always done in AST.
    """

    if not isinstance(astbody, LocusNode):
        raise ValueError("Expecting an AST node but got ", type(astbody))
    #

    totprops = 0
    totfolds = 0
    totdeaths= 0
    it = 0

    while True:
        #log.info(f"[combinedCfgAstOpts] {it} Before code:\n{astbody}")
        entry = LocusCFG.ast2cfg(astbody)
        ReachingDefs.exec(entry, entryout=entryout)
        props = ConstantProp.exec(entry)
        folds = ConstantFold.exec(entry)
        #LocusCFG.drawcfg(entry, str(it)+"_combinedCfgAstOpts cp_cf_dce")  
        dcec = DeadCodeEliminationConsts(astbody)
        deaths = dcec.execute()
        #log.info(f"[combinedCfgAstOpts] {it} After code:\n{astbody}")
        log.debug("it: "+str(it)+" props: "+str(props)+" folds: "+str(folds)+" deaths: "+str(deaths))

        totprops += props
        totfolds += folds
        totdeaths += deaths

        if props == 0 and folds == 0 and deaths == 0:
            break
        #

        it += 1
    ##
    return totprops, totfolds, totdeaths 
####

class DeadCodeElimOld():

    def __init__(self):
        pass

    @classmethod
    def exec(cls, root, bblst=None):
        """ Changes the structure of the CFG.
            Conditionals nodes are removed if they are constant booleans.
            Needs another optimizations to be effective.

            Reimplement this. get the predecessors, go throuhg the list. remove nodes. start again.
        """
        deaths = 0
        if bblst is None:
            bblst = LocusCFG.listfycfg(root)
        #

        for bb in bblst:
            #print("[Begin] bb ", bb)
            # try to find nodes that point to conditional nodes
            updates = []

            # points to a cond node?
            for e_desc in bb.edges:
                oldedge = e_desc
                newedge = None

                while True:
                    #tgt = bb.true
                    tgt, desc = e_desc
                    #if desc == "true":
                    #    tgt = e
                    #else:
                    #    break
                    #

                    #print("=> tgt", tgt, type(tgt))
                            #tgt.iscond())#,isinstance(tgt.stmts[0], Constant))
                    # if target is cond and a constant
                    if tgt is not None and\
                            tgt.iscond() and\
                            isinstance(tgt.stmts[0], Constant):
                        #print("   ", tgt.stmts[0])
                        if tgt.stmts[0]:
                            #bb.true = tgt.true
                            newedge = CFGBB.getAEdge(tgt.edges, "true")
                            #print("   T changed bb.true => ", bb.true.unqid)
                        else:
                            #bb.true = tgt.false
                            newedge = CFGBB.getAEdge(tgt.edges, "false")
                            #print("   F changed bb.true => ", bb.true.unqid)
                        #
                        deaths += 1
                    else:
                        break
                    #
                    e_desc = newedge
                ## end while

                if newedge is not None:
                    oe, odesc = oldedge
                    ne, ndesc = newedge
                    updates.append((oldedge, (ne,odesc)))
                #
            ## end for edges

            # remove old edges and add the new ones
            for old, new in updates:
                bb.edges.remove(old)
                bb.edges.add(new)
            ##
            #print("[End] bb ", bb)
        ##
        return deaths
    ###
####

