'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:

import logging, sys, os

try:
    from lark import Lark, inline_args, Transformer, lexer
    from lark.tree import pydot__tree_to_png
except ImportError as e:
    print("You need to install lark-parser (I've used pip for it)!")
    print("You should install apt-get install graphviz and ")
    print("pip install --user pydot to use pydot__tree_to_png .")
    raise

import locus.frontend.optlang.locus.locusast as locusast
from locus.frontend.optlang.locus.optimizer import replEnvVars
from locus.frontend.optlang.optlanginterface import AbstractOptLang
from locus.util.misc import getDynClass, getOptClass, getToolClass

log = logging.getLogger("LocusParser")

class LocusParser(AbstractOptLang):
    __grammarfile = 'locus.g'

    def __init__(self, optfilename=None, debug=False):
        log.debug("Start")
        self.locusparser = None
        self.locustree = None
        locusast.LocusNode.resetidcounter()
        try:
            path = os.path.abspath(__file__)
            dir_path = os.path.dirname(path)
            absgrammarfile = os.path.join(dir_path, LocusParser.__grammarfile)
            with open(absgrammarfile, 'r') as f:
                self.locusparser = Lark(f, parser='lalr', start="start")
        except Exception as e:
            raise

        if optfilename is not None:
            try:
                self.locustree = self.parseFile(optfilename, False) #debug)
            except Exception as e:
                if debug:
                    log.exception("Error parsing the locus file {}".format(e))
                else:
                    log.error("Error parsing the locus file {}.".format(e))

    def parseFile(self, optfilename, printlarktree=False):
        if optfilename:
            try:
                with open(optfilename, 'r') as f:
                    optfilebuf = f.read()

            except IOError as e:
                log.error("Optimization file %s not found.", optfilename)
                return None
            except Exception as e:
                raise
            else:
                return self.parse(optfilebuf, printlarktree)
        else:
            log.error("No optimization file provided.", optfilename)
            return None

    def parse(self, buf, printlarktree=False):
        try:
            larktree = self.locusparser.parse(buf)
            #pydot__tree_to_png(larktree, "./locusgrammar.png")
            if printlarktree:
                log.info(larktree.pretty())
                #print larktree.pretty()
            locustransformer = LocusTreeTransformer()
            locustransformer.transform(larktree)
            locustree = locustransformer.ast
        except Exception as e:
            raise
        else:
            return locustree

    def genOptUni(self, replEnvVar=True):
        repls = None
        if self.locustree is not None:
            if replEnvVar:
                repls = replEnvVars(self.locustree)
            #endif

            self.locustree.createscope()
            return self.locustree, repls
        else:
            return None, None
#endclass LocusParser

log = logging.getLogger("LocusTreeTransformer")

class LocusTreeTransformer(Transformer):
    def __init__(self):
        scp = locusast.Scope()
        self.topblk = locusast.Block(scope=scp)
        self.ast = locusast.LocusAST(topblk=self.topblk)

    def start(self, v):
        log.debug("Start %s",v)
        for n in v:
            if isinstance(n, locusast.SearchDef):
                self.ast.search = n
            elif isinstance(n, locusast.CodeRegDef):
                self.ast.functable[n.id.name] = n
            elif isinstance(n, locusast.OptSeqDef):
                self.ast.functable[n.id.name] = n
            elif isinstance(n, locusast.LocusNode):
                # A block member to this class and all the 
                # nodes that are not SearchDef, CodeRegDef and OptSeqDef are
                # added to it in order. They'll be interpreted before any
                # SearchDef, CodeRegDef and OptSeqDef.
#                print "%%% ", n.lvalue.name
                #self.ast.symtable[n.lvalue.name] = n.rvalue
                self.topblk.stmts.append(n)
            else:
                log.warning("Type %s not recognized!",type(n))

    def coderegdef(self,v):
#        print "coderegdef",v[0].value
        #log.debug("name: %s, block: %s",v[0].value,v[1])
        return locusast.CodeRegDef(locusast.ID(v[0].value),v[1])
    ###

    def optseqdef(self, v):
        decor = None
        name = None
        arglist = None
        blk = None
        for it in v:
            #print(f"item: {type(it)}")
            if isinstance(it, locusast.Block):
                blk = it
            elif isinstance(it, locusast.ArgList):
                arglist = it
            elif isinstance(it, locusast.Decorator):
                decor = it
            elif isinstance(it, lexer.Token):
                name = locusast.ID(it.value)
            else:
                raise ValueError("Type no expected on OptSeq parser: {}".format(type(it)))
            #
        ##
        ret = locusast.OptSeqDef(_id=name, bl=blk, args=arglist, decor=decor)
        return ret
    ###

    def optionalstmt(self, v):
        return locusast.OptionalStmt(*v)

    def decor(self, v):
        #print(f"{v}")
        return locusast.Decorator(v[0].value)

    @inline_args
    def string(self, s):
        #log.debug("%s",s)
        ret = s[1:-1] #s[1:-1].replace('\\"', '"')
        #print(s, type(s), ret)
        # Remove first and last quotes

        #print s.value,type(s.value), ret
        #return locusast.Constant(type(ret),ret)
        #return locusast.Constant(type(s.value),str(s.value))
        return locusast.Constant(type(s.value),str(ret))

    def testlist(self, l):
        #print "testlist", l
        return l

    def tuple(self, v):
        #print "tuple",v
        return locusast.TupleMaker(v)

    def print_stmt(self, v):
        #print ("print_stmt %%%",v)
        return locusast.PrintStmt(*v)

    def getitem(self, v):
        #print "getitem %%%", v
        return locusast.GetItem(*v)

    def len_call(self, v):
        return locusast.LenCall(v[0])

    def str_call(self, v):
        return locusast.StrCall(v[0])

    def getenv_call(self, v):
        #print("Getenv", v)
        defval = None
        if len(v) > 1:
            defval = v[1]
        return locusast.GetEnvCall(v[0], defval)

    def intconv_call(self, v):
        return locusast.IntConvCall(v[0])

    def floatconv_call(self, v):
        return locusast.FloatConvCall(v[0])

    def enum_call(self,v):
        return locusast.SearchEnum(v[0])

    def integer_call(self, v):
        #print v[0]
        initv = None
        if len(v) > 1:
            initv = v[1]
        return locusast.SearchInteger(v[0], initv)

    def pwtwo_call(self, v):
        #print v,v[0]
        initv = None
        if len(v) > 1:
            initv = v[1]
        return locusast.SearchPowerOfTwo(v[0], initv)

    def perm_call(self, v):
        #print "perm_call",v
        initv = None
        if len(v) > 1:
            initv = v[1]
        return locusast.SearchPerm(v[0], initv)

    def seq_call(self, v):
        #print "seq_call", v
        return locusast.SeqCall(*v)

    def neg_factor(self, v):
        return locusast.UnaryOp(locusast.UnaryOp.NEG,v[0])

    def pos_factor(self, v):
        return locusast.UnaryOp(locusast.UnaryOp.POS,v[0])

    def inv_factor(self, v):
        return locusast.UnaryOp(locusast.UnaryOp.INV,v[0])

    def dec_number(self, v):
        #print "dec Number", v
        return locusast.Constant(int,int(v[0]))

    def hex_number(self, v):
        #print "--hex Number", v
        return locusast.Constant(hex,hex(v[0]))

    def oct_number(self, v):
        #print "oct Number", v, v[0]
        return locusast.Constant(oct,oct(v[0]))

    def int_number(self, v):
        #print "int Number", v
        return locusast.Constant(int,int(v[0]))

    def float_number(self, v):
        return locusast.Constant(float,float(v[0]))

    def identifier(self, v):
        #log.debug("%s",v[0].value)
        #return v[0].value
        return locusast.ID(v[0].value)

    def func_call(self, v):
        #log.debug("%s %s", " ".join(map(str,v)), type(v[0]))
        return locusast.FuncCall(*v)

    def initvalrg(self,v):
        #print "initvalrg",v[0].value, v
        return v[0]

    def initlistmk(self, v):
        #print "initlistmk", v[0].value, v
        return v[0]

    def getattr(self, v):
        v[0].name = v[0].name+"."+v[1]
        #return ".".join(v)
        return v[0]

    def rangeexpr(self, v):
        #new_v = map(lambda x: x.value, v)
        #return locusast.RangeExpr(*new_v)
        return locusast.RangeExpr(*v)

    def argument(self, a):
#        print "argument:",a
        if len(a) == 1:
            if isinstance(a[0], locusast.ID):
#                print "parser argument", a[0]
                ret = locusast.Argument(_id=a[0])
            else:
                ret = locusast.Argument(expr=a[0])
        else:
            ret = locusast.Argument(*a)
        return ret

    def arglist(self, l):
        return locusast.ArgList(l)

    def listmaker(self, l):
        #new_l = map(lambda x: x.value, l)
        #print "listmaker",l, type(l)
        return locusast.ListMaker(l)

#    def expr_stmt(self, v):
#        log.debug("%s",v)
#        return locusast.Expr(v[0])

    def return_stmt(self, v):
        return locusast.Return(v)

    def block(self, v):
#        scope = locusast.Scope(self.current_scope)
#        self.current_scope = {}
        ret = []
#        print "---begin---"
#        print "Current scope",scope
#        print "Exploring block children:"
        for n in v:
            # This is to solve when set_stmt returns as list of stmts.
            # This list is flatten because we dont need a 
            # set_stmt object for now on the AST.
            if isinstance(n, list):
                ret.extend(n)
            else:
                ret.append(n)
            # Set parent scope for all children blocks.
#            if isinstance(n, locusast.Block):
#                print "child block", n.scope
#                n.scope.parent = scope
#            elif isinstance(n, locusast.OrBlock):
#                print "orblock"
#                for block in n:
#                    print "child block", block.scope
#                    block.scope.parent = scope
            #endif
        #endfor
#        print "---end---Exploring"
#        return locusast.Block(ret,scope)
        return locusast.Block(ret)

    def if_stmt(self, v):
        #print "----if_stmt----"
        #print v
        ifblks = []
        elseblk = None
        for i,k in zip(v[0::2], v[1::2]):
            ifblks.append((i, k))
        if len(v) % 2 != 0:
            elseblk = v[-1]
        #print "ifblks",ifblks
        #print "elseblk", elseblk
        return locusast.IfStmt(ifblks, elseblk)

    def for_stmt(self, v):
        return locusast.ForStmt(*v)

    def while_stmt(self, v):
        raise NotImplementedError("While not implemented")

    def set_stmt(self, v):
        return v

    def or_expr_stmt(self, v):
        return locusast.OrStmt(v)

    def or_block(self, v):
#       print "---or_block---"
        return locusast.OrBlock(v)

#    def subblock(self, v):
#        print "---subblock---", type(v[0])
#        return v[0]

    def comparison(self, v):
        #print "comparison",v
        return locusast.BinaryOp(v[1],v[0],v[2])

    def eq_expr(self, v):
        return locusast.BinaryOp.EQ

    def diff_expr(self, v):
        return locusast.BinaryOp.NE

    def gt_expr(self, v):
        return locusast.BinaryOp.GT

    def lt_expr(self, v):
        return locusast.BinaryOp.LT

    def ge_expr(self, v):
        return locusast.BinaryOp.GE

    def le_expr(self, v):
        return locusast.BinaryOp.LE

    def mod_expr(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.MOD,*v)

    def truediv_expr(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.TRUEDIV,*v)

    def floordiv_expr(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.FLOORDIV,*v)

    def mul_expr(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.MUL,*v)

    def add_expr(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.ADD,*v)

    def sub_expr(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.SUB,*v)

    def and_test(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.AND,*v)

    def or_test(self, v):
        return locusast.BinaryOp(locusast.BinaryOp.OR,*v)

#    def DEC_NUMBER(self, v):
#        print "DEC_NUMBER", v
#        return v[0]

    # This is just to clean the current_scope, that should not have
    # any global variable.
#    def global_var(self, val):
        #print val[0]
        #self.current_scope = {}
#        return val[0]

    def assign(self, val):
#        print "assign",  val[0], "=",val[1]
#        self.current_scope[val[0]] = val[1]
        return locusast.AssignStmt(locusast.AssignStmt.EQUAL,*val)

    def true_stmt(self, v):
        return locusast.Constant(bool, True)

    def false_stmt(self, v):
        return locusast.Constant(bool, False)

    def none_stmt(self, v):
        return locusast.Constant(type(None), None)

    def searchdef(self, v):
        """The search def block scope is the global scope."""
        #print "SearchDef",v
        #v_proc = {x: v[0][x].value if isinstance(v[0][x],locusast.Constant) \
        #        else  v[0][x] for x in v[0] }
        v_proc = {x: v[0][x] for x in v[0] }
        #print "v_proc", v_proc
        se = locusast.SearchDef(**v_proc)
        return se

    def search_block(self, v):
        #print "search_block", v
        # fuse all dicts
        final = {}
        for d in v:
            final.update(d)
        return final

    def prebuildstmt(self, v):
        #log.debug("%s", v[0])
        return {"prebuildcmd":v[0]}

    def measureorigstmt_true(self, v):
        #log.debug("measureorigstmt_true %s", v)
        return {"measureorig":True}

    def measureorigstmt_false(self, v):
        #log.debug("measureorigstmt_true %s", v)
        return {"measureorig":False}

    def buildcmdstmt(self, v):
        #log.debug("%s",v)
        return {'buildcmd':v[0]}

    def buildorigstmt(self, v):
        #log.debug("%s",v)
        return {'buildorig':v[0]}

    def runcmdstmt(self, v):
        return {'runcmd':v[0]}

    def runorigstmt(self, v):
        return {'runorig':v[0]}

    def checkcmdstmt(self, v):
        return {'checkcmd':v[0]}

