'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:
import itertools, logging

import locus.frontend.optlang.locus.locusast as modlocusast
from locus.frontend.optlang.locus.nodevisitor import NodeVisitor

log = logging.getLogger("LocusVisitor")

# this returns all nodes of a specifig time  and their parents.
class GenericVisitor(NodeVisitor):

    def __init__(self, astnode=None, par=None):
        self.values = []
        self.parent = par
        self.astnode = astnode

    def generic_visit(self, node):
#        print "generic_visit", type(node)
        if isinstance(node, self.astnode):
#           print type(self.parent), type(node)
            self.values.append((self.parent, node))

        oldparent = self.parent
        self.parent = node
        #log.info(f"id: {node.unqid} type: {type(node)}")
        for c in node:
            self.visit(c)
        self.parent = oldparent
#        print node

class GenericVisitorCond(NodeVisitor):

    def __init__(self, astnode=None, par=None, cond=None):
        self.values = []
        self.parent = par
        self.astnode = astnode
        self.cond = cond

    def generic_visit(self, node):
#        print "GVC/generic_visit", type(node)
        if isinstance(node, self.astnode) and self.cond(node):
#           print type(self.parent), type(node)
#           print "gen",node.lvalue.name
            self.values.append((self.parent, node))

        oldparent = self.parent
        self.parent = node
        for c in node:
            self.visit(c)
        self.parent = oldparent

class GenericVisitorWScope(NodeVisitor):

    def __init__(self, astnode=None):
        self.values = []
        self.parent = None
        self.astnode = astnode
        self.parentscope = None

    def generic_visit(self, node):
#        print "generic_visit", type(node)

        if hasattr(node, 'scope'):
            self.parentscope = node.scope

        if isinstance(node, self.astnode):
#           print type(self.parent), type(node)
            self.values.append((self.parent, node, self.parentscope))

#        if node == self.tgtnode:
#            self.values = (self.parent, node, self.parentscope)

        oldparent = self.parent
        oldparentscope = self.parentscope
        self.parent = node
        for c in node:
            self.visit(c)
        self.parent = oldparent
        self.parentscope = oldparentscope

# Update id of the nodes according to the   
# LocusNode.__id current value
class GenericVisitorUpdateId(NodeVisitor):

    def __init__(self):
        pass    
    #enddef

    def generic_visit(self, node):
        for c in node:
            self.visit(c)

        node.unqid = node.idcounter
        node.incidcounter()
    #enddef
#endclass

#
# Receives a dict with nodes id and which option to take.
#class ReplaceSearchVisitor(NodeVisitor):
#
#    def __init__(self, configdict=None):
#        self.cfg = configdict
#
#    def generic_visit(self, node):
#        if node.id in self.cfg:
#            self.cfg[node.id]
#            if type(node) is modlocusast.OrBlock or\
#                    type(node) is modlocusast.OrStmt:
#                self.parent.replace(node)
#            elif type(node) is modlocusast.:
#            elif type(node) is :
#            elif type(node) is :
#            else:
#
#        oldparent = self.parent
#        self.parent = node
#        for c in node:
#            self.visit(c)
#        self.parent = oldparent


class OrStmtVisitor(NodeVisitor):

    def __init__(self):
        self.values = []

    def visit_OrStmt(self, node):
#        print "visit_OrBlock", type(node)
        self.values.append(node)

        for c in node:
            self.visit(c)
#        print node

class OrStmtAndParentVisitor(NodeVisitor):

    def __init__(self):
        self.values = []
        self.parent = None

    def generic_visit(self, node):
#        print "visit_OrBlock", type(node)
        if isinstance(node, modlocusast.OrStmt):
#            print type(self.parent), type(node)
            self.values.append((self.parent, node))

        oldparent = self.parent
        self.parent = node
        for c in node:
            self.visit(c)
        self.parent = oldparent
#        print node

class OptionalStmtVisitor(NodeVisitor):

    def __init__(self):
        self.values = []

    def visit_OptionalStmt(self, node):
        #print "visit_OptionalStmt", type(node)
        self.values.append(node)

        for c in node:
            self.visit(c)
#        print node

class OrBlockVisitor(NodeVisitor):

    def __init__(self):
        self.values = []

    def visit_OrBlock(self, node):
#        print "visit_OrBlock", type(node)
        self.values.append(node)

        for c in node:
            self.visit(c)
#        print node
#endclass OrBlockVisitor

#
# Find all the parents of given node. Stop when the given nodes is found.
#
class NodePathVisitor(NodeVisitor):

    def __init__(self, tgtnode=None):
        #print "NodeVisitor", id(tgtnode),tgtnode
        self.pathnodes = []
        #self.pathscop  = []
        self.parent = None
        self.tgtnode = tgtnode
        self.parentscope = None
        self.found = False

    def generic_visit(self, node):
        #print "generic_visit", id(node),type(node)
        if node == self.tgtnode:
            self.found = True
            #print "Found node", node, self.tgtnode
            return

        if hasattr(node, 'scope'):
            self.parentscope = node.scope

        # Add this node to the path.
        self.pathnodes.append((self.parent, node, self.parentscope))

        oldparent = self.parent
        oldparentscope = self.parentscope
        self.parent = node
        for c in node:
            self.visit(c)
            if self.found:
                break

        # After visitng children not found, remove it.
        if not self.found:
            #print "removing..."
            self.pathnodes.pop()
        #endif

        self.parent = oldparent
        self.parentscope = oldparentscope
    #enddef
#endclass

class NodePathVisitorCond(NodeVisitor):
    """ More generic version of the visitor above.
    """
    def __init__(self, cond=None):
        #print "NodeVisitor", id(tgtnode),tgtnode   
        self.allpathnodes = []
        self.pathnodes = []
        #self.pathscop  = []
        self.parent = None
        self.cond = cond
        self.parentscope = None
        self.found = False

    def generic_visit(self, node):
        #print "generic_visit", id(node),type(node)
        if self.cond(node):
            self.allpathnodes.append((node, list(self.pathnodes)))
            #self.found = True
            #print "Found node", node, self.tgtnode
            #return

        if hasattr(node, 'scope'):
            self.parentscope = node.scope

        # Add this node to the path.
        self.pathnodes.append((self.parent, node, self.parentscope))

        oldparent = self.parent
        oldparentscope = self.parentscope
        self.parent = node
        for c in node:
            self.visit(c)
            #if self.found:
            #    break

        # After visitng children not found, remove it.
        #if not self.found:
            #print "removing..."
        self.pathnodes.pop()
        #endif

        self.parent = oldparent
        self.parentscope = oldparentscope
    #enddef
#endclass
