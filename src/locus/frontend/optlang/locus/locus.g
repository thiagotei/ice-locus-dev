//TODO 
// - coderegdef should not accept return (it might be useful on the main)?
// - main also should be able to return anythin (I dont see a reason).
// - add a way to import CodeReg, OptSeq, and variables
//
start: (maindef | coderegdef | optseqdef | global_var | searchdef)*
searchdef: "Search" search_block
?search_block: "{" search_stmt (";" search_stmt)* ";" "}"
?search_stmt: prebuildstmt | measureorigstmt | buildcmdstmt | runcmdstmt | runorigstmt | buildorigstmt | checkcmdstmt
prebuildstmt: "prebuildcmd" "=" test
?measureorigstmt: "measureorig" "=" true_stmt -> measureorigstmt_true
                | "measureorig" "=" false_stmt -> measureorigstmt_false
buildcmdstmt: "buildcmd" "=" test
runcmdstmt: "runcmd" "=" test
runorigstmt: "runorig" "=" test
buildorigstmt: "buildorig" "=" test
checkcmdstmt: "checkcmd" "=" test

maindef: "Main" NAME block
coderegdef: "CodeReg" NAME block
optseqdef: [decor] "OptSeq" NAME "(" [arglist] ")" block
block: "{" subblock* "}"

//?subblock: "{" subblock "}" ("OR" "{" block "}" )+ -> or_block
//         | "{" block "}"
//         | stmt
?subblock: block ("OR" block )+ -> or_block
         | block
         | stmt

?stmt: set_stmt | compound_stmt
?set_stmt: small_stmt (";" small_stmt)* ";"
?small_stmt:  (expr_stmt | flow_stmt | print_stmt)

?global_var: expr_stmt ";"
print_stmt: "print" [ test ("," test)* [","] ]
?expr_stmt: testlist augassign star_stmt -> augassign2
         | testlist "=" star_stmt    -> assign
         | star_stmt
?star_stmt: "*" or_expr_stmt -> optionalstmt
            | or_expr_stmt
?or_expr_stmt: testlist ("OR" testlist)*

augassign: ("+=" | "-=" | "*=" | "/=" | "%=" | "&=" | "|=" | "^=" | "<<=" | ">>=" | "**=")

?flow_stmt: break_stmt | continue_stmt | return_stmt
break_stmt: "break"
continue_stmt: "continue"
return_stmt: "return" [testlist]

?compound_stmt: if_stmt | for_stmt | while_stmt 
if_stmt: "if" test block ("elif" test block)* ["else" block]
while_stmt: "while" test block
for_stmt: "for" "(" expr_stmt ";" test ";" expr_stmt ")" block


?test: or_test
?or_test: and_test ("||" and_test)*
?and_test: not_test ("&&" not_test)*
?not_test: "not" not_test | comparison
//?comparison: topexpr (comp_op topexpr)*
?comparison: expr (comp_op expr)*
?comp_op: ">"  -> gt_expr
       | "<"  -> lt_expr
       | "==" -> eq_expr
       | ">=" -> ge_expr
       | "<=" -> le_expr
       | "!=" -> diff_expr

//?topexpr: rangeexpr
//       | expr
rangeexpr: expr ".." expr [".." expr]
?expr: xor_expr ("|" xor_expr)*
?xor_expr: and_expr ("^" and_expr)*
?and_expr: shift_expr ("&" shift_expr)*
?shift_expr: arith_expr (("<<"|">>") arith_expr)*
//?arith_expr: term (("+"|"-") term)*
?arith_expr: term
           | arith_expr "+" term -> add_expr
           | arith_expr "-" term -> sub_expr
?term: factor 
     | term "*" factor -> mul_expr
     | term "/" factor -> truediv_expr
     | term "//" factor -> floordiv_expr
     | term "%" factor -> mod_expr
?factor: "-" factor -> neg_factor
       | "~" factor -> inv_factor
       | "+" factor -> pos_factor
       | power
?power: molecule ["**" factor]
?molecule: molecule "(" [arglist] ")" -> func_call
         | molecule "[" [subscriptlist] "]" -> getitem
         | "enum" "(" testlist ")" -> enum_call
         | "poweroftwo" "(" rangeexpr [ initvalrg ] ")" -> pwtwo_call
         | "integer" "(" rangeexpr [ initvalrg ] ")" -> integer_call
         | "permutation" "(" (test | listmaker) [ initlistmk ] ")" -> perm_call
         | "seq" "(" test "," test ["," test] ")" -> seq_call
         | "len" "(" test ")" -> len_call
         | "str" "(" test ")" -> str_call
         | "int" "(" test ")" -> intconv_call
         | "float" "(" test ")" -> floatconv_call
         | "getenv" "(" test [ "," ["default" "="] test ] ")" -> getenv_call
         | molecule "." NAME -> getattr
         | atom
?atom: "(" test (("," test)+ [","] | ",") ")" -> tuple
      | listmaker
      | "(" test ")"
      | true_stmt
      | false_stmt
      | none_stmt
      | NAME -> identifier
      | number |  string+

//      | "{" [dictorsetmaker] "}"
listmaker: "[" [ test ("," test )* [","] ] "]"
//dictorsetmaker: (test ":" test ("," test ":" test)* [","])
?testlist: test ("," test)* [","]
?subscriptlist: test ("," test)* [","]
arglist: (argument ",")* (argument [","])
argument: test | test "=" test
initvalrg: "," "init" "=" expr
initlistmk: "," "init" "=" listmaker
decor: "@" NAME

?number: INT        -> int_number
      | HEX_NUMBER  -> hex_number
      | FLOAT       -> float_number

true_stmt: "True"
false_stmt: "False"
none_stmt: "None"


//?number: DEC_NUMBER -> dec_number
//      | HEX_NUMBER  -> hex_number
//      | OCT_NUMBER  -> oct_number
//      | _INT        -> int_number
//| FLOAT

string: ESCAPED_STRING

//DEC_NUMBER: /[1-9]\d*l?/i
HEX_NUMBER: /0x[\da-f]*l?/i
//OCT_NUMBER: /0o?[0-7]*l?/i
COMMENT: /#[^\n]*/

%import common.ESCAPED_STRING
%import common.FLOAT -> FLOAT
%import common.INT
%import common.WORD
%import common.CNAME -> NAME
%import common.NEWLINE
%ignore /[\t \f]+/  // WS
%ignore NEWLINE
%ignore COMMENT
