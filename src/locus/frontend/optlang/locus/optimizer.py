'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:

import logging, copy, os

from locus.frontend.optlang.locus.locusast import ID, Constant, AssignStmt, \
SearchEnum, SearchPowerOfTwo, Scope, ListMaker, SeqCall, BinaryOp, IfStmt, \
GetEnvCall, _assignID
from locus.frontend.optlang.locus.locusvisitor import GenericVisitor, \
GenericVisitorCond, GenericVisitorWScope, NodePathVisitor
from locus.util.exceptions import BoundsException

log = logging.getLogger("LocusOptimizer")

# Replace all the getenv with their values in the env.
# this is useful to some optimizations that require the variable valuable
# before interpreting time. So, before the everything starts it goes
# to the environment and replace the getenv nodes.
def replEnvVars(optuni, envdict=None):
    log.debug("starting replacing env vars...")
    repls = {}
    #go through all the entities
    for ent in optuni.topblk, optuni.entities():
        # find all the getenv nodes
        visitor = GenericVisitorCond(GetEnvCall, None, cond=lambda n: True)
        visitor.visit(ent)

        # go through vars
        for genvpar, genv in visitor.values:
            #log.debug("{} - {}".format(type(genv.inp),genv))
            # get  their value
            if isinstance(genv.inp, Constant):

                # If no dict with environment variables, then get them
                # from environment.
                if envdict is None:
                    res = genv.execute(None) #os.getenv(genv.inp, None)
                    #log.debug("{} {}".format(type(res), res))
                else:
                    #print(f"[replEnvVars] {genv.inp.rawstr()} {genv.inp.value}",envdict)
                    if genv.inp.rawstr() in envdict:
                        res = Constant(str, envdict[genv.inp.rawstr()])
                    else:
                        res = None
                    #
                #
                if res is not None:
                    # replace
                    genvpar.replace(genv, res)
                    # Creating a dict with strings with no quotes.
                    # Key is the envvar and value the envvalue
                    repls[genv.inp.rawstr()] = res.rawstr()
                    log.info("Replacing getenv({}) by {}".format(genv.inp, res))
                else:
                    log.warning("Not successful geting env var {}!".format(genv.inp))
                #endif
            else:
                log.warning("Not replacing {}. Only getenv with a constant string parameter can "
                        "be replaced for now! No action "
                        "required.".format(genv.inp))
            #
        #endfor
    #endfor
    log.debug("finished.")
    return repls
# enddef

# receives the expression inside the searchable variable, get all possible 
# values of the variables and applied the func on them. This func return a 
# single value.
# this is for searchable variables based on another searchable variables.
# For now, searchable variables cannot depend on variables that are function(OptSeq)
# parameter or on runtime values that are not deterministic across multiple
# executions in the search mode. The search variables needs to be based on
# values that defined when optimization space is defined.
# 08/05/2018 This implementation is not very robust. It just get the last write
# as the max value, does not consider flow control with different writes. The
# ideal would be to have  CFG and use-def chain to use as the max possible
# value.
def calcFuncExprVal(expr, scope, func):
    retval = None

    log.debug("start")

    myexpr = copy.deepcopy(expr)
    if isinstance(expr, SearchPowerOfTwo) or \
        isinstance(expr, SearchEnum):
        retval = resolveSearchVals(myexpr, scope, func)
    else:
        retval = findFuncVal(myexpr, scope, func)

    return retval

def findFuncVal(expr, exprscope, func):
#    print "findFuncVal start:", expr

    if expr.children():
        for info, termo in expr.children(): 
            term_new = resolveSearchVals(termo, exprscope, func)

            if termo != term_new:
#                print "  findFuncVal replace:", termo," by ", term_new
                expr.replace(termo, term_new)

#        print "  findFuncVal execute: ", expr
        # No vars on the expression
        resexpr = expr.execute(None)

    else:
        if isinstance(expr, ID):
#            print "  findFuncVal ID: ", expr
            #if the expr is a Var needs to check on the built scope.
            #resexpr = findScopeId(expr, exprscope, func)
            resexpr = resolveSearchVals(expr, exprscope, func)
        else:
            resexpr = expr

#    print "findFuncVal end:", resexpr
    return resexpr

def resolveSearchVals(termo, exprscope, func):
    term_old = termo
    if isinstance(termo, ID):
        #currscope[term] = findScopeId(term, exprscope, func)
        term_old = findScopeId(termo, exprscope, func)
#        print "  resolveSearchVals term ID", term_old

    if isinstance(term_old, SearchPowerOfTwo):
        if func == max:
            term_new = findFuncVal(term_old.rangeexpr.max, exprscope, func)
        elif func == min:
            term_new = findFuncVal(term_old.rangeexpr.min, exprscope, func)
        else:
            raise RuntimeError("optmizer/resolveSearchVals func not recognized")

#        print "  resolveSearchVals SearchPO2 ->", term_new

    elif isinstance(term_old, SearchEnum):
        reslist = []
        for enumv in term_old.values:
            reslist.append(findFuncVal(enumv, exprscope, func))
        term_new = func(reslist, key=lambda x: x.value)
#        print "  resolveSearchVals/reslist SearchEnum ->", term_new 

    else:
        term_new = term_old
#                term_new = findFuncVal(term_old, exprscope, func)
#        print "  resolveSearchVals Else ->",term_new

    return term_new

def findScopeId(expr_id, exprscope, func):
#    print "findScopeId start:", expr_id
    scope = exprscope
    varname = expr_id.name
    ressubexpr = None

#        print "dictid/varname:", varname
    #get the last write on idval
    while scope != None:
        if scope.node:
            visitor = GenericVisitorCond(AssignStmt, None,
                    cond=lambda n: True if n.lvalue.name == varname else False)
    #        print "Node ", scope.node 
            visitor.visit(scope.node)
#            print "  findScopeId Cond", visitor.values
            if visitor.values:
                value = visitor.values[::-1][0][1].rvalue
            else:
                value = []
        else:
            #higher scope
            value = scope[varname]
#            print "  findScopeId last scope", value

        if value:
            # Needs a deep copy in case of replace. Do not want to change the
            # original Locus AST
            cpvalue = copy.deepcopy(value)
            if isinstance(value, Constant) or \
                isinstance(value, SearchPowerOfTwo) or \
                isinstance(value, SearchEnum):
                ressubexpr = cpvalue
            else :
                ressubexpr = findFuncVal(cpvalue, scope, func)

#            print "  findScopeId ", ressubexpr
            break
        else:
            scope = scope.parent

#    print "findScopeId end:", expr_id
    return ressubexpr

##########################################
##########################################
#
def findFuncValCheckBounds(expr, exprscope, func, cfg):
#    print "findFuncVal start:", expr

    if expr.children():
        for info, termo in expr.children(): 
            term_new = resolveSearchValsCheckBounds(termo, exprscope, func, cfg)

            if termo != term_new:
#                print "  findFuncVal replace:", termo," by ", term_new
                expr.replace(termo, term_new)

#        print "  findFuncVal execute: ", expr
        # No vars on the expression
        resexpr = expr.execute(None)

    else:
        if isinstance(expr, ID):
#            print "  findFuncVal ID: ", expr
            #if the expr is a Var needs to check on the built scope.
            #resexpr = findScopeId(expr, exprscope, func)
            resexpr = resolveSearchValsCheckBounds(expr, exprscope, func, cfg)
        else:
            resexpr = expr

#    print "findFuncVal end:", resexpr
    return resexpr

def resolveSearchValsCheckBounds(termo, exprscope, func, cfg):
    term_old = termo
    if isinstance(termo, ID):
        #currscope[term] = findScopeId(term, exprscope, func)
        term_old = findScopeIdCheckBounds(termo, exprscope, func, cfg)
#        print "  resolveSearchVals term ID", term_old

    if isinstance(term_old, SearchPowerOfTwo):
        if term_old.unqid in cfg:
            term_new = Constant(int, cfg[term_old.unqid])
        else:
            raise RuntimeError("optmizer/resolveSearchValsCheckBounds SPO2 not found node.")

#        print "  resolveSearchVals SearchPO2 ->", term_new

    elif isinstance(term_old, SearchEnum):
        if term_old.unqid in cfg:
            term_new = Constant(int, cfg[term_old.unqid])
        else:
            raise RuntimeError("optmizer/resolveSearchValsCheckBounds SEN not found node.")

#        print "  resolveSearchVals/reslist SearchEnum ->", term_new 

    else:
        term_new = term_old
#                term_new = findFuncVal(term_old, exprscope, func)
#        print "  resolveSearchVals Else ->",term_new

    return term_new

def findScopeIdCheckBounds(expr_id, exprscope, func, cfg):
#    print "findScopeId start:", expr_id
    scope = exprscope
    varname = expr_id.name
    ressubexpr = None

#        print "dictid/varname:", varname
    #get the last write on idval
    while scope != None:
        if scope.node:
            visitor = GenericVisitorCond(AssignStmt, None,
                    cond=lambda n: True if n.lvalue.name == varname else False)
    #        print "Node ", scope.node 
            visitor.visit(scope.node)
#            print "  findScopeId Cond", visitor.values
            if visitor.values:
                value = visitor.values[::-1][0][1].rvalue
            else:
                value = []
        else:
            #higher scope
            value = scope[varname]
#            print "  findScopeId last scope", value

        if value:
            # Needs a deep copy in case of replace. Do not want to change the
            # original Locus AST
            cpvalue = copy.deepcopy(value)
            if isinstance(value, Constant) or \
                isinstance(value, SearchPowerOfTwo) or \
                isinstance(value, SearchEnum):
                ressubexpr = cpvalue
            else :
                ressubexpr = findFuncValCheckBounds(cpvalue, scope, func, cfg)

#            print "  findScopeId ", ressubexpr
            break
        else:
            scope = scope.parent

#    print "findScopeId end:", expr_id
    return ressubexpr

# Check after a the search variable replaced by the results of a search.
def checkBounds(searchnodes, cfg):

    for key, value in searchnodes.items():
        _, node, scope = value

        if isinstance(node, SearchPowerOfTwo):
            mynode = copy.deepcopy(node)

            # get the value max and min of node
            # when find a searchable var check its value on the cfg
            #_max =  resolveSearchValsCheckBounds(mynode.rangeexpr.max, scope, max, cfg)
            _max =  findFuncValCheckBounds(mynode.rangeexpr.max, scope, max, cfg)
            #_min =  resolveSearchValsCheckBounds(mynode.rangeexpr.min, scope, min, cfg)
            _min =  findFuncValCheckBounds(mynode.rangeexpr.min, scope, min, cfg)
            if isinstance(_max, Constant):
                _max = _max.value
            if isinstance(_min, Constant):
                _min = _min.value
            #print "_max",_max,"min",_min, "cfg", cfg[mynode.id] 

            if cfg[mynode.unqid] < _min or cfg[mynode.unqid] > _max:
                raise BoundsException("optmizer/checkBounds search node "+str(node.unqid)+
                        " = "+str(cfg[node.unqid])+" NOT in the range min "+str(_min)+
                        " max "+str(_max)+" .")
            else:
                log.info("Cfg node "+str(node.unqid)+" = "+str(cfg[mynode.unqid])+" VALID in "
                        "range: "+str(_min)+" .. "+str(_max)+" .")


##########################################
##########################################
# Resolve variables/ID in search types.
# This was specifically designed for SearchPerm working with seq. 
# More tests required.

def findScopeIdPerm(expr_id, exprscope):
    #print("findScopeIdPerm start:", expr_id)
    scope = exprscope
    varname = expr_id.name
    ressubexpr = None

    #print "dictid/varname:", varname
    #get the last write on idval
    while scope != None:
        if scope.node:
            visitor = GenericVisitorCond(AssignStmt, None,
                    cond=lambda n: True if n.lvalue.name == varname else False)
            #print "Node ", scope.node 
            visitor.visit(scope.node)
            #print "  findScopeIdPerm Cond", visitor.values
            if visitor.values:
                #value = visitor.values[::-1][0][1].rvalue
                value = visitor.values[-1][1].rvalue
            else:
                value = []
        else:
            #higher scope
            value = scope[varname]
            #print "  findScopeIdPerm last scope", value

        if value:
            # Needs a deep copy in case of replace. Do not want to change the
            # original Locus AST
            #print "findScopeIdPerm 1"
            cpvalue = copy.deepcopy(value)
            #print "***",cpvalue
            #cpvalue.show()
            if isinstance(cpvalue, ListMaker):
                ressubexpr = cpvalue
            elif isinstance(cpvalue, SeqCall):
               #print "findScopeIdPerm %%%%%%"
                if isinstance(cpvalue.min, ID):
                    scope[cpvalue.min.name] = findScopeIdPerm(cpvalue.min, scope)
                if isinstance(cpvalue.max, ID):
                    tmp = findScopeIdPerm(cpvalue.max, scope)
                   #print "##&##& findScopeIdPerm", tmp, type(tmp)
                    scope[cpvalue.max.name] = tmp
                if isinstance(cpvalue.step, ID):
                    scope[cpvalue.step.name] = findScopeIdPerm(cpvalue.step, scope)
                #print "Seqcall", value, value.max, type(value.max)
                #print "_%_%_",scope
                ressubexpr = cpvalue.execute(scope)
            elif isinstance(cpvalue, ID):
                #print "What to do here?", value, type(value)
                ressubexpr = findScopeIdPerm(cpvalue, scope)
            elif isinstance(cpvalue,Constant):
                ressubexpr = cpvalue
            elif isinstance(cpvalue, BinaryOp):
                resleft  = findScopeIdPerm(cpvalue.left, scope)
                #print "resleft = ",resleft
                resright = findScopeIdPerm(cpvalue.right, scope)
                #print "resright = ", resright
                ressubexpr = cpvalue.execute(scope)
            else:
                raise RuntimeError("What to do here? node value {}".format(type(cpvalue)))

            #print "  findScopeIdPerm", ressubexpr
            break
        else:
            scope = scope.parent

#    print "findScopeId end:", expr_id
    return ressubexpr

def getListPerm(permnode, scope):

    if isinstance(permnode.set, ID):
        ret =  findScopeIdPerm(permnode.set, scope)

    if not isinstance(ret, ListMaker):
        raise RuntimeError

    return ret


##########################################
##########################################
# Resolve variables/ID for recursive expansion.
# Different than findScopeIdPerm because it checks the current scope before looking for assignments.

def findScopeIdNew(expr_id, exprscope):
    log.info("findScopeIdNew start: ({}) {} {}".format(expr_id.unqid, expr_id, type(expr_id)))
    scope = exprscope
    ressubexpr = None
    if isinstance(expr_id, ID):
        varname = expr_id.name

        #print "dictid/varname:", varname
        #get the last write on idval
        while scope is not None:
            #print("scope...", id(scope), scope)
            log.info("scope... {} {}".format(id(scope), scope))
            if varname in scope and scope[varname] is not None:
                #print("Found",varname,"on scope...")
                log.info("Found {} on scope...".format(varname))
                ressubexpr = scope[varname]
                break

            elif scope.node:
                #print("Looking for assgn...",varname, expr_id.unqid)
                log.info("Looking for assgn... {} {} ".format(varname, expr_id.unqid))
                visitor = GenericVisitorCond(AssignStmt, None,
                        cond=lambda n: True if n.lvalue.name == varname else False)
                #print "Node ", scope.node 
                visitor.visit(scope.node)
                #print("  findScopeIdNew Cond val:",varname, visitor.values)
                if visitor.values:
                    # Get the lastest assign, acess the second member which is
                    # the assignment object. First is the parent.
                    #value = visitor.values[::-1][0][1].rvalue

                    # If there are more than one assigment needs to see which is
                    # valid. For instance, is there a if or get latest one.
                    if len(visitor.values) > 1:
                        #log.debug("More than one assignment. Work ahead:")
                        # the idea to traverse from the end of the list of
                        # assignment and get the latest one valid that happens before the
                        # use.
                        value = []

                        #list of assignmente in reversed order
                        revass = visitor.values[::-1]

                        # goes throgh all the assignemtns
                        for par, node in revass:
                            #log.info("rev assign {} {} ".format(node.unqid, node))

                            #checks if it happens before the use
                            if node.unqid < expr_id.unqid:
                                vispath = NodePathVisitor(node)
                                vispath.visit(scope.node)

                                validass = True
                                #go through the nodes on the path to the
                                # assignment and look for the if to check 
                                # if it is reachable.
                                for vpar, vno, vscp in vispath.pathnodes:
                                    if isinstance(vpar, IfStmt):
                                        #log.debug("checking ifsmt for assign")

                                        #find the node in ifstmt list or else
                                        #idx = vpar.ifblks.index(vno)

                                        #log.info("Ahhhhh {}".format(vpar.unqid))
                                        #tmpscope = scope
                                        #while tmpscope is not None:
                                        #    log.info("BUGGGG "
                                        #            "{}".format(tmpscope.node.unqid))
                                        #    if tmpscope.node.unqid == vpar.unqid:
                                        #        log.info("Found the node that "
                                        #                "scope represents!!!!")
                                        #        break
                                        #    tmpscope = tmpscope.parent
                                        ##endwhile
                                        tmpscope = scope

                                        # is it a else block?
                                        if vno == vpar.elseblk:
                                            validass = True
                                            break

                                            # Needs to check whether all
                                            # the ifblks are false so the else
                                            # is valid.
#                                            for cond, ifblk in vpar.ifblks:
#                                                # Using the right scope? Not
#                                                # sure. If fix here change the
#                                                # other one below as well.
#                                                rescond = cond.execute(scope)
#                                                # if any of the if is true, then
#                                                # the else is not valid
#                                                if rescond:
#                                                    validass = False
#                                                    break
#                                                #endif
#                                            #endfor
#
#                                            # Found any of the if was true,
#                                            # the else is not executed and
#                                            # the assignment node that child of
#                                            # this else will never be executed,
#                                            # so break the pathnodes loop as
#                                            # well.
#                                            if not validass:
#                                                break
                                        else:
                                            # get the list of blks since it is a
                                            # tuple
                                            ifblks = [b for c, b in vpar.ifblks]

                                            # it is a if block
                                            try:
                                                idx = ifblks.index(vno)
                                            except Exception as e:
                                                log.error("It should be in the"
                                                        " if stmt! Weird!")
                                                raise
                                            #endtry
                                            cond, ifblk = vpar.ifblks[idx]

                                            #Which scope to use? This one may
                                            #lack information. Maybe invoke
                                            #findScopeIdNew with its own scope.
                                            log.info("scope: {} ".format(tmpscope))
                                            rescond = cond.execute(tmpscope)

                                            #
                                            #rescond = findScopeIdNew(cond,
                                            #        tmpscope)
                                            if not rescond:
                                                #log.debug("cond not valid!")
                                                #if any conditional is not valid, stop
                                                validass = False
                                                break
                                            #endif
                                        #endif
                                    #endif
                                #endfor
                                if validass:
                                    value = node.rvalue
                                    #break
                                #endif
                            #endif
                        #endfor
                    else:
                        value = visitor.values[-1][1].rvalue
                        if value.unqid > expr_id.unqid:
                            # Thiago (01 Nov 2019) i am assuming here that: if the
                            # unqid of the expression is bigger than of the
                            # variable (expr_id), it means that it happens after and
                            # should not be used. Trying to fix a = a + 1 when using
                            # optimizer. Needs fix. Instead of getting the last of
                            # the list get the last one that has an smaller unqid.
                            value = []
                        #print "value id: ", value.unqid
                    #endif
                else:
                    value = []
            else:
                #higher scope
                value = scope[varname]
                #print "  findScopeIdPerm last scope", value

            if value:
                # Needs a deep copy in case of replace. Do not want to change the
                # original Locus AST
                #print "findScopeIdNew 1"
                cpvalue = copy.deepcopy(value)
                #print "***",cpvalue
                #cpvalue.show()
                if isinstance(cpvalue, ListMaker):
                    ressubexpr = cpvalue
                elif isinstance(cpvalue, SeqCall):
                   #print "findScopeIdPerm %%%%%%"
                    if isinstance(cpvalue.min, ID):
                        scope[cpvalue.min.name] = findScopeIdNew(cpvalue.min, scope)
                    if isinstance(cpvalue.max, ID):
                        tmp = findScopeIdNew(cpvalue.max, scope)
                       #print "##&##& findScopeIdPerm", tmp, type(tmp)
                        scope[cpvalue.max.name] = tmp
                    if isinstance(cpvalue.step, ID):
                        scope[cpvalue.step.name] = findScopeIdNew(cpvalue.step, scope)
                    #print "Seqcall", value, value.max, type(value.max)
                    #print "_%_%_",scope
                    ressubexpr = cpvalue.execute(scope)
                elif isinstance(cpvalue, ID):
                    #print "What to do here?", value, type(value)
                    #print "id...", cpvalue
                    ressubexpr = findScopeIdNew(cpvalue, scope)
                    _assignID(cpvalue, ressubexpr, scope)
                elif isinstance(cpvalue,Constant):
                    #print "constant...",cpvalue
                    ressubexpr = cpvalue
                    _assignID(ID(varname), ressubexpr, scope)
                elif isinstance(cpvalue, BinaryOp):
                    #print "binaryop ... l ",cpvalue.left," r ",cpvalue.right
                    #if isinstance(cpvalue.left, ID):
                    #    scope[cpvalue.left.name] = findScopeIdNew(cpvalue.left, scope)
                    #if isinstance(cpvalue.right, ID):
                    #    scope[cpvalue.right.name] = findScopeIdNew(cpvalue.right, scope)
                    resleft  = findScopeIdNew(cpvalue.left, scope)
                    #print "resleft = ",resleft
                    resright = findScopeIdNew(cpvalue.right, scope)
                    #print "resright = ", resright

                    ressubexpr = cpvalue.execute(scope)
                    _assignID(ID(varname), ressubexpr, scope)
                else:
                    raise RuntimeError("What to do here? node value {}".format(type(cpvalue)))

                #print "  findScopeIdPerm", ressubexpr
                break
            else:
                scope = scope.parent
    else:
        if expr_id.children():
            for info, termo in expr_id.children(): 
                #print "Found ", info
                ressubexpr = findScopeIdNew(termo, scope)
                #print "Vamama:",termo, ressubexpr

        #print "exper_id my friend:", expr_id, type(expr_id)
        ressubexpr = expr_id.execute(scope)

 
    #endif

#    print "findScopeId end:", expr_id
    return ressubexpr

##########################################
##########################################

class DeadCodeElimination(object):
    """This only works for some cases where definitions are unique. """

    def __init__(self, astnode):
        self.astnode = astnode

    def execute(self):
        # Find IfStmt
        visitor = GenericVisitorWScope(IfStmt)
        visitor.visit(self.astnode)

        # Find IDs in the IfStmt.cond
        for n_pa_if, n_if, n_if_scope in visitor.values:

            #print "Checking If",n_if.id
            delifblks = []
            # go on each if block, each IfStmt contains 1 or more if blocks and an 0 or 1 else block.
            for ind, (cond, blk) in enumerate(n_if.ifblks):
                visID = GenericVisitor(ID)
                #visID.visit(n_if.cond)
                visID.visit(cond)

                # Check if IDs  are the in the IfStmt scope
                # and if yes find last write.
                # Since it is in the scope it will return one write at least.
                for n_pa_id, n_id in visID.values:
                    #print "Checking ID",n_if.id, n_id.name
                    last_writes = {}
                   # if n_id.name in n_if_scope:
                    visAss = GenericVisitorCond(AssignStmt, None,
                               cond=lambda n: True if n.lvalue.name == n_id.name else False)
                    #visAss.visit(n_pa_if)
                    visAss.visit(self.astnode)

                    # go throught the assignments found, the order matter
                    for n_pa_ass, n_ass in visAss.values:
                        #print "Checking Ass",n_ass.id
                        last_writes[n_id.name] = n_ass.rvalue

                    #print "last_writes",last_writes
                    if n_id.name in last_writes:
                        newvalue = last_writes[n_id.name]
                        #print "Newvalue"
                        if isinstance(newvalue, Constant):
                            #print "Replacing", type(n_pa_id), "newvalue", newvalue
                            # when variable is alone in the cond its parent is None
                            if n_pa_id is None:
                                #n_if.cond = newvalue
                                cond = newvalue
                            else:
                                n_pa_id.replace(n_id, newvalue)
                #endfor

                # Test cond and see if can be resolved. If yes and cond means false, remove it!
                #print("Cond")
                #cond.show()
                #ret = n_if.cond.execute(n_if_scope)
                # (10 Oct 2020 - Added this to postpone changing to the CFG optimizer.
                #  But this DeadCodeElimination should not be used anymore.)
                try:
                    ret = cond.execute(n_if_scope)
                except Exception as e:
                    ret = True # just to not take any action.
                #
                #print "# UOW $$$", ret, type(ret)
                if ret == Constant(bool, False) or not ret:
                    #n_pa_if.replace(n_if, Constant(NoneType, None))
                    #log.info("ind {} to be removed".format(ind))
                    delifblks.append(ind)
                #endif
            #endfor

            #log.info("delifblks {}".format(delifblks))
            # remove the if blocks found as false. Needs to be sorted so indice removal start from the end.
            for ind in sorted(delifblks,reverse=True):
                # Eliminate the if statment
                del n_if.ifblks[ind]
                log.info("Dead code on if block: {} ind: {}.".format(n_if.unqid, ind))

            # needs to fix the code in case all if blocks were removed and there is an else
            if len(n_if.ifblks) == 0:      
                if n_if.elseblk is not None:
                    log.info("No ifblks, so else became a block outside of if.")
                    n_pa_if.replace(n_if, n_if.elseblk)
                else:
                    log.info("The whole IfStmt replaced by None (all ifblks removed and no else).")
                    n_pa_if.replace(n_if, Constant(type(None), None))
                #endif
            #endif
        #endfor
    #endef

##########################################


class DeadCodeEliminationConsts(object):
    """ Assumes that Constant Propagation and Constant folding before.
        Only eliminates IfStmts in which conditions have only constant values.
    """

    def __init__(self, astnode):
        self.astnode = astnode

    def execute(self):
        ndelim = 0

        # Find IfStmts
        visitor = GenericVisitorWScope(IfStmt)
        visitor.visit(self.astnode)

        # List for loops that are completely removed.
        # So do it later to not disrupt the iterator
        lastrm = []
        
        # Go though the ifs and remove the ones that are
        # resolved as False in the IfStmt.cond
        # Doing it in reverse, so the inner if are removed first,
        # and hopefully avoiding issues.
        for n_pa_if, n_if, n_if_scope in visitor.values[::-1]:
            loopevaled = 0

            #log.info("Checking ({}) if blks: {} else: {}...".format(n_if.unqid,
            #    len(n_if.ifblks), "Yes" if n_if.elseblk is not None else "No"))

            # loop through the ifblks, and if they are not true
            # remove them.
            delifblks = []
            trueblks = []
            for i, (cond, ifblk) in enumerate(n_if.ifblks):
                if exprHasOnlyConsts(cond):
                    rescond = cond.execute(n_if_scope)
                    log.debug("  {}) cond: {} res: {}".format(i, cond, rescond))
                    if rescond:
                        trueblks.append(i)
                    else:
                        delifblks.append(i)
                    #endif
                    loopevaled += 1
                #else:
                #    log.info("  {}) cond: {} res: {}".format(i, cond, "Undefined"))
                #endif
            #endfor

            # if there is an else and all ifblks were removed
            # replace the ifblk and put elseblk and child of the
            # parente blk.
            if n_if.elseblk is not None and \
                len(delifblks) == len(n_if.ifblks):

                #log.info("  else, delall {}".format(len(delifblks)))

                # lift the else block to that position.
                n_pa_if.replace(n_if, n_if.elseblk)
                ndelim += len(delifblks)

            elif len(delifblks) == len(n_if.ifblks):
                #log.info("  NOelse, delall {}".format(len(delifblks)))

                # no else and remove all ifs
                n_pa_if.remove(n_if)
                ndelim += len(delifblks)

            else:
                #log.info("  lopevaled: {} - trueblks {}: {} | del some {}: {}".format(
                #            loopevaled, len(trueblks), trueblks, len(delifblks), delifblks))

                # also remove else!
                # all loops must have been evaluated, otherwise
                # else might have happen. at his point, at least
                # one ifblk will stay.
                if loopevaled == len(n_if.ifblks):
                    n_if.elseblk = None

                # if there are more than one ifblks evaluated as true,
                # only the first one must stay.
                # remove the delblks and the unnecessary trueblks
                delmost = sorted(delifblks+trueblks[1:])

                # remove false blocks
                #for idx in delifblks[::-1]:
                for idx in delmost[::-1]:
                    del n_if.ifblks[idx]
                    ndelim += 1
                #endfor
                
                # Remove the if and put only the block
                # for the 1st true cond, if there is any
                if len(trueblks) > 0:
                    fcond, fblock = n_if.ifblks[0]
                    lastrm.append((n_pa_if, n_if, fblock))
                #n_pa_if.replace(n_if, fblock)
                #for stmt in fblock:
                #    n_pa_if.insertAfter(n_if,stmt)
                #
                #n_pa_if.remove(n_if)
            #endif
        #endfor

        for n_pa_if, n_if, fblock in lastrm:
            n_pa_if.replace(n_if, fblock)
            #for stmt in fblock:
            #    n_pa_if.insertAfter(n_if, stmt)
       #     ##
            #n_pa_if.remove(n_if)
            ndelim += 1
        ##

        log.debug("Dead code elimination: {} eliminations.".format(ndelim))
        return ndelim
    #enddef
#endclass

class ConstantPropagation():
    _ASS_UNDEF = 0
    _ASS_TRUE  = 1
    _ASS_FALSE = 2

    def __init__(self, root, topscp=None):
        self.root = root
        self.topscp = topscp
    #enddef

    def execute(self, limitExpr=None, limitIf=None):
        ntotalrepls = 0
        nrepls = 1
        ncheckedExpr = 0
        ncheckedIf = 0
        reachlimitExpr = False
        reachlimitIf = False

        while nrepls != 0:
            nrepls = 0
            repllist = []

            #log.info("Step 1 Being...")

            #####
            # Go through all lhs of the assignments. Two cases: ID and Expr
            visAss = GenericVisitorCond(AssignStmt, None,
                    cond=lambda n: True if not isinstance(n.rvalue, Constant) else
                    False)
            #       # cond=lambda n: True if n.lvalue.name == n_id.name else False)
            visAss.visit(self.root)

            #log.info("  assignments {}".format(len(visAss.values)))

            # For each if
            for n_pa_ass, n_ass in visAss.values:
                if limitExpr is not None and ncheckedExpr > limitExpr:
                    reachlimitExpr = True
                    break

                if isinstance(n_ass.rvalue, BinaryOp):
                    visIDs = GenericVisitor(ID)
                    visIDs.visit(n_ass.rvalue)

                    for n_pa_id, n_id in visIDs.values:
                        #log.info("Begin expr ass: {} id: {} ...".format(n_ass, n_id))
                        validDef, nDefs = ConstantPropagation.findPreviousAssignments(n_id,
                            self.root)
                        repllist.append((n_pa_id, n_id, validDef, nDefs))
                        ncheckedExpr += 1
                        #log.info("End expr ass: {} id: {} validDef: {} nDefs: "
                        #    "{}".format(n_ass, n_id, validDef, nDefs))
                    #endfor

                elif isinstance(n_ass.rvalue, ID):
                    #log.info("Begin id ass: {} id: {} ...".format(n_ass, n_ass.rvalue))
                    validDef, nDefs = ConstantPropagation.findPreviousAssignments(n_ass.rvalue,
                            self.root)
                    repllist.append((n_ass, n_ass.rvalue, validDef, nDefs))
                    ncheckedExpr += 1
                    #log.info("End id ass: {} id: {} validDef: {} nDefs: "
                    #        "{}".format(n_ass, n_ass.rvalue, validDef, nDefs))

                else:
                    log.warning("the {} type not covered for "
                            "now!!".format(type(n_ass.rvalue)))
            #endfor
            #####
            #log.info("Step 1 End")

            #log.info("Step 2 Begin...")
            #####
            # Go through the ifs
            visIfs = GenericVisitorWScope(IfStmt)
            visIfs.visit(self.root)

            # For each if
            for n_pa_if, n_if, n_if_scope in visIfs.values:
                if limitIf is not None and ncheckedIf > limitIf:
                    reachlimitIf = True
                    break

                # get all IDs on the if conds
                for cond, _ in n_if.ifblks:
                    visIDs = GenericVisitor(ID)
                    visIDs.visit(cond)

                    # go through each id in cond expr
                    for n_pa_id, n_id in visIDs.values:
                        #log.info("Begin if cond: {} id: {} ({}) {} ...".format(cond,
                        #    n_id.name, self.root.unqid, type(self.root)))
                        validDef, nDefs = ConstantPropagation.findPreviousAssignments(n_id, self.root)
                        repllist.append((n_pa_id, n_id, validDef, nDefs))
                        #log.info("End if cond: {} id: {} validDef: {} nDefs: "
                        #        "{}".format(cond, n_id.name, validDef, nDefs))
                        ncheckedIf += 1
                    #endfor
                #endfor
            #endfor
            #####
            #log.info("Step 2 End")

            #log.info("Step 3 Begin...")
            #####
            # Replace all the constants...
            for n_pa, n_id, validDef, nDefs in repllist:
                #log.info("n_id: {} validDef: {} nDefs: {} n_pa: {}". format(n_id,
                #    validDef, nDefs, type(n_pa)))
                if validDef is None and nDefs == 0:
                    # check the topblock/topscope
                    if n_id.name in self.topscp:
                        # replace
                        val = self.topscp[n_id.name]
                        if isinstance(val, Constant):
                            n_pa.replace(n_id, val)
                        else:
                            n_pa.replace(n_id, Constant(type(val), val))
                        #endif
                        #log.info("Replaced {} with {} {} from "
                        #        "topscope.".format(n_id.name, val, type(val)))
                        nrepls += 1
                    else:
                        errmsg = "Var definition for "+n_id.name+" not "+\
                                "found!"
                        log.warning(errmsg)
                        # could not find any definition of the variable
                        #raise RuntimeError(errmsg)
                    #endif

                elif validDef is not None and isinstance(validDef, Constant):
                    # only propagate constants for now
                    # replace
                    if n_pa is not None:
                        n_pa.replace(n_id, validDef)
                        nrepls += 1
                        #log.info("Replaced {} with {}.".format(n_id.name, validDef))
                    else:
                        # problem with if(balh) because it is not an expression and
                        # has no parent :(
                        log.warning("Parent of {} is None. Not "
                                " replaced!!!".format(n_id.name))
                    #endif
                else:
                    #log.info("Valid assignment for {} NOT found or constant!".format(n_id.name))
                    continue
                #endif
            #endfor
            #log.info("Step 3 End")

            ntotalrepls += nrepls
            log.info("Constant propagation: {} {} propagations. LimitExpr: {} "
                    "reached limitexpr: {} LimitIf: {} reached limitIf: {}".format(ntotalrepls,
                nrepls, limitExpr, reachlimitExpr, limitIf, reachlimitIf))

            # Running only once for now!
            break
        #endwhile

        return ntotalrepls, reachlimitExpr, reachlimitIf
    #enddef

    # Look for all assignments starting at root node.
    # Return only the ones that heppens before the id, that are also
    # verified to be executed (all ifs in the path are true). 
    # Return also the number of previous assignments, if the number is 0
    # then the topscope is checked.
    @classmethod
    def findPreviousAssignments(cls, _id, root):
        #log.info("finding for {} ...".format(_id.name))
        visAss = GenericVisitorCond(AssignStmt, None,
                cond=lambda n: True if n.lvalue.name == _id.name else
                False)
               # cond=lambda n: True if n.lvalue.name == n_id.name else False)
        visAss.visit(root)

        res = None
        # this is used to check topscope in case no assignments were found.
        nass = 0  #len(visAss.values)
        for n_pa_ass, n_ass in visAss.values[::-1]:
            #log.info("A def ({}) {} for ({}) {}".format(n_ass.unqid, n_ass, _id.unqid, _id.name))

            # assuming it happens later
            if n_ass.unqid < _id.unqid:
                nass += 1
                #log.info("Possible def {} for {}".format(n_ass, _id.name))

                # all the ifs on the path must be verifiable. Otherwise I consider
                # inconclusive and stop.
                # if there are no ifs, get the latest assingment that happens
                # before.
                vispath = NodePathVisitor(n_ass)
                vispath.visit(root)

                resAss = cls.checkPathNodes(vispath.pathnodes)

                #log.info("Conclusion: def {} for {} status: {} len(pathnodes) {}".format(
                #            n_ass, _id.name, cls.strAssStatus(resAss),
                #            len(vispath.pathnodes)))

                # the first statment found stop (reversed list)
                #if validass:
                if resAss == cls._ASS_TRUE:
                    res = n_ass.rvalue
                    break
                #elif undefined:
                elif resAss == cls._ASS_UNDEF:
                    res = None
                    break
                #endif
            #endif id less value
        #endfor
        #log.info("ending for {}".format(_id.name))
        return res, nass
    #enddef

    @classmethod
    def checkPathNodes(cls, pathnodes):
        #log.info("beginning...".format())
        # If there are no path nodes, get this definition as the one
        if pathnodes:
            resAss = cls._ASS_FALSE
            noneifstmt = True

            # go through the path of the current
            for vpar, vno, vscp in pathnodes:
                if isinstance(vpar, IfStmt):
                    noneifstmt = False

                    # is it in the else block?
                    if vno == vpar.elseblk:
                        validass = True

                        # Needs to check whether all
                        # the ifblks are false so the else
                        # is valid.
                        for cond, ifblk in vpar.ifblks:
                            # Only execute if expr has only
                            # constants.
                            rescond = True # in case it cannot verify interrupt!
                            if exprHasOnlyConsts(cond):
                                rescond = cond.execute(scope)

                                # if any of the if is true, then
                                # the else is not valid
                                if rescond:
                                    #validass = False
                                    resAss = cls._ASS_FALSE
                                    break
                                #endif
                            else:
                                #undefined = True
                                resAss = cls._ASS_UNDEF
                                break

                            #endif

                        #endfor

                        # Found any of the if was true,
                        # the else is not executed and
                        # the assignment node that child of
                        # this else will never be executed,
                        # so break the pathnodes loop as
                        # well. Breaking below not necessary anymore
                        #if not validass:
                        #    break

                    else:
                        foundblk = False
                        # all the ifs before must be false
                        # the if where the stmt is must be true
                        for cond, ifblk in vpar.ifblks:
                            #log.info("cond {}".format(cond))
                            if exprHasOnlyConsts(cond):
                                # Only execute if expr has only
                                # constants.
                                rescond = cond.execute(None)

                                #log.info("ifblk: {}".format(ifblk))
                                #log.info("vno: {}".format(vno))
                                if ifblk == vno:
                                    #log.info("the ifblk executing cond: {} rescond: {}".format(cond, rescond))

                                    # this must be true
                                    if rescond:
                                        #log.debug("cond not valid!")
                                        #if any conditional is not valid, stop
                                        #validass = True
                                        resAss = cls._ASS_TRUE
                                    else:
                                        #validass = False
                                        resAss = cls._ASS_FALSE
                                    #endif

                                    foundblk = True
                                    break
                                else:
                                    #log.info("other ifblk executing cond: {} rescond: {}".format(cond, rescond))

                                    # this must be false
                                    if rescond:
                                        #log.debug("cond not valid!")
                                        #if any conditional is not valid, stop
                                        #validass = False
                                        resAss = cls._ASS_FALSE
                                        break
                                    #endif
                                #endif which blk

                            else:
                                #log.info("executing cond: {} rescond: {}".format(cond, "Undef"))
                                #log.info("cond {} not executable".format(cond))
                                # cannot verify if so stop
                                #validass = False
                                #undefined = True
                                resAss = cls._ASS_UNDEF
                                break
                            #endif
                        #endfor

                    #if undefined do not look for any assingment
                    # break pathnode loop 
                    #if not validass or undefined:
                    if resAss == cls._ASS_FALSE or \
                        resAss == cls._ASS_UNDEF:
                        break
                    #endif
                #endif instance IfStmt
            #endfor pathnodes

            # if the path no ifs are encountered the assignment is valid
            if noneifstmt:
                resAss = cls._ASS_TRUE
            #endif
        else:
            # no pathnodes, so this stmt in the beginning is the one
            #validass = True
            #undefined = False
            resAss = cls._ASS_TRUE
        #endif if any pathnodes

        #log.info("ended".format())
        return resAss
    #enddef

    @classmethod
    def strAssStatus(cls, resAss):
        if resAss == cls._ASS_TRUE:
            return "True"
        elif resAss == cls._ASS_FALSE:
            return "False"
        elif resAss == cls._ASS_UNDEF:
            return "Undefined"
        else:
            return "?????"
    #enddef
#endclass

class ConstantFolding():

    def __init__(self, root):
        self.root = root

    def execute(self):
        visExpr = GenericVisitor(BinaryOp)
        visExpr.visit(self.root)
        ncfold = 0
        for n_pa, n_expr in visExpr.values:
            #log.info("Trying expr {} ...".format(n_expr))
            if exprHasOnlyConsts(n_expr):
                # skipping the conds from ifstmts for now
                if not isinstance(n_pa, IfStmt):
                    val = n_expr.execute(None)
                    n_pa.replace(n_expr, val)
                    #log.info("Folded expr {} -> {}".format(n_expr, val))
                    ncfold += 1
                #endif
            #endif
        #endfor

        log.info("Constant folding: {} foldings.".format(ncfold))
        return ncfold
    #enddef
#enddef

# return True if the expression has only constants.
# If True it can be calculated without scope.
def exprHasOnlyConsts(expr):
    visId = GenericVisitor(ID)
    visId.visit(expr)

    if len(visId.values) > 0:
        return False
    else:
        return True
#enddef

##########################################

# Def Use chain

#class BlockDefUse(object):
#    """DefUse per block Scope"""
#    def __init__(self, parentdefuse=None, scope=None):
#        # combine each write access with each read access (but NOT the other way round)
#        self.blockdefuse = {} # [var name in str][read access] = last write
#        self.parentdefuse = scope # defuse pointer to the parent block defuse
#        self.scope = parentdefuse # scope it refers to
#        self.last_write = {}
#
#class DefUseVisitor(NodeVisitor):
#
#    def __init__(self, rootdefuse, curdefuse=None):
#        self.root_defuse = rootdefuse # dict of the def use for each block
#        self.current_defuse = curdefuse
#        #self.current_parent_scope = None
#        self.current_parent_defuse = None
#
#    def generic_visit(self, node):
#        if isinstance(node, Block):
#           defuse = BlockDefUse(self.current_defuse,block.scope)
#           self.current_parent_defuse = self.current_defuse
#           self.current_defuse = defuse
#
#        elif isinstance(node, ID):
#            # This is read access!
#            if node.name not in self.current_defuse:
#                self.current_defuse.blockdefuse[node.name] = {}
#
#            if node.name in self.current_defuse.last_write:
#                self.current_defuse[node.name][node.id] = self.current_defuse.last_write[node.name]
#            else:
#                self.current_defuse[node.name][node.id] = "UNDEF"
#
#        oldparent = self.current_parent
#        self.current_parent = node
#        for c in node:
#            if isinstance(c, AssignStmt):
#                if isinstance(c.left, ID):
#                    # This is an write!
#                    self.current_defuse.last_write[c.left.name] = c.right
#            else:
#                self.visit(c)
#        self.current_parent = oldparent


######
######
######
# Return the value of the an expression. if the expression contains any variable it finds the last
# assingment. It is not robust to check for data-flow values.



