'''

@author: thiago
'''
import logging, sys

import locus.frontend.optlang.locus.optimizer as optimizer
from .locusast import Block, IfStmt, AssignStmt, PrintStmt,\
        OrBlock, FuncCall
from .nodevisitor import NodeVisitor
#from .locusvisitor import GenericVisitor

log = logging.getLogger("LocusCFG")

class LocusCFG():

    def __init__(self):
        #self.cfgroot = None
        pass

    @classmethod
    def ast2cfg(cls, inp):
        """Assuming receive a block of code."""
        vis = Ast2CfgVisitor()
        vis.visit(inp)
        exitnode = CFGBB(_type=CFGBB.EXIT)

        Ast2CfgVisitor.setInedgesTgt(vis.outedges, exitnode)

        # fix the ifs that do not receive the else arrow
        #cls.fixEmptyFalsePtr(vis.root, exitnode)

        return vis.root
    ##

    @classmethod
    def cfg2ast(cls, entry, bblst=None, preds=None):
        """ Convert back the cfg to the ast format.
            Receives a reference to the entry node.
            HARD TO IMPLEMENT
            needs something to else to know which parent
            to add a block of code when it has mutilple parents.
            Maybe a dominator tree...
        """
        pass
        # single predecessort:
            # is an if?
                # true or false arrow?
                    # -plug in true or false part of the if
            # is a regular
                # put in a list of the previous block
        # multiple predecessors:
            # new blokc or push it into the parent block
                
        # traverse for each reg nod create a block
        # for each if create an IfStmt
        # for each or block an OrBlock
        #pass
        #cls.traverse(entry,)

        if bblst is None:
            bblst = LocusCFG.listfycfg(entry)
        #

        if preds is None:
            preds = LocusCFG.predecessors(entry)
        #

        for k in preds:
            res = ""
            for v in preds[k]:
                res += str(v.unqid)+" " 
            print(str(k)+":"+res)

        # Has the nodes done and where to insert the info from sucessor nodes.
        ndready = {}

        # Has the nodes done, but which the predecessors are not done yet. Has what to
        # to insert on predecessor as soon as they are ready.
        ndnotready = {}

        # do it for the entry node
#        ndready[entry.unqid] = (entry, Block())
#        for bb in bblst:
#            if not bb.isentry() or not bb.isexit():
#                if bb.iscond():
#                    newanode = IfStmt()
#                elif bb.isorblk():
#                    newanode = OrBlock() 
#                else:
#                    newanode = Block()
#                    for st in bb.stmts:
#                        newanode.stmts.append(st)
#                    #
#                #
#
#                ptradd = (bb, newanode)
#                #this node is ready
#                ndready[bb.unqid] = ptradd
#
#                # check if its dependencies are done
#                if len(preds[bb.unqid]) == 1:
#                    p = preds[bb.unqid][0]
#                    if p.unqid in ndready:
#                        _, astnd = ndready[p.unqid]
#                        if p.iscond():
#                            for pe, pdesc in p.edges:
#                                if pe == bb:
#                                    if pdesc == "true":
#                                        astnd.ifblks.append((p.stmt[0],newanode))
#                                    #else:
#                                #
#                            ##
#                        elif p.isorblk():
#                            p.block.append(newanode) 
#                        else:
#                            p.stmts.append(newanode) 
#                        #
#                        #this done
#                        ndready[bb.unqid] = (bb, newanode) 
#                    else:
#                        if p not in ndnotready:
#                            ndnotready[e.unqid] = [] 
#                        #
#
#                        ndnotready[p.unqid].append(ptradd) 
#                    #
#                #else:
#
#                #
#
#                # check if anyone dependes on this
#
#            #
#        ##
#
#    ##

    @classmethod
    def predecessors(cls, entry):
        """ Receive a cfg entry node an return a dictionary of 
            (a list of) predecessors per node.
        """
        pred = {}
        def getpred(node):
            for e, desc in node.edges:
                idx = e.unqid
                if idx not in pred:
                    pred[idx] = []
                pred[idx].append(node)
            ##
        ##

        cls.traverse(entry, getpred)
        return pred
    ##

    @classmethod
    def getdefs(cls, entry):
        """Return dict of definitions per variable of the whole
           program.
        """
        alldefs = {}

        def findassigns(node):
            for inst in node.stmts:
                if isinstance(inst, AssignStmt):
                    #print("[getdefs] ",inst.unqid, inst)
                    varname = inst.lvalue.name
                    if varname not in alldefs:
                        alldefs[varname] = {}
                    #
                    alldefs[varname][inst.unqid] = inst
                    #
                #
            #
        ##

        cls.traverse(entry, findassigns)

        return alldefs
    ##

    @classmethod
    def dictfycfg(cls, root):
        """ Receive a cfg entry node and return a dict of of cfg bbs.
        """
        bbdict = {}

        def dictfy(node):
            if node.unqid not in bbdict:
                bbdict[node.unqid] = node
        ##
        cls.traverse(root, dictfy)
        return bbdict
    ###

    @classmethod
    def listfycfg(cls, root):
        """ Receive a cfg entry node and return a list of of cfg bbs.
        """
        bbdict = cls.dictfycfg(root)

        bblist = [v for k,v in sorted(bbdict.items())]
        return bblist
    ###

    @classmethod
    def findStmts(cls, root, typenode, cond):
        res = [] 
        def getstuff(node):
            for stmt, IN in zip(node.stmts, node.stmtsIN):
                if isinstance(stmt, typenode) and cond(stmt): 
                    info = (node.unqid, stmt, IN)
                    res.append(info)
                #
            ##
        ##

        cls.traverse(root, getstuff)
        return res 
    ###

    @classmethod
    def write(cls, root, buf=sys.stdout):
        def writeFunc(node):
            buf.write(str(node))

        LocusCFG.traverse(root, writeFunc)
    #enddef

    @classmethod
    def traverse(cls, root, func):
        if not isinstance(root, CFGBB):
            raise ValueError("Expecting an CFG BB node but got ", type(root))
        #

        visited = {} 
        visit = [root]
        while visit:
            node = visit[0]
            del visit[0]
            if node.unqid not in visited:
                #print("type(node)", node.unqid, type(node), node)
                visited[node.unqid] = True
                for e, desc in node.edges:
                    visit.append(e)
                func(node)
            #endif
        #endwhile
    #enddef

    @classmethod
    def fixEmptyFalsePtr(cls, root, exit):
        """ifs without an else in some cases end without
           the false pointer set. This set them to the exit node.
        """
        def chgCond(node):
            #if node.iscond() and node.false is None:
            if node.iscond(): # and node.false is None:
                foundfalse = False
                for e, desc in node.edges:
                    if desc == "false":
                        foundfalse = True
                        break
                    #
                ##
                if not foundfalse:
                    node.edges.add((exit,""))
                #
            #
        ##
        cls.traverse(root, chgCond)
    #enddef

    @classmethod
    def drawcfg(cls, root, title="LocusCFG"):
        from pydot import Dot, Edge, Node
        g = Dot(graph_type='digraph', label=title, labelloc='t')
        g.set_node_defaults(shape="box", nojustify="true")
        dictnodes = {}

        def inst2str(node, mlinejust="\l"):
            ret = ""
            if node.isentry() or node.isexit():
                return CFGBB.strType(node.type)
            elif node.isorblk() or node.isblk():
                return str(node.unqid)+") "+str(node.xinfo)+". "+CFGBB.strType(node.type)
            #

            ret += str(node.unqid)+") "

            for i in node.stmts:
                if node.iscond():
                    ret += str(i.unqid)+". if "+str(i)
                else:
                    ret += str(i.unqid)+". "+str(i)+";"
                #
                ret += mlinejust
                #break
            ##
            return ret
        ###

        def genedge(node):
            #if node.isentry():
            # Nodes
            if node.unqid not in dictnodes:
                nodeid = str(node.unqid)
                nodestr = inst2str(node) 
                dictnodes[node.unqid] = nodeid  #Node(shape='box')
                g.add_node(Node(nodeid, label=nodestr))

            for e, desc in node.edges:
                if e.unqid not in dictnodes:
                    nodeid = str(e.unqid)
                    nodestr = inst2str(e)
                    dictnodes[e.unqid] = nodeid
                    g.add_node(Node(nodeid, label=nodestr))

            #Edges
            if node.iscond():
                for e, desc in node.edges:
                    if desc == 'true':
                        color = 'green'
                    elif desc == 'false':
                        color = 'red'
                    else:
                        color = 'pink'

                    g.add_edge(Edge(dictnodes[node.unqid],
                        dictnodes[e.unqid],
                        color=color))
            else:
                for e, desc in node.edges:
                    g.add_edge(Edge(dictnodes[node.unqid],
                        dictnodes[e.unqid],
                        color='blue'))
                    #print(node.unqid,"->",node.true.unqid)
            #endif

        cls.traverse(root, genedge)
        #print("dictnodes: ", dictnodes)

        g.write_png(title.replace(' ','_')+'.png')
        #g.write_dot('opa.dot')
    #enddef

class CFGBB():
    """Represents a basic block on cfg"""
    ENTRY = 0
    EXIT  = 1
    REG = 2
    COND = 3
    ORBLK = 4
    ENDORBLK = 5
    BLK = 6
    ENDBLK = 7
    __id = 0
    def __init__(self, 
                edges=None,
                stmts=None,
                xinfo="",
                _type=REG):
        self.unqid = CFGBB.__id
        CFGBB.__id += 1
        #self.true = true
        #self.false = false
        self.edges = edges or set()
        self.stmts = stmts or []
        self.xinfo = xinfo
        self.type = _type
    #enddef

    def iscond(self):
        return self.type == self.COND

    def isentry(self):
        return self.type == self.ENTRY

    def isexit(self):
        return self.type == self.EXIT

    def isorblk(self):
        return self.type == self.ORBLK or\
                self.type == self.ENDORBLK

    def isblk(self):
        return self.type == self.BLK or\
                self.type == self.ENDBLK

    @staticmethod
    def resetid():
        CFGBB.__id = 0

    @classmethod
    def strType(cls, ty):
        if ty == cls.REG:
            return "Reg"
        elif ty == cls.COND:
            return "Cond"
        elif ty == cls.ENTRY:
            return "Entry"
        elif ty == cls.EXIT:
            return "Exit"
        elif ty == cls.ORBLK:
            return "OrBlk"
        elif ty == cls.ENDORBLK:
            return "EndOrBlk"
        elif ty == cls.BLK:
            return "Block"
        elif ty == cls.ENDBLK:
            return "EndBlock"
        else:
            return "Unknown"

    @classmethod
    def getAEdge(cls, edges, desc):
        ret = None
        for e, d in edges:
            if d == desc:
                ret = e,d
                break
            #
        ##

        return ret
    ###

    def __str__(self):
        ret = "Node "+self.strType(self.type)+" "+str(self.unqid)+":\n"
        for i in self.stmts:
            ret += "  stmt "+str(i.unqid)+": "+str(i)+"\n"

        for e, desc in self.edges:
            ret += "  "+desc+": "+str(e.unqid)+"\n"

        return ret
    ###
#endclass

# Ast2CfgVisitor 
class Ast2CfgVisitor(NodeVisitor):

    #def __init__(self, astnode=None, par=None):
    def __init__(self, par=None):
        #self.values = []
#        self.parent = par
#        self.astnode = [Block, IfStmt] #leadnodes
        CFGBB.resetid()
        self.root = CFGBB(_type=CFGBB.ENTRY) 
        self.parbb = self.root #par
        self.inedges = set([(self.root,"true")])
        #self.inedges = set() #set([(self.root,"true")])
        self.outedges = set()
        self.spaces = -2
        #self.root.true = self.parbb

    def generic_visit(self, node):
        self.spaces +=2

        if isinstance(node, Block):
            #for edg in self.inedges:
            #    edg = self.parbb
            #
            
            blkbg = CFGBB(_type=CFGBB.BLK, xinfo=node.unqid) 
            Ast2CfgVisitor.setInedgesTgt(self.inedges, blkbg)
            self.inedges = set([(blkbg,'block')])
            
            first = True
            afterif = False
            for i, c in enumerate(node):
                log.debug(self.spaces*" "+"Block/ Children["+str(i)+"]: "+str(c.unqid))
                if isinstance(c, (IfStmt, OrBlock, Block)):
                    log.debug(self.spaces*" "+"Block/IfStmt or Blks before")
                    #self.inedges.add((self.parbb,'true'))
                    afterif = True

                    #save the outedges
                    oldoutedges = self.outedges
                    self.outedges = set()

                elif first or afterif:
                    log.debug(self.spaces*" "+"Block/ first "+str(first)+" afterif "+str(afterif))
                    newnd = CFGBB()

                    #print(self.spaces*" ","Block/ indges [", "; ".join([str(i.unqid)+", "+x for
                    #    i,x in self.inedges]),"] -> ",newnd.unqid )

                    Ast2CfgVisitor.setInedgesTgt(self.inedges, newnd)

                    self.parbb = newnd
                    #self.indedges = set()
                    self.inedges = set([(self.parbb,'true')])
                    first = False
                    afterif = False
                ##

                self.visit(c)

                if isinstance(c, (IfStmt, OrBlock, Block)):
                    # make the outedges -> indedges
                    self.inedges = self.outedges
                    log.debug(self.spaces*" "+" if ifstmt orblk blk inedges "+
                            self.stredges(self.inedges))
                    #restore outedges
                    self.outedges = oldoutedges
                    log.debug(self.spaces*" "+ " if ifstmt orblk blk outedges "+
                            self.stredges(self.outedges))
                #
            #endfor

            #self.outedges.add((self.parbb, "true"))
            #self.outedges.update(self.inedges)
            log.debug(self.spaces*" "+"Block/ outedges "+
                    self.stredges(self.outedges))

            blkend = CFGBB(_type=CFGBB.ENDBLK, xinfo=node.unqid) 
            Ast2CfgVisitor.setInedgesTgt(self.inedges, blkend)
            self.inedges = set()
            #self.outedges = set([(blkend,'block')])
            self.outedges.add((blkend,'block'))
            #self.parbb = CFGBB()
            #Ast2CfgVisitor.setInedgesTgt([(blkend,'')], self.parbb)
            #self.inedges = set()
            #self.outedges = set([(self.parbb,'')])
             

        elif isinstance(node, IfStmt):
            first = False

            for cond, blk in node.ifblks:
                newcondnd = CFGBB(_type=CFGBB.COND)
                newcondnd.stmts = [cond]
                #print(self.spaces*" ","IfStmt/ newcond ", newcondnd.unqid, " ", str(cond))

                Ast2CfgVisitor.setInedgesTgt(self.inedges, newcondnd)

                self.inedges = set([(newcondnd,"true")])

                #print("IfStmt/ outedges A [", "; ".join([str(i.unqid)+", "+x for
                #    i,x in self.inedges]),"]" )

                self.visit(blk)

                # save the inedges
                self.inedges = set([(newcondnd,"false")])
            #endfor

            # if there is an else block the
            # the edge of the last cond false goes to else.
            if node.elseblk is not None:
                #print(self.spaces*" ","IfStmt/ else ")

                #Ast2CfgVisitor.setInedgesTgt(self.inedges, node.elseblk)

                self.visit(node.elseblk)

                # reset inedges because it was already used on node visit
                self.inedges = set()
            #endif

            self.outedges.update(self.inedges)
            log.debug(self.spaces*" "+"IfStmt/ outedges "+self.stredges(self.outedges))

        elif isinstance(node, OrBlock):
            newBB = CFGBB(_type=CFGBB.ORBLK, xinfo=node.unqid)
            Ast2CfgVisitor.setInedgesTgt(self.inedges, newBB)
            newedge = (newBB,'orblkpar')

            log.debug(self.spaces*" "+"OrBlock:")
            for blk in node:
                self.inedges = set([newedge])
                log.debug(self.spaces*" "+"Children:")
                self.visit(blk)
                #self.outedges.update(self.inedges)
            ##

            if newedge in self.outedges:
                self.outedges.remove(newedge)
            #

            newBB = CFGBB(_type=CFGBB.ENDORBLK, xinfo=node.unqid)
            Ast2CfgVisitor.setInedgesTgt(self.outedges, newBB)
            self.inedges = set()
            self.outedges = set([(newBB,'orblkend')])

        elif isinstance(node, (AssignStmt, PrintStmt, FuncCall)):
            self.parbb.stmts.append(node)
            log.debug(self.spaces*" "+"stmt: "+str(node))

        else:
            log.warning("Found a type {} not covered".format(type(node)))
            return
            #for c in node:
            #    self.visit(c)
        #endif
        self.spaces -=2
    #enddef

    @staticmethod
    def setInedgesTgt(inedges, tgt):
        #print(inedges, type(inedges))
        for obj, nam in inedges:
            obj.edges.add((tgt, nam))
            #if nam == "true":
            #    obj.true = tgt
            #else:
            #    obj.false = tgt
            #
        ##
    #enddef

    @staticmethod
    def stredges(edges):
        return "["+"; ".join([str(i.unqid)+", "+x for
                    i,x in edges])+"]"
    ###

#endclass
