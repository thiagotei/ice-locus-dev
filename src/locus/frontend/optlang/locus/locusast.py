'''
@author: thiago
This module is based on the pycparser by Eli Bendersky.
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:

import itertools, logging, sys, copy, os

from locus.frontend.optlang.locus.nodevisitor import NodeVisitor
from locus.util.misc import getOptClass, getToolClass
from locus.util.constants import ICE_TOP_BLK_STR

log = logging.getLogger("LocusAST")

class LocusAST(object):
    """ There is a top scope for code in global scope, and a function scope for 
    CoreRegDef, OptSeqDef, and def functions. The top code scope is interpreted
    before all the others.
    """

    def __init__(self, topblk=None, searchdef=None, functable=None):
        self.search = searchdef
        self.topblk = topblk
        self.functable = functable or {}

    def __iter__(self):
        return self.functable.__iter__()

    def __contains__(self, key):
        return key in self.functable

    def __setitem__(self, key, item):
        self.functable[key] = item

    def __getitem__(self, key):
        #return self.symtable[key]
        return self.functable[key]

    def __delitem__(self, key):
        #del self.symtable[key]
        del self.functable[key]

    def items(self):
        return self.iteritems()

    def iteritems(self):
        return self.functable.items()

    # generator for everything including the topblk
    def allspaces(self):
        if self.topblk:
            yield ICE_TOP_BLK_STR, self.topblk

        for symname in self.functable:
            if isinstance(self.functable[symname], CodeRegDef) or\
               isinstance(self.functable[symname], OptSeqDef):
                obj = self.functable[symname]
                yield symname, obj
        #endfor
    #enddef

    def optspacenames(self):
        for symname in self.functable:
            if isinstance(self.functable[symname], CodeRegDef):
                yield symname

    def optspaces(self):
        for symname in self.functable:
            obj = self.functable[symname]
            if isinstance(obj, CodeRegDef):
                yield obj

    def getoptspace(self, name):
        if self.functable is None:
            return KeyError("Optspace table empty!")
        #endif

        if name in self.functable and isinstance(self.functable[name], CodeRegDef):
            return self.functable[name]
        else:
            raise KeyError("OptSpace {} does not exist in the space.".format(name))
        #endif

    # In the locus AST entities are the optspaces (CodeRegDef) and 
    # also the OptSeqDef that can be called by CodeRegDef.
    def entities(self):
        for symname in self.functable:
            if isinstance(self.functable[symname], CodeRegDef) or\
               isinstance(self.functable[symname], OptSeqDef):
                obj = self.functable[symname]
                yield obj

    def entitiesnames(self):
        for symname in self.functable:
            if isinstance(self.functable[symname], CodeRegDef) or\
               isinstance(self.functable[symname], OptSeqDef):
                yield symname

    def show(self):
        retfun = [ent for ent in self.functable]
        retsym = [ent for ent in self.topblk.scope]
        return ", ".join(retsym+retfun)

    # Create the scope for entitities (CodeRegDef, OptSeqDef)
    def createscope(self):
        # Zero top-most scope
        # I was not sure that I could nullify all the scope during the execution of
        # ScopeCreator. So, I decided to nullify all blocks scopes before, in a separete step.
        # But, they should be fused.
        visnoscp = NullifyBlockScope()
        visnoscp.visit(self.topblk)

        # Create top-most scope
        visitor = ScopeCreator(functable=self.functable)
        visitor.visit(self.topblk)

        # The search constructs point to the global scope.
        # They could have their own scope. but needs change in the parser.
        if self.search is not None:
            self.search.scope = self.topblk.scope
        #

        # Create the scope for the entities (CodeReg, OptSeq) with
        # the top-most scope as parent scope.
        for ent in self.entities():
            visnoscp = NullifyBlockScope()
            visnoscp.visit(ent)

            visitor = ScopeCreator(parentscope=self.topblk.scope,
                    functable=self.functable)
            visitor.visit(ent)
        #endfor
    #enddef

    def showScope(self, buf=sys.stdout, offset=0):
        log.info("Showing scope...")
        for entname, ent in self.allspaces():
            log.info(f"Let's go into {entname}:")
            vis = PrintBlockScope()
            vis.visit(ent)

            for node, scop, off in vis.values:
                lead = " " * off
                log.info(f"{lead}node: {node.unqid:5d} scop.par {id(scop.parent)} scop {id(scop)}: {scop}")
            #
        ##
        log.info("End Showing scope.")
    ###
#endclass LocusAST

class NullifyBlockScope(NodeVisitor):
    def __init__(self):
        pass

    def visit_Block(self, node):
        #log.info(f"@@@!!!$$$ >>>>>>>> Found a block!! id: {node.unqid}")
        node.scope = None

        for c in node:
            self.visit(c)
    ###
####

class PrintBlockScope(NodeVisitor):
    def __init__(self):
        self.values = []
        self.offset = 0
        self.ofval = 2

    def visit_Block(self, node):
        self.values.append((node, node.scope, self.offset))
        self.offset += self.ofval
        for c in node:
            self.visit(c)
        ##
        self.offset -= self.ofval
    ###
####

# This creates the scopes after parsing the code.
class ScopeCreator(NodeVisitor):

    def __init__(self, parent=None, parentscope=None, functable=None):
        self.values = []
        self.parent = parent
        self.parentscope = parentscope
        self.functable = functable

    def generic_visit(self, node):
        oldparentscope = None
        #print type(node), "parentscope", repr(self.parentscope)
        if isinstance(node, Block):
            #print "block node", repr(node)
            #
            # Flow statments (if, for, while) share the same block parent scope.
            if True:
            #if isinstance(self.parent, IfStmt) or \
            #        isinstance(self.parent, ForStmt):
            #    #print "parent ifstmt"
            #    node.scope = self.parentscope
            #    oldparentscope = self.parentscope
            #else:
                # print "parent not flow"
                # (Thiago Apr 09 2020) Why i need to check if it is none?
                # Why no always create a new scope? If possible, it would
                # avoid the extra step of nullify the block scope that I
                # just added.
                if node.scope is None:
                    node.scope = Scope()
                #endif
                node.scope.parent = self.parentscope
                node.scope.node = node
                node.scope.functable = self.functable

                # this is to get only the 1st block
                if isinstance(self.parent, CodeRegDef):
                    #Not necessary to define codereg scope as the block scope.
                    #it is already referring to body block scope.
                    ###self.parent.scope = node.scope
                    pass
                elif isinstance(self.parent, OptSeqDef):
                    #Already reffering to body block scope no need for this assignment
                    #self.parent.scope = node.scope
                    # Add the OptSeq args to scope of the block.
                    # So far CodeSeq does not accept args.
                    for arg in self.parent.args or []:
                        if isinstance(arg.expr, AssignStmt):
                            node.scope[arg.id.name] = arg.expr.rvalue
                        else:
                            node.scope[arg.id.name] = arg.expr
                        #endif
                #endif
#               print node.scope

                oldparentscope = self.parentscope
                self.parentscope = node.scope
            #endif parent, IfStmt...
        elif isinstance(node, AssignStmt):
#            print "parentscope", repr(self.parentscope)
#            print "ScopeCreater",node.lvalue.name,type(node.rvalue)
            #
            # Check if variable can be found. If found,
            # do nothing! It means at this point that this variable is 
            # global and was put to scope before.
            #
            def findit(val):
                try:
                    _lookupID(val, self.parentscope)
                except KeyError as e:
#                   print "added by force"
                    self.parentscope[val.name] = None #node.rvalue
            #end def

            #print "ScopeCreator",node.lvalue
            if isinstance(node.lvalue, list):
                for lv in node.lvalue:
                    findit(lv)
            else:
                findit(node.lvalue)
#            print "ScopeCreator",node.lvalue, "END"

        #endif node, Block...

        oldparent = self.parent
        self.parent = node
        for c in node:
            self.visit(c)
        self.parent = oldparent

        if isinstance(node, Block):
            self.parentscope = oldparentscope
#endclass ScopeCreator 

def compareLocusASTs(ast1, ast2):
    #print type(ast1), type(ast2)
    if type(ast1) != type(ast2):
        return False
    sefields = ast1.children()
    otfields = ast2.children()
    if len(sefields) != len(otfields):
        return False
    for s, o in zip(sefields, otfields):
        if type(s) != type(o):
            return False
        if not compareLocusASTs(s[1], o[1]):
            return False
    return True

#end compareLocusASTs

def _assignID(_id, newvalue, topscope):
    #log.info("id: {} newv: {}".format(_id, newvalue))
    while topscope:
        #log.info("scope {}: {}".format(id(topscope),topscope))
        if _id.name in topscope:
            topscope[_id.name] = newvalue
            return
        topscope = topscope.parent
    raise KeyError("Could not find the var {}".format(_id))
#end assignID

def _captureFullScope(scop):
    scopdict = []
    key = 0
    while scop:
        scopdict[key] = scop
        key += 1
        scop = scop.parent
    #endwhile
#enddef

def _lookupID(_id, topscope):
    #print "_lookupID",_id
    while topscope:
        #print "   topscope",id(topscope), " par: ", id(topscope.parent),topscope
        if _id.name in topscope:
            return topscope[_id.name]
        topscope = topscope.parent
    raise KeyError("Could not find the var {}".format(_id))
#end lookupID

def _lookupFunc(_id, functable):
    if functable and _id.name in functable:
        return functable[_id.name]
    raise KeyError("Could not find the var {}".format(_id))
#end lookupID

class LocusNode(object):

    # These are to be used during runtime
    iceargs = None
    codeseqname = None
    combid = 0
    optid = 0
    srccode = None
    orighashes = None
    saverecurs = False
    ####
    __idcounter = 0

    def __init__(self):
        self._unqid = LocusNode.__idcounter
        LocusNode.__idcounter += 1

    def children(self):
        """ A sequence of all children that are Nodes
        """
        pass

    @property
    def idcounter(self):
        return LocusNode.__idcounter

    def incidcounter(self):    
        LocusNode.__idcounter += 1

    @property
    def unqid(self):
        return self._unqid

    # CAREFUL using this!!!
    @staticmethod
    def resetidcounter():
        LocusNode.__idcounter = 0

    @unqid.setter
    def unqid(self, v):
        self._unqid = v

    # Compare the two nodes fields.
#    def __eq__(self, other):
#        print "@@@",type(self), type(other)
#        if type(self) != type(other):
#            return False
#        sefields = self.children()
#        otfields = other.children()
#        if len(sefields) != len(otfields):
#            return False
#        for s, o in itertools.izip(sefields, otfields):
#            if type(s) != type(o):
#                return False
#            if s != o:
#                return False
#        return True

    def __ne__(self, other):
        return not self == other

    def show(self, buf=sys.stdout, offset=0, attrnames=False, nodenames=False, showcoord=False, _my_node_name=None):
        """ Pretty print the Node and all its attributes and
            children (recursively) to a buffer.
            buf:
                Open IO buffer into which the Node is printed.
            offset:
                Initial offset (amount of leading spaces)
            attrnames:
                True if you want to see the attribute names in
                name=value pairs. False to only see the values.
            nodenames:
                True if you want to see the actual node names
                within their parents.
            showcoord:
                Do you want the coordinates of each Node to be
                displayed.
        """
        lead = ' ' * offset
        if nodenames and _my_node_name is not None:
            buf.write(lead + str(self.unqid) + ' ' + self.__class__.__name__+ ' <' + _my_node_name + '>: ')
        else:
            buf.write(lead + str(self.unqid) + ' ' + self.__class__.__name__+ ': ')

        if self.attr_names:
            if attrnames:
                nvlist = [(n, getattr(self,n)) for n in self.attr_names]
                attrstr = ', '.join('%s=%s' % nv for nv in nvlist)
            else:
                vlist = [getattr(self, n) for n in self.attr_names]
                attrstr = ', '.join('%s' % v for v in vlist)
            buf.write(attrstr)

        if showcoord and False:
            buf.write(' (at %s)' % self.coord)
        buf.write('\n')

        for (child_name, child) in self.children():
            child.show(
                buf,
                offset=offset + 2,
                attrnames=attrnames,
                nodenames=nodenames,
                showcoord=showcoord,
                _my_node_name=child_name)
    #enddef

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

#endclass 

class Constant(LocusNode):

    def __init__(self, _type=None, value=None):
        super(Constant, self).__init__()
        self.type = _type
        self.value = value

    def __str__(self):
        if self.type == str:
            return "\""+str(self.value)+"\""
        else:
            return str(self.value)

    def rawstr(self):
        """Always return in str and without quotes."""
        return str(self.value)

    def __iter__(self):
        return
        yield

    def children(self):
        nodelist = []
        return tuple(nodelist)

    def __int__(self):
        return int(self.value)

    def __float__(self):
        return float(self.value)

    def __eq__(self, other):
        if not isinstance(other, Constant):
            return False
        return Constant(self.type, self.value == other.value)

    def __ne__(self, other):
        return Constant(self.type, not self == other)

    def __le__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant le types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value <= other.value)

    def __ge__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant ge types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value >= other.value)

    def __lt__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant lt types. {} {} > "
                    "{}".format(self, type(self),type(other)))
        return Constant(self.type, self.value < other.value)

    def __gt__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant gt types. {} + "
                    "{}".format(type(self),type(other)))
#        print "self",self, "other", other
#        print self.value > other.value
        return Constant(self.type, self.value > other.value)

    def __add__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant add diff types. {} + "
                    "{}".format(type(self),type(other)))
        #
        try:
            ret = Constant(self.type, self.value + other.value)
        except:
            log.error(f"Something is wrong at : {self.value} {type(self.value)} + {other.value} {type(self.value)}")
            raise
        #
        return ret

    def __sub__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant  sub types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value - other.value)

    def __mul__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant mul diff types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value * other.value)

    def __div__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant true div types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value /  other.value)

    def __truediv__(self, other):
        return self.__div__(other)

    def __floordiv__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant floor div types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value // other.value)

    def __mod__(self, other):
        if not isinstance(other, Constant):
            raise RuntimeError("Constant mod types. {} + "
                    "{}".format(type(self),type(other)))
        return Constant(self.type, self.value % other.value)

    def __pos__(self):
        return Constant(self.type, + self.value)

    def __neg__(self):
        return Constant(self.type, - self.value)

    def __invert__(self):
        return Constant(self.type, ~ self.value)

    def __bool__(self):# Python3 change to __bool__
        try:
            v = self.value.__bool__()
        except AttributeError as e:
            try:
                v = len(self.value) > 0
            except AttributeError as e:
                v = True

        return v

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
#        return self.type(self.value)
        #print("Constant/execute", self, type(self))
        return self

    attr_names = ('type', 'value',)

class StrCall(LocusNode):
    _PRINTNAME = "str"

    def __init__(self, inp):
        super(StrCall, self).__init__()
        self.inp = inp

    def __iter__(self):
        if self.inp is not None:
            yield self.inp

    def children(self):
        nodelist = []
        if self.inp is not None: nodelist.append(("inp", self.inp))
        return tuple(nodelist)

    def replace(self, old, new):
        self.inp = new

    def __str__(self):
        ret = self._PRINTNAME+"("+str(self.inp)+")"
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        ret = self.inp.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        #print("StrCall",ret)
        return Constant(str,str(ret))

    attr_names = ()

class LenCall(StrCall):
    _PRINTNAME = "len"

    def execute(self, scope):
        ret = self.inp.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        #print("LenCall",ret)
        return Constant(int, len(ret))

class IntConvCall(StrCall):
    _PRINTNAME = "int"
    _TYPE = int

    def execute(self, scope):
        ret = self.inp.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        #print("IntConvCall: before", type(ret), ret)
        return Constant(self._TYPE, self._TYPE(ret))

class FloatConvCall(IntConvCall):
    _PRINTNAME = "float"
    _TYPE = float

class GetEnvCall(StrCall):
    _PRINTNAME = "getenv"

    def __init__(self, inp, defval=None):
        super(GetEnvCall, self).__init__(inp)
        self.defval = defval

    def __iter__(self):
        if self.inp is not None:
            yield self.inp
        if self.defval is not None:
            yield self.defval

    def children(self):
        nodelist = []
        if self.inp is not None: nodelist.append(("inp", self.inp))
        if self.defval is not None: nodelist.append(("defval", self.defval))
        return tuple(nodelist)

    def execute(self, scope):
        ret = self.inp.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)

        #print "GetEnvCall: before", type(ret), ret, str(ret), type(str(ret)), type(str(ret.value))
        retfn = os.getenv(str(ret.value), default=None)
        if retfn is None and self.defval is None:
            ret = Constant(str, str(None))

        elif retfn is None and self.defval is not None:
            ret = self.defval
        else:
            ret = Constant(str, retfn)
        #endif

        #print "GetEnvCall:", type(ret), ret
        return ret
    #enddef
#endclass

class SeqCall(LocusNode):

    def __init__(self, _min=None, _max=None, step=Constant(int,1)):
        super(SeqCall, self).__init__()
        self.max = _max
        self.min = _min
        self.step = step

    def __iter__(self):
        if self.max is not None:
            yield self.max
        if self.min is not None:
            yield self.min
        if self.step is not None:
            yield self.step

    def children(self):
        nodelist = []
        if self.min is not None: nodelist.append(("min", self.min))
        if self.max is not None: nodelist.append(("max", self.max))
        if self.step is not None: nodelist.append(("step", self.step))
        return tuple(nodelist)

    def __str__(self):
        ret = "seq("
        if self.min is not None:
            ret += str(self.min)+","
        if self.max is not None:
            ret += str(self.max)+","
        if self.step is not None:
            ret += str(self.step)
        ret +=")"
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        #log.debug("SeqCall")
        #self.show()
        #print "Scope",scope
        retmin = self.min.execute(scope)
        if isinstance(retmin, ID):
            retmin = _lookupID(retmin, scope)
        #print "seq min 2", retmin, type(retmin), retmin.value, type(retmin.value)

        retmax = self.max.execute(scope)
        #print "seq max", retmax
        if isinstance(retmax, ID):
            #print "SCOPE",retmax,scope[retmax.name], type(scope[retmax.name])
            tmp = _lookupID(retmax, scope)
            #print "seq max tmp", tmp, type(tmp),type(tmp.value)
            retmax = tmp
        #print "seq max 2", retmax, type(retmax), retmax.value, type(retmax.value)

        retst = self.step.execute(scope)
        if isinstance(retst, ID):
            retst = _lookupID(retst, scope)

        #print type(retmin.value), "What:",type(retmax.value), type(retst.value)
        retseq = []
        for i in range(retmin.value, retmax.value, retst.value):
            retseq.append(Constant(type(i),i))

        return ListMaker(retseq)

    attr_names = ()

class GetItem(LocusNode):

    def __init__(self, _id, index):
        super(GetItem, self).__init__()
        self.id = _id
        self.index = index

    def __iter__(self):
        if self.id is not None:
            yield self.id
        if self.index is not None:
            yield self.index

    def children(self):
        nodelist = []
        if self.id is not None: nodelist.append(("id", self.id))
        if self.index is not None: nodelist.append(("index", self.index))
        return tuple(nodelist)

    def __str__(self):
        ret = str(self.id)+"["+str(self.index)+"]"
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        retind = self.index.execute(scope)
        if isinstance(retind, ID):
            retind = _lookupID(retind, scope)

        ret = self.id.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)

        #print "GetItem-execute",ret, type(ret)
        #print "   index", retind, type(retind)
        return ret[retind.value]

    attr_names = ('index',)

class PrintStmt(LocusNode):

    def __init__(self, inp):
        super(PrintStmt, self).__init__()
        self.inp = inp

    def __iter__(self):
        if self.inp is not None:
            yield self.inp

    def children(self):
        nodelist = []
        if self.inp is not None: nodelist.append(("inp", self.inp))
        return tuple(nodelist)

    def __str__(self):
        ret = "print "+str(self.inp)
        return ret

    def replace(self, old, new):
        self.inp = new

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        ret = self.inp.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        print(ret)

    attr_names = ()

class MainDef(LocusNode):

    def __init__(self):
        raise NotImplementedError()

class SearchPowerOfTwo(LocusNode):

    def __init__(self, rg=None, initv=None):
        super(SearchPowerOfTwo, self).__init__()
        self.rangeexpr = rg
        self.initv = initv

    def __iter__(self):
        if self.rangeexpr is not None:
            yield self.rangeexpr

    def children(self):
        nodelist = []
        if self.rangeexpr is not None: nodelist.append(("rangeexpr", self.rangeexpr))
        return tuple(nodelist)

    def __str__(self):
        ret = "poweroftwo("+str(self.rangeexpr)+")"
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        ret = self.rangeexpr.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        return ret

    attr_names = ()

class SearchInteger(SearchPowerOfTwo):

    def __str__(self):
        ret = "integer("+str(self.rangeexpr)+")"
        return ret

class SearchPerm(LocusNode):
    """ Receives a ListMaker """

    def __init__(self, rg=None, initv=None):
        super(SearchPerm, self).__init__()
        self.set = rg
        self.initv = initv or rg

    def __iter__(self):
        if self.set is not None:
            yield self.set

    def children(self):
        nodelist = []
        if self.set is not None: nodelist.append(("set", self.set))
        return tuple(nodelist)

    def __str__(self):
        ret = "permutation("+str(self.set)+")"
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        ret = self.set.execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        return ret

    attr_names = ()

class SearchEnum(LocusNode):

    def __init__(self, vs=None):
        super(SearchEnum, self).__init__()
        self.values = vs or []
        self.initv = 0

    def __iter__(self):
        for v in self.values:
            yield v

    def children(self):
        nodelist = []
        for i, child in enumerate(self.values):
            nodelist.append(("value[{}]".format(i), child))
        return tuple(nodelist)

    def __str__(self):
        ret = "enum("+str(','.join([str(x) for x in self.values]))+")"
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        ret = None
        if self.values:
            ret = self.values[0].execute(scope)
        if isinstance(ret, ID):
            ret = _lookupID(ret, scope)
        return ret

    attr_names = ()

class OptionalStmt(LocusNode):

    def __init__(self, stmt=None):
        super(OptionalStmt, self).__init__()
        self.stmt = stmt
        self.initv = True

    def __str__(self):
        ret = "OptionalStmt:"
        ret += "\n" + str(self.stmt)
        return ret

    def __iter__(self):
        if self.stmt is not None:
            yield self.stmt

    def children(self):
        nodelist = []
        if self.stmt is not None: nodelist.append(("stmt", self.stmt))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + '* ')
        self.stmt.showCode(buf)

    def execute(self, scope):
        raise RuntimeError("OptionalStmt cannot be executed!")

    attr_names = ()

class Decorator(LocusNode):
    """Decorators for the OptSeq."""
    """Example: to expand recursion before search space creation."""

    def __init__(self, name):
        super(Decorator, self).__init__()
        self.name = name

    def __str__(self):
        return "@"+str(self.name)

    def __iter__(self):
        return
        yield

    def __eq__(self, other):
        if not isinstance(other, Decorator):
            return False
        return self.name == other.name

    def __ne__(self, other):
        return not self == other

    def children(self):
        nodelist = []
        return tuple(nodelist)

    attr_names = ('name',)
####

class ID(LocusNode):

    def __init__(self, name=None):
        super(ID, self).__init__()
        self.name = name

    def __str__(self):
        return str(self.name)

    def __iter__(self):
        return
        yield

    def children(self):
        nodelist = []
        return tuple(nodelist)

    def __eq__(self, other):
        if not isinstance(other, ID):
            return False
        return self.name == other.name

    def __ne__(self, other):
        return not self == other

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
#        val = _lookupID(self, scope)
#        ret = val.execute(scope)
        return self

    attr_names = ('name',)

class RangeExpr(LocusNode):

    def __init__(self, _min=None, _max=None, step=Constant(int,1)):
        super(RangeExpr, self).__init__()
        self.min = _min
        self.max = _max
        self.step = step
        if self.step.value != 1:
            raise NotImplementedError("Step values different than 1 have not been fully implemented yet! Open an issue if you need it.")

    def __str__(self):
        return str(self.min)+" .. "+str(self.max)+" .. "+str(self.step)

    def __iter__(self):
        if self.min is not None:
            yield self.min
        if self.max is not None:
            yield self.max
        if self.step is not None:
            yield self.step

    def children(self):
        nodelist = []
        if self.min is not None: nodelist.append(("min", self.min))
        if self.max is not None: nodelist.append(("max", self.max))
        if self.step is not None: nodelist.append(("step", self.step))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        # The default value of RangeExpr is its minimum value. So when executed it
        # should execute and return the min value.
        return self.min.execute(scope)
        #raise RuntimeError("RangeExpr cannot be executed!")

    attr_names = ()

class Argument(LocusNode):

    def __init__(self, _id=None, expr=None):
        super(Argument, self).__init__()
        self.id = _id
        self.expr = expr

    def __str__(self):
        if self.id and (self.expr or 
            self.expr == Constant(int, 0) or
            self.expr == Constant(str, "")):
            ret = str(self.id)+"="+str(self.expr)
        elif self.id:
            ret = str(self.id)
        else:
            ret = str(self.expr)
        return ret

    def __iter__(self):
        if self.id is not None:
            yield self.id
        if self.expr is not None:
            yield self.expr

    def replace(self, old, new):
        # This is very messy. Assuming replacements of 
        # arguments are for function calls, I complicated
        # too much when implemented the grammar parsing of this :(
        if self.id is not None and \
                self.expr is not None:
            self.expr = new
        elif self.id is not None:
            if isinstance(new, ID):
                self.id = new
            else:
                self.id = None
                self.expr = new
        else:
            self.expr = new
        #
    ###

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def children(self):
        nodelist = []
        if self.id is not None: nodelist.append(("id", self.id))
        if self.expr is not None: nodelist.append(("expr", self.expr))
        return tuple(nodelist)

    attr_names = ()

class Return(LocusNode):

    def __init__(self, exprs=None):
        super(Return, self).__init__()
        self.exprs = exprs or []

    def __str__(self):
        ret = "return " + ", ".join(map(str, self.exprs))
        return ret

    def __iter__(self):
        for child in self.exprs:
            yield child

    def children(self):
        nodelist = []
        #if self.exprs is not None: nodelist.append(("exprs", self.exprs))
        for i, child in enumerate(self.exprs):
            nodelist.append(("exprs[{}]".format(i), child))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + 'return ') 
        if self.exprs:
            self.exprs[0].showCode(buf)

            for exp in self.exprs[1:] or []:
                buf.write(', ')
                exp.showCode(buf) 

    def execute(self, scope):
        ret = []
        for exp in self.exprs:
            expret = exp.execute(scope)
            if isinstance(expret, ID):
                expret = _lookupID(expret, scope)
            ret.append(expret)
        #log.debug("End Return: %s", ", ".join([str(x) for x in ret]))
        #print "End Return"
        return tuple(ret) if len(ret) > 1 else ret[0]

    attr_names = ()

class ArgList(LocusNode):

    def __init__(self, args=None):
        super(ArgList, self).__init__()
        self.args = args or []

    def __str__(self):
        if self.args:
            a = list(map(str,self.args))
            ret = ", ".join(a)
        else:
            ret = ""
        return ret

    def __iter__(self):
        for child in self.args:
            yield child

    def __getitem__(self, idx):
        return self.args[idx]

    def children(self):
        nodelist = []
        for i, child in enumerate(self.args):
            nodelist.append(("args[{}]".format(i), child))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    attr_names = ()

class FuncCall(LocusNode):

    def __init__(self, name=None, arglist=None):
        super(FuncCall, self).__init__()
        self.name = name
        self.arglist = arglist or []

    def __str__(self):
        ret = str(self.name)
        if self.arglist is not None:
            ret += " ("+", ".join(map(str,self.arglist))+")"
        else:
            ret += " ()"
        return ret

    def __iter__(self):
        if self.name is not None:
            yield self.name
        for arg in self.arglist:
            yield arg

    def children(self):
        nodelist = []
        if self.name is not None: nodelist.append(("name", self.name))
        for i, child in enumerate(self.arglist):
            nodelist.append(("arglist[{}]".format(i), child))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        self.name.showCode(buf, offset=offset);
        buf.write(' (');
        if self.arglist:
            self.arglist[0].showCode(buf)

            for exp in self.arglist[1:] or []:
                buf.write(', ')
                exp.showCode(buf)

        buf.write(')');


    def execute(self, scope):
        ret = None
        # Lookup in the scope, if not found assume it is OptTool
        if self.name is not None:
            func = None
            try:
                func = _lookupFunc(self.name, scope.functable)
            except KeyError as e:
                log.debug("FuncCall Not found %s in scope, assuming optimization.", self.name)

            if func is None:
                log.debug("Need to call this %s", self.name)
                # Loading the tool module (eg. RoseUiuc, Pips)$
                try:
                    classname = str(self.name).split('.')[0]
                    toolclass = getToolClass('locus.tools.opts.', classname)
                    # Loading the nested class that represents the optimization.
                    optclass = getOptClass(toolclass, str(self.name).split('.')[1])
                except Exception as e:
                    raise

                actualparams = {}
                # Go through actual arguments and initialized the formal
                # arguments.
                for i, actarg in enumerate(self.arglist):
                    actargid = actarg.id
                    actargexp = actarg.expr

                    # For now ICE optimizations only accept name actual arguments.
                    if actargid is not None and actargexp is not None:
                        # look only in the scope of the 
                        # print "FuncCall ", actargid, actargexp
                        evalexp = actargexp.execute(scope)
                        if isinstance(evalexp, ID):
                            evalexp = _lookupID(evalexp, scope)

                        if isinstance(evalexp, Constant):
                            evalexp = evalexp.value

                        actualparams[actargid.name] = evalexp
                    else:
                        raise NotImplementedError("For now these actual arguments need to be named."
                                " Calling {} and argument {}.".format(self.name, i))
                #endfor

                optobj = optclass()
#                print self.iceargs
#                print self.srccode 
                #log.info(f"classtype: {type(optobj)} {optobj.__class__.__name__} {optobj.__class__.__class__.__name__} {optobj.__module__} {type(optobj).__module__} {optobj.__class__.__bases__}")
                # (Thiago Feb 19 2022) The actual arguments of Query.ListInnerLoops prints a lot of code. It is a tuple. 
                # So, I just dont print the arg here if is tuple. Needs a better fix though. Not sure how it affects all the actual argument types.
                stractparam = ", ".join([str(k)+"="+str(v) if type(v) != tuple else str(k)+"="+str(type(v)) for k, v in actualparams.items()])
                #stractparam = ", ".join([str(k)+"="+str(v) for k, v in actualparams.items()])
                stroptobj= optobj.__module__.rsplit('.',1)[-1]+"."+optobj.__class__.__qualname__ #str(type(optobj))
                log.info("Calling opt "+str(LocusNode.optid)+
                        " "+stroptobj+" ("+stractparam+") .")
                # 
                # This is just a workaround to execute queries on the code for arbitrary code
                # using the optmization tools infra.
                #
                funcret = optobj.apply(self.srccode, self.orighashes, actualparams,
                        self.codeseqname, self.iceargs, LocusNode.combid, LocusNode.optid)
                if classname == "Query":
                    ret = funcret
                else:
                    LocusNode.srccode = funcret
                    LocusNode.optid += 1
                #endif

            else:
                # Create a new scope for the call, so recusivity can work.
                #newscope = copy.deepcopy(func.scope)
                newscope = func.scope.customCopy()
                oldscope = func.scope
                func.scope = newscope
                #print ">Funccall new", id(newscope), id(newscope.parent), newscope #, "inst",type(inst)
                #print "          old", id(oldscope), id(oldscope.parent), oldscope #, "inst",type(inst)
                #print "          fun", id(func.scope), func.scope #, "inst",type(inst)

                processCallArgList(self.arglist, scope, func.scope, newscope, func.args.args)

                #print ">>Funccall new", id(newscope), newscope #, "inst",type(inst)
                #print "           old", id(oldscope), oldscope #, "inst",type(inst)
                #print "           fun", id(func.scope), func.scope #, "inst",type(inst)
                ret = func.execute(newscope)
                func.scope = oldscope

            #endif func is None
        else:
            raise RuntimeError("FuncCall has no name!")
        #log.debug("FuncCall ret: %s", ret)
        return ret
    #enddef execute

    attr_names = ()
#endclass

# input: arglist from funccall, funcscope is the function scope, callscope is the
# scope where the func call ocurred. searchvar is to search for an assignment that will provide, used by recursive expansion.
# returns: newscope a dictionary with values
def processCallArgList(arglist, callscope, funcscope, newscope, funcargsargs):

    # Go through actual arguments and initialized the formal
    # arguments.
    for i, actarg in enumerate(arglist):
        actargid = actarg.id
        actargexp = actarg.expr
        #print "FuncCall args:", actargid, actargexp

        # This is for call with args of type <id>=<expr>
        if actargid is not None and actargexp is not None:
            # look only in the scope of the 
            if actargid.name in funcscope:
                evalexp = actargexp.execute(callscope)

                if isinstance(evalexp, ID):
                    evalexp = _lookupID(evalexp, callscope)

                newscope[actargid.name] = evalexp
            else:
                raise RuntimeError("Formal arg {} not "
                        "found!".format(actargid))
        else:
            # Solving positioning arguments. Look at locusparser
            # argument rule before changing this.
            if i < len(funcargsargs):
                farg = funcargsargs[i]

                #log.debug("actual arg (pos: %s, type: %s, expr: %s) "
                #        "formal arg (name: %s)",
                #        i, str(type(actargexp))[-15:-2], actargexp, farg.id.name)

                if actargexp is None:
                    evalarg = actargid.execute(callscope)
                else:
                    evalarg = actargexp.execute(callscope)
                #endif

                if isinstance(evalarg, ID):
                    #print "evalarg: ", evalarg
                    evalarg = _lookupID(evalarg, callscope)
                    #print "=> evalarg: ", evalarg 
                #endif

                #print "processCallArgList before",id(newscope),newscope
                newscope[farg.id.name] = evalarg 
                #print "processCallArgList after",id(newscope),newscope
                #sys.stdout.flush()
                #print "adding to scope",farg.id.name," = ",newscope[farg.id.name]
            else:
                raise RuntimeError("Number of arguments "
                        "inconsistent. func %s has %s formal args"
                        " and tried pos %s. ", func.id.name,
                        len(func.args.args), i)
        #endif is not None 
    #endfor actargs
#enddef

class UnaryOp(LocusNode):
    NEG = "-"
    INV = "~"
    POS = "+"

    def __init__(self, op=None, right=None):
        super(UnaryOp, self).__init__()
        self.op = op
        self.right = right

    def __str__(self):
        ret = ""
        if self.op is not None:
            ret += str(self.op)+" "
        if self.right is not None:
            ret += str(self.right)
        return ret

    def __iter__(self):
        if self.right is not None:
            yield self.right

    def children(self):
        nodelist = []
        if self.right is not None: nodelist.append(("right", self.right))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        #self.show()

        ri = self.right.execute(scope)
        if isinstance(self.right, ID):
            ri = _lookupID(self.right, scope)

        #print "ri", type(ri)
        if self.op == UnaryOp.NEG:
            ret = - ri
        elif self.op == UnaryOp.INV:
            ret = ~ ri
        elif self.op == UnaryOp.POS:
            ret = + ri
        else:
            raise NotImplementedError("Not yet {} !".format(self.op))
        #log.debug("UnaryOp {}".format(ret))
        return ret

    attr_names = ('op',)

class BinaryOp(LocusNode):
    ADD = "+"
    SUB = "-"
    MUL = "*"
    TRUEDIV = "/"
    FLOORDIV = "//"
    MOD = "%"
    AND = "&&"
    OR  = "||"
    EQ  = "=="
    NE  = "!="
    LT  = "<"
    LE  = "<="
    GT  = ">"
    GE  = ">="
    BAND = "&"
    BOR = "|"

    def __init__(self, op=None, left=None, right=None):
        super(BinaryOp, self).__init__()
        self.op = op
        self.left = left
        self.right = right

    def __str__(self):
        ret = ""
        if self.left is not None:
            ret += "("+str(self.left)+" "
        if self.op is not None:
            ret += str(self.op)+" "
        if self.right is not None:
            ret += str(self.right)+")"
        return ret

    def __iter__(self):
        if self.left is not None:
            yield self.left
        if self.right is not None:
            yield self.right

    def children(self):
        nodelist = []
        if self.left is not None: nodelist.append(("left", self.left))
        if self.right is not None: nodelist.append(("right", self.right))
        return tuple(nodelist)

    def replace(self, old, new):
        if self.left == old:
            self.left = new
        elif self.right == old:
            self.right = new
        else:
            raise RuntimeError("Replace gone wronge!")

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        #self.show()
#        if not isinstance(self.left, Constant) and\
#            not isinstance(self.left, ID):
        le = self.left.execute(scope)
        #log.info("le before lookup {} {} {} {}".format(le, self.op, self.right, type(le)))
        if isinstance(self.left, ID):
            le = _lookupID(self.left, scope)
            #print("le after lookup", type(le))
            #le = val.execute(scope)
#        else:
#            le = self.left

#        if not isinstance(self.right, Constant) and\
#            not isinstance(self.right, ID):
        ri = self.right.execute(scope)
        if isinstance(self.right, ID):
            ri = _lookupID(self.right, scope)
            #ri = val.execute(scope)
#        else:
#            ri = self.right

        #print("le",type(le), le, self.op, "ri", type(ri), ri)
        if self.op == BinaryOp.ADD:
            ret = le + ri
        elif self.op == BinaryOp.SUB:
            ret = le - ri
        elif self.op == BinaryOp.MUL:
            ret = le * ri
        elif self.op == BinaryOp.TRUEDIV:
            ret = le / ri
        elif self.op == BinaryOp.FLOORDIV:
            ret = le // ri
        elif self.op == BinaryOp.MOD:
            ret = le % ri
        elif self.op == BinaryOp.AND:
            # And cannot be overloaded in Constant class so i need to do this.
            #cle = le.value if isinstance(le, Constant) else le
            #cri = ri.value if isinstance(ri, Constant) else ri
            #ret = cle and cri
            if isinstance(le, Constant) and isinstance (ri, Constant):
                rettmp = le.value and ri.value
                ret = Constant(type(rettmp), rettmp)
            else:
                raise RuntimeError(f"{le} {type(le)} BinaryOp.AND {ri} {type(ri)} currently not allowed!")
            #
        elif self.op == BinaryOp.OR:
            # OR cannot be overloaded in Constant class so i need to do this.
            #cle = le.value if isinstance(le, Constant) else le
            #cri = ri.value if isinstance(ri, Constant) else ri
            #ret = cle or cri
            if isinstance(le, Constant) and isinstance (ri, Constant):
                rettmp = le.value or ri.value
                ret = Constant(type(rettmp), rettmp)
            else:
                raise RuntimeError(f"{le} {type(le)} BinaryOp.OR {ri} {type(ri)} currently not allowed!")
            #
        elif self.op == BinaryOp.EQ:
            ret = le == ri
        elif self.op == BinaryOp.NE:
            ret = le != ri
        elif self.op == BinaryOp.LE:
            ret = le <= ri
        elif self.op == BinaryOp.GE:
            ret = le >= ri
        elif self.op == BinaryOp.LT:
            ret = le < ri
        elif self.op == BinaryOp.GT:
            ret = le > ri
        else:
            raise NotImplementedError("Not yet {} !".format(self.op))
        #log.debug("BinaryOp {}".format(ret))
        return ret

    attr_names = ('op',)

class ListMaker(LocusNode):
    def __init__(self, values=None):
        super(ListMaker, self).__init__()
        self.values = values or []

    def __str__(self):
        return "["+", ".join(map(str,self.values))+"]"

    def __iter__(self):
        for v in self.values:
            yield v

    def __eq__(self, other):
        if not isinstance(other, ListMaker):
            return False
        if len(self) != len(other):
            return False

        for v, o in zip(self, other):
            if v != o:
                return False
        return True

    def __getitem__(self, i):
        #print("i: ",i," len: ",len(self.values))
        return self.values[i]

    def __setitem__(self, i, val):
        self.values[i] = val

    def __delitem__(self, i):
        del self.values[i]

    def __ne__(self, other):
        return not self == other

    def __len__(self):
        return len(self.values)

    def __add__(self, other):
        return ListMaker(self.values + other.values)

    def children(self):
        nodelist = []
        #if self.values is not None: nodelist.append(("values", self.values))
        for i, child in enumerate(self.values):
            nodelist.append(("values[{}]".format(i), child))
        return tuple(nodelist)

    def replace(self, old, new):
        oldindex = _myindexmethod(old, self.values)
        self.values[oldindex] = new

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        ret = []
        for v in self.values:
            exp = v.execute(scope)
            if isinstance(exp, ID):
                exp = _lookupID(exp, scope)

            #if isinstance(exp, Constant):
            #    exp = exp.value

            ret.append(exp)
        return ListMaker(ret)

    attr_names = ()

class TupleMaker(ListMaker):
    def __init__(self, values=None):
        super(TupleMaker, self).__init__()
        self.values = values or ()

    def __str__(self):
        return "("+", ".join(map(str,self.values))+")"

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + str(self))

    def execute(self, scope):
        return TupleMaker(tuple(ListMaker.execute(self, scope)))

class AssignStmt(LocusNode):

    EQUAL = "="

    def __init__(self, op=None, lvalue=None, rvalue=None):
        super(AssignStmt, self).__init__()
        self.op = op
        self.lvalue = lvalue
        self.rvalue = rvalue

    def __str__(self):
        ret = ""
        epilog = ""
        if isinstance(self.lvalue, list):
            ret += ",".join(map(str,self.lvalue))
        else:
            ret += epilog+str(self.lvalue)
        ret += self.op
        if isinstance(self.rvalue, list):
            ret += ",".join(map(str,self.rvalue))
        else:
            ret += epilog+str(self.rvalue)
        return ret

    def __iter__(self):
        if self.lvalue is not None:
            yield self.lvalue
        if self.rvalue is not None:
            yield self.rvalue

    def replace(self, old, new):
        self.rvalue = new

    def children(self):
        nodelist = []
        if self.lvalue is not None: nodelist.append(("lvalue", self.lvalue))
        if self.rvalue is not None: nodelist.append(("rvalue", self.rvalue))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        if isinstance(self.lvalue, list):
            self.lvalue[0].showCode(buf, offset=offset)
            for lv in self.lvalue[1:]:
                buf.write(', ')
                lv.showCode(buf, offset=0)
        else:
            if self.lvalue is not None:
                self.lvalue.showCode(buf, offset=offset)
            else:
                raise RuntimeError("Error! Assignment left value not valid.")
            #endif
        #endif

        #print "AssignStmt", self.lvalue
        buf.write(' ' + self.op + ' ')
        if isinstance(self.rvalue, list):
            self.rvalue[0].showCode(buf, offset=0)
            for rv in self.rvalue[1:]:
                buf.write(', ')
                rv.showCode(buf)
        else:
            if self.rvalue is not None:
                self.rvalue.showCode(buf)
            else:
                raise RuntimeError("Error! Assignment right value not valid.")
            #endif
        #endif
    #enddef

    def execute(self, scope):
        #log.debug("AssignStmt: {} - {} lvalue: {} rvalue: {}".format(self.lvalue,
        #            self.rvalue, type(self.lvalue), type(self.rvalue)))
        if isinstance(self.lvalue, list):
            left = []
            for lv in self.lvalue:
                left.append(lv.execute(scope))
#            left = tuple(left)
        else:
            left = self.lvalue.execute(scope)

        if isinstance(self.rvalue, list):
            right = []
            for rv in self.rvalue:
                right.append(rv.execute(scope))
#            right = tuple(right)
        else:
            try:
                right = self.rvalue.execute(scope)
            except:
                log.error(f"AssignStmt: {self.rvalue} {self.rvalue.show()}\n")
                raise


        if self.op != AssignStmt.EQUAL:
            raise NotImplementedError("Not implemented the {} op for "
                    "now".format(self.op))

        if (isinstance(left,(list, TupleMaker)) and\
                not isinstance(right, (list, TupleMaker))): # or\
        #    (not isinstance(left,list) and\
        #    isinstance(right, list)):
                raise RuntimeError("Assignment types incompatible! types: l {} r {}".format(type(left),type(right)))

        elif isinstance(left, (list, TupleMaker)) and\
                isinstance(right, (list, TupleMaker)):
            if len(left) != len(right):
                raise RuntimeError("Tuple's lenght in assignment incompatible! >>> {} = {} <<<".format(",".join(map(str,left)),
                    ",".join(map(str,right))))
            else:
                for l, r in zip(left, right):
                    #print ("assign tuple",l,r)
                    if isinstance(r, ID):
                        r = _lookupID(r, scope)

                    #scope[l.name] = r
                    _assignID(l, r, scope)
                #endif
            #endif
        else:
            if isinstance(right, ID):
                right = _lookupID(right, scope)

#            scope[left.name] = right
            #log.info(f"before _assignID: {left} = {right} scope {id(scope)}: {scope}")
            _assignID(left, right, scope)
        #endif
        #log.debug("AssignStmt End l: {} r: {}".format(left,right))

    attr_names = ('op',)

class ForStmt(LocusNode):

    def __init__(self, init=None, cond=None, _next=None, body=None):
        super(ForStmt, self).__init__()
        self.init = init
        self.cond = cond
        self.next = _next
        self.body = body

    def children(self):
        nodelist = []
        if self.init is not None: nodelist.append(("init", self.init))
        if self.cond is not None: nodelist.append(("cond", self.cond))
        if self.__next__ is not None: nodelist.append(("next", self.__next__))
        if self.body is not None: nodelist.append(("body", self.body))
        return tuple(nodelist)

    def __str__(self):
        ret = "for ("
        epilog = "\n\t{"
        prolog = "\n\t}"
        if self.init is not None:
            ret += " "+str(self.init)
        ret += ";"
        if self.cond is not None:
            ret += " "+str(self.cond)
        ret += ";"
        if self.__next__ is not None:
            ret += " "+str(self.next)
        ret += ") "
        if self.body is not None:
            ret += epilog+str(self.body)+prolog
        else:
            ret += "{}"
        return ret

    def __iter__(self):
        if self.init is not None:
            yield self.init
        if self.cond is not None:
            yield self.cond
        if self.next is not None:
            yield self.next
        if self.body is not None:
            yield self.body

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + 'for (')
        if self.init is not None:
            self.init.showCode(buf)
        buf.write('; ')
        if self.cond is not None:
            self.cond.showCode(buf)
        buf.write(';')
        if self.next is not None:
            self.next.showCode(buf)
        buf.write(')\n')
        if self.body is not None:
            self.body.showCode(buf, offset)

    def execute(self, scope):
        retinit = self.init.execute(scope)
        retcond = self.cond.execute(scope)

        if isinstance(retcond, ID):
            retid = _lookupID(retcond, scope)

            # True means integer > 0 or True
            if retid is not None and retid == Constant(bool, True):
                retcond = True
            else:
                retcond = False
        #endif isinstance ID

        #log.debug("ForStmt cond = {}".format(retcond))

#        print "retcond", retcond, type(retcond)
        # What about the break keyword?
        while retcond:
            self.body.execute(scope)
            retnext = self.next.execute(scope)
            retcond = self.cond.execute(scope)
            if isinstance(retcond, ID):
                retcond = _lookupID(retcond, scope)

            # True means integer > 0 or True
            if retcond is not None and retcond == Constant(bool, True):
                retcond = True
            else:
                retcond = False

#            print "retcond", retcond, type(retcond)
            #log.debug("cond = {}".format(retcond))
        #endif
    #enddef
    attr_names = ()

class IfStmt(LocusNode):
    def __init__(self, ifblks=None, elseblk=None):
        """ifblks is list of tuples (cond,block) and elseblk is the block for the else stmt.""" 
        super(IfStmt, self).__init__()
        self.ifblks = ifblks
        self.elseblk = elseblk

    def children(self):
        nodelist = []
        for i, (cond, blk) in enumerate(self.ifblks):
            nodelist.append(("cond[{}]".format(i), cond))
            nodelist.append(("ifblk[{}]".format(i), blk))
        if self.elseblk is not None:
            nodelist.append(("elseblk", self.elseblk))
        return tuple(nodelist)

    def __str__(self):
        ret = ""
        epilog=" {"
        prolog="\n\t} "
        ifepilog ="if"
        for cond,blk in self.ifblks:
            ret += ifepilog+" ("+str(cond)+")"
            ret += epilog+str(blk)+prolog
            ifepilog = "elif"
        if self.elseblk is not None:
            ret += " else "
            ret += epilog+str(self.elseblk)+prolog
        return ret

    def __iter__(self):
        for cond, blk in self.ifblks:
            yield cond
            yield blk
        if self.elseblk is not None:
            yield self.elseblk

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        cond,blk = self.ifblks[0]
        buf.write(lead + 'if (')
        cond.showCode(buf)
        buf.write(')\n')
        blk.showCode(buf, offset=offset)
        for cond,blk in self.ifblks[1:]:
            buf.write(lead + 'elif (')
            cond.showCode(buf)
            buf.write(')\n')
            blk.showCode(buf, offset=offset)
        #endfor
        if self.elseblk is not None:
            buf.write(lead + 'else\n')
            self.elseblk.showCode(buf, offset=offset)
        #endif
    #enddef

    def execute(self, scope):
        theblk = None
        for cond,blk in self.ifblks:
            #print "blk", blk
            ret = cond.execute(scope)

            if isinstance(ret, ID):
                retid = _lookupID(ret, scope)

                # True means integer > 0 or True
                if retid is not None and retid == Constant(bool, True):
                    ret = True
                    theblk = blk
                    break
                else:
                    ret = False
            elif isinstance(ret, Constant):
                if ret is not None and ret == Constant(bool, True):
                    ret = True
                    theblk = blk
                    break
                else:
                    ret = False
            elif ret:
                theblk = blk
                break
            #endif isinstance ID
            #print "ret", type(ret), ret, theblk
        #endfor

        if theblk is not None:
            #print "execuint theblk"
            ret = theblk.execute(scope)
        elif self.elseblk is not None:
            ret = self.elseblk.execute(scope)

        return ret

    attr_names = ()

class OrStmt(LocusNode):

    def __init__(self, stmts=None):
        super(OrStmt, self).__init__()
        self.stmts = stmts or []
        self.initv = 0

    def __str__(self):
        ret = ""#"OrStmt:"
        for child in self.stmts:
            ret += "\n" + str(child)
        return ret

    def __iter__(self):
        for child in self.stmts:
            yield child

    def __len__(self):
        return len(self.stmts)

    def __getitem__(self, index):
        return self.stmts[index]

    def replace(self, old, new):
        oldindex = _myindexmethod(old, self.stmts)
        self.stmts[oldindex] = new

    def children(self):
        nodelist = []
        #if self.stmts is not None: nodelist.append(("stmts", self.stmts))
        for i, child in enumerate(self.stmts):
            nodelist.append(("stmts[{}]".format(i), child))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        if self.stmts is not None:
            self.stmts[0].showCode(buf)
            for stm in self.stmts[1:]:
                buf.write(' OR ');
                stm.showCode(buf)

    def execute(self, scope):
        #raise RuntimeError("OrStmts cannot be executed!")
        pass

    attr_names = ()

class Block(LocusNode):

    def __init__(self, stmts=None, scope=None):
        super(Block, self).__init__()
        self.stmts = stmts or []
        self.scope = scope

    def __str__(self):
        ret = "" #"Block:"
        epilog="\n\t"
        prolog=""
        for child in self:
            prolog = ";"
            if isinstance(child, Block) or\
                isinstance(child, OrBlock) or\
                isinstance(child, IfStmt):
                prolog = ""
            #endif

            ret += epilog+str(child)+prolog
        #endfor

        return ret

    def __len__(self):
        return len(self.stmts)

    def __getitem__(self, index):
        return self.blocks[index]

    def __iter__(self):
        for child in self.stmts:
            yield child

    def append(self, new):
        self.stmts.append(new)

    def insertBefore(self, mark, new):
        markidx = _myindexmethod(mark, self.stmts)
        self.stmts.insert(markidx, new)

    def insertAfter(self, mark, new):
        markidx = _myindexmethod(mark, self.stmts)
        self.stmts.insert(markidx+1, new)

    def remove(self, elem):
        self.stmts.remove(elem)

    def replace(self, old, new):
        oldindex = _myindexmethod(old, self.stmts)
        self.stmts[oldindex] = new

    def children(self):
        nodelist = []
        for i, child in enumerate(self.stmts):
            nodelist.append(("stmts[{}]".format(i), child))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        #buf.write(lead + '{ '+str(self.id)+'\n' )
        buf.write(lead + '{\n' )
        self.showBlkStmts(buf, offset=offset + 2)
        buf.write(lead + '}\n')

    def showBlkStmts(self, buf=sys.stdout, offset=0):
        for st in self.stmts:
            st.showCode(buf, offset=offset)
            if not isinstance(st, Block) and \
                not isinstance(st, OrBlock) and\
                not isinstance(st, ForStmt) and\
                not isinstance(st, IfStmt):
                buf.write(';\n')
            #endif

    def execute(self, scope, inplace=False):
        """If inplace is false a new scope is created to permit recursion.
           All blocks have their own scope including if and for stmts.
           It points to the received scop as parent.
        """
        #log.debug("Block Start")
        ret = None
        if not inplace:
            oldscope = self.scope
            self.scope = self.scope.customCopy()
            self.scope.parent = scope

            #print ">Block par", id(scope), " par: ", id(scope.parent), scope #, "inst",type(inst)
            #print "       new", id(self.scope), " par: ", id(self.scope.parent), self.scope
            #print "       old", id(oldscope), " par: ", id(oldscope.parent), oldscope
        #endif

        for inst in self:
            #print "       inst ",type(inst)

            # Run on the block local scope.
            #ret = inst.execute(scope)
            ret = inst.execute(self.scope)
            # When a block find a return or receive a return 
            # it need to pass it up and stop executing.
            if isinstance(inst, Return):
                ret = (Return, ret)
                break
            elif type(ret) is tuple and ret[0] == Return:
                break
            #endif
        #endfor

        #log.debug("Block End")

        if not inplace:
            #print "<Block", id(scope), scope #, "inst",type(inst)
            #print "      ", id(self.scope), self.scope

            self.scope = oldscope
        #endif

        return ret
    #enddef

    attr_names = ()

class OrBlock(LocusNode):

    def __init__(self, blocks=None):
        super(OrBlock, self).__init__()
        self.blocks = blocks or []
        self.initv = 0

    def __str__(self):
        ret = "" #"OrBlock:"
        epilog="\t"
        prolog=""
        for child in self:
            ret += "\n"+epilog+"{"
            ret += epilog+str(child)+prolog
            ret += "\n"+epilog+"} OR "
#        ret += "\nEnd OrBlock"
        return ret

    def __iter__(self):
        for child in (self.blocks):
            yield child

    def __len__(self):
        return len(self.blocks)

    def __getitem__(self, index):
        return self.blocks[index]

    def remove(self, elem):
        self.blocks.remove(elem)

    def children(self):
        nodelist = []
        for i, child in enumerate(self.blocks):
            nodelist.append(("blocks[{}]".format(i), child))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        if self.blocks:
            self.blocks[0].showCode(buf, offset=offset)
            for midblk in self.blocks[1:]:
                buf.write(lead + 'OR\n')
                midblk.showCode(buf, offset=offset)
            #endfor
        else:
            buf.write(lead + '#Empty OR block\n')
        #endif
    #enddef

    def execute(self, scope):
        raise RuntimeError("OrBlocks cannot be executed!")

    attr_names = ()

class CodeRegDef(LocusNode):

    def __init__(self, _id=None, bl=None):
        super(CodeRegDef, self).__init__()
        self.id = _id
        self.body = bl
        #self.scope = bl.scope

    @property
    def scope(self):
        return self.body.scope

    @scope.setter
    def scope(self, v):
        self.body.scope = v

    def __str__(self):
        ret = str(self.id)
        if self.body is not None:
            ret += str(self.body)
        return ret

    def __iter__(self):
        if self.body is not None:
            yield self.body

    def children(self):
        nodelist = []
        if self.id is not None: nodelist.append(("id", self.id))
        if self.body is not None: nodelist.append(("", self.body))
        return tuple(nodelist)

    def execute(self, scope):
        self.body.execute(scope.parent)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + 'CodeReg ')
        self.id.showCode(buf)
        buf.write(lead + '\n')
        self.body.showCode(buf, offset=offset)

    @property
    def name(self):
        return self.id.name

    attr_names = ()

class OptSeqDef(LocusNode):

    def __init__(self, _id=None, bl=None, args=None, decor=None):
        super(OptSeqDef, self).__init__()
        self.id = _id
        self.args = args
        self.body = bl
        self.decor = decor
        #self.scope = bl.scope

    @property
    def scope(self):
        return self.body.scope

    @scope.setter
    def scope(self, v):
        self.body.scope = v

    def __str__(self):
        ret = ""
        if self.decor is not None:
            ret += str(self.decor)
        ret += str(self.id)
        if self.args is not None:
            ret += " " + str(self.args) 
        if self.body is not None:
            ret += str(self.body)
        return ret

    def __iter__(self):
        if self.args is not None:
            yield self.args
        if self.body is not None:
            yield self.body

    def children(self):
        nodelist = []
        if self.decor is not None: nodelist.append(("decor", self.decor))
        if self.id is not None: nodelist.append(("id", self.id))
        if self.args is not None: nodelist.append(("args", self.args))
        if self.body is not None: nodelist.append(("body", self.body))
        return tuple(nodelist)

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        if self.decor is not None:
            buf.write(lead + str(self.decor) + '\n')
        buf.write(lead + 'OptSeq ')
        self.id.showCode(buf)
        buf.write(' (')
        if self.args is not None:
            self.args.showCode(buf)
        buf.write(')\n')
        self.body.showCode(buf, offset=offset)

    def execute(self, scope):
        ret = None
        if self.body is not None:
            #print "OptSeq", id(scope), scope
            ret = self.body.execute(scope.parent)
            # When a block find a return or receive a return 
            # it need to pass it up and stop executing.
            if type(ret) is tuple:
                ret = ret[1]
            #endif
        #print "OptSeq ended", self.id, ret, type(ret)
        #log.debug("OptSeq %s ended. ret:  %s",self.id, ret)
        return ret

    @property
    def name(self):
        return self.id.name

    attr_names = ()

class Scope(object):

    def __init__(self, table=None, parent=None, functable=None):
        self.table = table or {}
        self.parent = parent
        self.node = None # this the node that this scope represents.
        self.functable = functable # contains list of CodeReg,OptSeq,def.

    def __getitem__(self, key):
        return self.table[key]

    def __setitem__(self, key, item):
        self.table[key] = item

    def __iter__(self):
        for var in self.table:
            yield var

    def __delitem__(self, key):
        del self.table[key]

    def __contains__(self, key):
        return key in self.table

    def __str__(self):
#        return str(self.table)
        ret = [str(key+": "+str(var)) if isinstance(var, Constant) else
                str(key+": "+str(type(var))) for key,var in self.items()]
        return ", ".join(ret)

    def items(self):
        return self.iteritems()

    def iteritems(self):
        for name, obj in self.table.items():
            yield name, obj

    def customCopy(self):
        """Duplicate only the table. Preserves the others."""
        ret = Scope()
        ret.table = copy.deepcopy(self.table)
        ret.node = self.node
        ret.functable = self.functable
        ret.parent = self.parent
        return ret

class SearchDef(LocusNode):

    def __init__(self, prebuildcmd=None, measureorig=False, buildcmd=None,
                    runcmd=None, runorig=None, buildorig=None, 
                    checkcmd=None, scope=None):
        self.scope = scope
        self.prebuildcmd = prebuildcmd
        self.measureorig = measureorig
        self.buildcmd = buildcmd
        self.runcmd = runcmd
        self.runorig = runorig
        self.buildorig = buildorig
        self.checkcmd = checkcmd

    def __str__(self):
        ret = ""
        epilog = " "
        if self.prebuildcmd is not None:
            ret += "prebuildcmd: "+str(self.prebuildcmd) + epilog
        ret += "measureorig: "+str(self.measureorig) + epilog
        if self.buildcmd is not None:
            ret += "buildcmd: " + str(self.buildcmd) + epilog
        if self.runcmd is not None:
            ret += "runcmd: " + str(self.runcmd) + epilog
        if self.runorig is not None:
            ret += "runorig: " + str(self.runorig) + epilog
        if self.buildorig is not None:
            ret += "buildorig: " + str(self.buildorig) + epilog
        if self.checkcmd is not None:
            ret += "checkcmd: " + str(self.checkcmd) + epilog
        return ret

    def showCode(self, buf=sys.stdout, offset=0):
        lead = ' ' * offset
        buf.write(lead + 'Search')
        buf.write(lead + '{\n')
        buf.write(lead + '  ' + "measureorig = "+str(self.measureorig) + ";\n")
        svars = [(self.prebuildcmd, "prebuildcmd" ), (self.buildcmd, "buildcmd" ), 
                (self.runcmd, "runcmd"), (self.runorig, "runorig"), 
                (self.buildorig, "buildorig"), (self.checkcmd, "checkcmd")]
        for var, desc in svars:
            if var is not None:
                buf.write(lead + '  ' + desc + " = " + str(var) + ";\n")
        ##
        buf.write(lead + '}\n')

    def execute(self, cmd):
        """After the execution it must return a string."""
        log.debug("Interpreting: {} type: {}".format(cmd, type(cmd)))
        ret = None
        if cmd is not None:
            tmp = cmd.execute(self.scope)
            if isinstance(tmp, ID):
                tmp = _lookupID(tmp, self.scope)
            #endif

            try:
                ret = tmp.value
            except AttributeError as e:
                log.error("cmd.execute has not return a Constant: {} type: {}".format(tmp,type(tmp)))
            #endif
        else:
            log.warning("  Cannot execute! cmd is None")
        #endif

        log.debug("  cmd ret: {} type: {}".format(ret,type(ret)))

        return ret
    #enddef

    attr_names = ()
#end class

def _myindexmethod(old, inplist):
    oldindex = None
    for ind, node in enumerate(inplist):
        if old.unqid == node.unqid:
            oldindex = ind
            break
    #endfor
    if oldindex is None:
        raise RuntimeError("Could not find the node to replace!")
    return oldindex
#end def
