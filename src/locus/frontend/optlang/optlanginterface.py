'''
@author: Thiago Teixeira
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:

import argparse
from abc import ABCMeta, abstractmethod

argparser = argparse.ArgumentParser(add_help=False)
argparser.add_argument('--optfile','-t', type=str,
                        required=True,
                       help="Specifies the optimization file.")

class AbstractOptLang(object, metaclass=ABCMeta):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def genOptUni(self):
        '''All subclass must convert their input to ICE's optimization space.'''
        pass
