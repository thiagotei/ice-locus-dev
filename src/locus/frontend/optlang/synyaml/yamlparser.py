'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:autoindent:

import yaml, string, logging
import sys, copy

from locus.optspace.parameters import Integer, PowerOfTwo, Parameter,\
Primitive, Boolean, Enum
from locus.optspace.optspaceinterface import OptSpace, OptUniverse,\
OptNodeCond, OptNode, Search, OptTool
from locus.frontend.optlang.optlanginterface import AbstractOptLang
from locus.util.misc import getDynClass, getOptClass, getToolClass

#  _                                    _
# |  matmul:                             | 
# |      <cond:>          ] OptNodeCond  |
# |			tool.opt:     -              | OptSpace
# |				paramA:    | OptNode     | 
# |				paramB:    |             |
# |                       _|             |
#  -                                    -
# This parser require each optspace gotta have at least one cond.

#argparser = argparse.ArgumentParser(add_help=False)
#argparser.add_argument('--optfile','-t', type=str,
#                        required=True,
#                       help="Specifies the optimization file.")

log = logging.getLogger("YamlParser")

class YamlParser(AbstractOptLang):
    """ Parser the optimization file in yaml format to optspace  """

    _keywords = ['compilers', 'prebuildcmd', 'buildcmd', 'runcmd', 'tuning', 'search']

    def __init__(self, optmization_file_name):
        #debug(" Init - {}".format(optmization_file_name), name="TuningParser", level=1)

        self.__keywords_map = {}
        self.__optmization_map  = None

#        print "^^&&& YamlParser "
        if optmization_file_name:
            try:
                optmization_file = open(optmization_file_name)
         #       info("Using optmization file: %s " % optmization_file_name) 
                self.__optmization_map = yaml.safe_load(optmization_file)

            except Exception as e:
                raise
         
        if self.__optmization_map:
            for k in YamlParser._keywords:
                if k in self.__optmization_map:
                    self.__keywords_map[k] = self.__optmization_map[k]
                    del self.__optmization_map[k]

    def genOptUni(self):
        """ Parse the optimization file and create the optspace data """
        if not self.__optmization_map:
            raise RuntimeError("No map on the Yaml")

        # The search has a field in optuni. So it is removed from map.
        if 'search' in self.__keywords_map: 
            # put search in a object
            se = Search(**self.__keywords_map['search'])
            del self.__keywords_map['search'] # it should not be considered as keyword. Keyword are global variables


#        print self.__optmization_map
        optuni = OptUniverse(self.__keywords_map, se)
        for kseq, vseq in self.__optmization_map.items():
#            if kseq in self.__keywords_map:
#                continue

            depth = 0

            if vseq is not None:
#                print 'vseq ', vseq
                # Level 0 (seq)
                optsp = OptSpace(kseq)
                optuni.addOptSpace(kseq, optsp)

                for opt in vseq:
#                    print 'opt ', opt
                    currkey = list(opt.keys())[0]
                    paramdict = opt[currkey]

                    # Loading the tool module (eg. RoseUiuc, Pips)$
                    try:
                        classname = str(currkey).split('.')[0]
                        toolclass = getToolClass('locus.tools.opts.', classname)
                        # Loading the nested class that represents the optimization.
                        optclass = getOptClass(toolclass, str(currkey).split('.')[1])
                    except Exception as e:
                        raise

                    paramtypes = optclass.getParams()
                    searchparamsspec = optclass.getSearchParams()
                    searchparamstype = optclass.getSearchTypes() # Types for the search

                    # Go through optimizations parameters.
                    for p, v in paramdict.items():
                        log.debug("{} {} - {} - {}".format(p, v,\
                            searchparamsspec,\
                            searchparamstype))
                        # Check if the fixed param type are correct. 
                        if p in paramtypes:
                            #if paramtypes[p] is list:
                            if type(v) != paramtypes[p]:
                                log.error("In {} optimization: {} value of" 
                                        " param: {}, {} is incorrect. Should "
                                        "be {} instead of {}.".format(kseq,currkey, p,v,
                                    paramtypes[p], type(v)))
                                raise

                        elif p in searchparamsspec:
                            # p (the parameters) must also be in
                            # searchparamstype.
                            assert p in searchparamstype

                            # If there is no type on the optuni assume the 1st on in the
                            # list inside 'type' key on transformation param specs.
                            try:
                                spt = searchparamstype[p]
                                if spt is list:
                                    thetype = spt[0]
                                else:
                                    thetype = spt
                            except Exception as e:
                                log.error("Search Parameter must have a search "
                                        "type in the tool definition. "
                                        "{}".format(e))
                                raise

                            # Get types of the search params correctly.
                            if 'type' in paramtypes and 'type' in paramdict:
                                if paramdict['type'].lower() == 'poweroftwo' and \
                                        PowerOfTwo in searchparamstype[p]:
                                    thetype = PowerOfTwo
                                elif paramdict['type'].lower() == 'integer' and \
                                        Integer in searchparamstype[p]:
                                    thetype = Integer
                                else:
                                    log.error("Could not find the correct type for the "
                                              "search param in opt {} param {} in "
                                              "{}! Received {} .".format(currkey,p,\
                                              kseq, paramdict['type'].lower()))
                                #endif
                            #endif

                            log.debug("param: {} searchtype: {}".format(p,\
                                thetype))

                            if thetype == Integer or thetype == PowerOfTwo:
                                # Parse min..max..step to opt structure
                                if isinstance(v, str) and v.find("..") > 0:
                                    # append to the dict and remove the previous
                                    (_min, _max, _step) = YamlParser.parseIntRangeMinMax(v)
                                    opt[currkey][p] = thetype(_min, _max, _step)
                                elif isinstance(v, int):
                                    # Assuming here that the parameter does not
                                    # need search since it is a single value.
                                    # Then the searchable parameters are not
                                    # used.
                                    opt[currkey][p] = v #thetype(v, v, 1)
                                #endif
                            elif thetype == Boolean:
                                opt[currkey][p] = thetype(v)
                            elif thetype == Enum:
                                if isinstance(v, str):
                                    # Assuming here that the parameter does not
                                    # need search since it is a single value.
                                    # Then the searchable parameters are not
                                    # used.
                                    opt[currkey][p] = v #thetype(v, v, 1)
                                elif type(v) is list:
                                    opt[currkey][p] = thetype(v)
                                else:
                                    log.error("this - {} - type - {}  - input should be a list or "
                                            "string.".format(v, type(v)))
                            #endif

                        #endif

                    #endfor paramdict
                    log.debug("After {}".format(opt[currkey]))
                    nd = OptNode(OptTool(currkey,opt[currkey], optclass))
                    optsp.addOptNode(nd)

                #endfor vseq
            #endif
        #endfor
        return optuni

    @staticmethod
    def parseIntRangeMinMax(inp):
        """ Parse parameters that contain start..end..step into three\
        different keys.\
        """
        #ret = {'min' : 0, 'max': 0, 'step': 1}
        ret = (0, 0, 1)
        if isinstance(inp,int): 
            #ret = {'min' : inp, 'max': inp, 'step': 1}
            ret = (inp, inp, 1)
        else:
            list_range = inp.split("..")
            if len(list_range) == 2:
                #list_values = range(int(list_range[0]),int(list_range[1])+1)
                #ret = {'min' : int(list_range[0]), 'max': int(list_range[1]), 'step': 1}
                ret = (int(list_range[0]), int(list_range[1]), 1)
            elif len(list_range) == 3:
                #ret = {'min' : int(list_range[0]), 'max': int(list_range[1]), 'step': int(list_range[2])}
                ret = (int(list_range[0]),int(list_range[1]),int(list_range[2]))

        log.debug("{}".format(ret))

        return ret 


    def parseWithCond(self):
        """ Parse the optimization file and create the optspace data """
        if not self.__optmization_map:
            raise RuntimeError("No map on the Yaml")

        print(self.__optmization_map)
        optuni = OptUniverse(self.__keywords_map)
        for kseq, vseq in self.__optmization_map.items():
            print("&&& Yaml parses")
            if kseq in self.__keywords_map:
                continue

            depth = 0

            if vseq is not None:
                print('vseq ', vseq)
                # Level 0 (seq)
                optsp = OptSpace(kseq)

                # Go through list of conditionals for the current sequence in the tuning map (variable vseq).
                for cond in vseq:
                    # Level 1 (cond)
                    depth = depth + 1

                    # Each element of the list of conditionals is dictionary that should have 
                    # only one element.
                    print('cond ',cond)
                    #kcond, vcond = cond.items()[0]
                    optcond = OptNodeCond(cond)
                    optsp.addOptNode(optcond)
                    vcond = vseq[cond]
                
                    for opt in vcond:
#                        print opt
                        currkey = list(opt.keys())[0]
                        nd = OptNode(OptTool(currkey,opt[currkey]))
                        optsp.addOptNode(nd)
                   # endfor vcond
                # endfor vseq
            #endif
        #endfor
        return optuni 
