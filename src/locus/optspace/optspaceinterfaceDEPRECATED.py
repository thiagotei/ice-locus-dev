'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging
from abc import ABCMeta, abstractmethod

from locus.tools.opts.opttoolinterface import AbstractOptToolOpt
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo, Boolean

log = logging.getLogger("OptSpaceInterface")

class OptTool:
    """ Optimization tool class """
    
    def __init__(self, name="Empty", params={}, classptr=AbstractOptToolOpt):
        self.name = name
        self.params = params
        self.classptr = classptr

    def __str__(self):
        return "OptTool: "+str(self.name)+" params: "+str(self.params)

class OptStmtAbstract(object, metaclass=ABCMeta):

    def __init__(self):
        pass

class OptStmt(OptStmtAbstract):

    def __init__(self, stmt=None, optional=False):
        self.stmt = stmt # can an opttool but also any expresion in, for now, Optseq
        self.optional = optional

class OptStmtOR(OptStmtAbstract):

    def __init__(self):
        self.stmts = []

class OptStmtCond(OptStmtAbstract):

    def __init__(self, true=None, false=None):
        self.cond = []
        self.trueblock = true
        self.falseblock = false

class OptStmtBlockAbstract(object, metaclass=ABCMeta):

    def __init__(self):
        pass

class OptStmtBlock(OptStmtBlockAbstract):

    def __init__(self):
        self.__optstmt = []

    def addOptStmt(self, stmt):
        if isinstance(stmt, OptStmtAbstract):
            self.__optstmt.append(stmt)
        else:
            raise TypeError("Expecting types of OptStmtAbstract.")

    def __getitem__(self, index):
        return self.__optstmt[index]

    def selectStmtfromOR(self, stmtor, indexor):
        try:
            indexstmtor = self.__optstmt.index(stmtor)
        except ValueError as e:
            log.error("Could find the block!")
            return False
        except Exception as e:
            log.error("Error looking for item {}.".format(stmtor))
            return False

        self.__optstmt[indexstmtor] = stmtor[indexor]
        return True

class OptStmtBlockOR(OptStmtBlockAbstract):
    """ Contains blocks involved in OR command """
    def __init__(self):
        self.__optblockstmt = []

    def addOptStmtBlock(self, block):
        if isinstance(block, OptStmtBlockAbstract):
            self.__optblockstmt.append(block)
        else:
            raise TypeError("Expecting types from OptStmtBlockAbstract.")

    def __getitem__(self, index):
        return self.__optblockstmt[index]

class OptSpace(object):
    """ Holds a sequence of block of optimizations  """

    def __init__(self, name):
        self.__optblockstmt = []
        self.name = name

    def addOptStmtBlock(self, block):
        if isinstance(block, OptStmtBlockAbstract):
            self.__optblockstmt.append(block)
        else:
            raise KeyError("This optspace exists already!")

    def selectBlockfromOR(self, block, indexor):
        try:
            indexorblock = self.__optblockstmt.index(block)
        except ValueError as e:
            log.error("Could find the block!")
            return False
        except Exception as e:
            log.error("Error looking for item {}.".format(child))
            return False

        self.__optblockstmt[indexorblock] = block[indexor]
        return True

    def __str__(self):
        ret = self.name + ":"
#        for t in self.traverse():
#            ret = ret + str(t)
        return ret 

    def __iter__(self):
        return self.__optblockstmt.__iter__()

    def traverse(self):
        return self.__iter__()

class OptUniverse:
    """ Holds a dictionary of OptSpaces. """ 

    def __init__(self, keywords={}, se=None):
        self.__keywords = keywords
        self.__optspaces = {}
        self.search = se

    def addOptSpace(self, key, opts):
        if isinstance(opts, OptSpace):
            if key in self.__optspaces:
                raise KeyError("This optspace exists already!")
            else:
                self.__optspaces[key] = opts

        else:
            raise TypeError 
    
    def getOptSpaceNames(self):
        return list(self.__optspaces.keys())

    def getKeyword(self, key):
        if key in self.__keywords:
            return self.__keywords[key]
        else:
            raise KeyError("{} does not exist in the space.".format(key))

    def getOptSpace(self, name):
        if name in self.__optspaces:
            return self.__optspaces[name] 
        else:
            raise KeyError("OptSpace {} does not exist in the space.".format(name))
    
    def __str__(self):
        ret = "#####\t"+"OptUniverse: \n"
        for space in self.__optspaces.values():
            ret = ret + str(space) + "\n"
            for opt in space.traverse():
                ret = ret + str(opt) + "\n"
        return ret 

#    def __getitem__(self, k):
#        return self.__tuning_seqs[k]

    def __iter__(self):
#        print "OptUniverse __iter__"
        #return iter(self.__optspaces.itervalues())
        return iter(self.__optspaces.values())


class Search():
    ''' Holds the search object info '''

    def __init__(self, prebuildcmd=None, measureorig=None, buildcmd=None, buildoptions=None, runcmd=None):
        self.prebuildcmd = prebuildcmd # Executed before the tuning starts.
        self.measureorig = measureorig
        self.buildcmd = buildcmd # Executed for each variant. (e.g. make realclean;  make CC={compiler} COPT={params})
        self.buildoptions = buildoptions # options such as compiler and flags are put here and keywords should match the buildcmd (e.g. {'icc': {'params': {'-O': {'default': 3, 'max': 3, 'min': 0}}}, 'gcc': {'params': {'-O': {'default': 3, 'max': 3, 'min': 0}}}}).
        self.runcmd = runcmd # Executed for each variant.

