'''
@author: Thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

class Parameter(object):
    """ Base class for parameter. They are used by the each optimization tool to define the space of search for each optmization. 
    The search tool interface has to have a map from this to their own space."""

    PRIMITIVE = 0
    COMPLEX = 1

class Primitive(Parameter):
    """ Base class for primitive parameter """
    def __init__(self, _min, _max, step):
        self.min = _min
        self.max = _max
        self.step = step

    def __str__(self):
        return "{}..{}..{}".format(self.min, self.max, self.step)

class Integer(Primitive):
    """ Range of integer values """

class Float(Primitive):
    """ Range of float values """

class ScaledNumeric(Primitive):
    """ Base class for special range of values """

class PowerOfTwo(ScaledNumeric):
    """ Integer range power of two"""

    @staticmethod
    def getReqInfo():
        return {'min': int, 'max': int}

class Complex(Parameter):
    """Base class for complex parameters """
    def __init__(self, info):
        self.info = info

class Boolean(Complex):
    """ Boolean parameter. """ 

class Switch(Complex):
    """ Switch parameter. """ 

class Enum(Complex):
    """ Enumarator """
    def __init__(self, options):
        self.options = list(options)

    def __str__(self):
        return str(self.options)

    def __getitem__(self, key):
        return self.options[key]

class Permutation(Complex):
    """ Ordered list of values """
