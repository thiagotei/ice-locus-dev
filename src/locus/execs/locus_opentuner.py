#!/usr/bin/env python

import logging

from locus.tools.search.opentunertool_locus import main as omain

log = logging.getLogger(__name__)

def main():
    omain()

if __name__ == '__main__':
    main()
#
