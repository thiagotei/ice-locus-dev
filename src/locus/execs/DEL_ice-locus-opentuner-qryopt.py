#!/usr/bin/env python

# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import argparse
import logging

from locus.ice import ICE, argparser as iceargparser, init_logging as iceinit_logging
from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser
from locus.tools.search.opentunertool_locus_queryopt import OpenTunerTool, \
                    argparser as tunerargparser

#log = logging.getLogger('Ice-Locus-Opentuner')
log = logging.getLogger(__name__)

argparser = argparse.ArgumentParser(add_help=True,
        parents=[iceargparser,
        optlangargparser,
        tunerargparser])

if __name__ == '__main__':
    args = argparser.parse_args() 
    iceinit_logging()
    if args.debug:
        logging.root.setLevel(logging.DEBUG)
        #log.setLevel(logging.DEBUG)
#    print "Main running"
    log.debug("Main running!")
    lp = LocusParser(args.optfile, args.debug)
    optuner = OpenTunerTool(args)
    ice_inst = ICE(lp, optuner, args)
    ice_inst.run()
