#!/usr/bin/env python

import argparse
import logging

from locus.ice import ICE, argparser as iceargparser, init_logging as iceinit_logging
from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser
from locus.tools.search.chocoopttool_locus import ChocoOptTool, argparser as chopargparser

log = logging.getLogger(__name__)

argparser = argparse.ArgumentParser(add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        parents=[iceargparser,
        optlangargparser,
        chopargparser])

def main():
    args = argparser.parse_args() 
    args.rundirpath = iceinit_logging()
    #print "debug",args.debug
    if args.debug:
        logging.root.setLevel(logging.DEBUG)
        #log.setLevel(logging.DEBUG)
#    print "Main running"
    log.debug("Main running!")
    lp = LocusParser(args.optfile, args.debug)
    optuner = ChocoOptTool(args)
    ice_inst = ICE(lp, optuner, args)
    ice_inst.run()

if __name__ == '__main__':
    main()
