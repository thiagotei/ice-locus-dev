#!/usr/bin/env python

# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import argparse
import logging

from locus.ice import ICE, argparser as iceargparser, init_logging as iceinit_logging
from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser

log = logging.getLogger('Ice-Locus')

argparser = argparse.ArgumentParser(add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        parents=[iceargparser,
        optlangargparser])

def main():
    args = argparser.parse_args() 
    args.rundirpath = iceinit_logging()
    if args.debug:
        logging.root.setLevel(logging.DEBUG)
        #log.setLevel(logging.DEBUG)
#    print "Main running"
    log.debug("Main running!")
    lp = LocusParser(args.optfile, args.debug)
    ice_inst = ICE(lp, None, args)
    ice_inst.run()


if __name__ == '__main__':
    main()
