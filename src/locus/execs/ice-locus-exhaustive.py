#!/usr/bin/env python

import logging

from locus.tools.search.exhaustivetool_locus import main

log = logging.getLogger(__name__)

if __name__ == '__main__':
    main()
#
