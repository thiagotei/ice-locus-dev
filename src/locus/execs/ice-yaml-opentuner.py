#!/usr/bin/env python

# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import argparse
import logging

from locus.ice import ICE, argparser as iceargparser, init_logging as iceinit_logging
from locus.frontend.optlang.synyaml.yamlparser import YamlParser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser
from locus.tools.search.opentunertool_yaml import OpenTunerTool, \
                    argparser as tunerargparser

log = logging.getLogger('Ice-Yaml-Opentuner')

argparser = argparse.ArgumentParser(add_help=True,
        parents=[iceargparser,
        optlangargparser,
        tunerargparser])

if __name__ == '__main__':
    args = argparser.parse_args() 
    iceinit_logging()
    if args.debug:
        logging.root.setLevel(logging.DEBUG)
#    print "Main running"
    log.info("Main running!")
    yl = YamlParser(args.optfile) 
    optuner = OpenTunerTool(args)
    ice_inst = ICE(yl, optuner, args)
    ice_inst.run()
