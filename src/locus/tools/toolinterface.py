# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging
from abc import ABCMeta, abstractmethod

log = logging.getLogger("AbstractTool")

class AbstractTool(object, metaclass=ABCMeta):
    """ This is a superclass to all tools for static
        transformations
    """

    def __init__(self, name):
#        log.debug("Init Tools class")
        self.toolname = name
