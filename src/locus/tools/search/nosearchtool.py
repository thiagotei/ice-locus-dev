'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.optspace.parameters import Integer, PowerOfTwo, Boolean, Enum

log = logging.getLogger("NoSearchTool")

class NoSearchTool(object):
    """ This choose one sequence for each optimization space on the optimization
    universe. """

    def __init__(self):
        pass

    @staticmethod
    def getSingleSequenceDEPRECATED(optuni):
        newoptuni = copy.deepcopy(optuni)

        log.info("New optuni")
        for space in newoptuni:
#            space.name = "Teste123"+space.name
#            log.debug(space)
            for opt in space.traverse():
                for p, v in opt.val.params.items():
#                    log.debug("{}: {} && {}".format(opt.val.name,p,v))
                    if type(v) == PowerOfTwo:
                        opt.val.params[p] = v.min
                    elif type(v) == Integer:
                        opt.val.params[p] = v.min
                    elif type(v) == Boolean:
                        opt.val.params[p] = v.info
                    elif type(v) == Enum:
                        opt.val.params[p] = v[0]
#                log.debug(opt)


        log.info("end")
        return newoptuni





