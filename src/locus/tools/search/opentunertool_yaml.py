'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys
import argparse, copy
#import adddeps
import opentuner

from opentuner.measurement import MeasurementInterface
from opentuner.search.manipulator import ConfigurationManipulator,\
BooleanParameter, PowerOfTwoParameter, EnumParameter
from opentuner import IntegerParameter, PermutationParameter
from opentuner import Result

from locus.backend.codegen import CodeGen
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo, Boolean, Enum
from locus.tools.toolinterface import AbstractTool
from locus.tools.opts.opttoolinterface import AbstractOptTool
from locus.util.misc import getDynClass, getOptClass, getToolClass,\
unwindBackup, getTimingFunc
from .searchtoolinterface import AbstractSearchTool
from locus.util.constants import ICE_OUTPUT_MODE_STDOUT,\
ICE_OUTPUT_MODE_INPLACE, ICE_OUTPUT_MODE_SUFFIX

argparser = argparse.ArgumentParser(add_help=False)

logice = logging.getLogger("OpenTunerTool")

class ToolSpec():
    """ It is an class to define information that is attached to the optimizations space.\
    It receives the opt space name, the tool and opt classes used and other info. """

    OPTTOOL = 0
    NOTOPTTOOL = 1

    def __init__(self, optspacename, toolclass, optclass, ind, paramname, extra, _type=OPTTOOL):
        self.optspacename = optspacename # code region optimization space name
        self.toolclass = toolclass # ptr to the tool class
        self.optclass = optclass # ptr to optimization class
        self.ind = ind # index of the optimization in the optimzation space.
        self.paramname = paramname # name of the parameter being optimized.
        self.type = _type # OPTTOOL vs NOTOPTTOOL (22Feb2018) I dont what this means.
        self.extra = extra # the other parameters on the optimization that are not searchable, maybe useless.

    #This is used by the Opentuner to hash the parameter objects and store in database. they gotta be unique.
    def __repr__(self):
        return "{}_{}_{}_{}_{}_{}_{}".format(self.optspacename, self.toolclass,\
                self.optclass, self.ind, self.paramname, self.extra, self.type)
        #return str((str(self.optspacename), str(self.toolclass), str(self.optclass), self.ind, self.extra, self.type))

class OpenTunerTool(AbstractSearchTool, MeasurementInterface):
    """ Opentuner driver to search optimization space """

    def __init__(self, iceargs): 
        self.iceargs = iceargs
        self.parsedsrcfiles = None
        self.optuni = None

        AbstractSearchTool.__init__(self, "OpenTuner")

        logice.debug("OpenTunerTool init")

        argparser_opentu = opentuner.default_argparser()
        self.otargs = argparser_opentu.parse_args(
                ['--no-dups','--test-limit',str(self.iceargs.ntests),'--parallelism','1'])
#                '--results-log-details','./opentuner-log-details-'+self.iceargs.suffixinfo])

        MeasurementInterface.__init__(self, self.otargs)

        # OpenTuner was returning weird error messages when passing the 
        # ToolSpec object. So I pass the a string in place save the ToolSpec 
        # obj in this dict.
        self.otparamdict = {}

    def convertOptUni(self, optuni):
        logice.debug("start")
        self.optuni = optuni
        self.__manipulator = ConfigurationManipulator()

        #
        # Going through the optimization space in optimization universe.
        #
        for space in self.optuni:
            ind_opt = 0
            for opt in space.traverse():
                logice.debug("opt %s",opt.val.name)

                # Getting the parameters about the search and the optimization
                # to give to OpenTuner.
                optclass = opt.val.classptr # get the optimization class
                optname = optclass.getOptName()
                searchparamsspec = optclass.getSearchParams() # Types of parameters required by the tool
                searchparamstype = optclass.getSearchTypes() # Types for the search
                toolname = optclass.getToolName()

                # Going through the search params
                for p, v in opt.val.params.items():
                    logice.debug("sp {} = {} type {}".format(p,v,type(v)))
                    # Check if there is any search params
                    #TODO check if the attribute is a star, which means that the instruction 
                    # So far star is not used with opentuner
#                    if not searchparamsspec:
#                        self.__manipulator
#                    else:
                    if p in searchparamsspec:
                        obj = ToolSpec(space.name, toolname, optname, ind_opt,\
                                p, None, ToolSpec.OPTTOOL)
                        objrepr = repr(obj)
                        self.otparamdict[objrepr] = obj

                        if type(v) == PowerOfTwo:
                            self.__manipulator.add_parameter(PowerOfTwoParameter(objrepr,v.min, v.max))

                        elif type(v) == Integer:
                            self.__manipulator.add_parameter(IntegerParameter(objrepr, v.min, v.max))

                        elif type(v) == Boolean:
                            # v.info are some parameters to perform the optimization.
                            self.__manipulator.add_parameter(BooleanParameter(objrepr))

                        elif type(v) == Enum:
                            self.__manipulator.add_parameter(EnumParameter(objrepr, v.options))

                        elif type(v) == str or type(v) == int:
                            # Here the searchable parameter have only option,
                            # which means they dont need to be searchable.
                            # add them to the paramtup so they will be using
                            # when optimizing tool is called.
                            logice.warning("searchable param - {} = {} - do not require "
                                    "search. ".format(p,v))
                        else:
                            logice.error("Could not find the correct type for the "
                                "param - {} - in opt {} in "
                                "{}!".format(opt.val.name, space.name))
                        #endif
                    #endif searchparamsspec
                    #endif
                #endfor

                ind_opt = ind_opt + 1
            #endfor opt
        #endfor space

        if self.optuni.search.buildoptions and 'params' in self.optuni.search.buildoptions:
            params_dict = self.optuni.search.buildoptions['params']
            # Iterate over the params
            for key, val in params_dict.items(): 
                self.__manipulator.add_parameter(IntegerParameter(key,val['min'],val['max']))

        # Check if there is anything to search!
        if not self.__manipulator.params:
            return False
        else:
            logice.info("Search space size: {}".format(self.__manipulator.search_space_size()))
            return True

    def manipulator(self):
        return self.__manipulator

    def convertDesiredResult(self, cfg):
        # The otparamdict has the ToolSpec as values in dict.
        # The cfg is the result of the search only for the optimizations parameters that are
        # searchable.
        # The goal here is to get the results of the search and put it in a new optimization universe.

        # TODO so far *+ on optimizations are NOT supported, which means all the optimizations are executed.
        # Here we will to differentiate a False from opt param from if the opt was chosen in the cfg to execute or not.

        # Did not work! Opentuner messes up with logger, so I put it back in place.
#        if self.iceargs.debug and not logice.isEnabledFor(logging.DEBUG):
#            logging.root.setLevel(logging.DEBUG)
#            print "set level *** DEBUG"

        cfgoptuni = copy.deepcopy(self.optuni)

        sortedcfg = {}
        # Put cfg in a dict ordered by space and position
        for key, val in cfg.items():
            obj = self.otparamdict[key]
            if obj.optspacename not in sortedcfg:
                sortedcfg[obj.optspacename] = {}
            if obj.ind not in sortedcfg[obj.optspacename]:
                sortedcfg[obj.optspacename][obj.ind] = {}
            sortedcfg[obj.optspacename][obj.ind][obj.paramname] = (obj,val)

        logice.debug("sortedcfg: {}".format(sortedcfg))

        for space in cfgoptuni:
            ind_opt = 0
            for opt in space.traverse():
                logice.debug("opt %s",opt.val.name)

                # Check if it has been in the search.
                if space.name in sortedcfg and ind_opt in sortedcfg[space.name]:

                    # Go through all parameters for the same optimization that
                    # were searched.
                    for p in sortedcfg[space.name][ind_opt]:
                        logice.debug("updating a parameter - {} - ".format(p))
                        obj = sortedcfg[space.name][ind_opt][p][0]
                        val = sortedcfg[space.name][ind_opt][p][1]

                        if obj.paramname in opt.val.params:
                            opt.val.params[obj.paramname] = val
                        else:
                            raise ValueError("Could not find the parameter: {} opt: {} tool: {} space: {}".format(obj.paramname, opt.val.name, "X", space.name))

                ind_opt += 1

        return cfgoptuni

    def search(self, parsedsrcfiles):
        from opentuner.tuningrunmain import TuningRunMain
        self.parsedsrcfiles = parsedsrcfiles

        if self.optuni.search.prebuildcmd and self.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            prebuildres = self.call_program(self.optuni.search.prebuildcmd)
            assert prebuildres['returncode'] == 0

        oj = TuningRunMain(self, self.otargs)
        oh = oj.main()
        return oj

    def save_final_config(self, configuration):
        logice.info("Final config: {}".format(configuration.data))

    def compile_and_run(self, desired_result, input, limit):
        logice.info("start")

        cfg = desired_result.configuration.data
        cfg_id = desired_result.configuration_id

        #print "input ", input
        logice.debug("cfg_id: {}\n{}".format(cfg_id, cfg))
        try:
            cfgoptuni = self.convertDesiredResult(cfg)

            logice.debug("cfgoptuni:\n{}".format(cfgoptuni))

            undoList = CodeGen.genCode(self.parsedsrcfiles, cfgoptuni,\
                self.iceargs, cfg_id)
        except Exception as e:
            # Code generation failed, go to next!
            logice.exception(e)
            # Fixed What to return to OpenTuner when experiment fail?
            return Result(state='ERROR',time=float('inf'))

        #TODO define this properly with compiler selection and different types of flags
        tmpbuildcmd = self.optuni.search.buildcmd

        if self.optuni.search.buildcmd and \
            self.iceargs.output != ICE_OUTPUT_MODE_STDOUT:

# TODO  Needs to find the compiler first. Either allow one single compiler, or do one search for each compiler, or 
# make opentuner select a compiler.
#            if self.optuni.search.buildoptions and \
#                'params' in self.optuni.search.buildoptions:
#
 #               params_dict = self.optuni.search.buildoptions['params']
 #               for key in params_dict:
 #                   # Check if the param is also part of the cfg.
 #                   if key in cfg:
 #                       paramsbuildcmd += " "+key+str(cfg[key])
                    #endif
                #end for
            #endif

#            if self.optuni.search.compiler and self.optuni.search.buildoptions:
#                tmpbuildcmd = tmpbuildcmd.format(compiler=self.optuni.search.compiler,params="\""+paramsbuildcmd+"\"")
#            elif self.optuni.search.compiler:
#                tmpbuildcmd = tmpbuildcmd.format(compiler=self.optuni.search.compiler)
#            elif self.optuni.search.buildoptions:
#                tmpbuildcmd = tmpbuildcmd.format(params=paramsbuildcmd)

            logice.debug("buildcmd: {}".format(tmpbuildcmd))
            build_result = self.call_program(tmpbuildcmd)
            if build_result['returncode']:
                logice.debug("stdout: {}\nstderr: "
                        "{}".format(build_result['stdout'],build_result['stderr']))
            assert build_result['returncode'] == 0
        #endif

        if self.optuni.search.runcmd and self.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            runresult = self.call_program(self.optuni.search.runcmd)
            if runresult['returncode']:
                logice.debug("stdout: {}\nstderr: "
                        "{}".format(runresult['stdout'],runresult['stderr']))
            assert runresult['returncode'] == 0
        else:
            logice.warning("no run cmd!")

        # Undo the if changes are in place for next tests.
        if self.iceargs.output == ICE_OUTPUT_MODE_INPLACE:
            logice.debug("undolist after each run: {}".format(undoList))
            for ud in undoList:
                unwindBackup(ud[1], ud[0])

        # External function parse metric
        timingfunc = getTimingFunc(self.iceargs.tfunc)
        if timingfunc:
            metric = timingfunc(runresult['stdout'], runresult['stderr'])
            logice.debug("Using external function to get kernel timing: {}".format(timingfunc))
            logice.debug("Output:\n {} \nErr:\n {}".format(runresult['stdout'], runresult['stderr']))
        else:
            metric = run_result['time']

        logice.info("Run cfg_id: {} time: {} cc: {} cfg: {}".format(cfg_id, metric, "X", cfg))
        return Result(time=metric)

    @staticmethod
    def getMinMax(node_params):
        try:
            _min = node_params['min']
            _max = node_params['max']
        except KeyError as e:
            logice.error("The frontend should have put {} as optimization parameter.".format(e))
            raise
        except Exception as e:
            raise
        #endtry
        return (_min, _max)

    @staticmethod
    def getParamTup(node_params, paramspec):
        try:
            paramtup = ()
            # get the non-search params ordered in a tuple as
            # info to carry out transformations.
            for p in sorted(paramspec.keys()):
                if p in node_params:
                    paramtup = paramtup + (node_params[p],)
        except KeyError as e:
            logice.error("The {} should be one of the parameter on the optimization.".format(e))
            raise
        except Exception as e:
            raise
        #endtry
        return paramtup

