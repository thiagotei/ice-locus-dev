'''
@author: thiago
'''

import logging, sys, os, time, math, copy, random
from datetime import datetime

import locus.frontend.optlang.locus.locuscfg as lcfg
import locus.frontend.optlang.locus.dataflowopts as dfw
from locus.frontend.optlang.locus.locusvisitor import GenericVisitorWScope, \
        GenericVisitorUpdateId, GenericVisitorCond, NodePathVisitorCond
from locus.frontend.optlang.locus.locusast import LocusNode, OrStmt, OptionalStmt, IfStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, ListMaker, FuncCall, processCallArgList, Block,\
    Scope, Argument, OptSeqDef, CodeRegDef
from locus.backend.genlocus import GenLocus
#from locus.frontend.optlang.locus.optimizer import findScopeIdNew

log = logging.getLogger(__name__)

def createOPTRecursionTree(optuni, recoptseqs, iceargs):
    log.info("Start")

    funccalls = [] # all funccalls excepts the ones that represent recursion.

    # find all func calls that are not recursive
    for ent in optuni.entities():
        #senodes = IfStmt, FuncCall 
        senodes = FuncCall 
        visitor = GenericVisitorWScope(senodes)
        visitor.visit(ent)
        # Remove the recursion calls
        nonselfcalls = [(ent, fcpar, fcnode, fcscop) for fcpar, fcnode, fcscop in visitor.values 
                if ent.id.name != fcnode.name.name and isinstance(ent, CodeRegDef)]
                #if ent.id.name != fcnode.name.name]
                #if isinstance(fcnode, FuncCall) and ent.id.name != fcnode.name.name]
        funccalls.extend(nonselfcalls)
    #endfor

    #optuni.topblk.execute(optuni.topblk.scope, inplace=True)

    log.info(f"Approximate recursion expansion by {iceargs.approxexprec}%.")

    #
    # optimize the topblk before anything else
    #
    dfw.combinedCfgAstOpts(optuni.topblk)

    # get reachdef of exit node
    topentry = lcfg.LocusCFG.ast2cfg(optuni.topblk)
    dfw.ReachingDefs.exec(topentry)
    bbdict = lcfg.LocusCFG.dictfycfg(topentry)
    exitnd = None
    for nd in bbdict.values():
        if nd.isexit():
            exitnd = nd
            break
    ##

    topblkOUT = set()
    if nd is not None:
        #print(len(exitnd.stmtsIN))
        log.debug("Exit nd OUT: "+"; ".join([str(a) for a in nd.OUT]))
        topblkOUT = nd.OUT
    #print("Exit nd IN: ", exitnd.stmtsIN[0]) 

    # saves entities were expanded because they called an recursive optseq
    entsexpanded = []

    # saves recursive optset that were actually expanded
    optseqsexpanded = set()

    # loop over recursive optseqs
    for ts in recoptseqs:
        #print ts.args
        optseqname = ts.id.name

        interbody = copy.deepcopy(ts.body) 

        for callerent, fcpar, fcnode, fcscop in funccalls:
            fcallname = fcnode.name.name
            # Is this fuction on the recursion list?
            if fcallname == optseqname:
                #ts.show()
                log.info("expanding {} at {} args: {}".format(optseqname, ent.id.name, fcnode.arglist))
                exp_start = time.time()
                #fcpar.show()

                entsexpanded.append(callerent)
                optseqsexpanded.add(ts)

                # get params for the 1st call of the recursive OptSeq in the CodeReg.
                # Needs to analyze it first.
                mainentry = lcfg.LocusCFG.ast2cfg(fcpar)  

                #lcfg.LocusCFG.drawcfg(mainentry, iceargs.rundirpath+"/befrecdef"+"_exprec") 

                dfw.ReachingDefs.exec(mainentry, entryout=topblkOUT)

                # Find on graph the call and the reaching definitions of the calls
                cfgcallstmts = lcfg.LocusCFG.findStmts(mainentry, FuncCall, 
                        lambda s: True if s == fcnode else False)

                # looking for the parent call
                # add the AST parent of funccalls to the structure
                callstmts = []
                for bbid, stmt, IN in cfgcallstmts:
                    if stmt == fcnode:
                        callstmts.append((bbid, stmt, IN, fcpar))
                    else:
                        raise RuntimeError("Could not find node to add parent.")
                    #
                ##

                #interbody.show()

                it = 0
                tltcallsit = 0
                tltskippedcalls = 0

                #lcfg.LocusCFG.drawcfg(mainentry, iceargs.rundirpath+"/"+str(it)+"_exprec") 

                anycalls = True if len(callstmts) > 0 else False

                while anycalls:
                    it += 1

                    log.info(f"it {it} ...")

                    callit = 0
                    skipedcalls = 0
                    nextcallstmts = []

                    log.debug(f"sample k: {math.ceil(len(callstmts)*(iceargs.approxexprec/100))}")
                    samplecalls = random.sample(callstmts,
                            math.ceil(len(callstmts)*(iceargs.approxexprec/100)))

                    for cst in callstmts:
                        bbid, stmt, IN, parstmt = cst

                        # Approximate recursion expansion by choosing to run it
                        # or not.
                        #if callit > 0 and random.randint(1,100) > iceargs.approxexprec:
                        if cst not in samplecalls:
                            # skipe the first call 
                            newstmts = [FuncCall(ID("RecursionStopped")),
                                        AssignStmt(AssignStmt.EQUAL,
                                                ID("APPROX"),
                                                Constant(bool, True))]
                            tmpnextbody = Block(newstmts)
                            skipedcalls += 1

                        else:
                            tmpnextbody = copy.deepcopy(interbody) 

                            # add actual arguments to the beginning of the body
                            for i, farg in enumerate(ts.args.args): 
                                if isinstance(farg, Argument):
                                    # i'd say that all the formal arguments are only id
                                    lhs = copy.deepcopy(farg.id)
                                    #print("lhs:")
                                    #lhs.show()
                                else:
                                    raise RuntimeError("Expecting Formal as Argument type, received ",type(farg))
                                #

                                actarg = stmt.arglist[i]
                                if isinstance(actarg, Argument):
                                    if actarg.id is not None and \
                                            actarg.expr is not None:
                                        if actarg.id.name != lhs.name:
                                            raise RuntimeError(f"Caught on order of parameters! ARG RHS: {actarg.id.name} != {lhs.name} ?")
                                        #

                                        rhs = copy.deepcopy(actarg.expr)  

                                    elif actarg.expr is not None:
                                        rhs = copy.deepcopy(actarg.expr)  

                                    elif actarg.id is not None: 
                                        rhs = copy.deepcopy(actarg.id)  
                                    #

                                else:
                                    raise RuntimeError("Expecting Actual as Argument type, received ",type(actarg))
                                #
                                #print("rhs:")
                                #rhs.show()
                                argstmt = AssignStmt(AssignStmt.EQUAL, lhs, rhs)
                                tmpnextbody.stmts.insert(0, argstmt)
                            ##
                            #print(tmpnextbody)

                            #tmpentry = lcfg.LocusCFG.ast2cfg(tmpnextbody)

                            #lcfg.LocusCFG.drawcfg(tmpentry, iceargs.rundirpath+"/"+str(it)+"_bef_exprec") 

                            # Optimize and prune the code
                            props, folds, deaths = dfw.combinedCfgAstOpts(tmpnextbody, entryout=topblkOUT)

                            if callit % 100 == 0:
                                log.info("it: "+str(it)+" call: "+str(callit)+" props: "+str(props)+" folds: "+str(folds)+" deaths: "+str(deaths))

                            # Recreate the cfg again because the ast might have changed
                            # and structural changes only in AST, to reflect in the CFG
                            # needs to create it again
                            tmpentry = lcfg.LocusCFG.ast2cfg(tmpnextbody)
                            dfw.ReachingDefs.exec(tmpentry, entryout=topblkOUT)

                            #lcfg.LocusCFG.drawcfg(tmpentry, iceargs.rundirpath+"/"+str(it)+"_exprec") 

                            # find recursive calls that are still in the code
                            cfgtmpnextcalls = lcfg.LocusCFG.findStmts(tmpentry, FuncCall, 
                                lambda s: True if s.name.name == optseqname else False)
                            #nextcallstmts.append(tmpnextcalls)
                            #log.info(f'cfgtmpnextcalls: {cfgtmpnextcalls}')

                            # find the node in the AST along with parent.
                            # replace the call with created body
                            vis = GenericVisitorCond(FuncCall,
                                    #cond=lambda s: True)
                                cond=lambda s: True if s.name.name == optseqname else False)
                                    #cond=lambda s: True if s == stmt else False)
                            vis.visit(tmpnextbody)

                            # looking for the parent call
                            # add the AST parent of funccalls to the structure
                            for nextbbid, nextstmt, nextIN in cfgtmpnextcalls:
                                for otherpar, otherstmt in vis.values: 
                                    if nextstmt == otherstmt:
                                        nextcallstmts.append((nextbbid, nextstmt, nextIN, otherpar))
                                        break
                                    #
                                ##
                            ##
                        #

                        parstmt.replace(stmt, tmpnextbody)
                        callit += 1
                    ##
                    callstmts = nextcallstmts
                    anycalls = True if len(callstmts) > 0 else False
                    tltcallsit += callit
                    tltskippedcalls += skipedcalls

                    log.info(f"it {it} done. Worked on {callit - skipedcalls}/{callit} calls.")
                ##
                exp_end = time.time()
                log.info("Expansion of {} at {} finished. "
                        "{}/{} total calls. ({:.2f} sec)".format(
                        ent.id.name, optseqname,
                        tltcallsit-tltskippedcalls,
                        tltcallsit,
                        exp_end - exp_start))
            #
        ## for funccalls
    ##

    # After so much change reset the node unqid. 
    # It matters for the optimizations ahead.
    LocusNode.resetidcounter()
    splst = [(n, s) for n, s in optuni.allspaces()]
    for name, sp in sorted(splst, key=lambda tup: tup[0]):
        visitor = GenericVisitorUpdateId()
        visitor.visit(sp)
        log.info("Reset nodes id: {} id: {}".format(name, sp.unqid))
    #endfor

    if iceargs.debug or iceargs.saveexprec:
        exprecfile = iceargs.rundirpath+"/"+"bef_expandedrec.locus"
        log.debug("Generating expanded recursion before in {} ...".format(exprecfile))
        GenLocus.searchmode(optuni, exprecfile)
        log.info("Generated expanded recursion before in {} .".format(exprecfile))
    #endif

    # Remove all the OR Blocks that has RecursionStopped FuncCall.
    # Only on expanded recursive calls
    # This is a way to prune the way for useless cases 
    for name, sp in sorted(splst, key=lambda tup: tup[0]):
        if sp in entsexpanded:
            prune_start = time.time()

            tltorbklrem = 0
            tltblkrem = 0
            keepcleaning = True

            # This is to avoid trying to remove the same block
            # more than once.
            blksremoved = {}

            while keepcleaning:
                orbklrem = 0
                blkrem = 0

                visitor = NodePathVisitorCond(cond=lambda s: 
                        True if isinstance(s, FuncCall) and s.name.name == "RecursionStopped" else False)
                visitor.visit(sp)

                #
                # goes through all the funccalls called RecursionStopped
                #
                for node, path in reversed(visitor.allpathnodes):
                    parorblknd = None
                    orblknd = None
                    blknd = None
                    #log.info(f"{node.unqid} {type(node)}: {path}")

                    # find the immediate orblock parent
                    for i, (path_par, path_nd, _) in enumerate(reversed(path)):
                        #log.info(f"{path_par.unqid} {type(path_par)} | {path_nd.unqid} {type(path_nd)}")
                        if isinstance(path_nd, OrBlock):
                            parorblknd = path_par
                            orblknd = path_nd
                            break
                        elif isinstance(path_nd, Block):
                            blknd = path_nd
                        #
                    ##

                    # remove the block from the Orblock
                    if parorblknd is not None and \
                            orblknd is not None and \
                            blknd is not None:
                        # check whether the block has been removed already
                        if blknd.unqid not in blksremoved:
                            orblknd.remove(blknd)
                            blksremoved[blknd.unqid] = True
                            log.debug(f"removed 1 block {blknd.unqid} from {orblknd.unqid}")
                            blkrem += 1
                        else:
                            log.debug(f"block {blknd.unqid} from {orblknd.unqid} "
                                      f"was ALREADY removed.")
                        #

                    else:
                        log.warning("nothing remove. Weird.")
                    #
                ##

                #
                # Look for and remove empty orblocks
                #
                vis2 = GenericVisitorCond(OrBlock,
                    cond=lambda s: True if len(s) <= 1 else False)
                vis2.visit(sp)

                for par, nd in vis2.values:
                    # if len is 0 removed the whole orblock.
                    # if len is 1 replace the orblock by its single child.
                    if len(nd) == 0:
                        #par.remove(nd)
                        par.replace(nd, FuncCall(ID("RecursionStopped")))
                        orbklrem += 1
                    elif len(nd) == 1:
                        par.replace(nd, nd[0])
                        orbklrem += 1
                    else:
                        log.warning("It was not supposed to reach here!")
                    #
                ##
                #sp.show()

                tltorbklrem += orbklrem
                tltblkrem += blkrem

                log.info(f"Pruned blocks: {blkrem} orblks: {orbklrem} "
                        f"id: {name} calls found: {len(visitor.allpathnodes)}")
                if blkrem == 0 and orbklrem == 0: 
                    keepcleaning = False
                #break
            ##
            prune_end = time.time()
            log.info(f"Final Pruning blocks: {tltblkrem} orblks: {tltorbklrem} "
                     f"id: {name} calls found: {len(visitor.allpathnodes)} "
                     f"({prune_end - prune_start:.2f} sec)")
        #
    #endfor

    # Recalculate the scope. not sure is the correct place if there are multiple
    # calls to a recursive optset.
    log.info("Recreating the scope to account for all the changes...")
    optuni.createscope()
    log.info("Done scope.")

    #optuni.showScope()

    if iceargs.debug or iceargs.saveexprec:
        exprecfile = iceargs.rundirpath+"/"+"aft_expandedrec.locus"
        log.debug("Generating expanded recursion in {} ...".format(exprecfile))
        GenLocus.searchmode(optuni, exprecfile)
        log.info("Generated expanded recursion in {} .".format(exprecfile))
    #endif

    return optseqsexpanded

#enddef
