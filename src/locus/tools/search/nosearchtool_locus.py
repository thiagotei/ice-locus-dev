'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

#from locus.optspace.parameters import Integer, PowerOfTwo, Boolean, Enum
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, RangeExpr, SearchPerm
from locus.frontend.optlang.locus.locusvisitor import GenericVisitor

log = logging.getLogger("NoSearchTool")

class NoSearchTool(object):
    """ This choose one sequence for each optimization space on the optimization
    universe. """

    def __init__(self):
        pass

    @staticmethod
    def getSingleSequence(optuni):
        newoptuni = copy.deepcopy(optuni)
        log.debug("Start")

#        for name, ent in optuni:
#            ent.show()

        # go through codseqs, and optseqs as well
        # go to all enum, powertwo, ...
        for name, ent in newoptuni.items():
            NoSearchTool.getSingleSequenceEnt(ent)

        log.debug("End")
        return newoptuni
    #enddef

    @staticmethod
    def getSingleSequenceTopBlock(optuni):
        newoptuni = copy.deepcopy(optuni)
        log.debug("Start")

        NoSearchTool.getSingleSequenceEnt(newoptuni.topblk)

        log.debug("End")
        return newoptuni
    #enddef

    @staticmethod
    def getSingleSequenceEnt(ent):
        ##
        # Call Visitor on these types
        for ty in OrBlock, OptionalStmt, OrStmt:
            visitor = GenericVisitor(ty)
            visitor.visit(ent)

            # Choose the 1st one, and replace
            for p, n in visitor.values:
                t = next(iter(n))
                p.replace(n, t)
            #endfor
        #endfor
        ##
        # Call Visitor on these types
        for ty in SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm:
            visitor = GenericVisitor(ty)
            visitor.visit(ent)

            # Choose the 1st one, and replace
            for p, n in visitor.values:
                #n.show()
                t = next(iter(n))
                if isinstance(t, RangeExpr):
                    t = next(iter(t))
                #print "v",t 
                p.replace(n, t)
            #endfor
            #endfor
        #endfor
        #ent.show()
    #enddef

