'''

@author: thiago
'''
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import logging

from .models import Base, _Meta
#

log = logging.getLogger(__name__)

DB_VERSION = "0.04"

def connect(dbstr, debug=False):
    engine = create_engine(dbstr, echo=debug)
    connection = engine.connect()

    if engine.dialect.has_table(connection, "_meta"):
        Session = scoped_session(sessionmaker(autocommit=False,
                                          autoflush=False,
                                          bind=engine))
        version = _Meta.get_version(Session)
        if not DB_VERSION == version:
            raise Exception(f'Your Locus database version {version} is '
            f'out of date with the current version {DB_VERSION}')
        #
    #

    Base.metadata.create_all(engine)
    Session = scoped_session(sessionmaker(autocommit=False,
                                          autoflush=False,
                                          bind=engine))

    # mark database with current version
    _Meta.add_version(Session, DB_VERSION)
    Session.commit()

    return engine, Session
##
