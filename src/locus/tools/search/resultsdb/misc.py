'''
@author: thiago
'''

import hashlib, os, logging, socket, statistics as stat

from ..resultsdb.connect import connect
import locus.tools.search.resultsdb.models as m

log = logging.getLogger(__name__)

def hashmycfg(cfg):
    m = hashlib.sha256()
    for k, v in sorted(cfg.items()):
        m.update(str(k).encode())
        m.update(str(v).encode())
        #m.update(str().encode())
        m.update(b"|")
    ##
    return m.hexdigest()
###

def createDBDir(args):
    """ The database dir is to store data about multiple searches in
        a single place. Later the results can be used by another searches.
    """
    if args.database is None:
        #args.database = 'sqlite://' #in memory
        if not os.path.isdir('locus.db'):
            os.mkdir('locus.db')
        #
        args.database = 'sqlite:///' + os.path.join('locus.db',
              socket.gethostname() + '.db') 

    if '://' not in args.database:
        args.database = 'sqlite:///' + args.database
###


def hashlocusfile(locfilename):
    lochash = None
    outlocwspace = ""
    with open(locfilename,'r') as locinp:
        outlocbuf = ""
        for line in locinp:
            #new_line = line.split("#")[0].replace(" ","").rstrip()
            new_line = line.split("#")[0]
            outlocwspace += new_line
            outlocbuf += new_line.replace(" ","").rstrip()
        ##
        lochash = hashlib.sha256(outlocbuf.encode('utf-8')).hexdigest()
    ##
    return lochash, outlocwspace
###

def getDBseeds(ctx=None, **kwargs):
    log.info(f"Starting...")

    engine, session = connect(ctx.iceargs.database)

    locfilename = ctx.iceargs.optfile
    lochash, buflocfile = hashlocusfile(locfilename)

    if 'disregardenvvar' in ctx.iceargs:
        disenvvar = ctx.iceargs.disregardenvvar
    else:
        disenvvar = {}
    #

    #
    # Remove from the env vars replace don the Locus program the ones
    # passed to be ignored.
    #
    tgtenvvalues = {k : v for k, v in ctx.iceargs.getenvsreplaced.items() 
                        if k not in disenvvar}

    log.debug(f'  Envvars to selected search: {tgtenvvalues}')
    matchedsearchs = []

    # Get all the searches for the same locus file
    x = session.query(m.Search).join(m.LocusFile).filter(m.LocusFile.hash == 
                                                                    lochash)
    for e in x:
        searchmatched = True
        for evalue in e.envvalues:
            envvar = evalue.envvar

            if envvar.name in tgtenvvalues and \
                evalue.value != tgtenvvalues[envvar.name]:
                searchmatched = False
                break
            #
        ##
        if searchmatched:
            matchedsearchs.append(e)
        #
    ##

    log.info(f"  {len(matchedsearchs)} matching searches!")

    bestconfs = {}
    for ms in matchedsearchs:
        for v in ms.variants:
            #emetrics = [e.metric for e in v.experiments]
            # It uses the first Timer/metric from the expvalue table
            emetrics = [e.expvalues[0].metric for e in v.experiments]
            #emetrics = []
            #for e in v.experiments:
            #    log.info(f"found exp {e}")
            #    for ev in e.expvalues:
            #        log.info(f"adding {ev.metric}")
            #        emetrics.append(ev.metric)
            #        break # It uses the first one!
            #    ##
            ##
            log.debug(f"Emetrics {emetrics} ")
            if emetrics:
                metric = stat.median(emetrics)
                confid = v.configurationid

                #allconfs.append((v.id, v.configurationid, metric))
                if confid not in bestconfs or \
                   metric < bestconfs[confid][1]:
                    bestconfs[confid] = (v.id, metric)
                    log.debug(f"  Appending faster experiment varid: {v.id}"
                              f" confid: {v.configurationid} metric: {metric}")
                #
            else:
                log.warning("Found search with no experiments! That should not have happened!")
            #
        ##
    ##
    sortedconfs = sorted([(vid, confid, metric) for confid, (vid, metric) in 
                        bestconfs.items()], key=lambda x: x[2])

    retcfgs = []
    maxcfgs = ctx.iceargs.dbseeds
    #maxdatakeys = 14
    for vid, confid, metric in sortedconfs[:maxcfgs]:
        conf = session.query(m.Configuration).filter(confid == m.Configuration.id).one()
        data = conf.data
        cfghash = conf.hash
        #sortdata = dict(sorted(data.items())[:maxdatakeys])
        log.info(f"  Conf {conf.id} selected! varid: {vid} metric: {metric}"
                 f" hash: {cfghash} ")
        log.info(f"   data: {dict(sorted(data.items()))}")
        retcfgs.append(data)
    ##

    session.close()
    log.info(f"Ended (returning {len(retcfgs)} cfgs)")
    return retcfgs
###

