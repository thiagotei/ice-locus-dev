'''

@author: thiago
'''

import logging
import sqlalchemy
from sqlalchemy import Column, Integer, Float, String, DateTime, func, \
                    ForeignKey, PickleType, Table
from sqlalchemy.ext.declarative  import declarative_base
from sqlalchemy.orm import relationship
from datetime import datetime
from pickle import dumps, loads
from gzip import zlib


log = logging.getLogger(__name__)

class CompressedPickler(object):
    @classmethod
    def dumps(cls, obj, protocol=2):
        s = dumps(obj, protocol)
        sz = zlib.compress(s, 9)
        if len(sz) < len(s):
            return sz
        else:
            return s
        #
    ###

    @classmethod
    def loads(cls, string):
        try:
            s = zlib.decompress(string)
        except:
            s = string
        return loads(s)
    ###
####

class Base():
    id = Column(Integer, primary_key=True)
####

Base = declarative_base(cls=Base)

class _Meta(Base):
    """ meta table to track current version """
    __tablename__ = '_meta'
    db_version = Column(String(128))

    @classmethod
    def has_version(cls, session, version):
        try:
            session.flush()
            session.query(_Meta).filter_by(db_version=version).one()
            return True
        except sqlalchemy.orm.exc.NoResultFound:
            return False
    ###

    @classmethod
    def get_version(cls, session):
        try:
            session.flush()
            x = session.query(_Meta).one()
            return x.db_version
        except sqlalchemy.orm.exc.NoResultFound:
            return None
    ###

    @classmethod
    def add_version(cls, session, version):
        if not cls.has_version(session, version):
            session.add(_Meta(db_version=version))
        #
    ###
###

# This is required for m-to-m asscoiation between searches and envvars.
#association_table = Table('association', Base.metadata,
searchtoenvvars = Table('ass_searchtoenvvars', Base.metadata,
        Column('searchid', Integer, ForeignKey('searches.id')),
        Column('envvarid', Integer, ForeignKey('envvars.id')),
)

searchstoenvvals = Table('ass_searchtoenvvals', Base.metadata,
        Column('searchid', Integer, ForeignKey('searches.id')),
        Column('envvalueid', Integer, ForeignKey('envvalues.id'))
)

class Search(Base):
    """Search is based on a Locus file (opt space). 
       Involves multiples variants execution.
    """
    __tablename__ = 'searches'

    locusfileid = Column(Integer, ForeignKey('locusfiles.id'))

    hostname = Column(String(18), default="N/A")
    # Path in which Locus was invoked
    #invokepath = Column(String(256), default="N/A")
    # Path in which the search files were saved.
    rundirpath = Column(String(256), default="N/A") 
    searchtool = Column(String(16), default="N/A")
    searchtech = Column(String(64), default="N/A")
    started_at = Column(DateTime(timezone=True))
    finished_at = Column(DateTime(timezone=True), default=None)
    #strategyinfo = Column(String(16))
    #problem = Column(String(18))
    # code regions optimized
    # code regions hashes

    envvars = relationship("EnvVar", secondary=searchtoenvvars,
                            back_populates="searches")
    envvalues = relationship("EnvValue", secondary=searchstoenvvals,
                            back_populates="searches")

    def __str__(self):
        ret = (f"[Search] {self.id} {self.hostname} {self.locusfileid} {self.searchtool}"
               f" {self.searchtech} {self.started_at} {self.finished_at}")
               #f" nvariants: {len(self.variants)}")
        return ret
    ###
####

class EnvVar(Base):
    """Save here all the environment variables related to Locus
    """
    __tablename__ = 'envvars'

    name = Column(String(32))

    searches = relationship("Search", secondary=searchtoenvvars,
            back_populates="envvars")

    @classmethod
    def get(cls, session, namev):
        try:
            session.flush()
            res = session.query(EnvVar).filter_by(name=namev).one()
            return res
        except sqlalchemy.orm.exc.NoResultFound:
            t = EnvVar(name=namev)
            session.add(t)
            return t
        #
    ###

    def __str__(self):
        ret = f"{self.name}"
        return ret
    ###
####

class EnvValue(Base):
    """Save values of environment variables
    """
    __tablename__ = 'envvalues'

    envvarid = Column(Integer, ForeignKey('envvars.id'))
    value = Column(String(128))

    envvar = relationship("EnvVar", back_populates="values")
    searches = relationship("Search", secondary=searchstoenvvals,
                            back_populates="envvalues")

    @classmethod
    def get(cls, session, value, envvar):
        try:
            session.flush()
            res = session.query(EnvValue).filter_by(value=value, 
                                                    envvar=envvar).one()
                                                    #envvarid=envvar.id).one()
            return res
        except sqlalchemy.orm.exc.NoResultFound:
            t = EnvValue(value=value, envvar=envvar)
            session.add(t)
            return t
        #
    ###

    def __str__(self):
        ret = f"{self.value}"
        return ret
    ###
####

class Variant(Base):
    """Variant belongs to a search and involves a configuration"""
    __tablename__ = 'variants'

    # Id of the search in which the variant was executed
    searchid = Column(Integer, ForeignKey('searches.id'))
    configurationid= Column(Integer, ForeignKey('configurations.id'))

    timestamp = Column(DateTime(timezone=True), default=func.now())
    # This the cfg id receive during the search
    searchcfgid = Column(Integer, default=None)

    search = relationship("Search", back_populates = "variants")

    def __str__(self):
        ret = (f"[Variant] {self.id} searchid: {self.searchid}"
            f" cfgid: {self.configurationid} timestamp: {self.timestamp}"
            f" searchcfgid: {self.searchcfgid}")
        return ret
    ###
####

class Experiment(Base):
    """Experiment belongs to a variant. A variant can have N experiments."""
    __tablename__ = 'experiments'

    variantid = Column(Integer, ForeignKey('variants.id'))

    variant = relationship("Variant", back_populates="experiments")
####

class ExpValue(Base):
    """ ExpValue belongs to experiment. An experiment can have N
        expvalues (different timings of the same experiment program).
    """
    __tablename__ = 'expvalues'

    expid = Column(Integer, ForeignKey('experiments.id'))

    metric = Column(Float, default=float('inf'))
    unit = Column(String(16), default='N/A')
    desc = Column(String(16), default='N/A')

    experiment = relationship("Experiment", back_populates="expvalues")
####

class LocusFile(Base):
    """Represents an optimization space."""
    __tablename__ = 'locusfiles'

    hash = Column(String(64))
    locusfilename = Column(String(128))
    data = Column(PickleType(pickler=CompressedPickler))

    searches = relationship("Search", back_populates="locusfile")

    @classmethod
    def get(cls, session, hashv, fname, datav):
        try:
            session.flush()
            return (session.query(LocusFile).filter_by(hash=hashv).one())
        except sqlalchemy.orm.exc.NoResultFound:
            t = LocusFile(hash=hashv, locusfilename=fname, data=datav)
            session.add(t)
            #print("Created new LocusFile!")
            return t
        #
    ###

    def __str__(self):
        ret = f"{self.locusfilename}"
        return ret
    ###
####

class Configuration(Base):
    """Configurantion used by N variants. """
    __tablename__ = 'configurations'

    locusfileid = Column(Integer, ForeignKey('locusfiles.id'))
    hash = Column(String(64))
    data = Column(PickleType(pickler=CompressedPickler))

    locusfile = relationship("LocusFile", back_populates="configurations")
    variants = relationship("Variant", back_populates="configuration")

    @classmethod
    def get(cls, session, hashv, datav, lfile):
        try:
            session.flush()
            return (session.query(Configuration)
                  .filter_by(hash=hashv, locusfile=lfile).one())
        except sqlalchemy.orm.exc.NoResultFound:
            t = Configuration(hash=hashv, data=datav, locusfile=lfile)
            session.add(t)
            #print("Created new Configuration!")
            return t
        #
    ###
####

LocusFile.configurations = relationship("Configuration",
                                    order_by=Configuration.id,
                                    back_populates="locusfile")

Search.variants = relationship("Variant",
                               order_by=Variant.id,
                               back_populates="search")

Search.locusfile = relationship("LocusFile",
                                order_by=LocusFile.id,
                                back_populates="searches")

#Search.envvars = relationship("EnvVar", order_by=EnvVar.id,
#                                back_populates="search")

EnvVar.values = relationship("EnvValue", back_populates="envvar")

Variant.experiments = relationship("Experiment",
                                   order_by=Experiment.id,
                                   back_populates="variant") 

Variant.configuration = relationship("Configuration",
                                    order_by=Configuration.id,
                                    back_populates="variants")

Experiment.expvalues = relationship("ExpValue",
                                    back_populates="experiment")

