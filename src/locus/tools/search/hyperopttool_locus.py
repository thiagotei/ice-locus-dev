'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, os
import argparse, copy, time, math
import itertools
from datetime import datetime
from functools import partial

from hyperopt import fmin, anneal, rand, mix, tpe, hp,\
    STATUS_OK, STATUS_FAIL, Trials

try:
    from hyperopt import atpe # atpe not working properly
except ImportError as e:
    atpeavail = False
else:
    atpeavail = True

from hyperopt.fmin import generate_trials_to_calculate
from hyperopt.exceptions import AllTrialsFailed
import hyperopt.pyll.stochastic

from .searchtoolinterface import AbstractSearchTool, argparser as absstparser
from locus.util.exceptions import OptimizationError, StopAfterInterruption,\
    BoundsException
from locus.util.cmds import Commands
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, Constant, ListMaker, FuncCall, Block, OptSeqDef
from locus.util.constants import ICE_OUTPUT_MODE_STDOUT, bcolors,\
    ICE_TOP_BLK_STR, ICE_CLR_INITSEED, ICE_CLR_FAIL
from locus.frontend.optlang.locus.nodevisitor import NodeVisitor
from locus.backend.interpreter import topblkinterpreter
from locus.backend.codegen import CodeGen
from locus.tools.search.handlers.defaulthandlers import PrebuildHandler,\
        BuildHandler, EvalHandler, CheckHandler
from .handlers.locusdbhandlers import LocusDBResultHandler
from locus.tools.search.context.searchctxhy import SearchCtxHY
from locus.tools.search.resultsdb.misc import getDBseeds

import locus.tools.search.searchbase as searchbase
import locus.tools.search.misc.loreprj as loreprj
import locus.frontend.optlang.locus.optimizer as optimizer

ICE_HYPALGO_TPE = "tpe" #tpe
ICE_HYPALGO_ATPE = "atpe" #atpe
ICE_HYPALGO_RND = "rand" #rand
ICE_HYPALGO_MIX = "mix" #mix
ICE_HYPALGO_ANNEAL = "anneal" #anneal


argparser = argparse.ArgumentParser(add_help=False,
                                    parents=[absstparser])
argparser.add_argument('--hypalg',
                        required=False, default=ICE_HYPALGO_TPE,
                        choices=[ICE_HYPALGO_TPE,
                                ICE_HYPALGO_ATPE,
                                ICE_HYPALGO_RND,
                                ICE_HYPALGO_MIX,
                                ICE_HYPALGO_ANNEAL],
                        help="Hyperopt: algorithm to conduct the"+\
                        "search. "+ICE_HYPALGO_MIX+" requires --hypmixdsc.")
argparser.add_argument('--hypmixdsc', default=None, type=str,
                        help="Hyperopt: for combination of search algorithms"+\
                             " using mix needs to"+\
                             " define the probability of each selection."+\
                             " Some combinations are not working (e.g.,"+\
                             " rand+atpe+anneal and rand+atpe+tpe)."+\
                             " Probabilities must add to 1. "+\
                             " Requires the use of --hypalg mix ."+\
                             " Example: .1:rand:.5:anneal:.4:tpe")
argparser.add_argument('--hypalgcheck', default=True, action='store_true',
                      help='Some mix combinations are not working. '
                            'This makes sure they are not being used.')

logice = logging.getLogger(__name__)

class HyperOptTool(AbstractSearchTool):
    """Hyperopt driver to search the optimization space. """

    def __init__(self, iceargs,
            searchctx=SearchCtxHY,
            prebuildhandler=PrebuildHandler,
            buildhandler=BuildHandler,
            evalhandler=EvalHandler,
            checkhandler=CheckHandler,
            resulthandler=LocusDBResultHandler):

        self.ctx = searchctx(iceargs,
                               None,
                               None,
                               prebuildhandler,
                               buildhandler,
                               evalhandler,
                               checkhandler,
                               resulthandler) 

        logice.info(f"Search carried out by {bcolors.OKBLUE} Hyperopt {bcolors.ENDC}!")
        self.space = None
        # saving cfgint for each variant, indexed by cfgid
        self.mytrials = {}
        # saving a hash of the variants already evaluated
        # to avoid repetition, indexed hash from cfgint
        self.alreadyexec = {}
        # metric saved by successful hashes, indexed by cfgid
        self.trialsloss = {}
        # Initial configuration
        self.__seedcfg = []
        self.finalresult = None

        def gethypalgo(algo):
            if algo == ICE_HYPALGO_RND:
                return rand.suggest
            elif algo == ICE_HYPALGO_TPE:
                return tpe.suggest
            elif algo == ICE_HYPALGO_ATPE:
                if atpeavail:
                    sug = atpe.suggest
                else:
                    sug = None
                    logice.error("Hyperopt ATPE module could not be imported!")
                    raise RuntimeError("Hyperopt ATPE module could not be "
                            "imported! Installing scikit-learn and lightgbm "
                            "should solve the issue.")
                #endif
                return sug
            elif algo == ICE_HYPALGO_ANNEAL:
                return anneal.suggest
            else:
                raise ValueError("Hyperopt search algorithm option {} not "
                        "available.".format(algo))
        #enddef

        self.hypalg = None

        logice.debug("iceargs hypalg: {} hypmixdsc: {}".format(iceargs.hypalg,
            iceargs.hypmixdsc))

        # Check in mix case
        if iceargs.hypalg == ICE_HYPALGO_MIX:
            if iceargs.hypmixdsc is None:
                logice.error("To use Hyperopt mix needs to pass --hypmixdsc!")
                raise ValueError("Argument --hypmixdsc required!")
            else:
                lst = iceargs.hypmixdsc.split(":")
                if len(lst) % 2 != 0:
                    logice.error("Requires a pair of values. one number and one"
                            "valid name for each option of the mix.")
                    raise ValueError("Incorrect format for --hypmixdsc!")
                #endif

                # must add to 1.0
                totalprob = 0.0 

                # this list contains tuples of the algo and probility
                # [(.1, rand.suggest),
                #  (.2, anneal.suggest),
                #  (.4, tpe.suggest),
                #  (.3, atpe.suggest),]
                psugg = []

                # traverse list two by two
                it = iter(lst)
                for v, n in zip(it,it):
                    totalprob += float(v)
                    psugg.append((float(v), gethypalgo(n)))
                #endif

                if totalprob != 1.0:
                    logice.error("Total probability for mix algorithm search "
                            "selection is {}. Must be 1.0!".format(totalprob))
                    raise ValueError("Mix probability must total 1.0. Current "
                                     "total {}.".format(totalprob))
                #endif

                if iceargs.hypalgcheck:
                    # Not workign configs. Dont know why. issue #603 on github
                    notcfgs = [[gethypalgo(ICE_HYPALGO_RND),
                                gethypalgo(ICE_HYPALGO_ATPE),
                                gethypalgo(ICE_HYPALGO_ANNEAL)],
                               [gethypalgo(ICE_HYPALGO_RND), 
                                gethypalgo(ICE_HYPALGO_ATPE), 
                                gethypalgo(ICE_HYPALGO_TPE)]]

                    for nc in notcfgs:
                        infonc = [al.__module__ for al in nc]
                        logice.debug("checking {}".format(infonc))

                        if len(nc) != len(psugg):
                            continue
                        #endif

                        match = True
                        for pr, al in psugg:
                            if al not in nc:
                                match = False
                                break
                        #endfor

                        if match:
                            logice.error("This mix selection {} is known to not work. "
                                         "Try another one, or disable checking.".format(infonc))
                            raise RuntimeError("Mix selected {} known to have issues. "
                                    "Try another one, or disable checking.".format(infonc))
                        #endif
                    #endfor
                #endfor

                self.hypalg = partial(mix.suggest, p_suggest=psugg,)
                infopsugg = [(pr, al.__module__)for pr, al in psugg]
                logice.debug("Hyperopt search algorithm selected {} p_suggest = {} ".format(
                    mix.suggest.__module__,
                    infopsugg))
            #endif
        else:
            self.hypalg = gethypalgo(iceargs.hypalg)
            logice.debug("Hyperopt search algorithm selected {}".format(self.hypalg.__module__))
        #endif
    #enddef

    def _buildSearchNodes(self, ent):
        #print ent
        #print "==="
        senodes = OrBlock, OptionalStmt, OrStmt,\
                SearchEnum, SearchPerm, SearchPowerOfTwo
        visitor = GenericHierarchicalVisitor(senodes)
        visitor.visit(ent)

        spacesize = visitor.spacesizetmp
        #print "Final space size", spacesize
        dictid = {n.unqid:(p,n,s) for p,n,s in visitor.searchnodes}
        #print(visitor.hierpointer)
        #for k,v in visitor.hierpointer.items():
        #    print(str(k)+": ",v.pprint(sys.stdout)," --")
        #    print str(k)+": ",v
        #sample = hyperopt.pyll.stochastic.sample(visitor.hierpointer)
        #logice.info("Sample:    {}".format(sample))

        if self.ctx.iceargs.initseed:
            logice.debug("Init seed: {}".format(visitor.inithierptr))
        #endif

        initseed = visitor.searchndinit #visitor.inithierptr
        #initseed = sample

        return visitor.hierpointer, dictid, initseed, spacesize
    #enddef

    def convertOptUni(self, inpoptuni, parsedsrcfiles, orighashes):
        logice.debug("start")
        self.ctx.optuni = copy.deepcopy(inpoptuni)
        self.ctx.parsedsrcfiles = parsedsrcfiles
        self.ctx.orighashes = orighashes

        if self.ctx.iceargs.equery or self.ctx.iceargs.applyDFAOpt:
            topblkinterpreter(self.ctx.optuni.topblk)

            if self.ctx.iceargs.equery:
                loreprj.queryOpt(self.ctx.optuni, self.ctx.parsedsrcfiles, self.ctx.iceargs)
                logice.info("Queries information collected and replaced.")
            #

            if self.ctx.iceargs.applyDFAOpt:
                searchbase.applyDFAOpts(self.ctx.optuni, self.ctx.iceargs)
                logice.info("DFA Compiler optimizations applied.")
            #
        #endif

        if self.ctx.iceargs.exprec:
            searchbase.expandRecursion(self.ctx.optuni, self.ctx.iceargs)
        #endif

        # Hierarchical representation of the space. traverse the space
        # conditionally.
        self.space = {}

        # Search node dict has information of parent, scope for each search node.
        self.ctx.searchnodes = {}

        # Revert search dict saves how to revert the search nodes to the
        # original state.
        self.ctx.revertsearch = {}

        totalspace = 0
        topblkspacesize = 0
        fullinitseed = {}

        # Add each CodeReg to the search space.
        for name, ent in self.ctx.optuni.allspaces():
            logice.debug("Adding codereg {}{}{} to the space...".format(
                bcolors.OKBLUE, name, bcolors.ENDC))

            #print name,":\n",ent
            self.space[name], dictid, initseed, spacesize = self._buildSearchNodes(ent)
            self.ctx.searchnodes.update(dictid)

            if name == ICE_TOP_BLK_STR:
                topblkspacesize = spacesize
            #endif

            logice.info("Done adding codereg {}{}{} to space. space size: {} (incl topblk: "
                    "{})".format(bcolors.OKBLUE, name, bcolors.ENDC,
                        spacesize, spacesize*topblkspacesize))
            #totalspace *= spacesize
            totalspace = totalspace * spacesize if totalspace > 0 else spacesize

            # Aggregate the seed from all ent
            if self.ctx.iceargs.initseed or self.ctx.iceargs.dbseeds:
                fullinitseed[name] = initseed 
            #endif
        #endfor

        if self.ctx.iceargs.initseed or self.ctx.iceargs.dbseeds:
            # All keys must be strings to pass a point to hyperopt.
            # Besides, the list must have all the search nodes, and the ones not
            # used with an empty list.
            newseedcfg     = {}

            convtoinitcfgHyopt(fullinitseed, newseedcfg)
            logice.info(f'Initseed: {fullinitseed} \n--------> {newseedcfg}')

            if self.ctx.iceargs.initseed:
                self.__seedcfg.append(newseedcfg)
                logice.debug(searchbase.seedcfgsstr([newseedcfg],
                                                "Initial seed for the search:"))
            #endif

            if self.ctx.iceargs.dbseeds:
                dbseeds = getDBseeds(self.ctx)
                t_dbseeds = encodeInitCfgSeeds(dbseeds, newseedcfg)
                #convtoinitcfgHyopt(dbseeds, t_dbseeds)
                self.__seedcfg += t_dbseeds
                logice.debug(searchbase.seedcfgsstr(t_dbseeds,
                                                "Initial seeds from DB are:"))
            #
        #

        if self.__seedcfg:
            logice.info(searchbase.seedcfgsstr(self.__seedcfg,
                                "All initial seeds for the search:"))
        else:
            logice.info(f"{ICE_CLR_INITSEED}No initial seeds!{bcolors.ENDC}")
        #

        logice.info("Total space size: {}".format(totalspace))

        if self.ctx.ntests > totalspace or self.ctx.ntests == 0:
            self.ctx.ntests = max(totalspace, 1)
            logice.warning("Number of tests updated to {}.".format(self.ctx.ntests))
        #endif

        #logice.info('Space generated {}'.format(self.space))
        #print "Complet space", self.space
        return True
    #enddef

    def search(self, origmetric=None, origres=None):
        self.ctx.searchstarttime = datetime.now()

        self.ctx.origmetric = origmetric
        self.ctx.origres = origres
        logice.info("Orig metric {}.".format(self.ctx.origmetric))

        # prebuild handler
        # prebuild. ATTENTION: No search variables allowed on prebuild, since it
        # happens before the search starts.
        searchobj = self.ctx.optuni.search
        prebuildcmd = searchobj.execute(searchobj.prebuildcmd)
        logice.info("prebuildcmd {}  type {}".format(prebuildcmd, type(prebuildcmd)))
        if prebuildcmd is not None and \
           prebuildcmd and \
           self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            logice.info("trying to execute it!")
            self.ctx.prebldhand.run(prebuildcmd)
        #endif

        srch_start = time.time()

        if self.ctx.iceargs.initseed or self.ctx.iceargs.dbseeds:
            pnts_to_eval = self.__seedcfg
            #trials = None
            trials = generate_trials_to_calculate(self.__seedcfg)
        else:
            pnts_to_eval = None
            trials = Trials()
        #endif

        # If it is hyperopt.mix it is inside a partial obj
        if isinstance(self.hypalg, partial):
            # these are fields of partial obj
            algodesc = self.hypalg.func.__module__
            infopsugg = [ ""+str(pr)+":"+al.__module__+""
                    for pr, al in self.hypalg.keywords['p_suggest']]
            algodesc += " "+":".join(infopsugg)+""
        else:
            algodesc = self.hypalg.__module__+" None"

        logice.info("Search algorithm {}".format(algodesc))

        trialfailmsgdone = False

        try:
            dbinfo = {'searchtool': 'hyperopt',
                      'searchtech': algodesc}
            with self.ctx.resulthandler(self.ctx, **dbinfo) as rh:
                self.ctx.reshandobj = rh
                best = fmin(self.objective,
                            self.space,
                            algo=self.hypalg,#algo=tpe.suggest,
                            max_evals=self.ctx.ntests,
                            trials=trials,
                            points_to_evaluate=pnts_to_eval,
                            show_progressbar=False)
            #endwith
        except StopAfterInterruption as e:
            logice.info("Search interrupted using stop-after value!")

        except KeyboardInterrupt as e:
            logice.info("Shutdown requested...exiting")

        except AllTrialsFailed:
            logice.warning("####")
            logice.warning("### ALL trials failed!!!")
            logice.warning("####")
            trialfailmsgdone = True

        finally:
            bestmetric = None
            cfgint = None
            best_tid = None

            try:
                if trials.best_trial is not None:
                    #print type(best),best, 
                    #for t in trials.trials:
                    #    if 'vals' in t['misc']:
                    #        kv = t['misc']['vals']
                    #        newkv = {k: v for k, v in kv.iteritems() if v != []}
                    #        print t['tid'], t['result'], newkv

                    #print "best_trial", trials.best_trial
                    #inp = {k: (v[0] if type(v) is list else v) for k, v in trials.best_trial['misc']['vals'].iteritems() if v != []}
                    #print "best inp", inp
                    best_tid = trials.best_trial['tid']
                    cfgint = self.mytrials[best_tid]
                    #print "best cfgint", cfgint

                    # Create and prune a temporary search nodes 
                    self.ctx.searchnodesPr = {j: self.ctx.searchnodes[j] for j in cfgint if j in self.ctx.searchnodes}
                    #print "best searchnodes_obj", cfgint

                    bestmetric = trials.best_trial['result']['loss']

                    # Converts the final best result into self.optuni to be used later.
                    # For instance, on saving the best_<something>.locus file.
                    searchbase.convertDesiredResult(self.ctx.searchnodesPr, cfgint, self.ctx.revertsearch)
                    self.finalresult = self.ctx.optuni
                #endif
            except AllTrialsFailed:
                if not trialfailmsgdone:
                    logice.warning("####")
                    logice.warning("### ALL trials failed!!!")
                    logice.warning("####")
            #endif

            logice.info("Final Summary algo: {} bestid: {} bestmetric: {} origmetric: {} speedup: "
                    "{} suggestedcfgs: {} cfgexec: {} cfgstimeout: {} cfgserror: {} cfgsinvalid: {} cfgrepeated: {} "
                    "bestconfig: {}".format(
                        algodesc,
                        best_tid, bestmetric,
                        self.ctx.origmetric,
                        None if self.ctx.origmetric is None or bestmetric is None else self.ctx.origmetric/bestmetric,
                        self.ctx.lastcfgid+1, self.ctx.cfgsexecuted,
                        self.ctx.cfgstimeout, self.ctx.cfgserror,
                        self.ctx.cfgsinvalid, self.ctx.cfgsrepeated,
                        None if cfgint is None else searchbase.prettyprintcfg(self.ctx.searchnodesPr, cfgint)))

            srch_end = time.time()
            self.ctx.timing[searchbase.TIME_SRCH].append(srch_end - srch_start)

            searchbase.printTimingReport(self.ctx.timing)
        #endtry
    #enddef

    def _createcfgint(self, inp):
        cfgint = {}
        def visit(dic, cfgint):
            for k,v in dic.items():
                #if isinstance(k, int) and isinstance(v, dict):
                #logice.info("{}: {}".format(k,v))

                # v can be a dict for an optspace (e.g., CodeReg, Optseq, topblk)
                # or for an OrBlock.
                if isinstance(v, dict):
                    # This to assign the choice of an OrBlock node based on the
                    # value of the OR_IDX_STR.
                    if GenericHierarchicalVisitor.OR_IDX_STR in v:
                        cfgint[int(k)] = v[GenericHierarchicalVisitor.OR_IDX_STR]
                    #endif
                    visit(v, cfgint)
                elif k != GenericHierarchicalVisitor.OR_IDX_STR:
                    # (Thiago Teixeira Jan 10 15:06:21 2020) The code assumes that the keys are integers.
                    # Hyperopt internal code do some sorting of the keys. On
                    # OrBlocks the keys are string for or_blk_idx and integers.
                    # Not a problem on Python 2, but python 3 does not allow
                    # sorting integers ans strings, so I decided to make them
                    # all strings. Here needs to convert them back to integers.
                    cfgint[int(k)] = v
                #endif
            #endfor
        #enddef
        visit(inp, cfgint)
        return cfgint
    #enddef

    def objective(self, inp):
        logice.info("### start (search time {} (sec))".format(
            (datetime.now() - self.ctx.searchstarttime).total_seconds()))

        if self.ctx.iceargs.stop_after and \
            searchbase.convergenceCriteria(self.ctx.searchstarttime,
                    self.ctx.iceargs.stop_after):
            raise StopAfterInterruption()
        #endif

        self.ctx.lastcfgid += 1
        varcfgid = self.ctx.lastcfgid

        #print "Objective eita!", inp
        cfgint = self._createcfgint(inp)

        # Saving the cfgint, but could extract from trials using space_eval and
        # calling _createcfgint
        self.mytrials[varcfgid] = cfgint

        # Hash to track which configurations have been ran
        hashcfg = "_".join([str(key)+"_"+str(cfgint[key]) for key in sorted(cfgint)])

        # Check if this configuration have already been executed
        if hashcfg in self.alreadyexec:
            self.ctx.cfgsrepeated += 1
            idorig = self.alreadyexec[hashcfg]

            cfgmet = float("inf")
            if idorig in self.trialsloss:
                cfgmet = self.trialsloss[idorig]
            #endif

            logice.info("Config already executed by id: {} loss: "
                    "{} hash: {} .".format(idorig, cfgmet, hashcfg))
            return {'loss': cfgmet, 'status': STATUS_FAIL}
            #return {'loss': cfgmet, 'status': STATUS_OK}
            #return {'loss': cfgmet}
        else:
            logice.info("Config not executed yet. hash: {} .".format(hashcfg))
            self.alreadyexec[hashcfg] = varcfgid
        #endif

        #print ("cfgint",cfgint)
        #print ("searchnodes",len(self.ctx.searchnodes), self.ctx.searchnodes)

        # Create and prune a temporary search nodes 
        self.ctx.searchnodesPr = {j: self.ctx.searchnodes[j] for j in cfgint if j in self.ctx.searchnodes}

        #print "Current searchnodes",len(searchnodes_obj), searchnodes_obj

        # check bounds
        logice.info("@@@ cfg_id: {} -> {}".format(varcfgid,
            searchbase.prettyprintcfg(self.ctx.searchnodesPr, cfgint)))

        try:
            # Check searchable variable found is between the range.
            # It is important if searchable variables depends on another
            # searchable variables the values will only be known at this time,
            # at "runtime".
            optimizer.checkBounds(self.ctx.searchnodesPr, cfgint)

        except BoundsException as e:
            # Code generation failed, go to next!
            self.ctx.incCfgsInv()
            #logice.exception(e)
            logice.warning(e)
            # Fixed What to return when experiment fail?
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}
        #end except

        searchobj = self.ctx.optuni.search
        evalstat = searchbase.EVAL_ERROR
        metric = float("inf")

        try:
            logice.debug("cfgint:\n{}".format(cfgint))

            searchbase.convertDesiredResult(self.ctx.searchnodesPr, cfgint,
                    self.ctx.revertsearch)

            ###
            # Evaluate the global scope.
            # before interpreting global scope need to add the global scope
            # search variables need to be added to global scope.
            topblkinterpreter(self.ctx.optuni.topblk)

            ###
            # Evaluate the search def block statements that may depend on the
            # global scope.
            localbuildcmd = searchobj.execute(searchobj.buildcmd)
            localruncmd = searchobj.execute(searchobj.runcmd)
            localcheckcmd = searchobj.execute(searchobj.checkcmd)
            #endfor

            #self.convertdebug.write("%%% End Cfg {}.\n".format(cfg_id))
            gencode_start = time.time()
            undoList = CodeGen.genCode(self.ctx.parsedsrcfiles, self.ctx.orighashes, 
                    self.ctx.optuni, self.ctx.iceargs, varcfgid)
            gencode_end = time.time()

            self.ctx.timing[searchbase.TIME_GEN].append(gencode_end - gencode_start)

        except OptimizationError as e:
            self.ctx.cfgserror += 1
            #return {'status': STATUS_FAIL}
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}

        except Exception as e:
            self.ctx.cfgserror += 1

            if self.ctx.iceargs.evalfail:
                raise e
            #

            # Code generation failed, go to next!
            logice.exception(e)
            #logice.error(e)

            # Fixed What to return to when experiment fail?
            #return {'status': STATUS_FAIL}
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}

        else:
            if self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
                metric, evalstat = searchbase.empiricalEvaluation(self.ctx,
                                                                  localbuildcmd, localruncmd,
                                                                  localcheckcmd, cfgint)

                if evalstat == searchbase.EVAL_TIMEOUT:
                    self.ctx.cfgstimeout += 1
                elif evalstat == searchbase.EVAL_ERROR:
                    self.ctx.cfgserror += 1
                #
            #endif

        finally:
            searchbase.revert2origsearchnodes(self.ctx.searchnodesPr,
                    self.ctx.revertsearch)
            logice.debug("Returned the optuni to original state.")
        #end try

        # Number of configurations executed
        self.ctx.incCfgsExec()

        searchbase.variantEpilog(undoList, self.ctx.iceargs.output)

        if evalstat == searchbase.EVAL_ERROR:
            if self.ctx.iceargs.evalfail:
                raise RuntimeError(f"{ICE_CLR_FAIL}Search interrupted by error!{bcolors.ENDC}")
            #
            resobj = {'loss':float("inf"), 'status': STATUS_FAIL}
        else:
            resobj = {'loss': metric, 'status': STATUS_OK}
        #

        self.trialsloss[varcfgid] = metric
        logice.info("end")
        return resobj
    #enddef
#endclass

def encodeInitCfgSeeds(seeds, fileinitseed):
    """ Expect a list of seeds. Each seeds is a dictionary.
        In some hierarchical spaces the seed contains only the
        necessary searchvars in the database. i
        However, hyperopt requires all the
        seeds for an initial cfgs evens the ones not used.
        I use the initseed from the Locus file to replace 
        the ones missing.
    """
    ret = []
    # make the search var keys as strings instead of integers
    for d in seeds:
        newd = {}
        for k in d:
            newd[str(k)] = d[k]
        ##
        for k in fileinitseed:
            if k not in newd:
                newd[k] = fileinitseed[k]
            #
        ##
        ret.append(newd)
    ##

    return ret
###

# The initial cfg accepted by Hyperopt needs to be formatted as
# a dict of strings with no hierarquical levels in which the keys are the labels
# of the search objs (e.g., hp.choice). In this case, the labels are the
# node.unqid.
# It also needs to have all the search nodes (Teixeira 17 Oct 2019).
#
def convtoinitcfgHyopt(dictin, dictout, dictid=None):
    #print "keytostr"
    for k, v in dictin.items():
        #print "Before:",k,v
        if isinstance(v, dict):
            convtoinitcfgHyopt(v, dictout, k)
        else:
            if k == GenericHierarchicalVisitor.OR_IDX_STR:
                k = dictid
            #endif
            #print "in if:",k,v
            dictout.update({str(k): v})
        #endif
    #endfor
#enddef


class GenericHierarchicalVisitor(NodeVisitor):
    """Generate a dict for search variables. Hierarchical by OR statements. it
    must be traversed in posorder."""

    OR_IDX_STR = "or_blk_idx"

    def __init__(self, astnode=None):
        self.astnode = astnode
        self.hierpointer = {} # output, hierarquical representation of the space
        self.orlst = []
        self.searchnodes = [] # output, info about each search node

        # searchndinit has all the search nodes in the same dict.
        # Diffently than hierpointer, which prunes the init space based on
        # the initial ORblocks. I just learned that hyperopt requires all the inital
        # search variables, not onlyt the pruned ones ad I initally thougt. (Teixeira 17 Oct 2019)
        self.searchndinit = {} 
        self.parent = None
        self.parentscope = None
        self.spacesizetmp = 1 # ouput, size of hierarquical space

        # initseed variables
        self.inithierptr = {}
        self.initorlst = []
    #enddef

    def update(self, node, parentscope):
        #print type(node),"  updating..."
        #print "Before",self.hierspace,"hierpointer", id(self.hierpointer)
        nscope = parentscope
        newnode = None
        dimlen = 1
        if type(node) is OrBlock:
            nblks = len(node.blocks)
            newlst = self.orlst #[{} for i in range(nblks)]
            #print "ids of blks",",".join([str(id(x)) for x in newlst])
            #print "orlst:", newlst
            newnode = hp.choice(str(node.unqid), newlst)
            initvalue = self.initorlst[node.initv]
            #print node.unqid,node.initv,": OR initvalue", initvalue

        elif type(node) is OptionalStmt:
            optlst = [False, True]
            newnode = hp.choice(str(node.unqid), optlst)
            # get True index
            initvalue = optlst.index(node.initv)

        elif type(node) is OrStmt:
            nblks = len(node.stmts)
            newlst = list(range(nblks))
            newnode = hp.choice(str(node.unqid), newlst)
            self.spacesizetmp = len(newlst)
            initvalue = node.initv

        elif type(node) is SearchInteger:
            #lcmin = node.rangeexpr.min.value
            #lcmax = node.rangeexpr.max.value
            lcmin = optimizer.calcFuncExprVal(node, nscope, min).value
            lcmax = optimizer.calcFuncExprVal(node, nscope, max).value
            if lcmin > lcmax:
                logice.warning("SearchInteger {} min: {} max: {}"
                        " Incorrect!".format(node.unqid, lcmin, lcmax))
                raise ValueError
            #endif
            #FIXME step is no being used!
            newnode = hp.quniform(str(node.unqid), lcmin, lcmax, 1)
            dimlen = lcmax - lcmin + 1
            #print "update spacesize", dimlen,type(node)
            self.spacesizetmp = dimlen

            try:
                initvalue = node.initv.value
            except AttributeError as e:
                initvalue = lcmin
            #endtry

        elif type(node) is SearchPowerOfTwo:
            #lcmin = node.rangeexpr.min.value
            #lcmax = node.rangeexpr.max.value
            lcmin = optimizer.calcFuncExprVal(node, nscope, min).value
            lcmax = optimizer.calcFuncExprVal(node, nscope, max).value
            if lcmin > lcmax:
                warning("SearchPowerOfTwo {} min: {} max: {}"
                        " Incorrect!".format(node.unqid, lcmin, lcmax))
                raise ValueError
            #endif
            choicelst = [int(math.pow(2,x)) for x in
                    range(int(math.log(lcmin,2)),int(math.log(lcmax,2))+1)]
            newnode = hp.choice(str(node.unqid), choicelst)
            dimlen = len(choicelst)
            #print "update spacesize", dimlen,type(node)
            self.spacesizetmp = dimlen

            try:
                #index of the initvalue
                initvalue = choicelst.index(node.initv.value)
                #print "@@@ SearchPerm",initvalue
            except AttributeError as e:
                if lcmin in choicelst:
                    initvalue = choicelst.index(lcmin)
                else:
                    raise ValueError("Could not find default value (min) {} in the options!".format(lcmin))
                #
                #print("@@@ SearchPerm Except",initvalue)
            #endtry

        elif type(node) is SearchEnum:
            dimlen = len(node.values)
            newnode = hp.choice(str(node.unqid), list(range(dimlen)))
            #print "update spacesize", dimlen,type(node)
            self.spacesizetmp = dimlen
            #index of the initvalue
            initvalue = node.initv

        elif type(node) is SearchPerm:
            currlist = node.set
            if isinstance(node.set, ID):
                currlist = optimizer.getListPerm(node, nscope)
            #print "View 2", currlist.values, type(currlist.values)
            _li = [it.value for it in currlist.values]
            choicelst = list(itertools.permutations(_li))
            newnode = hp.choice(str(node.unqid), choicelst)
            self.spacesizetmp = len(choicelst)

            # Convert ListMaker type to regular list
            if node.initv == node.set:
                _lh = tuple(_li)
            else:
                _lh = tuple([it.value for it in node.initv.values])
            #

            # index of the initvalue
            initvalue = choicelst.index(_lh)

        else:
            raise NotImplementedError("Not implemented for this "
                    "type:"+str(type(node)))
        #endif
        self.hierpointer[str(node.unqid)] = newnode
        self.inithierptr[str(node.unqid)] = initvalue
        self.searchndinit[str(node.unqid)]= initvalue
        #print "hierpointer", id(self.hierpointer)
        #for k,v in self.hierpointer.iteritems():
        #    print k,")",v
        #print "==="
    #enddef

    # Do it in pos-order
    def generic_visit(self, node):
        #print "B gen_vis", node.unqid," : ", type(node), self.spacesizetmp #" = "#, node

        if hasattr(node, 'scope'):
            self.parentscope = node.scope

        if isinstance(node, self.astnode):
            self.searchnodes.append((self.parent, node, self.parentscope))

        if type(node) is OrBlock:
            oldorlst     = self.orlst
            oldinitorlst = self.initorlst
            oldpointer = self.hierpointer
            oldinitptr = self.inithierptr
            #print "oldpointer", id(oldpointer)
            self.orlst     = []
            self.initorlst = []
        #endif

        if type(node) is OrBlock or \
            type(node) is Block:
            #print "gen_vis", node.unqid, " : ", type(node)
            childspacesize = []
            #oldspacesizetmp = self.spacesizetmp
            self.spacesizetmp = 1
        #endif

        oldparent = self.parent
        oldparentscope = self.parentscope
        self.parent = node
        ind = 0
        for c in node:
            if type(node) is OrBlock:
                self.hierpointer = {}
                # track the index of each block in the orblk
                self.hierpointer[self.OR_IDX_STR] = ind
                self.orlst.append(self.hierpointer)
                #print "new hierpointer", id(self.hierpointer)

                self.inithierptr = {}
                self.inithierptr[self.OR_IDX_STR] = ind
                self.initorlst.append(self.inithierptr)
                #print "new initptr", id(self.inithierptr)
            #endif

            self.visit(c)
            if type(node) is OrBlock or \
                type(node) is Block:
                childspacesize.append(self.spacesizetmp)
                self.spacesizetmp = 1
            #endif

            ind += 1
        #endfor

        self.parent = oldparent
        self.parentscope = oldparentscope

        if type(node) is OrBlock:
            self.hierpointer = oldpointer
            self.inithierptr = oldinitptr
            #print "hierspace restablished",id(self.hierpointer)
        #endif

        if isinstance(node, self.astnode):
            self.update(node, self.parentscope)
        #endif

        if type(node) is OrBlock:
            # in hierarchical spaces it is a sum
            total = 0
            for v in childspacesize:
                #print "OR child",v
                total += v
            self.spacesizetmp = total
            #print "OR update spacesize", self.spacesizetmp
        elif type(node) is Block:
            total = 1
            for v in childspacesize:
                total *= v
            self.spacesizetmp = total
            #print "Block update", self.spacesizetmp
        #endif

        if type(node) is OrBlock:
            self.orlst = oldorlst
            self.initorlst = oldinitorlst
        #endif
        #print "END B gen_vis", node.unqid," : ", type(node), self.spacesizetmp #" = "#, node
    #enddef
#endclass

def main(args=None):
    from locus.main import main as smain
    smain(HyperOptTool, argparser, args)
###
