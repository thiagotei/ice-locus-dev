'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, socket, os
#import ..resultsdb.connect as rdb
from ..resultsdb.connect import connect
#import ..resultsdb.models as dbm
from ..resultsdb.models import (Experiment, Search, Variant, LocusFile, EnvVar,
        EnvValue, Configuration, ExpValue)
from ..resultsdb.misc import hashmycfg, hashlocusfile
from datetime import datetime
from locus.util.cmds import Commands
from locus.util.constants import ICE_CLR_FAIL, bcolors

log = logging.getLogger(__name__) 

class LocusDBResultHandler(object):

    def __init__(self, ctx=None, **kwargs):
        self.ctx = ctx
        self.locusdbhandlerinit(**kwargs)

    def locusdbhandlerinit(self, **kwargs):
        log.info(f"Connecting to database {self.ctx.iceargs.database}...")
        self.engine, self.session = connect(self.ctx.iceargs.database)
        log.info("Session started!")
        log.info(f"kwargs: {kwargs}")
        locfilename = self.ctx.iceargs.optfile
        rdirpath = os.path.abspath(self.ctx.iceargs.rundirpath)
        #print(f"FULL PATH {os.path.abspath(rdirpath)}")

        lochash, buflocfile = hashlocusfile(locfilename)
        self.loc = LocusFile.get(self.session, lochash, locfilename, 
                                buflocfile)

        # Store/Get the envvar from the DB
        # And add a possibly add new value.
        dbenvvars = []
        dbenvvalues= []
        for var, val in self.ctx.iceargs.getenvsreplaced.items():
            #print(f"Type envvar {var} {type(var)} value: {val} {type(val)}")
            dbvar = EnvVar.get(self.session, str(var))
            dbenvvars.append(dbvar)
            dbvalue = EnvValue.get(self.session, str(val), dbvar)
            dbenvvalues.append(dbvalue)
        ##

        self.searchdb = Search(hostname=socket.gethostname(),
                                    locusfile=self.loc,
                                    rundirpath=rdirpath,
                                    envvars=dbenvvars,
                                    envvalues=dbenvvalues,
                                    searchtool=kwargs['searchtool'],
                                    searchtech=kwargs['searchtech'],
                                    started_at=datetime.now())
    ###

    def __enter__(self):
        return self
    ###

    def __exit__(self, type, value, traceback):
        self.searchdb.finished_at = datetime.now()
        self.session.commit()
        self.session.close()
    ###

    def run(self, cfg,
            bldres, evalres, chkres, 
            metric, prettyprintcfg_fn, starttime, **kwargs):

        # Time is seconds passed relatively to the search process start.
        elapsed = (datetime.now() - starttime).total_seconds()

        log.info("ElapsedSearch: {:7.1f} (sec) Variant result cfgid := {} cfgexec := {} check := {} time := {} speedup := {} cc := {} cfg := {} ".format(elapsed, 
            "{:5d}".format(self.ctx.lastcfgid),
            "{:5d}".format(self.ctx.cfgsexecuted),
            None if chkres is None else chkres['returncode'],
            "{:7.2f}".format(metric),
            None if self.ctx.origmetric is None else "{:7.2f}".format(self.ctx.origmetric/metric),
            "X", prettyprintcfg_fn(self.ctx.searchnodes, cfg)))

        # If check command return is not ok, do not save in the database.
        if chkres is not None and chkres['returncode'] != 0:
            log.warning(f"{ICE_CLR_FAIL}Not saving on the DB, check did not return ok.{bcolors.ENDC}")
            return
        #

        #log.info(f"searchnodes: {self.ctx.searchnodes}")
        #log.info(f"cfg: {cfg}")
        ## Save in database

        dbcfg = Configuration.get(self.session, hashmycfg(cfg), cfg, self.loc)

        dbvar = Variant(timestamp=datetime.now(), searchcfgid=self.ctx.lastcfgid,
                        search=self.searchdb, configuration=dbcfg)

        exps = []
        #
        # If the experiment had multiple runs and they are provided, 
        # add them to the database.
        # Otherwise just the chosen metric (e.g., median).
        #
        if 'metricinfo' in kwargs and 'exps' in kwargs['metricinfo']:
            log.info(f"metricinfo: {kwargs}")
            metric_kwargs = kwargs['metricinfo']
            search_metric = metric_kwargs['metric']

            #unitv = 'N/A'
            #if 'unit' in kwargs['metricinfo']:
            #    unitv = kwargs['metricinfo']['unit']
            ##

            #for v in kwargs['metricinfo']['evals']:
            #    newexp=Experiment(metric=v, variant=dbvar, unit=unitv)
            #    exps.append(newexp)
            ###

            # DEPRECATED
            # res: {'metric': 406.17589, 'exps': {'Core': 
            #                                        {'evals': [406.41994, 406.17589, 406.1505, 405.99375, 408.83176], 
            #                                         'unit': 'milisec'}
            #                                    'Total' :
            #                                        {'evals': [406.41994, 406.17589, 406.1505, 405.99375, 408.83176], 
            #                                         'unit': 'milisec'}
            #                                   }}
#            # loop over each exp
#            for kexps, vexps in metric_kwargs['exps'].items():
#                unitv = 'N/A...'
#                if 'unit' in vexps:
#                    unitv = vexps['unit']
#                #
#
#                newexp = Experiment(variant=dbvar)
#
#                # loop over each expval
#                for v in vexps['evals']:
#                    newexpval = ExpValue(metric=v, unit=unitv, experiment=newexp, desc=kexps)
#                    exps.append(newexpval)
#                ##
#            ##

            # VALID
            # res: {'metric': 7.21292, 
            #       'exps': [(12.41161, 7.48545), (10.53209, 7.21292), 
            #                (10.50103, 7.21561), (10.49045, 7.20725), 
            #                (10.55268, 7.19714)], 
            #       'unit': ('milisec', 'milisec'), 
            #       'desc': ('Total', 'Core')}

            if 'unit' not in metric_kwargs:
                raise ValueError('Could not find unit in metric args!')
            #

            if 'desc' not in metric_kwargs:
                raise ValueError('Could not find desc in metric args!')
            #

            unitv = metric_kwargs['unit']
            if type(unitv) != tuple:
                raise ValueError('unit must be of tuple type!')
            #

            desc = metric_kwargs['desc']
            if type(desc) != tuple:
                raise ValueError('desc must be of tuple type!')
            #

            # loop over each exp
            for exp in metric_kwargs['exps']:
                newexp = Experiment(variant=dbvar)

                if type(exp) != tuple:
                    raise ValueError('Each value of exps must be of tuple type! '+str(type(exp)))
                #

                if len(exp) != len(unitv) or len(exp) != len(desc):
                    raise ValueError('Metric args exps, unit, and desc must have '
                                     ' the same length.')
                #

                for v, u, d in zip(exp, unitv, desc):
                    newexpval = ExpValue(metric=v,
                                         unit=u,
                                         desc=d,
                                         experiment=newexp)
                    exps.append(newexpval)
                ##
            ##
        else:
            #newexp=Experiment(metric=metric, variant=dbvar)
            newexp = Experiment(variant=dbvar)
            newexpval = ExpValue(metric=metric, experiment=newexp)
            exps.append(newexpval)
        #
        #self.session.add(newexp)
        self.session.add_all(exps)
        log.info(f"Saved in DB {len(exps)} experiments.")
        self.session.commit()
        #
    #enddef

    @staticmethod
    def hashmycfg(cfg):
        m = hashlib.sha256()
        for k, v in sorted(cfg.items()):
            m.update(str(k).encode())
            m.update(str(v).encode())
            #m.update(str().encode())
            m.update(b"|")
        ##
        return m.hexdigest()
    ###
###

#endclass


