'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, argparse, re

from locus.util.cmds import Commands
from .defaulthandlers import DefaultHandler
import locus.tools.search.misc.loreprj as loreprj
import locus.tools.search.misc.loredb as loredb

argparser = argparse.ArgumentParser(add_help=False)
argparser.add_argument('--loredb_save',
                        required=False, default=False, action='store_true',
                       help="Save the variants result on the DB.")

argparser.add_argument('--loredb_user',
                        required=False, default=None, 
                       help="User for the DB.")

argparser.add_argument('--loredb_ip',
                        required=False, default=None, 
                       help="Ip address for the DB's machine.")

argparser.add_argument('--loredb_pass',
                        required=False, default=None, 
                       help="Password  for the DB.")

argparser.add_argument('--loredb_bench',
                        required=False, default=None, 
                       help="Info about the benchmark to saved on DB's machine (6 infos separated by colon).")

argparser.add_argument('--loredb_cc', required=False, default='icc',
                       help="Which compiled is being used to save on the db.")

argparser.add_argument('--loredb_ccvs', required=False, default='17.0.1',
                       help="Which compiled is being used to save on the db.")

argparser.add_argument('--loredb_mach', required=False, default='E5-1630',
                       help="Which machine is being used to save on the db.")

log = logging.getLogger(__name__)

cclist  = ["sse", "avx", "avx2", "novec", "o3", "base"]
ccstats = ['min', 'max', 'mean', 'median', 'sd']

class LoredbHandler(DefaultHandler):
    INFO = ""

    def __init__(self):
        pass

    @classmethod
    def run(cls, cmd, timeout=None, limstd=600, limerr=600, _raise_=True):
        ccret = {'misc': {}, 'time': 0.0}

        for ccmode in cclist:
            revcmd = cmd.replace('CCMODE',ccmode)
            ccret['misc'][ccmode] = super(LoredbHandler, cls).run(revcmd, timeout, limstd, limerr, _raise_)
            ccret['time'] += ccret['misc'][ccmode]['time']
        #endfor

        ccret['stderr'] = ccret['misc']['avx2']['stderr']
        ccret['stdout'] = ccret['misc']['avx2']['stdout']
        ccret['returncode'] = ccret['misc']['avx2']['returncode']

        return ccret
    #enddef
#endclass

class LoredbPrebuildHandler(LoredbHandler):
    INFO = "LoredbPrebuild"

class LoredbBuildHandler(LoredbHandler):
    INFO = "LoredbBuild"

class LoredbEvalHandler(LoredbHandler):
    INFO = "LoredbEval"
    OUTEXTRA = ""

    @classmethod
    def run(cls, cmd, timeout=None, limstd=600, limerr=600, _raise_=False):
        ccret = super(LoredbEvalHandler, cls).run(cmd, timeout, limstd, limerr, _raise_)

        # tee commands masks the command return code, so instead of tee save the stdout on file from here.
        loopname = cmd.split()[3]
        for ccmode in cclist:
            revlname = loopname.replace('CCMODE',ccmode)
            fname = "output"+cls.OUTEXTRA+"_"+revlname[2:]+'.log'
            with open(fname, 'w') as outp:
                outp.write(ccret['misc'][ccmode]['stdout'])
            #
            log.info("saved stdout to file {}".format(fname))
        #

        return ccret
    #

class LoredbEvalHandlerOrig(LoredbEvalHandler):
    INFO = "LoredbEvalOrig"
    OUTEXTRA = "_orig"

class LoredbCheckHandler(LoredbHandler):
    INFO = "LoredbCheck" 

class LoredbResultHandler(object):

    def __init__(self, ctx=None):
       self.ctx          = ctx
       self.loredb_save  = self.ctx.iceargs.loredb_save
       self.loredb_ip    = self.ctx.iceargs.loredb_ip
       self.loredb_user  = self.ctx.iceargs.loredb_user
       self.loredb_pass  = self.ctx.iceargs.loredb_pass
       self.loredb_bench = self.ctx.iceargs.loredb_bench
       self.loredb_cc    = self.ctx.iceargs.loredb_cc
       self.loredb_ccvs  = self.ctx.iceargs.loredb_ccvs
       self.loredb_mach  = self.ctx.iceargs.loredb_mach
       self.ldb_lid      = None

    def __enter__(self):
        if self.loredb_save:
            if self.loredb_bench is None:
                raise ValueError("loredb_bench is missing!")
            else:
                ldb_bench_list = self.loredb_bench.split(':')
                if len(ldb_bench_list) != 6:
                    raise ValueError("Expecting 6 values separated by colon. Received {} .".format(self.loredb_bench))
                #endif
           #endif
            args = (self.loredb_ip, self.loredb_user, \
                    self.loredb_pass, "locus") 
            log.info("Creating the connection... {} {}".format(args, self.loredb_bench))
            loredb.connect(args)

            #Save loop infor in the DB.
            #ldb_bench_list[5] = int(ldb_bench_list[5])
            self.ldb_lid = loredb.insert_loop(*ldb_bench_list)
            loredb.commit()
            log.info("Saved loopid= {}".format(self.ldb_lid))

            if self.ctx.origres is not None:
                ccmet = LoredbResultHandler.parseStd(self.ctx.origres['misc'])
                #log.info("ccmet: {}".format(ccmet))
                valid = True
                # Check eval to fill the invalid mutation
                for valkey, valret in self.ctx.origres['misc'].items():
                    if valret['returncode'] != 0:
                        valid = False
                        break
                #   #

                mid = LoredbResultHandler.saveInLoreDB(ccmet, self.ldb_lid, {}, [],
                                                 self.loredb_cc, self.loredb_ccvs, self.loredb_mach, valid)
                log.info("Final outcome orig loop id := {} mutid := {} valid := {} ccmet := {}".format(self.ldb_lid, mid, 
                         valid, LoredbResultHandler.dictStr(ccmet)))
            #endif
        #endif
        return self
    #enddef

    def __exit__(self, type, value, traceback):
        # Closing connection to the db
        if self.loredb_save:
            loredb.disconnect()
            log.info("Closed the db connection.")
        #endif
    #enddef

    def run(self, cfg,
            bldres, evalres, chkres, 
            metric, prettyprintcfg_fn, **kwargs):
        dbmutationdict = {}
        dbmutationlist = []
        if self.loredb_save:
            # Info about the transformations to be save on the Database.
            dbmutationdict, dbmutationlist = loreprj.getTransfInfo(self.ctx.optuni)
            log.info("dbmutationdict: {}".format(dbmutationdict))
            log.info("dbmutationlist: {}".format(dbmutationlist))
        #endif

        # Process the results of the eval and check
        ccmet = LoredbResultHandler.parseStd(evalres['misc'])

        log.debug("ccmet {}".format(ccmet))

        valid = True
        # Check eval to fill the invalid mutation
        for valkey, valret in evalres['misc'].items():
            if valret['returncode'] != 0:
                valid = False
                break
        #   #

        # Check res to fill the field of invalid mutation
        if chkres:
            for valkey, valret in chkres['misc'].items():
                #log.info("valid? {} = {}".format(valkey, valret))
                if valret['returncode'] != 0:
                    valid = False
                    break
            #   #
        else:
            valid = False
        #

        mid = None
        if self.loredb_save:
            mid = LoredbResultHandler.saveInLoreDB(ccmet, self.ldb_lid, dbmutationdict, dbmutationlist,
                                                   self.loredb_cc, self.loredb_ccvs, self.loredb_mach,
                                                   valid)
        #else:
        #    log.warning("Because of unmatching results from the checkcmd the metrics were not saved to the database.")
        #endif
        log.info("Final outcome cfgid := {} cfgexec := {} loop id := {} mutid := {} valid := {} ccmet := {} cfg := {}".format(self.ctx.lastcfgid,
                self.ctx.cfgsexecuted,
                self.ldb_lid, mid, valid, LoredbResultHandler.dictStr(ccmet),
                prettyprintcfg_fn(self.ctx.searchnodes, cfg)))
    #enddef

    @staticmethod
    def saveInLoreDB(ccmet, ldb_lid, dbmutationdict, dbmutationlist, loredb_cc, loredb_ccvs, loredb_mach, valid=True):
        #compinfo = ("icc", "17.0.1")
        #machinfo = "E5-1630"
        compinfo  = (loredb_cc, loredb_ccvs)
        machinfo  = loredb_mach
        log.info("Saving metrics at the DB... ")
        if ldb_lid is not None:
            mid = loredb.insert_mutation(ldb_lid, dbmutationdict, dbmutationlist, valid)
            loredb.insert_execution(ldb_lid, mid, compinfo, machinfo, ccmet)
            loredb.commit()
            #####loredb.insert_execution(lid, mid,  ("icc", "17.0.1"), "E5-1630", {'vec_median' : metric } )
            log.info("   DB loopid= {} mutid= {}".format(ldb_lid, mid))
        else:
            log.error("Problem on loop insertion in the database!")
        #endif
        return mid
    #enddef

    @staticmethod
    def parseStd(stddic):
        pattern = re.compile(r"^ min\s+max.*")
        ccmet = {}
        for cc in cclist:
            std = stddic[cc]['stdout']
            lines_iter = iter(std.splitlines())
            r = None
            for l in lines_iter:
                mat = pattern.match(l)
                if mat:
                    r = next(lines_iter)
                    break
                #
            #
            #result = float('inf')
            log.debug("{} std: {}".format(cc, std))
            if r:
                m = re.split('\s*', r)
                log.debug("m: {}".format(m))
                if m and len(m) >= 5:
                    for val, typ in zip(m,ccstats):
                        if val:
                            ccmet[cc+"_"+typ] = float(val)
                        else:
                            ccmet[cc+"_"+typ] = float('inf')
                        #
                    #
                else:
                    for typ in ccstats:
                        ccmet[cc+"_"+typ] = float('inf')
                    #
                #
            #
        #endfor

        return ccmet
    #enddef

    @staticmethod
    def dictStr(ccmet):
        ccprint = ["{}: {}".format(k,ccmet[k]) for k in sorted(ccmet.keys())]
        return ccprint
#endclass

