'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging
from datetime import datetime
from locus.util.cmds import Commands

log = logging.getLogger(__name__) 

class DefaultHandler(object):
    INFO = ""

    def __init__(self):
        pass

    @classmethod
    def run(cls, cmd, timeout=None, limstd=600, limerr=600, _raise_=True):
        log.info("Command... {} timeout: {} raise: {}". format(cmd, timeout, _raise_))

        retdic = Commands.callCmdDic(cmd, timeout)
        retcode = retdic['returncode']
        stdout = "N/A" if retdic['stdout'] is None else retdic['stdout']
        stderr = "N/A" if retdic['stderr'] is None else retdic['stderr']
        elapsed = retdic['time']
        timedout = retdic['timeout']

        if _raise_ and retcode is not None and retcode:
            log.error("{} retcode: {}\nstdout:\n{}\nstderr:\n{}".format(
                cls.INFO, retcode, stdout[-limstd:], stderr[-limerr:]))
            raise RuntimeError("Problem running {} cmd!".format(cls.INFO))

        elif timedout:
            log.info("{} command timed out elapsed: {} retcode: {}".format(
                cls.INFO, elapsed, retcode))

        else:
            log.info("{} command outcome elapsed: {} retcode: {}\nstdout:\n{}{}stderr:{}{}".format(
                cls.INFO, elapsed, retcode, stdout[-limstd:], 
                '\n' if len(stdout) > 0 and stdout[-1] != '\n' else '',
                '\n' if len(stderr) > 0 else '',
                stderr[-limerr:]))

        return retdic
    #endif
#endclass

class PrebuildHandler(DefaultHandler):
    INFO = "Prebuild"

class BuildHandler(DefaultHandler):
    INFO = "Build"

class EvalHandler(DefaultHandler):
    INFO = "Eval"

class CheckHandler(DefaultHandler):
    INFO = "Check" 

class ResultHandler(object):

    def __init__(self, ctx=None):
        self.ctx = ctx

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def run(self, cfg,
            bldres, evalres, chkres, 
            metric, prettyprintcfg_fn, starttime, **kwargs):

        # Time is seconds passed relatively to the search process start.
        elapsed = (datetime.now() - starttime).total_seconds()

        log.info("ElapsedSearch: {:7.1f} (sec) Variant result cfgid := {} cfgexec := {} check := {} time := {} speedup := {} cc := {} cfg := {} ".format(elapsed, 
            "{:5d}".format(self.ctx.lastcfgid),
            "{:5d}".format(self.ctx.cfgsexecuted),
            None if chkres is None else chkres['returncode'],
            "{:7.2f}".format(metric),
            None if self.ctx.origmetric is None else "{:7.2f}".format(self.ctx.origmetric/metric),
            "X", prettyprintcfg_fn(self.ctx.searchnodes, cfg)))
    #enddef
#endclass


