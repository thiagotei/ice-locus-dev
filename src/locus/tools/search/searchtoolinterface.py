'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, argparse
from abc import ABCMeta, abstractmethod

from locus.util.misc import addBoolArg
from ..toolinterface import AbstractTool

argparser = argparse.ArgumentParser(add_help=False)
argparser.add_argument('--initseed',
                       required=False, default=False, action='store_true',
                       help='Use as initial seed for the search. '+
                       'The initial seed is defined from: init=<p> on range and '+
                       'permutation variables, '+
                       'the 1st block/stmt of the OR blocks/stmts, '+
                       'the 1st value of the enum lists, '+
                       'optional stmts are assumed as true.')

argparser.add_argument('--dbseeds', required=False, default=0, nargs='?',
                        const=10, type=int, metavar='n',
                        help='Use [n] fastest configurations as seeds for the search.'
                        ' They are taken from "similar" searches at the locus.db in current' 
                        ' directory. To control similiraty among searches use ' 
                        ' disregardenvvar=<> in .locus.yaml to tell which'
                        ' env vars to disconsider. ')

argparser.add_argument('--equery',
                        required=False, default=False, action='store_true',
                        help="[Experimental] Replace the queries information before the search starts.")

argparser.add_argument('--exprec',
                        required=False, default=False, action='store_true',
                        help='[Experimental] Expand the recursion on the Locus Language.')

argparser.add_argument('--approxexprec',
                        required=False, default=100, type=int,
                        choices=range(1,101), metavar='1-100',
                        help="[Experimental] Approximate expansion of the "+
                        "recursion on the Locus language. Integer values in the range [1,100] "+
                        "represents the %% of recursion calls expanded. "+
                        "Requires --exprec.")

argparser.add_argument('--saveexprec',
                        required=False, default=False, action='store_true',
                        help='[Experimental] Save the expanded recursion in '
                        'Locus format. Requires use of --exprec.')

addBoolArg(argparser, 'evalfail', default=True,
           help="Interrupt search upon any problem "
                "(e.g., transformation tool, code generation).")

addBoolArg(argparser, 'applyDFAOpt', default=True, 
           help="Apply compiler optimizations based on "
                "data-flow analysis to the Locus program before "
                "the search starts. It applies deadcode elimination, constant "
                "folding and constant propagation.")

log = logging.getLogger("AbstractSearchTool")

class AbstractSearchTool(AbstractTool, metaclass=ABCMeta):
    """ This is a superclass to all optimization tools.
    """

    def __init__(self, name):
        AbstractTool.__init__(self, name)
        log.debug("Init SearchTool class")

    @abstractmethod
    def convertOptUni(self, optuni):
        pass

    @abstractmethod
    def search(self, origmetric=None):
        pass
