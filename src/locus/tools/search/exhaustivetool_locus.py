'''

@author: thiago
'''

import logging, sys, os
import argparse, copy, time, math
from datetime import datetime

from .exhaustivesearch import EXH_STATUS_OK as STATUS_OK, EXH_STATUS_FAIL as STATUS_FAIL
from .exhaustivesearch.driver import ExhaustiveSearch 
from .exhaustivesearch.parameters import BooleanParameter, \
    PowerOfTwoParameter, EnumParameter, IntegerParameter, PermutationParameter

from locus.backend.codegen import CodeGen
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, Constant, ListMaker, FuncCall
from locus.tools.toolinterface import AbstractTool
from locus.tools.opts.opttoolinterface import AbstractOptTool
from locus.frontend.optlang.locus.locusvisitor import GenericVisitorWScope
from .searchtoolinterface import AbstractSearchTool, argparser as absstparser 
from locus.util.constants import ICE_OUTPUT_MODE_STDOUT,\
    ICE_OUTPUT_MODE_INPLACE, ICE_OUTPUT_MODE_SUFFIX,\
    ICE_CLR_INITSEED, ICE_CLR_FAIL, ICE_CLR_NEWBEST, bcolors

from locus.util.exceptions import OptimizationError, StopAfterInterruption,\
    BoundsException
from locus.backend.interpreter import topblkinterpreter
from .handlers.defaulthandlers import PrebuildHandler,\
    BuildHandler, EvalHandler, CheckHandler
from .handlers.locusdbhandlers import LocusDBResultHandler
from .context.searchctxes import SearchCtxES 
from .resultsdb.misc import getDBseeds

import locus.tools.search.searchbase as searchbase
import locus.tools.search.misc.loreprj as loreprj
import locus.frontend.optlang.locus.optimizer as optimizer

from locus.backend.genlocus import GenLocus

argparser = argparse.ArgumentParser(add_help=False,
                                    parents=[absstparser])

logice = logging.getLogger(__name__)

class ExhaustiveTool(AbstractSearchTool):
    """ Exhaustive search driver. """

    def __init__(self, 
            iceargs,
            searchctx=SearchCtxES,
            prebuildhandler=PrebuildHandler,
            buildhandler=BuildHandler,
            evalhandler=EvalHandler,
            checkhandler=CheckHandler,
            resulthandler=LocusDBResultHandler):

        self.ctx = searchctx(iceargs,
                               None,
                               None,
                               prebuildhandler,
                               buildhandler,
                               evalhandler,
                               checkhandler,
                               resulthandler) 

        AbstractSearchTool.__init__(self, "Exhaustive")

        logice.info(f"Search carried out by {bcolors.OKBLUE} Exhaustive {bcolors.ENDC}!")

        exlistargs = []

        if self.ctx.iceargs.ntests:
            logice.info("Max number of tests: {}".format(self.ctx.iceargs.ntests))
        #endif

        if self.ctx.iceargs.stop_after:
            logice.info("Stop search after {} minutes.".format(self.ctx.iceargs.stop_after))
        else:
            logice.info("No time limit for the search.".format())
        #endif

        # Holds the space parameters
        self.space = []

        # metric saved by successful hashes, indexed by cfgid
        self.trialsloss = {}

        # saving cfgint for each variant, indexed by cfgid
        self.mytrials = {}

        self.__seedcfg = []
        self.finalresult = None

        self.best_tid = None
        self.best_metric = float('inf')
    #enddef

    def convertOptUni(self, inpoptuni, parsedsrcfiles, orighashes):
        logice.debug("start")
        self.ctx.optuni = copy.deepcopy(inpoptuni)
        self.ctx.parsedsrcfiles = parsedsrcfiles
        self.ctx.orighashes = orighashes

        self.ctx.revertsearch = {}
        self.ctx.searchnodes = {}

        if self.ctx.iceargs.equery or self.ctx.iceargs.applyDFAOpt:
            topblkinterpreter(self.ctx.optuni.topblk)

            if self.ctx.iceargs.equery:
                loreprj.queryOpt(self.ctx.optuni, self.ctx.parsedsrcfiles, self.ctx.iceargs)
                logice.info("Queries information collected and replaced.")
            #

            if self.ctx.iceargs.applyDFAOpt:
                searchbase.applyDFAOpts(self.ctx.optuni, self.ctx.iceargs)
                logice.info("DFA Compiler optimizations applied.")
            #
        #endif

        if self.ctx.iceargs.exprec:
            searchbase.expandRecursion(self.ctx.optuni, self.ctx.iceargs)
        #endif

        # Global scope added to the search space.
        searchbase.buildSearchNodes(self.ctx.optuni.topblk, self.ctx.searchnodes)

        # Add each CodeReg to the search space.
        for name, ent in self.ctx.optuni.items():
            logice.debug("name: %s",name)
            searchbase.buildSearchNodes(ent, self.ctx.searchnodes)
        #endfor optuni

        # nodes that do not have a range valid will be removed from the search dict.
        self.__removenodes = []

        initseedcfg = {}

        for key, value in self.ctx.searchnodes.items():
            p, node, nscope = value
            paramkey = str(node.unqid)
            logice.debug("search vars: {} {}".format(paramkey, type(node)))

            if type(node) is OrBlock:
                self.space.append(EnumParameter(paramkey,
                    list(range(len(node.blocks)))))
                initvalue = node.initv

            elif type(node) is OptionalStmt:
                self.space.append(BooleanParameter(paramkey))
                initvalue = node.initv

            elif type(node) is OrStmt:
                self.space.append(EnumParameter(paramkey,
                    list(range(len(node.stmts)))))
                initvalue = node.initv

            elif type(node) is SearchEnum:
                #print "###Param###",node.values, type(node.values)
                self.space.append(EnumParameter(paramkey,
                    list(range(len(node.values)))))
                initvalue = node.initv

            elif type(node) is SearchPerm:
                #print "###Param###",node.set.values, type(node.set.values)
                # node.rangeexpr is a ListMaker type. It is comprise Constant types. 
                # need the conversion.
                currlist = node.set
                if isinstance(node.set, ID):
                    currlist = optimizer.getListPerm(node, nscope)
                #print "View 2", currlist.values, type(currlist.values)

                # Convert ListMaker type to regular list
                _li = [it.value for it in currlist.values]
                #print "SearchPerm",_li, type(_li)
                self.space.append(PermutationParameter(paramkey, _li))

                # Convert ListMaker type to regular list
                if node.initv == node.set:
                    _lh = _li
                else:
                    _lh = [it.value for it in node.initv.values]
                #
                initvalue = _lh

            elif type(node) is SearchPowerOfTwo:
                #TODO needs to dataflow here. It is implemented. Needs to change and test.
                #TODO needs to think about the semantics of the step here.
                # traverse AST get the max of all paths and put that value
                # during execution it will be curbed.
                _min = optimizer.calcFuncExprVal(node, nscope, min)
                _max = optimizer.calcFuncExprVal(node, nscope, max)

                if _min.value < _max.value:
                    self.space.append(PowerOfTwoParameter(paramkey,
                        _min.value, _max.value))
                else:
                    t = Constant(int, _min.value)
                    p.replace(node, t)
                    logice.warning("SearchPowerOfTwo {} min: {} max: {} set to {}".format(
                                    paramkey, _min.value, _max.value, t.value))
                    self.__removenodes.append(key)
                #endif

                try:
                    initvalue = node.initv.value
                except AttributeError as e:
                    initvalue = _min.value
                #endtry

            elif type(node) is SearchInteger:
                #TODO needs to dataflow here is implemented. It is implemented. Needs to change and test.
                #TODO add the step field here.
                _min = optimizer.calcFuncExprVal(node, nscope, min)
                _max = optimizer.calcFuncExprVal(node, nscope, max)

                if _min.value < _max.value:
                    self.space.append(IntegerParameter(paramkey,
                        _min.value, _max.value))
                else:
                    t = Constant(int, _min.value)
                    p.replace(node, t)
                    logice.warning("SearchInteger {} min: {} max: {} set to {}".format(paramkey, _min.value, _max.value, t.value))
                    self.__removenodes.append(key)
                #endif

                try:
                    initvalue = node.initv.value
                except AttributeError as e:
                    initvalue = _min.value
                #endtry

            else:
                logice.error("Could not find the correct type for the node {}"
                    "!".format(node))
            #endif

            initseedcfg[paramkey] = initvalue
        #endfor

        if self.__removenodes:
            for n_id in self.__removenodes:
                if n_id in self.ctx.searchnodes:
                    del self.ctx.searchnodes[n_id]
                else:
                    raise RuntimeError("The node {} should be in searchnodes! Weird!".format(n_id))
                ##
            ##
        ##

        if self.ctx.iceargs.initseed:
            raise NotImplementedError("--initseed not implemented for exhaustive search. Open an issue if you need it.") 
            self.__seedcfg.append(initseedcfg)
            logice.debug(searchbase.seedcfgsstr([initseedcfg], 
                                        "Initial seed for the search:"))
        #endif

        #
        # Get seeds from database.
        #
        if self.ctx.iceargs.dbseeds:
            raise NotImplementedError("--dbseeds not implemented for exhaustive search. Open an issue if you need it.") 
            dbseeds = getDBseeds(self.ctx)
            t_dbseeds = self.encodeInitCfgSeeds(dbseeds, initseedcfg)
            self.__seedcfg += t_dbseeds 
            logice.debug(searchbase.seedcfgsstr(t_dbseeds, 
                                            "Initial seeds from DB are:"))
        #

        if self.__seedcfg:
            logice.info(searchbase.seedcfgsstr(self.__seedcfg, 
                                    "All initial seeds for the search:"))
        else:
            logice.info(f"{ICE_CLR_INITSEED}No initial seeds!{bcolors.ENDC}")
        #

        # Check if there is anything to search!
        if not self.space:
            return False
        else:
            logice.info("Search space size: {}".format(self.search_space_size()))
            return True

    #enddef

    def search_space_size(self):
        space_size = 0
        logice.debug(len(self.space))
        if self.space:
            space_size = 1
            for v in self.space:
                logice.debug(str(type(v))+str(v.search_space_size()))
                space_size *= v.search_space_size() 
            #
        #
        logice.debug(str(space_size))
        return space_size
    #enddef

    def search(self, origmetric=None, origres=None):
        self.ctx.searchstarttime = datetime.now()
        srch_start = time.time()

        self.ctx.origmetric = origmetric
        self.ctx.origres = origres
        logice.info("Orig metric {}.".format(self.ctx.origmetric))

        # prebuild handler
        searchobj = self.ctx.optuni.search
        prebuildcmd = searchobj.execute(searchobj.prebuildcmd)
        if prebuildcmd is not None and \
           prebuildcmd and \
           self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            self.ctx.prebldhand.run(prebuildcmd)
        #endif

        exhobj = ExhaustiveSearch() 
        try:
            dbinfo = {'searchtool': 'exhaustive',
                      'searchtech': 'default'}
            with self.ctx.resulthandler(self.ctx, **dbinfo) as rh:
                self.ctx.reshandobj = rh
                oh = exhobj.exec(self.objective, 
                                    self.space,
                                    max_evals=self.ctx.iceargs.ntests)
            #endwith
        except StopAfterInterruption as e:
            logice.info("Search interrupted using stop-after value!")

        except KeyboardInterrupt as e:
            logice.info("Shutdown requested...exiting")
        finally:
            cfgint = None

            if self.best_tid is not None:
                cfgint = self.mytrials[self.best_tid]

                self.ctx.searchnodesPr = {j: self.ctx.searchnodes[j] 
                        for j in cfgint if j in self.ctx.searchnodes}

                # Converts the final best result into self.optuni to be used later.
                # For instance, on saving the best_<something>.locus file.
                searchbase.convertDesiredResult(self.ctx.searchnodesPr, 
                                                cfgint, self.ctx.revertsearch)
                self.finalresult = self.ctx.optuni
            #

            logice.info("Final Summary algo: exhaustive None bestid: {} "
                    "bestmetric: {} origmetric: {} "
                    "speedup: {} cfgexec: {} cfgstimeout: {} "
                    "cfgerror: {} cfgsinvalid: {} config: "
                    "{}".format(
                        self.best_tid, self.best_metric,
                        self.ctx.origmetric,
                        None if self.ctx.origmetric is None or self.best_metric is None 
                            else self.ctx.origmetric/self.best_metric,
                        self.ctx.cfgsexecuted, self.ctx.cfgstimeout,
                        self.ctx.cfgserror, self.ctx.cfgsinvalid,
                        None if cfgint is None 
                            else searchbase.prettyprintcfg(self.ctx.searchnodes, 
                                                            cfgint)))

            srch_end = time.time()
            self.ctx.timing[searchbase.TIME_SRCH].append(srch_end - srch_start)

            searchbase.printTimingReport(self.ctx.timing)
        #end try
    #enddef

    def objective(self, cfg):
        logice.info("### start (search time {} (sec))".format(
            (datetime.now() - self.ctx.searchstarttime).total_seconds()))

        if self.ctx.iceargs.stop_after and \
            searchbase.convergenceCriteria(self.ctx.searchstarttime,
                    self.ctx.iceargs.stop_after):
            raise StopAfterInterruption()
        #endif

        self.ctx.lastcfgid += 1
        varcfgid = self.ctx.lastcfgid

        logice.info(cfg)
        noScfgint = {int(key):val for key, val in cfg.items()}
        cfgint = dict(sorted(noScfgint.items()))

        # Saving the cfgint, but could extract from trials using space_eval and
        # calling _createcfgint
        self.mytrials[varcfgid] = cfgint

        # Create and prune a temporary search nodes 
        self.ctx.searchnodesPr = {j: self.ctx.searchnodes[j] for j in cfgint if j in self.ctx.searchnodes}

        #print "Current searchnodes",len(searchnodes_obj), searchnodes_obj

        # check bounds
        logice.info("@@@ cfg_id: {} -> {}".format(varcfgid,
            searchbase.prettyprintcfg(self.ctx.searchnodesPr, cfgint)))

        try:
            # Check searchable variable found is between the range.
            # It is important if searchable variables depends on another
            # searchable variables the values will only be known at this time,
            # at "runtime".
            optimizer.checkBounds(self.ctx.searchnodesPr, cfgint)

        except BoundsException as e:
            # Code generation failed, go to next!
            self.ctx.incCfgsInv()
            #logice.exception(e)
            logice.warning(e)
            # Fixed What to return when experiment fail?
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}
        #end except

        searchobj = self.ctx.optuni.search
        evalstat = searchbase.EVAL_ERROR
        metric = float("inf")

        try:
            logice.debug("cfgint:\n{}".format(cfgint))

            searchbase.convertDesiredResult(self.ctx.searchnodesPr, cfgint,
                    self.ctx.revertsearch)

            ###
            # Evaluate the global scope.
            # before interpreting global scope need to add the global scope
            # search variables need to be added to global scope.
            topblkinterpreter(self.ctx.optuni.topblk)

            ###
            # Evaluate the search def block statements that may depend on the
            # global scope.
            localbuildcmd = searchobj.execute(searchobj.buildcmd)
            localruncmd = searchobj.execute(searchobj.runcmd)
            localcheckcmd = searchobj.execute(searchobj.checkcmd)
            #endfor

            #self.convertdebug.write("%%% End Cfg {}.\n".format(cfg_id))
            gencode_start = time.time()
            undoList = CodeGen.genCode(self.ctx.parsedsrcfiles, self.ctx.orighashes, 
                    self.ctx.optuni, self.ctx.iceargs, varcfgid)
            gencode_end = time.time()

            self.ctx.timing[searchbase.TIME_GEN].append(gencode_end - gencode_start)

        except OptimizationError as e:
            self.ctx.cfgserror += 1
            #return {'status': STATUS_FAIL}
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}

        except Exception as e:
            self.ctx.cfgserror += 1

            if self.ctx.iceargs.evalfail:
                raise e
            #

            # Code generation failed, go to next!
            logice.exception(e)
            #logice.error(e)

            # Fixed What to return to when experiment fail?
            #return {'status': STATUS_FAIL}
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}

        else:
            if self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
                metric, evalstat = searchbase.empiricalEvaluation(self.ctx,
                                                                  localbuildcmd, localruncmd,
                                                                  localcheckcmd, cfgint)

                if evalstat == searchbase.EVAL_TIMEOUT:
                    self.ctx.cfgstimeout += 1
                elif evalstat == searchbase.EVAL_ERROR:
                    self.ctx.cfgserror += 1
                #
            #endif

        finally:
            searchbase.revert2origsearchnodes(self.ctx.searchnodesPr,
                    self.ctx.revertsearch)
            logice.debug("Returned the optuni to original state.")
        #end try

        # Number of configurations executed
        self.ctx.incCfgsExec()

        searchbase.variantEpilog(undoList, self.ctx.iceargs.output)

        if evalstat == searchbase.EVAL_ERROR:
            if self.ctx.iceargs.evalfail:
                raise RuntimeError(f"{ICE_CLR_FAIL}Search interrupted by error!{bcolors.ENDC}")
            #
            resobj = {'loss':float("inf"), 'status': STATUS_FAIL}
        else:
            resobj = {'loss': metric, 'status': STATUS_OK}
            if metric < self.best_metric:
                improv = 100*(1 - (metric/self.best_metric))
                self.best_tid = varcfgid
                self.best_metric = metric
                logice.info(f"{ICE_CLR_NEWBEST}New best configuration found! "\
                        f"{self.best_tid}: {self.best_metric} "\
                        f"+{improv:.2f}%{bcolors.ENDC}")
            #
        #

        self.trialsloss[varcfgid] = metric
        logice.info("end")
        return resobj
    #enddef

def main(args=None):
    from locus.main import main as smain
    smain(ExhaustiveTool, argparser, args)
###
