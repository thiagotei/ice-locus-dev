'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, os, time, math, copy
from datetime import datetime


from locus.frontend.optlang.locus.locusvisitor import GenericVisitorWScope
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt, IfStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, ListMaker, FuncCall, processCallArgList,\
    Scope, Argument, OptSeqDef, CodeRegDef, _assignID, _lookupID
from locus.frontend.optlang.locus.optimizer import findScopeIdNew

log = logging.getLogger(__name__)

space = 0
#key is unique key for each recursive call, it is to have unique stack variables
def recCheckPrintStack(retexpr, reccalls, othercalls, scope, interiorbody,
        leafbody, ts, suf="0"):
    global space

    log.debug("Starting recCheckPrintStack {}".format(scope))

    #print retexpr, scope
    stop = False if retexpr is None else retexpr.execute(scope)
    #print "EITA", type(stop), stop, scope 
    log.debug("stop: {} {} {}".format(type(stop), stop, scope))

    if not stop:
        nodestomove = []
        for rcpar, rcnode, rcscop, pathnodes in reccalls:
            #print " "*space,"### Entering ", id(rcnode)
            space += 1
            #print "reccall scope", rcscop, " vs pscope",scope,"vs ts.scope", ts.scope
            #newscope = copy.deepcopy(scope)
            #tmprcscop = copy.deepcopy(rcscop)
            #tmprcscop.parent = scope.parent
            #newscope = tmprcscop.customCopy()
            #newscope.parent = scope
            newscope = ts.scope.customCopy()
            tmprcscop = rcscop.customCopy()
            #print "newscope",id(newscope),newscope
            #print "tmprcscop",id(tmprcscop), tmprcscop

            # Reset the parent scopes, so they are clean.
            # pathnodes does not include the globalscope. It
            # includes from the OptSeq scope and down.
            for par, node, scp in pathnodes:
                #print id(node), type(node), scp, id(scp)
                for key, val in scp.items():
                    scp[key] = None
                #endfor
            #endfor

            #print "ts.scope --",  ts.scope, id(ts.scope)
            # Sync the scope received and the one copied.
            # Here the parameters of the OptSeq are assigned to
            # the scope, so it can execute its expressions.
            for key, val in scope.items():
                #print "updating up loop:", key, val
                _assignID(ID(key), val, tmprcscop) 
            #endif

            inpcheck = []
            # find var
            # Assuming arguments of type <id>
            #log.info("rcnode arglist: {}".format(rcnode.arglist))
            for i, arg in enumerate(rcnode.arglist):
                #actargexp = arg.expr
                #if arg.id is not None:
                actargexp = arg.id
                log.info("arg {} ... {} {}".format(actargexp,
                    id(tmprcscop), tmprcscop))
                res = findScopeIdNew(actargexp, tmprcscop)
                #print "arg ", actargexp," = ", res, id(tmprcscop), tmprcscop
                #else:
                #    res = arg.expr

                inpcheck.append(res)
            #endfor

            # If the arguments of the recursion call matches the parent call: Stop recursion!
            argsmatch = True

            # if the recfunction arguments have the same value
            for i, formarg in enumerate(ts.args.args):
                if formarg.id.name in scope and scope[formarg.id.name] is not None:
                    #print i,":",formarg.id.name,"=",scope[formarg.id.name]," ? ",inpcheck[i]
                    if scope[formarg.id.name] != inpcheck[i]:
                        argsmatch = False
                        break
                else:
                    RuntimeError("Argument {} supposed to be in scope.".format(formag.id.name))
                #endif
            #endfor

            ifret = checkIfs(rcnode, pathnodes, scope)
            #print "checkIfs", ifret

            # Backup just the dictionary of the scope.
            savedcurscp = [copy.deepcopy(scp.table) for par, node, scp in pathnodes]

            # Had to make a full copy because the it's used a
            # customCopy.
            #savedtmprcscop = copy.deepcopy(tmprcscop)

            if not argsmatch and ifret:
                # add the func block to the tree
                #processCallArgList(rcnode.arglist, scope, ts.scope, newscope, ts.args.args)
                processCallArgList(rcnode.arglist, tmprcscop, scope, newscope, ts.args.args)
                #processCallArgList(rcnode.arglist, rcscop, ts.scope, newscope, ts.args.args)
                #print "Start",",".join(str(k)+":"+str(v) for k,v in newscope.iteritems())
                #print "newscope",newscope
                child = recCheckPrintStack(retexpr, reccalls, othercalls, newscope,
                        interiorbody, leafbody, ts, str(int(suf) + 1))
                #if child is None:
                #    print "Child None"
                #nodestomove.append((orndchi, rcpar, child, rcnode, savedtmprcscop))
                #nodestomove.append((pathnodes, rcpar, child, rcnode, savedtmprcscop))
                nodestomove.append((pathnodes, rcpar, child, rcnode, None))
                #endif
            else:
                log.debug("Recursive call the same as the parent call. Avoided infinite recursion.")
                #nodestomove.append((pathnodes, rcpar, None, rcnode, savedtmprcscop))
                nodestomove.append((pathnodes, rcpar, None, rcnode, None))
            #endif

            # After the recursion needs to put back of the
            # pathnodes of the rcnode.
            for (_, _, scp), bkpscp in zip(pathnodes, savedcurscp):
                for key, val in scp.items():
                    scp[key] = bkpscp[key]
                #endfor
            #endfor

            space -= 1
            #print " "*space,"$$$ Leaving ", id(rcnode)
        #endfor

        dictparnone = {}
        dictpartlt  = {}
        dictpar = {}

        # Goal: Remove blocks from OR blocks that have no calls (also are not a
        # leaf), useless.
        # separate the nodestomove by parent. If all the nodes of the same
        # parent are to be moved, the full parent block is to be removed.
        # there is one element in nodestomove for each recursive call.
        # This is is supposed to with nested or blocks as well.
        for pathnodes, par, chi, no, _ in nodestomove:
            # on the path the recursive call, find all blocks that has an OR
            # block as parent. Look for the blocks in reverse order.
            for pthpar, pthno, pthscp in pathnodes[::-1]:
                if isinstance(pthpar, OrBlock):
                    idx = pthno
                    if idx not in dictpar:
                        dictpar[idx]     = []
                    if idx not in dictparnone:
                        dictparnone[idx] = 0
                    if idx not in dictpartlt:
                        dictpartlt[idx]  = 0

                    dictpartlt[idx] += 1

                    if chi is None:
                        dictparnone[idx] += 1

                    dictpar[idx].append((pthpar, par, chi, no))
                #endif
            #endfor pathnodes
        #endfor nodestomove

        #print "dictpar:",dictpar
        #print "dictparnone",dictparnone
        #print "dictpartlt",dictpartlt

        # If None replace the call with None; stmt.
        # This list holds them.
        noneconstlst = []

        #for k, v in scope.iteritems():
        #    for _, _, _, _, sc in nodestomove:
        #        v2 = _lookupID(ID(k), sc)
        #        if v != v2:
        #            raise RuntimeError("Scope inconsistent!!!")
        #endfor

        addedStkVars = []

        # After getting the children, gen replace
        # on the block
        #log.debug("Adding params suf: {}".format(suf))
        for pathnodes, par, chi, no, _ in nodestomove:
            if chi is None:
                ah = Constant(type(None), None)
                par.replace(no, ah)
                noneconstlst.append(ah)
            else:
                # Add the parameters of the replaced call as assignment

                # Get the index of chi to insert the stack variables
                # before it. Also, save these stmts inserted because they
                scp = par.scope

                #Go trhouth the scope and save all the variables (unique, only
                #closest ones)
                # 1. go through the scope up to function scope and build a dic
                # of vars
                # 2. for var in the dic created lookup their value and created a
                # new temporary var + key to story.
                # After the call needs to undo the process by saving the value
                # on the temporary var to actual var!
                varsreach = getReachVars(scp)
                #log.debug("varsreach: {}".format(varsreach.keys()))
                addedStkVars += saveScopeVarsBeforeAfter(par, no, varsreach, suf)

                # The actual argmuents are assigend using the formal names.
                addedStkVars += argsAssignStmts(scope, ts.args.args, no.arglist, par, no)
                #log.debug("addedStkVars: {}".format([str(a) for b,a in addedStkVars]))

                # Replace the call with the children
                par.replace(no, chi)
        #endfor
        #log.debug("END adding params suf: {}".format(suf))

        remoblks = []

        # if all the rec calls are to None, remove the entire block from the OrBlock.
        for idxblk in dictpar:
            if dictparnone[idxblk] == dictpartlt[idxblk]:
                # access the 1st of the list. should be enough.
                pthpar, _, _, _ = dictpar[idxblk][0]
                posblk = pthpar.blocks.index(idxblk)
                remoblks.append((posblk, pthpar, idxblk))
        #endfor

        # First, get the parents and their position, then actually remove.
        # pos: index of the blk in pthpar list.
        # pthpar: is the Orblock parent of blk.
        # blk: is the block ancestor (may be not parent) of the recursive
        #      call and children of the pthpar.
        #for pos, pthpar, par in remoblks:
        for pos, pthpar, blk in remoblks:
            log.debug("Removing block...")
            pthpar.remove(blk)
        #endfor

        # Copied the original block
        newblock = copy.deepcopy(interiorbody)

        # Put the removed blocks back. 
        #sortedremoblks = sorted(remoblks, reverse=True, key=lambda tup: tup[0])
        sortedremoblks = sorted(remoblks, key=lambda tup: tup[0])

        for pos, pthpar, blk in sortedremoblks:
            log.debug("Adding back block...")
            pthpar.blocks.insert(pos, blk)
        #endfor

        # Return the block to the original state
        for _, par, chi, no, _ in nodestomove:
            if chi is None:
                par.replace(noneconstlst.pop(0), no)
            else:
                par.replace(chi, no)
        #endfor

        # Remove the stack variables added
        for par, stmt in addedStkVars:
            par.remove(stmt)
            #log.debug("Removed stmt: {} par: {}".format(stmt,par))

        #log.debug("After putting everything back {}".format(interiorbody))

        return newblock

    else:
        allequal = allArgsEqual(scope, ts.args.args)

        # If all the arguments of the recursive optset are equal avoid it!
        if allequal:
            tmpbody = None
        else:
            tmpbody = copy.deepcopy(leafbody) 

            # Replace the variables by a their values in the scope.
            #for k, v in scope.iteritems():
            #    log.debug("[Leaf] Replace scope:  {} <= {} ".format(k, v))
            #    replaceVars(tmpbody, k, v)
            #endfor
        #endif

        #print "returning a tmpbody:",tmpbody
        #print "-------------------"
        return tmpbody
    #endif
#enddef

def checkIfs(reccal, parlist, optseqinpt):
    #print "CheckIFs: "

    auxscplist = []
    # Save the all the scope
    for par, node, scp in parlist:
        #print id(node), type(node), scp, id(scp)
        auxscp = {}
        for key, val in scp.items():
            auxscp[key] = val
            #scp[key] = None
        #endfor
        auxscplist.append(auxscp)
    #endfor

    # Update scope of the 1st block of the list that representes
    # the optseq. 
    _, _, optseqscp = parlist[0]

    # Sync the scope received and the one copied.
    for key, val in optseqinpt.items():
        #print "updating", key, val
        _assignID(ID(key), val, optseqscp)
    #endif

    retval = True

    # Execute all the ifs
    for par, node, scp in parlist:
        if isinstance(node, IfStmt):
            # check only the 1st if
            ifcond = node.ifblks[0][0]
            res = findScopeIdNew(ifcond, scp)
            #print "ifcond:",ifcond,type(ifcond),"res:",  res
            if not res:
                retval = False
                break
            #endif
        #endif
    #endfor

    # Restore the all the scope
    for par, node, scp in parlist:
        #print id(node), type(node), scp, id(scp)
        auxscp = auxscplist.pop(0)
        for key, val in scp.items():
            scp[key] = auxscp[key]
            #scp[key] = None
        #endfor
    #endfor

    return retval
#enddef

#Get All the variables reachable to a scope, up to CodeReg or OptSeq. returns a list
def getReachVars(scp):
    varsreach = {}
    while scp:
        #print "getReachVars",type(scp.parent)
        # Stop the var search up to OptSeq or Codereg.
        # To avoid getting the vars in global scope, I check
        # befere whether this is last, i.e. top scope. If yes
        # stop right away. top scope has not parent scope.
        if scp.parent is None:
            break
        #

        for key, val in list(scp.items()):
            if key not in varsreach:
                varsreach[key] = val
            #endif
        #endfor
        scp = scp.parent
    #endwhile
    return varsreach
#enddef

# Saves all the scope vars (except the global) in temporary
# variables. par is the parent of marknode, varsreach are all
# the variables. 
def saveScopeVarsBeforeAfter(par, marknode, varsreach, suf):
    addedStkVars = []
    for name, val in sorted(varsreach.items()):
        #log.debug("saveScopeVarsBeforeAfter adding-> {} {}".format(name, suf))
        assI = AssignStmt(AssignStmt.EQUAL, ID(name+"_"+str(suf)), ID(name))
        par.insertBefore(marknode, assI)
        addedStkVars.append((par, assI))

        assI = AssignStmt(AssignStmt.EQUAL, ID(name), ID(name+"_"+str(suf)))
        par.insertAfter(marknode, assI)
        addedStkVars.append((par, assI))
    #endfor
    return addedStkVars
#endif

# create the arguments
def argsAssignStmts(scope, formargs, actargs, par, marknode):
    newstmts = []
    for i, (form, act)  in enumerate(zip(formargs, actargs)):
        #actargexp = arg.id
        #val = scope
        if act.expr is not None:
            actobj = copy.deepcopy(act.expr)
        elif act.id is not None:
            actobj = ID(act.id.name)
        #endif
        #print "form: ", form.id.name, "act:", actobj, type(actobj)
        assI = AssignStmt(AssignStmt.EQUAL, ID(form.id.name), actobj)
        par.insertBefore(marknode, assI)
        newstmts.append((par, assI))
    #endfor
    return newstmts
#endif

#if numbers scope received are all equal ret null?
def allArgsEqual(scope, tsargs):
    pastVal = None
    for i, formarg in enumerate(tsargs):
        if formarg.id.name in scope and scope[formarg.id.name] is not None:
            if i == 0:
                pastVal = scope[formarg.id.name]
            else:
                if pastVal != scope[formarg.id.name]:
                    return False
                #endif
            #endif
        else:
            RuntimeError("Argument {} supposed to be in scope.".format(formag.id.name))
        #endif
    #endfor
    return True
#enddef

# Replace the argument on other calls that are not recursive, e.g.,
# transformations.
# nodestove has the scope updated of the recursive calls.
# It is used to fill the params on the other calls.
def replaceVarsOtherCalls(nodestomove, othercalls, replvars):
    # for each reccall, replace all arguments not set
    for parot, noot, _  in othercalls:
        #for pathnodes, par, chi, no, sc in nodestomove:
        for pathnodes, par, chi, _, _ in nodestomove:
            # Go through pathnodes, which is ancestor list to reccall
            lstpar = [s for _, x, s in pathnodes if x == par]
            # get closest scope that has the same parent as the 
            # othercalls.
            sc = lstpar[-1]

            #if they have the same parent, use its scope.
            #if chi is not None and parot == par:
            if chi is not None:
                # Goes through the arguments of the call
                for arg in noot.arglist:
                    if arg.expr is None:
                        newv = _lookupID(arg.id, sc)
                        #print arg.id,type(newv), newv
                        arg.replace(_, newv)
                        replvars.append((parot, arg))
                    #endif
                #endfor
                break
            #endif
        #endfor
    #endfor
#enddef

# Replace all the Function arguments (ID) that are None to the new value. 
def replaceVars(node, var, new):
    senodes = ID
    visitor = GenericVisitorWScope(senodes)
    visitor.visit(node)
    for par, _id, scop in visitor.values:
        #log.info("ID {} ({}) == {} ({}) ?".format(_id, type(_id), var, type(var)))
        if _id.name == var:
            #log.info("---- Yes. par {} par id {} - this {} by {} ({})".format(type(par),id(par), _id, new, type(new)))
            tmpnew = copy.deepcopy(new)
            # Only replace arguments if expr is None.
            if isinstance(par, Argument):
                if par.expr is None:
                    par.replace(_id, tmpnew)
            else:
                par.replace(_id, tmpnew)
                #raise RuntimeError("Expecting only dealing with Arguments.")
            #endif
    #endfor
#enddef


