'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

from locus.tools.search.handlers.defaulthandlers import PrebuildHandler,\
        BuildHandler, EvalHandler, CheckHandler, ResultHandler

import locus.tools.search.searchbase as searchbase

class SearchCTX(object):
    
    def __init__(self, iceargs, 
                       parsedsrcfiles=None,
                       optuni=None,
                       prebuildhandler=PrebuildHandler,
                       buildhandler=BuildHandler,
                       evalhandler=EvalHandler,
                       checkhandler=CheckHandler,
                       resulthandler=ResultHandler):
        self.iceargs         = iceargs
        self.parsedsrcfiles  = parsedsrcfiles
        self.optuni          = optuni
        self.prebldhand      = prebuildhandler
        self.bldhand         = buildhandler
        self.evalhand        = evalhandler
        self.chkhand         = checkhandler
        self.resulthandler   = resulthandler
        self.reshandobj      = None
        self.orighashes      = None
        self.revertsearch    = None
        self.searchnodes     = None
        self.searchstarttime = None
        self.origmetric      = None
        self.origres         = None

        self.timing = {searchbase.TIME_GEN:[], 
                       searchbase.TIME_CHK:[], 
                       searchbase.TIME_RUN:[], 
                       searchbase.TIME_BLD:[], 
                       searchbase.TIME_SRCH:[]}

        # Roof limit: number of times of the fastest experiment so far0
        self.timeoutfactor = self.iceargs.timeoutfactor
        # Elapsed total time of fastest experiment
        self.elapsedmin = None
        # Metric time of the fastest experiment
        self.metricmin = float("inf")

        #self.finalresult  = None
        self.cfgsexecuted = 0

        # Total cfgs suggested that violate the check bounds
        # on dependent search variables.
        self.cfgsinvalid  = 0
        # Unique id for each suggested variant
        self.lastcfgid    = -1
        # Total cfgs that timed out
        self.cfgstimeout = 0
        # Total cfgs that had error on gen code
        self.cfgserror = 0
    #enddef

    def incCfgsInv(self):
        self.cfgsinvalid += 1
    #enddef

    def incCfgsExec(self):
        self.cfgsexecuted += 1
    #enddef
