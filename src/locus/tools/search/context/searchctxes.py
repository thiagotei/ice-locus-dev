'''

@author: thiago
'''

from locus.tools.search.handlers.defaulthandlers import PrebuildHandler,\
        BuildHandler, EvalHandler, CheckHandler, ResultHandler

from locus.tools.search.context.searchctx import SearchCTX 

class SearchCtxES(SearchCTX):

    def __init__(self, iceargs, 
                       parsedsrcfiles=None,
                       optuni=None,
                       prebuildhandler=PrebuildHandler,
                       buildhandler=BuildHandler,
                       evalhandler=EvalHandler,
                       checkhandler=CheckHandler,
                       resulthandler=ResultHandler):

        super(SearchCtxES, self).__init__(iceargs, 
                       parsedsrcfiles=None,
                       optuni=None,
                       prebuildhandler=PrebuildHandler,
                       buildhandler=buildhandler,
                       evalhandler=evalhandler,
                       checkhandler=checkhandler,
                       resulthandler=resulthandler)
    #enddef

