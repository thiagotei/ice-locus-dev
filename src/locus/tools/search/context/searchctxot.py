'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

from locus.tools.search.handlers.defaulthandlers import PrebuildHandler,\
        BuildHandler, EvalHandler, CheckHandler, ResultHandler

from locus.tools.search.context.searchctx import SearchCTX 

class SearchCtxOT(SearchCTX):

    def __init__(self, iceargs, 
                       parsedsrcfiles=None,
                       optuni=None,
                       prebuildhandler=PrebuildHandler,
                       buildhandler=BuildHandler,
                       evalhandler=EvalHandler,
                       checkhandler=CheckHandler,
                       resulthandler=ResultHandler):

        super(SearchCtxOT, self).__init__(iceargs, 
                       parsedsrcfiles=None,
                       optuni=None,
                       prebuildhandler=PrebuildHandler,
                       buildhandler=buildhandler,
                       evalhandler=evalhandler,
                       checkhandler=checkhandler,
                       resulthandler=resulthandler)
    #enddef

