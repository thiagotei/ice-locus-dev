'''

@author: thiago
'''
import logging, sys, os
import argparse, copy, time, math
import itertools
from datetime import datetime

import chocolate as choco

from .searchtoolinterface import AbstractSearchTool, argparser as absstparser
from locus.util.exceptions import OptimizationError, StopAfterInterruption,\
    BoundsException
from locus.backend.interpreter import topblkinterpreter
from locus.backend.codegen import CodeGen
from locus.tools.search.handlers.defaulthandlers import PrebuildHandler,\
        BuildHandler, EvalHandler, CheckHandler, ResultHandler
from locus.tools.search.context.searchctxhy import SearchCtxHY
from locus.util.constants import ICE_OUTPUT_MODE_STDOUT, bcolors,\
    ICE_TOP_BLK_STR, ICE_CLR_INITSEED
from locus.frontend.optlang.locus.nodevisitor import NodeVisitor
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, Constant, ListMaker, FuncCall, Block, OptSeqDef

import locus.frontend.optlang.locus.optimizer as optimizer
import locus.tools.search.searchbase as searchbase


argparser = argparse.ArgumentParser(add_help=False,
                                    parents=[absstparser])

logice = logging.getLogger(__name__)

STATUS_FAIL = 0
STATUS_OK = 1

class ChocoOptTool(AbstractSearchTool):
    """Chocalate driver to navigate the optimization space. """

    def __init__(self, iceargs, 
            searchctx=SearchCtxHY,
            prebuildhandler=PrebuildHandler,
            buildhandler=BuildHandler,
            evalhandler=EvalHandler,
            checkhandler=CheckHandler,
            resulthandler=ResultHandler):

        self.ctx = searchctx(iceargs,
                               None,
                               None,
                               prebuildhandler,
                               buildhandler,
                               evalhandler,
                               checkhandler,
                               resulthandler) 

        self.space = None
        # Initial configuration
        self.__seedcfg = {}

        # metric saved by successful hashes, indexed by cfgid.
        # decided to save this because the results are save in a 
        # panda dataframe that if not deleted may contain info
        # from multiple runs. 
        # This contains only info of the this run.
        self.trialsloss = {}

        self.mintrial = (float("inf"), 0)

        self.finalresult = None
    ###

    def _buildSearchNodes(self, ent):
        #print ent
        #print "==="
        senodes = OrBlock, OptionalStmt, OrStmt,\
                SearchEnum, SearchPerm, SearchPowerOfTwo
        visitor = GenericHierarchicalVisitor(senodes)
        visitor.visit(ent)

        spacesize = visitor.spacesizetmp
        #print "Final space size", spacesize
        dictid = {n.unqid:(p,n,s) for p,n,s in visitor.searchnodes}
        #print(visitor.hierpointer)
        #for k,v in visitor.hierpointer.items():
        #    print(str(k)+": ",v.pprint(sys.stdout)," --")
        #    print str(k)+": ",v

        initseed = visitor.searchndinit

        return visitor.hierpointer, dictid, initseed, spacesize
    #enddef

    def convertOptUni(self, inpoptuni, parsedsrcfiles, orighashes):
        logice.debug("start")
        self.ctx.optuni = copy.deepcopy(inpoptuni)
        self.ctx.parsedsrcfiles = parsedsrcfiles
        self.ctx.orighashes = orighashes

        if self.ctx.iceargs.initseed:
            #TODO Looks like chocolate does not support running the initseed.
            # So, the idea would be run the initseed and add it to the database.
            raise ValueError("--initseed yet not implemented for chocolate tool. "
                            "Remove --initseed and try again.")
        #

        if self.ctx.iceargs.equery:
            loreprj.queryOpt(self.ctx.optuni, self.ctx.parsedsrcfiles, self.ctx.iceargs)
            log.info("Queries information collected and replaced.")
        #endif

        if self.ctx.iceargs.exprec:
            searchbase.expandRecursion(self.ctx.optuni, self.ctx.iceargs)
        #endif

        # Hierarchical representation of the space. traverse the space
        # conditionally.
        self.space = [{'locus': 'all'}]

        # Search node dict has information of parent, scope for each search node.
        self.ctx.searchnodes = {}

        # Revert search dict saves how to revert the search nodes to the
        # original state.
        self.ctx.revertsearch = {}

        totalspace = 0
        topblkspacesize = 0

        # Add each CodeReg to the search space.
        for name, ent in self.ctx.optuni.allspaces():
            logice.debug("Adding codereg {}{}{} to the space...".format(
                bcolors.OKBLUE, name, bcolors.ENDC))

            tmpspace, dictid, initseed, spacesize = self._buildSearchNodes(ent)
            self.space[0].update(tmpspace)
            self.ctx.searchnodes.update(dictid)

            if name == ICE_TOP_BLK_STR:
                topblkspacesize = spacesize
            #endif

            logice.info("Done adding codereg {}{}{} to space. space size: {} (incl topblk: "
                    "{})".format(bcolors.OKBLUE, name, bcolors.ENDC,
                        spacesize, spacesize*topblkspacesize))
            #totalspace *= spacesize
            totalspace = totalspace * spacesize if totalspace > 0 else spacesize

            # Aggregate the seed from all ent
            if self.ctx.iceargs.initseed:
                self.__seedcfg[name] = initseed
            #endif
        #endfor

        if self.ctx.iceargs.initseed:
            raise NotImplementedError("initseed not implemented yet. "
                                      "not sure choco accepts it.")
        ##

        return True
    ###

    def search(self, origmetric=None, origres=None):
        self.ctx.searchstarttime = datetime.now()

        self.ctx.origmetric = origmetric
        self.ctx.origres = origres
        logice.info("Orig metric {}.".format(self.ctx.origmetric))

        # prebuild handler
        # prebuild. ATTENTION: No search variables allowed on prebuild, since it
        # happens before the search starts.
        searchobj = self.ctx.optuni.search
        prebuildcmd = searchobj.execute(searchobj.prebuildcmd)
        logice.info("prebuildcmd {}  type {}".format(prebuildcmd, type(prebuildcmd)))
        if prebuildcmd is not None and \
           prebuildcmd and \
           self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            logice.info("trying to execute it!")
            self.ctx.prebldhand.run(prebuildcmd)
        #endif

        srch_start = time.time()

        conn = choco.SQLiteConnection("sqlite:///choco.db")
        self.searchalgo = choco.Random(conn, self.space, clear_db=True)
        algodesc = "choco.Random"

        logice.debug(f"space: {self.space}")

        try:
            with self.ctx.resulthandler(self.ctx) as rh:
                self.ctx.reshandobj = rh
                algo = self.searchalgo
                for i in range(self.ctx.ntests):
                    token, params = algo.next()

                    cfgid = token['_chocolate_id']
                    metric = self.objective(cfgid, params)

                    algo.update(token, metric['loss'])
                ##
            #endwith
        except StopAfterInterruption as e:
            logice.info("Search interrupted using stop-after value!")

        except KeyboardInterrupt as e:
            logice.info("Shutdown requested...exiting")

        except StopIteration as e:
            logice.info("####\n## Stopeed because all space's been "
                    "traversed!\n####")

        finally:
            bestmetric = None
            cfgint = None
            best_tid = None
            choco_tid = None

            # Get the best from the database
            reslist = conn.all_results()
            if reslist:
                #print(f"reslist: {reslist}")
                results = conn.results_as_dataframe()
                #bestmetric = results['_loss'].min()
                if '_loss' in results:
                    minidx = results['_loss'].idxmin()
                    print(f'results:\n{results}')
                    logice.info(f"results= shape: {results.shape} minidx: {minidx} "
                                f"{type(minidx)}")
                    #minrow = results.iloc[minidx]
                    minrow = results.loc[minidx]
                    #print(f"minrow: {minrow}")
                    bestmetric_db = minrow['_loss']
                    best_tid_db = minidx #minrow['_chocolate_id']
                #
            #

            # Get the best from the this search
            if self.mintrial[0] < float("inf"):
                best_tid = self.mintrial[1]
                choco_tid, bestmetric, cfgint = self.trialsloss[best_tid]

                # Create and prune a temporary search nodes 
                self.ctx.searchnodesPr = {j: self.ctx.searchnodes[j] for j in cfgint if j in self.ctx.searchnodes}

                # Converts the final best result into self.optuni to be used later.
                # For instance, on saving the best_<something>.locus file.
                searchbase.convertDesiredResult(self.ctx.searchnodesPr, cfgint, self.ctx.revertsearch)
                self.finalresult = self.ctx.optuni
            #

            logice.info("Final Summary algo: {} bestid: {} chocoid: {} bestmetric: {} origmetric: {} speedup: "
                    "{} suggestedcfgs: {} cfgexec: {} cfgstimeout: {} cfgserror: {} cfgsinvalid: {} cfgrepeated: {} "
                    "bestconfig: {}".format(
                        algodesc,
                        best_tid,
                        choco_tid, bestmetric,
                        self.ctx.origmetric,
                        None if self.ctx.origmetric is None or bestmetric is None else self.ctx.origmetric/bestmetric,
                        self.ctx.lastcfgid+1, self.ctx.cfgsexecuted,
                        self.ctx.cfgstimeout, self.ctx.cfgserror,
                        self.ctx.cfgsinvalid, self.ctx.cfgsrepeated,
                        None if cfgint is None else searchbase.prettyprintcfg(self.ctx.searchnodesPr, cfgint)))

            logice.info(f"Summary from dataframe algo: {algodesc} "
                    f"bestid: {best_tid} chocoid: {choco_tid} "
                    f"bestid_db: {best_tid_db} bestmetric: {bestmetric} "
                    f"bestmetric_db: {bestmetric_db}")

            srch_end = time.time()
            self.ctx.timing[searchbase.TIME_SRCH].append(srch_end - srch_start)

            searchbase.printTimingReport(self.ctx.timing)
        ##endtry
    ###

    def _createcfgint(self, inp):
        #print(f"inp: {inp}")
        cfgint = {}
        updatelist = []
        for key, val in inp.items():
            if key.startswith(GenericHierarchicalVisitor.OR_IDX_STR):
                opakey = key.split(GenericHierarchicalVisitor.OR_IDX_STR)
                #print(f"opakey: {opakey[1]}")
                intkey = int(opakey[1])
                updatelist.append((intkey, val))
            elif key.isdigit():
                cfgint[int(key)] = val
            #
        ##
        for intkey, val in updatelist:
            if intkey in cfgint:
                cfgint[intkey] = val
            else:
                raise RuntimeError(f"This {intkey} should be found here! Weird!")
            #
        ##
        #print(f"cfgint: {cfgint}")
        return cfgint
    #enddef

    def objective(self, cfgid, inp):
        logice.info("### start (search time {} (sec))".format(
            (datetime.now() - self.ctx.searchstarttime).total_seconds()))

        if self.ctx.iceargs.stop_after and \
            searchbase.convergenceCriteria(self.ctx.searchstarttime,
                    self.ctx.iceargs.stop_after):
            raise StopAfterInterruption()
        #endif

        self.ctx.lastcfgid += 1
        varcfgid = self.ctx.lastcfgid

        cfgint = self._createcfgint(inp)

        self.ctx.searchnodesPr = {j: self.ctx.searchnodes[j] for j in cfgint if j in self.ctx.searchnodes}

        logice.info("@@@ cfg_id: {} choco_id: {} -> {}".format(varcfgid, cfgid,
            searchbase.prettyprintcfg(self.ctx.searchnodesPr, cfgint)))
        try:
            # Check searchable variable found is between the range.
            # It is important if searchable variables depends on another
            # searchable variables the values will only be known at this time,
            # at "runtime".
            optimizer.checkBounds(self.ctx.searchnodesPr, cfgint)

        except BoundsException as e:
            # Code generation failed, go to next!
            self.ctx.incCfgsInv()
            #logice.exception(e)
            logice.warning(e)
            # Fixed What to return when experiment fail?
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}
        #end except

        searchobj = self.ctx.optuni.search
        metric = float("inf")

        try:
            logice.debug("cfgint:\n{}".format(cfgint))

            searchbase.convertDesiredResult(self.ctx.searchnodesPr, cfgint,
                    self.ctx.revertsearch)

            ###
            # Evaluate the global scope.
            # before interpreting global scope need to add the global scope
            # search variables need to be added to global scope.
            topblkinterpreter(self.ctx.optuni.topblk)

            ###
            # Evaluate the search def block statements that may depend on the
            # global scope.
            localbuildcmd = searchobj.execute(searchobj.buildcmd)
            localruncmd = searchobj.execute(searchobj.runcmd)
            localcheckcmd = searchobj.execute(searchobj.checkcmd)
            #endfor

            #self.convertdebug.write("%%% End Cfg {}.\n".format(cfg_id))
            gencode_start = time.time()
            undoList = CodeGen.genCode(self.ctx.parsedsrcfiles, self.ctx.orighashes, 
                    self.ctx.optuni, self.ctx.iceargs, varcfgid)
            gencode_end = time.time()

            self.ctx.timing[searchbase.TIME_GEN].append(gencode_end - gencode_start)

        except OptimizationError as e:
            self.ctx.cfgserror += 1
            #return {'status': STATUS_FAIL}
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}

        except Exception as e:
            self.ctx.cfgserror += 1
            # Code generation failed, go to next!
            logice.exception(e)
            #logice.error(e)

            # Fixed What to return to when experiment fail?
            #return {'status': STATUS_FAIL}
            return {'loss':float("inf"), 'status': STATUS_FAIL}
            #return {'loss':float("inf"), 'status': STATUS_OK}

        else:
            if self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
                metric, evalstat = searchbase.empiricalEvaluation(self.ctx,
                                                                  localbuildcmd, localruncmd,
                                                                  localcheckcmd, cfgint)

                if evalstat == searchbase.EVAL_TIMEOUT:
                    self.ctx.cfgstimeout += 1
            #endif

        finally:
            searchbase.revert2origsearchnodes(self.ctx.searchnodesPr,
                    self.ctx.revertsearch)
            logice.debug("Returned the optuni to original state.")
        #end try

        # Number of configurations executed
        self.ctx.incCfgsExec()

        searchbase.variantEpilog(undoList, self.ctx.iceargs.output)

        # Save this result
        self.trialsloss[varcfgid] = (cfgid, metric, cfgint)

        # Keeping the faster
        if self.mintrial[0] > metric:
            self.mintrial = (metric, varcfgid)
        #

        if metric == float("inf"):
            metric = sys.float_info.max
        #

        logice.info(f"{metric} end")
        return {'loss': metric, 'status': STATUS_OK}
    ###
####

class GenericHierarchicalVisitor(NodeVisitor):
    """Generate a dict for search variables. Hierarchical by OR statements. it
    must be traversed in posorder."""

    OR_IDX_STR = "orblkidx"

    def __init__(self, astnode=None):
        self.astnode = astnode
        self.hierpointer = {} # output, hierarquical representation of the space
        self.orlst = []
        self.searchnodes = [] # output, info about each search node

        # searchndinit has all the search nodes in the same dict.
        # Diffently than hierpointer, which prunes the init space based on
        # the initial ORblocks. I just learned that hyperopt requires all the inital
        # search variables, not onlyt the pruned ones ad I initally thougt. (Teixeira 17 Oct 2019)
        self.searchndinit = {} 
        self.parent = None
        self.parentscope = None
        self.spacesizetmp = 1 # ouput, size of hierarquical space

        # initseed variables
        self.inithierptr = {}
        self.initorlst = []
    #enddef

    def update(self, node, parentscope):
        #print type(node),"  updating..."
        #print "Before",self.hierspace,"hierpointer", id(self.hierpointer)
        nscope = parentscope
        newnode = None
        dimlen = 1
        if type(node) is OrBlock:
            nblks = len(node.blocks)
            newlst = {str(k): v for k,v in self.orlst} #self.orlst
            #print "ids of blks",",".join([str(id(x)) for x in newlst])
            #print "orlst:", newlst
            newnode = newlst
            initvalue = self.initorlst[node.initv]
            #print node.unqid,node.initv,": OR initvalue", initvalue

        elif type(node) is OptionalStmt:
            optlst = [False, True]
            newnode = choco.choice(optlst)
            # get True index
            initvalue = optlst.index(node.initv)

        elif type(node) is OrStmt:
            nblks = len(node.stmts)
            newlst = list(range(nblks))
            newnode = choco.choice(newlst)
            self.spacesizetmp = len(newlst)
            initvalue = node.initv

        elif type(node) is SearchInteger:
            #lcmin = node.rangeexpr.min.value
            #lcmax = node.rangeexpr.max.value
            lcmin = optimizer.calcFuncExprVal(node, nscope, min).value
            lcmax = optimizer.calcFuncExprVal(node, nscope, max).value
            if lcmin > lcmax:
                logice.warning("SearchInteger {} min: {} max: {}"
                        " Incorrect!".format(node.unqid, lcmin, lcmax))
                raise ValueError
            #endif
            #FIXME step not being used! check choco manual
            newnode = choco.quantized_uniform(lcmin, lcmax, 1)
            dimlen = lcmax - lcmin + 1
            #print "update spacesize", dimlen,type(node)
            self.spacesizetmp = dimlen

            try:
                initvalue = node.initv.value
            except AttributeError as e:
                initvalue = lcmin
            #endtry

        elif type(node) is SearchPowerOfTwo:
            #lcmin = node.rangeexpr.min.value
            #lcmax = node.rangeexpr.max.value
            lcmin = optimizer.calcFuncExprVal(node, nscope, min).value
            lcmax = optimizer.calcFuncExprVal(node, nscope, max).value
            if lcmin > lcmax:
                warning("SearchPowerOfTwo {} min: {} max: {}"
                        " Incorrect!".format(node.unqid, lcmin, lcmax))
                raise ValueError
            #endif
            choicelst = [int(math.pow(2,x)) for x in
                    range(int(math.log(lcmin,2)),int(math.log(lcmax,2))+1)]
            newnode = choco.choice(choicelst)
            dimlen = len(choicelst)
            #print "update spacesize", dimlen,type(node)
            self.spacesizetmp = dimlen

            try:
                #index of the initvalue
                initvalue = choicelst.index(node.initv.value)
                #print "@@@ SearchPerm",initvalue
            except AttributeError as e:
                initvalue = choicelst.index(lcmin)
                #print "@@@ SearchPerm Except",initvalue
            #endtry

        elif type(node) is SearchEnum:
            dimlen = len(node.values)
            newnode = choco.choice(list(range(dimlen)))
            #print "update spacesize", dimlen,type(node)
            self.spacesizetmp = dimlen
            #index of the initvalue
            initvalue = node.initv

        elif type(node) is SearchPerm:
            currlist = node.set
            if isinstance(node.set, ID):
                currlist = optimizer.getListPerm(node, nscope)
            #print "View 2", currlist.values, type(currlist.values)
            _li = [it.value for it in currlist.values]
            choicelst = list(itertools.permutations(_li))
            newnode = choco.choice(choicelst)
            self.spacesizetmp = len(choicelst)

            # Convert ListMaker type to regular list
            _lh = tuple([it.value for it in node.initv.values])
            # index of the initvalue
            initvalue = choicelst.index(_lh)

        else:
            raise NotImplementedError("Not implemented for this "
                    "type:"+str(type(node)))
        #endif
        self.hierpointer[str(node.unqid)] = newnode
        self.inithierptr[str(node.unqid)] = initvalue
        self.searchndinit[str(node.unqid)]= initvalue
        #print "hierpointer", id(self.hierpointer)
        #for k,v in self.hierpointer.iteritems():
        #    print k,")",v
        #print "==="
    #enddef

    # Do it in pos-order
    def generic_visit(self, node):
        #print "B gen_vis", node.unqid," : ", type(node), self.spacesizetmp #" = "#, node

        if hasattr(node, 'scope'):
            self.parentscope = node.scope

        if isinstance(node, self.astnode):
            self.searchnodes.append((self.parent, node, self.parentscope))

        if type(node) is OrBlock:
            oldorlst     = self.orlst
            oldinitorlst = self.initorlst
            oldpointer = self.hierpointer
            oldinitptr = self.inithierptr
            #print "oldpointer", id(oldpointer)
            self.orlst     = []
            self.initorlst = []
        #endif

        if type(node) is OrBlock or \
            type(node) is Block:
            #print "gen_vis", node.unqid, " : ", type(node)
            childspacesize = []
            #oldspacesizetmp = self.spacesizetmp
            self.spacesizetmp = 1
        #endif

        oldparent = self.parent
        oldparentscope = self.parentscope
        self.parent = node
        ind = 0
        for c in node:
            if type(node) is OrBlock:
                self.hierpointer = {}
                # track the index of each block in the orblk
                self.hierpointer[self.OR_IDX_STR+str(node.unqid)] = ind
                self.orlst.append((c.unqid, self.hierpointer))
                #print "new hierpointer", id(self.hierpointer)

                self.inithierptr = {}
                self.inithierptr[self.OR_IDX_STR+str(node.unqid)] = ind
                self.initorlst.append(self.inithierptr)
                ##print "new initptr", id(self.inithierptr)
            #endif

            self.visit(c)
            if type(node) is OrBlock or \
                type(node) is Block:
                childspacesize.append(self.spacesizetmp)
                self.spacesizetmp = 1
            #endif

            ind += 1
        #endfor

        self.parent = oldparent
        self.parentscope = oldparentscope

        if type(node) is OrBlock:
            self.hierpointer = oldpointer
            self.inithierptr = oldinitptr
            #print "hierspace restablished",id(self.hierpointer)
        #endif

        if isinstance(node, self.astnode):
            self.update(node, self.parentscope)
        #endif

        if type(node) is OrBlock:
            # in hierarchical spaces it is a sum
            total = 0
            for v in childspacesize:
                #print "OR child",v
                total += v
            self.spacesizetmp = total
            #print "OR update spacesize", self.spacesizetmp
        elif type(node) is Block:
            total = 1
            for v in childspacesize:
                total *= v
            self.spacesizetmp = total
            #print "Block update", self.spacesizetmp
        #endif

        if type(node) is OrBlock:
            self.orlst = oldorlst
            self.initorlst = oldinitorlst
        #endif
        #print "END B gen_vis", node.unqid," : ", type(node), self.spacesizetmp #" = "#, node
    #enddef
#endclass
