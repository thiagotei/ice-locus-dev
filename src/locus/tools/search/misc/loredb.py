# LORE Python3 API
# Author: Justin Szaday <szaday2@illinois.edu>
# Requires: mysqlclient (https://pypi.org/project/mysqlclient/)
# Please notify author of any bugs or feature requests

import MySQLdb as mdb

static_features =  ["stmt_cnt", "mem_access_cnt", "fp_bin_op_cnt", "int_bin_op_cnt",
                    "int_un_op_cnt", "assign_cnt", "const_cnt", "const_0_cnt", "const_1_cnt",
                    "bin_op_with_int_const_cnt", "bb_cnt", "bb_1_succ_cnt", "bb_2_succ_cnt",
                    "bb_3_plus_succ_cnt", "bb_1_pred_cnt", "bb_2_pred_cnt", "bb_3_plus_pred_cnt",
                    "bb_1_pred_1_succ_cnt", "bb_1_pred_2_succ_cnt", "bb_2_pred_1_succ_cnt",
                    "bb_2_pred_2_succ_cnt", "bb_3_plus_pred_3_plus_succ_cnt", "bb_5_minus_stmt_cnt",
                    "bb_6_to_10_stmt_cnt", "bb_11_plus_stmt_cnt", "cfg_edge_cnt", "has_branch"]

confs     = ["base", "novec", "sse", "avx", "avx2", "o3"]
mutations = ["interchange_arg", "tiling_arg", "unrolling_arg", "unrolljam_arg", "distribution_arg"]

def connect(args):
    global conn, cursor
    conn   = mdb.connect(*args)
    cursor = conn.cursor()
    return cursor

def disconnect():
    global conn
    conn.close()

def select_compilers():
    global cursor
    query = "SELECT DISTINCT compiler_vendor FROM executions";
    _, vendors = cursor.execute(query), [x for (x,) in cursor.fetchall()]
    query = "SELECT DISTINCT compiler_version FROM executions WHERE compiler_vendor=%s"
    def find_versions(vendor):
        _, versions = cursor.execute(query, (vendor,)), cursor.fetchall()
        return [x for (x,) in versions]
    return dict(list(zip(vendors, list(map(find_versions, vendors)))))

def select_loops(benchmark = None):
    global cursor
    if (benchmark is not None):
        query = "SELECT DISTINCT table_ptr FROM loops WHERE benchmark=%s"
        cursor.execute(query, (benchmark,))
    else:
        query = "SELECT DISTINCT table_ptr FROM loops"
        cursor.execute(query)
    return [str(x) for (x,) in cursor.fetchall()]

def select_loop_info(lid, force = False):
    global cursor, info_cache
    query = """SELECT benchmark, version, application, file, function, line
               FROM loops WHERE table_ptr=%s"""
    if ("info_cache" not in globals()):
        info_cache = dict()
    elif (lid in info_cache and not force):
        return info_cache[lid]
    _, info_cache[lid] = cursor.execute(query, (lid,)), cursor.fetchone()
    return info_cache[lid]

def select_loop_id(benchmark, version, application, file, function, line):
    global cursor
    query = """SELECT table_ptr FROM loops
               WHERE `benchmark`=%s AND `version`=%s AND `application`=%s AND `file`=%s AND `function`=%s AND `line`=%s"""
    _, lid = cursor.execute(query, (benchmark, version, application, file, function, line)), cursor.fetchone()
    return None if (lid is None) else lid[0]

def select_mutations(lid):
    global cursor
    query = "SELECT DISTINCT mutation_number FROM mutations WHERE id=%s"
    _, mutations = cursor.execute(query, (lid,)), cursor.fetchall()
    return None if (mutations is None) else [int(x) for (x,) in mutations]

def select_mutation_info(lid, mid):
    global cursor
    fmt   = lambda x : x if not (x is None or x == "NULL") else None
    query = "SELECT %s FROM mutations WHERE id = %%s AND mutation_number=%%s" % (", ".join(mutations))
    _, mutation = cursor.execute(query, (lid, mid)), cursor.fetchone()
    return None if (mutation is None) else dict(list(zip(mutations, [fmt(x) for x in mutation])))

def select_static_features(lid):
    global cursor
    query = "SELECT %s FROM loops WHERE table_ptr = %%s" % (", ".join(static_features))
    _, features = cursor.execute(query, (lid,)), cursor.fetchone()
    return None if (features is None) else dict(list(zip(static_features, [int(x) for x in features])))

# compiler is a tuple of (vendor, version)
def select_runtime(lid, mid, compiler, conf = "base"):
    global cursor
    query   = """SELECT %s_median FROM executions WHERE id = %%s AND mutation_number = %%s
                 AND compiler_vendor = %%s AND compiler_version = %%s AND pluto=0""" % conf
    #_, mean = cursor.execute(query, (lid, mid, *compiler)), cursor.fetchone()
    _, mean = cursor.execute(query, tuple([lid, mid] + list(compiler))), cursor.fetchone()
    return None if mean is None else mean[0]

def select_best_mutation(lid, compiler, conf = "base"):
    global cursor
    query  = """SELECT mutation_number FROM executions WHERE id=%%s
                AND compiler_vendor=%%s AND compiler_version=%%s AND pluto=0
                ORDER BY %s_median ASC LIMIT 1""" % conf
    _, mean = cursor.execute(query, tuple([lid] + list(compiler))), cursor.fetchone()
    return None if mean is None else int(mean[0])

def select_best_mutation_any_conf(lid, compiler):
    global cursor
    tmp    = [x + "_median" for x in confs]
    query  = """SELECT mutation_number
                FROM executions WHERE id=%%s
                AND compiler_vendor=%%s AND compiler_version=%%s AND pluto=0
                ORDER BY LEAST(%s) ASC
                LIMIT 1""" % (", ".join(tmp))
    _, mid = cursor.execute(query, tuple([lid] + list(compiler))), cursor.fetchone()
    if (mid is None):
        return None
    query  = """SELECT %s FROM executions WHERE id=%%s AND mutation_number = %%s
                AND compiler_vendor=%%s AND compiler_version=%%s AND pluto=0""" % (", ".join(tmp))
    _, means = cursor.execute(query, tuple([lid] + list(mid) + list(compiler))), [int(x) for x in cursor.fetchone()]
    val, idx = min((val, idx) for (idx, val) in enumerate(means))
    return (mid[0], confs[idx], val)

def insert_loop(benchmark, version, application, file, function, line, transformations = "unknown"):
    global cursor
    query = """INSERT INTO loops(id, table_ptr, transformations, benchmark, version, application, file, function, line)
               VALUES (UUID(), UUID(), %s, %s, %s, %s, %s, %s, %s)"""
    cursor.execute(query, (transformations, benchmark, version, application, file, function, line))
    return select_loop_id(benchmark, version, application, file, function, line)

def insert_mutation(lid, transformations, order, valid=True):
    query    = "SELECT MAX(mutation_number) FROM mutations WHERE id = %s"
    _,(mid,) = cursor.execute(query, (lid,)), cursor.fetchone()
    mid    = 0 if mid is None else (mid + 1)
    args   = ", ".join([("%s_order, %s_arg" % (t, t)) for t in transformations])
    values = ", ".join([("%d, \"%s\"" % (order.index(t), transformations[t])) for t in transformations])
    if args and values:
        query  = """INSERT INTO mutations(id, mutation_number, valid, %s)
                    VALUES (%%s, %%s, %%s, %s)""" % (args, values)
    else:
        query  = """INSERT INTO mutations(id, mutation_number, valid)
                    VALUES (%s, %s, %s)"""

    cursor.execute(query, (lid, mid, int(valid)))
    return mid

def insert_execution(lid, mid, compiler, cpu, times, vect_tweak = "", pluto = False):
    args   = ", ".join(times)
    values = ", ".join(map(str, list(times.values())))
    query  = """INSERT INTO executions(id, mutation_number, compiler_vendor, compiler_version, cpu_model, vectorization_tweak, pluto, %s)
                VALUES (%%s, %%s, %%s, %%s, %%s, %%s, %d, %s)""" % (args, int(pluto), values)
    #return cursor.execute(query, (lid, mid, list(compiler), cpu, vect_tweak))
    #return cursor.execute(query, (lid, mid, compiler[0], compiler[1], cpu, vect_tweak))
    return cursor.execute(query, tuple([lid, mid] + list(compiler) + [cpu, vect_tweak]))

def commit():
    global conn
    return conn.commit()
