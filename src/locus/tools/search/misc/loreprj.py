'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging

from locus.frontend.optlang.locus.locusvisitor import GenericVisitorWScope
from locus.frontend.optlang.locus.locusast import ID, FuncCall, Constant, Argument

import locus.frontend.optlang.locus.optimizer as optimizer

log = logging.getLogger(__name__)

__INTERCHANGE_NAME = "RoseLore.Interchange"
__DIST_NAME = "RoseLoreRef.Distribute"
__TILING_NAME = "RoseLore.Tiling"
__UNAJ_NAME = "RoseLore.UnrollAndJam"
__UNROLL_NAME = "RoseLoreRef.Unroll"

def queryOpt(optuni, parsedsrcfiles, iceargs):
    ## Get all the query information before going to define the space.
    ## this has not been implemented to be use with multiple code regios using the same label.
    ## FIXME: this needs to be generalized to check for variables on the scope and other query functions.
    #for spacename, ent in optuni.items():
    for spacename, ent in optuni.allspaces():
        visitor = GenericVisitorWScope(FuncCall)
        visitor.visit(ent)
        for p,n,s in visitor.values:
            #print ("FuncCall \\",n.name,"\\", type(n.name))
            if n.name == ID("Query.LoopNestInfo"):
                #print "+++Found !!!", n, " in ", spacename
                for k_psf, v_psf in parsedsrcfiles.items():
                    for coreg in v_psf:
                        if coreg.label == spacename:
                            from locus.tools.opts.query import Query
                            optobj = Query.LoopNestInfo()
                            ret = optobj.apply(v_psf, None, None, spacename, None, -1, -1)
                            #print "-",p,"-", type(p),"-", n,"-", type(n)
                            p.replace(n, Constant(int, ret.value))
                            #print "-",p,"-", type(p),"-", n,"-", type(n)
                            #print "ret from LoopNestInfo", ret
            elif n.name == ID("Query.IsPerfectLoopNest"):
                for k_psf, v_psf in parsedsrcfiles.items():
                    for coreg in v_psf:
                        if coreg.label == spacename:
                            from locus.tools.opts.query import Query
                            optobj = Query.IsPerfectLoopNest()
                            ret = optobj.apply(v_psf, None, None, spacename, None, -1, -1)
                            #print "-",p,"-", type(p),"-", n,"-", type(n)
                            p.replace(n, Constant(bool, ret.value))
            elif n.name == ID("Query.IsDepAvailable"):
                for k_psf, v_psf in parsedsrcfiles.items():
                    for coreg in v_psf:
                        if coreg.label == spacename:
                            from locus.tools.opts.query import Query
                            optobj = Query.IsDepAvailable()
                            ret = optobj.apply(v_psf, None, None, spacename, iceargs, -1, -1)
                            #print "-",p,"-", type(p),"-", n,"-", type(n)
                            p.replace(n, Constant(bool, ret.value))
            elif n.name == ID("Query.SpMatInfo") or \
                    n.name == ID("Query.SpMatDim"):
                from locus.tools.opts.query import Query
                ret = n.execute(s)
                #print("SpMatInfo", type(ret),ret)
                p.replace(n, ret)
            #else:
            #    print "---Not Found"

    ## what if the search variable is inside an if or for and it 
    ## never gets executed because it depends in variable of the code? It should not be in the search.
    ## A simple dead code elimination does the job here.
    for ent in optuni.entities():
        log.info("DeadCodeElimination Starting on {} ...".format(ent.id.name))
        #ent.show()
        dead = optimizer.DeadCodeElimination(ent)
        dead.execute()
        #ent.show()
        log.info("DeadCodeElimination Ended on {} ...".format(ent.id.name))
    #endif
#endif

def getTransfInfo(optuni):
    dbmutationdict = {}
    dbmutationlist = []
    # Get all the optimizations executed
    #for ent in self.optool.optuni.entities():
    for ent in optuni.entities():
        visID = GenericVisitorWScope(FuncCall)
        visID.visit(ent)

        for pa_func, func, funcscope in visID.values:
            log.info("Ent: {} Func name: {}".format(ent.id, func.name))

            for arg in func.arglist:
                log.debug("    Args: {} type: {}".format(arg, type(arg)))

            func_name_str = str(func.name)

            if func_name_str == __INTERCHANGE_NAME:
                arg = next(iter(func.arglist)) # Assuming 1 arg
                depth_value="None"
                depth_value = optimizer.findScopeIdPerm(ID("depth"), funcscope)

                if arg.expr is not None:
                    #get depth value from permorder variable
                    order = optimizer.findScopeIdPerm(arg.expr, funcscope)
                #endif

                #Calc the lexicographic order
                lexorder = getLexicoRank(order)
                dbmutationlist.append("interchange")
                dbmutationdict['interchange'] = "depth="+str(depth_value)+",order="+str(lexorder)

            elif func_name_str == __TILING_NAME or\
                    func_name_str == __UNAJ_NAME:
                iter_arg = iter(func.arglist)
                arg = next(iter_arg) # Assuming 1 arg
                level="None"
                if arg.id is not None and\
                    str(arg.id) == "loop" and\
                    arg.expr is not None:
                    level = optimizer.findScopeIdPerm(arg.expr, funcscope)
                #endif

                arg = next(iter_arg) # Assuming 2 arg
                factor="None"
                if arg.id is not None and\
                    str(arg.id) == "factor" and\
                    arg.expr is not None:
                    factor = optimizer.findScopeIdPerm(arg.expr, funcscope)
                #endif

                if func_name_str == __TILING_NAME:
                    optname = 'tiling'
                elif func_name_str == __UNAJ_NAME:
                    optname = 'unrolljam'
                #endif

                #log.info("level|indexUAJ={} size|T1fac={}".format(level, factor))
                dbmutationlist.append(optname)
                dbmutationdict[optname] = "level="+str(level)+",size="+str(factor)

            elif func_name_str == __UNROLL_NAME:
                factor="None"
                for arg in func.arglist:
                    if arg.id is not None and\
                        str(arg.id) == "factor" and\
                        arg.expr is not None:
                        factor = optimizer.findScopeIdPerm(arg.expr, funcscope)
                        break
                    #endif
                #endfor

                #log.info("size|UNfac={}".format(factor))
                optname = 'unrolling'
                dbmutationlist.append(optname)
                dbmutationdict[optname] = str(factor)

            elif func_name_str == __DIST_NAME:
                #get depAvail  var
                allow_dep = optimizer.findScopeIdPerm(ID("depAvail"), funcscope)
                str_allow_dep = "1" if allow_dep else "0"
                optname = "distribution"
                dbmutationlist.append(optname)
                dbmutationdict[optname] = "allow_dep="+str_allow_dep
                #log.info("depAvail={}".format(allow_dep))

            elif "Query." in str(func_name_str):
                log.debug("Query function {} ignored!".format(func_name_str))

            else:
                raise RuntimeError("Found an FuncCall {} not recognized.".format(func_name_str))
            #endif
        #endfor
    #endfor
    return dbmutationdict,dbmutationlist
#enddef

def getLexicoRank(vecorder):
    vecorderval = [int(val.value) 
            if val.type == int else TypeError("Expecting int and got {} .".format(val.type)) 
            for val in vecorder]
    pos = len(vecorder)
    origvec = list(range(pos))
    # if the vec is in ascendent order return 0 (Which means no interchange)
    if origvec == vecorderval:
        return 0
    #endif
    final_pos = 0
    for val_int in vecorderval:
        minv = pos-1 if pos-1 < val_int else val_int
        final_pos += minv * myfactorial(pos-1)
        pos -= 1
    #endfor
    return final_pos
#enddef

def myfactorial(n):
    result = 1
    for i in range(2,n+1):
        result *= i
    #endfor
    return result
#endif

