'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, os
import argparse, copy, time, math
#import adddeps
import opentuner
from datetime import datetime

from opentuner.measurement import MeasurementInterface
from opentuner.search.manipulator import ConfigurationManipulator,\
BooleanParameter, PowerOfTwoParameter, EnumParameter
from opentuner import IntegerParameter, PermutationParameter
from opentuner import Result

from locus.backend.codegen import CodeGen
#from locus.optspace.parameters import Parameter, Integer, PowerOfTwo, Boolean, Enum
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, Constant, ListMaker, FuncCall
from locus.tools.toolinterface import AbstractTool
from locus.tools.opts.opttoolinterface import AbstractOptTool
from locus.frontend.optlang.locus.locusvisitor import GenericVisitorWScope
from .searchtoolinterface import AbstractSearchTool, argparser as absstparser 
from locus.util.constants import ICE_OUTPUT_MODE_STDOUT,\
    ICE_OUTPUT_MODE_INPLACE, ICE_OUTPUT_MODE_SUFFIX,\
    ICE_CLR_INITSEED, ICE_CLR_FAIL, bcolors
from locus.util.exceptions import OptimizationError, BoundsException
from locus.backend.interpreter import topblkinterpreter
from locus.tools.search.handlers.defaulthandlers import PrebuildHandler,\
    BuildHandler, EvalHandler, CheckHandler
from .handlers.locusdbhandlers import LocusDBResultHandler
from locus.tools.search.context.searchctxot import SearchCtxOT 
from .resultsdb.misc import getDBseeds

import locus.tools.search.searchbase as searchbase
import locus.tools.search.misc.loreprj as loreprj
import locus.frontend.optlang.locus.optimizer as optimizer

from locus.backend.genlocus import GenLocus

argparser = argparse.ArgumentParser(add_help=False,
                                    parents=[absstparser])
argparser.add_argument('--otprune',
                        required=False, default=False, action='store_true',
                        help="OpenTuner: Prune the configuration of the OR blocks before checking bounds.")

logice = logging.getLogger(__name__)

class OpenTunerTool(AbstractSearchTool, MeasurementInterface):
    """ Opentuner driver to search optimization space """

    def __init__(self, 
            iceargs,
            searchctx=SearchCtxOT,
            prebuildhandler=PrebuildHandler,
            buildhandler=BuildHandler,
            evalhandler=EvalHandler,
            checkhandler=CheckHandler,
            resulthandler=LocusDBResultHandler):

        self.ctx = searchctx(iceargs,
                               None,
                               None,
                               prebuildhandler,
                               buildhandler,
                               evalhandler,
                               checkhandler,
                               resulthandler) 

        AbstractSearchTool.__init__(self, "OpenTuner")

        otlistargs = ['--no-dups','--parallelism','1']
#                '--results-log-details','./opentuner-log-details-'+self.ctx.iceargs.suffixinfo])

        logice.info(f"Search carried out by {bcolors.OKBLUE} OpenTuner {bcolors.ENDC}!")

        if self.ctx.iceargs.ntests:
            logice.info("Max number of tests: {}".format(self.ctx.iceargs.ntests))
            otlistargs.append('--test-limit')
            otlistargs.append(str(self.ctx.iceargs.ntests))
        #endif

        if self.ctx.iceargs.stop_after:
            logice.info("Stop search after {} minutes.".format(self.ctx.iceargs.stop_after))
            otlistargs.append('--stop-after')
            otlistargs.append(str(self.ctx.iceargs.stop_after * 60))
        else:
            logice.info("No time limit for the search.".format())
        #endif

        argparser_opentu = opentuner.default_argparser()
        self.otargs = argparser_opentu.parse_args(otlistargs)

        MeasurementInterface.__init__(self, self.otargs)

        # id map between OpenTuner conf id and this class id
        self.idsmap = {}

        # OpenTuner was returning weird error messages when passing the 
        # ToolSpec object. So I pass the a string in place save the ToolSpec 
        # obj in this dict.
        self.otparamdict = {}
        self.finalresult = None

        self.__seedcfg = []
    #enddef

    def convertOptUni(self, inpoptuni, parsedsrcfiles, orighashes):
        logice.debug("start")
        self.ctx.optuni = copy.deepcopy(inpoptuni)
        self.ctx.parsedsrcfiles = parsedsrcfiles
        self.ctx.orighashes = orighashes

        self.ctx.revertsearch = {}
        self.ctx.searchnodes = {}

        if self.ctx.iceargs.equery or self.ctx.iceargs.applyDFAOpt:
            topblkinterpreter(self.ctx.optuni.topblk)

            if self.ctx.iceargs.equery:
                loreprj.queryOpt(self.ctx.optuni, self.ctx.parsedsrcfiles, self.ctx.iceargs)
                logice.info("Queries information collected and replaced.")
            #

            if self.ctx.iceargs.applyDFAOpt:
                searchbase.applyDFAOpts(self.ctx.optuni, self.ctx.iceargs)
                logice.info("DFA Compiler optimizations applied.")
            #
        #endif

        if self.ctx.iceargs.exprec:
            searchbase.expandRecursion(self.ctx.optuni, self.ctx.iceargs)
        #endif

        # Global scope added to the search space.
        searchbase.buildSearchNodes(self.ctx.optuni.topblk, self.ctx.searchnodes)

        # Add each CodeReg to the search space.
        for name, ent in self.ctx.optuni.items():
            logice.debug("name: %s",name)
            searchbase.buildSearchNodes(ent, self.ctx.searchnodes)
        #endfor optuni

        #info = "\n".join([str(val[1].id)+" "+repr(val[1]) for key,val in
         #   self.ctx.searchnodes.iteritems()])
        logice.info("end buildind search nodes. Searchable nodes: {}".format(
            "\n"+searchbase.prettyprintcfg(self.ctx.searchnodes, printempty=True) if
            self.ctx.searchnodes else "None"))

        self.__manipulator = ConfigurationManipulator()

        # nodes that do not have a range valid will be removed from the search dict.
        self.__removenodes = []

        initseedcfg = {}

        for key, value in self.ctx.searchnodes.items():
            p, node, nscope = value
            paramkey = str(node.unqid)
            logice.debug("search vars: {} {}".format(paramkey, type(node)))

            if type(node) is OrBlock:
                self.__manipulator.add_parameter(EnumParameter(paramkey,
                    list(range(len(node.blocks)))))
                initvalue = node.initv

            elif type(node) is OptionalStmt:
                self.__manipulator.add_parameter(BooleanParameter(paramkey))
                initvalue = node.initv

            elif type(node) is OrStmt:
                self.__manipulator.add_parameter(EnumParameter(paramkey,
                    list(range(len(node.stmts)))))
                initvalue = node.initv

            elif type(node) is SearchEnum:
                #print "###Param###",node.values, type(node.values)
                self.__manipulator.add_parameter(EnumParameter(paramkey,
                    list(range(len(node.values)))))
                initvalue = node.initv

            elif type(node) is SearchPerm:
                #print "###Param###",node.set.values, type(node.set.values)
                # node.rangeexpr is a ListMaker type. It is comprise Constant types. 
                # need the conversion.
                currlist = node.set
                if isinstance(node.set, ID):
                    currlist = optimizer.getListPerm(node, nscope)
                #print "View 2", currlist.values, type(currlist.values)

                # Convert ListMaker type to regular list
                _li = [it.value for it in currlist.values]
                #print "SearchPerm",_li, type(_li)
                self.__manipulator.add_parameter(PermutationParameter(paramkey, _li))

                # Convert ListMaker type to regular list
                if node.initv == node.set:
                    _lh = _li
                else:
                    _lh = [it.value for it in node.initv.values]
                #
                initvalue = _lh

            elif type(node) is SearchPowerOfTwo:
                # traverse AST get the max of all paths and put that value
                # during execution it will be curbed.
                _min = optimizer.calcFuncExprVal(node, nscope, min)
                _max = optimizer.calcFuncExprVal(node, nscope, max)

                if _min.value < _max.value:
                    self.__manipulator.add_parameter(PowerOfTwoParameter(paramkey,
                        _min.value, _max.value))
                else:
                    t = Constant(int, _min.value)
                    p.replace(node, t)
                    logice.warning("SearchPowerOfTwo {} min: {} max: {} set to {}".format(
                                    paramkey, _min.value, _max.value, t.value))
                    self.__removenodes.append(key)
                #endif

                try:
                    initvalue = node.initv.value
                except AttributeError as e:
                    initvalue = _min.value
                #endtry

            elif type(node) is SearchInteger:
                _min = optimizer.calcFuncExprVal(node, nscope, min)
                _max = optimizer.calcFuncExprVal(node, nscope, max)

                if _min.value < _max.value:
                    self.__manipulator.add_parameter(IntegerParameter(paramkey,
                        _min.value, _max.value))
                else:
                    t = Constant(int, _min.value)
                    p.replace(node, t)
                    logice.warning("SearchInteger {} min: {} max: {} set to {}".format(paramkey, _min.value, _max.value, t.value))
                    self.__removenodes.append(key)
                #endif

                try:
                    initvalue = node.initv.value
                except AttributeError as e:
                    initvalue = _min.value
                #endtry

            else:
                logice.error("Could not find the correct type for the node {}"
                    "!".format(node))
            #endif

            initseedcfg[paramkey] = initvalue
        #endfor

        if self.__removenodes:
            for n_id in self.__removenodes:
                if n_id in self.ctx.searchnodes:
                    del self.ctx.searchnodes[n_id]
                else:
                    raise RuntimeError("The node {} should be in searchnodes! Weird!".format(n_id))
                ##
            ##
        ##

        if self.ctx.iceargs.initseed:
            self.__seedcfg.append(initseedcfg)
            logice.debug(searchbase.seedcfgsstr([initseedcfg], 
                                        "Initial seed for the search:"))
        #endif

        #
        # Get seeds from database.
        #
        if self.ctx.iceargs.dbseeds:
            dbseeds = getDBseeds(self.ctx)
            t_dbseeds = self.encodeInitCfgSeeds(dbseeds, initseedcfg)
            self.__seedcfg += t_dbseeds 
            logice.debug(searchbase.seedcfgsstr(t_dbseeds, 
                                            "Initial seeds from DB are:"))
        #

        if self.__seedcfg:
            logice.info(searchbase.seedcfgsstr(self.__seedcfg, 
                                    "All initial seeds for the search:"))
        else:
            logice.info(f"{ICE_CLR_INITSEED}No initial seeds!{bcolors.ENDC}")
        #

        # Check if there is anything to search!
        if not self.__manipulator.params:
            return False
        else:
            logice.info("Search space size: {}".format(self.__manipulator.search_space_size()))
            return True

    def manipulator(self):
        return self.__manipulator

    def seed_configurations(self):
        #logice.info("initial seed {} {} ".format(self.__seedcfg))
        #if self.__seedcfg:
        #    for se in self.__seedcfg:
        #        for k,v in se.iteritems():
        #            print k,type(k), v,type(v)

        return self.__seedcfg

    def search(self, origmetric=None, origres=None):
        self.ctx.searchstarttime = datetime.now()
        srch_start = time.time()

        from opentuner.tuningrunmain import TuningRunMain

        self.ctx.origmetric = origmetric
        self.ctx.origres = origres
        logice.info("Orig metric {}.".format(self.ctx.origmetric))

        # prebuild handler
        searchobj = self.ctx.optuni.search
        prebuildcmd = searchobj.execute(searchobj.prebuildcmd)
        if prebuildcmd is not None and \
           prebuildcmd and \
           self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            self.ctx.prebldhand.run(prebuildcmd)
        #endif

        # Opentuner adds their own handlers, which is not nice.
        # So, I save and add my handlers back.
        myhandlers = logging.getLogger().handlers
        savedhandlers = []
        for han in myhandlers:
            savedhandlers.append(han)

        self._trm = TuningRunMain(self, self.otargs)

        otunerhandlers = logging.getLogger().handlers
        for han in otunerhandlers:
            han.setLevel(logging.CRITICAL)

        #root = logging.getLogger()
        #root.setLevel(logging.DEBUG)

        for han in savedhandlers:
            logging.getLogger().addHandler(han)
            if self.ctx.iceargs.debug and \
                    han.level <= logging.DEBUG:
                han.setLevel(logging.DEBUG)

        #han.getLevel() <= logging.DEBUG:

        if self.ctx.iceargs.debug:
            logice.setLevel(logging.DEBUG)
        #print "logice level", logice.getEffectiveLevel()

        # Go for search!
        try:
            dbinfo = {'searchtool': 'opentuner',
                      'searchtech': 'default'}
            with self.ctx.resulthandler(self.ctx, **dbinfo) as rh:
                self.ctx.reshandobj = rh
                oh = self._trm.main()
        except KeyboardInterrupt as e:
            logice.info("Shutdown requested...exiting")
        finally:
            srch_end = time.time()
            self.ctx.timing[searchbase.TIME_SRCH].append(srch_end - srch_start)

            searchbase.printTimingReport(self.ctx.timing)
        #end try

        return self._trm
    #enddef

    def save_final_config(self, configuration):
        best_result = self._trm.search_driver.best_result
        metric = best_result.time
        cfg    = configuration.data
        cfgotid = best_result.configuration_id
        cfgint = {int(key):val for key, val in cfg.items()}

        logice.info("Final Summary algo: opentuner None bestid: {} otid: {} bestmetric: {} origmetric: {} "
                "speedup: {} cfgexec: {} cfgstimeout: {} cfgerror: {} cfgsinvalid: {} config: "
                "{}".format(
                    self.idsmap[cfgotid], cfgotid, metric,
                    self.ctx.origmetric,
                    None if self.ctx.origmetric is None or metric is None else self.ctx.origmetric/metric,
                    self.ctx.cfgsexecuted, self.ctx.cfgstimeout,
                    self.ctx.cfgserror, self.ctx.cfgsinvalid,
                    searchbase.prettyprintcfg(self.ctx.searchnodes, cfgint)))

        logice.info(cfgint)

        # Converts the final best result into self.optuni to be used later.
        # For instance, on saving the best_<something>.locus file.
        searchbase.convertDesiredResult(self.ctx.searchnodes, cfgint, self.ctx.revertsearch)
        self.finalresult = self.ctx.optuni
    #enddef

    def compile_and_run(self, desired_result, input, limit):
        logice.info("###")
        logice.info("### start (search time {} (sec))".format(
            (datetime.now() - self.ctx.searchstarttime).total_seconds()))
        logice.info("###")

        cfg = desired_result.configuration.data
        self.ctx.lastcfgid += 1
        varcfgid = self.ctx.lastcfgid
        cfg_idot = desired_result.configuration_id
        self.idsmap[cfg_idot] = varcfgid

        # change back the key of the cfg to integer
        noScfgint = {int(key):val for key, val in cfg.items()}
        cfgint = dict(sorted(noScfgint.items()))

        searchobj = self.ctx.optuni.search

        try:
            logice.info("cfgint:\n{}".format(cfgint))

            #for ent in self.optuni.entities():
            #    ent.show()
            #sys.stdout.flush()

            if self.ctx.iceargs.otprune:
                # Convert the ORs only, get a new searchnodes to be used at checkbounds.
                orlist = searchbase.convertOnlyORDesiredResult(self.ctx.searchnodes, cfgint, self.ctx.revertsearch)

                logice.info("~~~ cfg_id: {} OR nodes -> {}".format(varcfgid, orlist))

                searchnodes_aux = {}

                # Global scope added to the search space.
                searchbase.buildSearchNodes(self.ctx.optuni.topblk, searchnodes_aux)

                # Add each CodeReg to the search space.
                for name, ent in self.ctx.optuni.items():
                    logice.debug("name: %s",name)
                    searchbase.buildSearchNodes(ent, searchnodes_aux)
                    #ent.show()
                #endfor optuni
            else:
                searchnodes_aux = self.ctx.searchnodes
            #endif

            logice.info("@@@ cfg_id: {} ot_id: {} -> {}".format(varcfgid, cfg_idot,
                searchbase.prettyprintcfg(searchnodes_aux, cfgint)))
                #searchbase.prettyprintcfg(self.ctx.searchnodes, cfgint)))

            try:
                # Check searchable variable found is between the range.
                # It is important if searchable variables depends on another
                # searchable variables the values will only be known at this time,
                # at "runtime".
                #optimizer.checkBounds(self.ctx.searchnodes, cfgint)
                optimizer.checkBounds(searchnodes_aux, cfgint)
            except BoundsException as e:
                # Code generation failed, go to next!
                self.ctx.incCfgsInv()
                #logice.exception(e)
                logice.warning(e)
                # Fixed What to return to OpenTuner when experiment fail?
                return Result(state='ERROR',time=float('inf'))
            #end except

            searchbase.convertDesiredResult(searchnodes_aux, cfgint, self.ctx.revertsearch)
            ###
            # Evaluate the global scope.
            # before interpreting global scope need to add the global scope
            # search variables need to be added to global scope.
            topblkinterpreter(self.ctx.optuni.topblk)

            ###
            # Evaluate the search def block statements that may depend on the
            # global scope.
            localbuildcmd = searchobj.execute(searchobj.buildcmd)
            localruncmd = searchobj.execute(searchobj.runcmd)
            localcheckcmd = searchobj.execute(searchobj.checkcmd)
            #endfor

            #GenLocus.searchmode(self.ctx.optuni, './after_'+str(varcfgid)+'.locus')

            #self.convertdebug.write("%%% End Cfg {}.\n".format(cfg_id))
            gencode_start = time.time()
            undoList = CodeGen.genCode(self.ctx.parsedsrcfiles, self.ctx.orighashes,
                    self.ctx.optuni, self.ctx.iceargs, varcfgid)

            gencode_end = time.time()
            self.ctx.timing[searchbase.TIME_GEN].append(gencode_end - gencode_start)

        except OptimizationError as e:
            self.ctx.cfgserror += 1
            return Result(state='ERROR',time=float('inf'))

        except Exception as e:
            self.ctx.cfgserror += 1

            if self.ctx.iceargs.evalfail:
                raise e
            #

            # Code generation failed, go to next!
            logice.exception(e)
            #logice.error(e)

            # Fixed What to return to OpenTuner when experiment fail?
            return Result(state='ERROR',time=float('inf'))

        else:
            if self.ctx.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
                metric, evalstat = searchbase.empiricalEvaluation(self.ctx,
                                                              localbuildcmd, localruncmd,
                                                              localcheckcmd, cfgint)

                if evalstat == searchbase.EVAL_TIMEOUT:
                    self.ctx.cfgstimeout += 1
                elif evalstat == searchbase.EVAL_ERROR:
                    self.ctx.cfgserror += 1
                #
            #endif

        finally:
            searchbase.revert2origsearchnodes(self.ctx.searchnodes, self.ctx.revertsearch)
            logice.debug("Returned the optuni to original state.")
        #end try

        # Number of configurations executed
        self.ctx.incCfgsExec()

        searchbase.variantEpilog(undoList, self.ctx.iceargs.output)

        if evalstat == searchbase.EVAL_TIMEOUT:
            resobj = Result(time=float('inf'), state='TIMEOUT')
        elif evalstat == searchbase.EVAL_ERROR:
            if self.ctx.iceargs.evalfail:
                raise RuntimeError(f"{ICE_CLR_FAIL}Search interrupted by error!{bcolors.ENDC}")
            #
            resobj = Result(time=float('inf'), state='ERROR')
        else:
            resobj = Result(time=metric)
        #endif

        return resobj
    ###

    @staticmethod
    def encodeInitCfgSeeds(seeds, fileinitseed):
        """ Expect a list of seeds. Each seeds is a dictionary.
            In some hierarchical tools the cfg are saved in DB
            using only a few varibles. The initseed from locus file
            is used to fill the gaps. I think opentuner wants all
            the searchvariables not matter what.
        """
        ret = []
        # make the search var keys as strings instead of integers
        for d in seeds:
            newd = {}
            for k in d:
                newd[str(k)] = d[k]
            ##
            for k in fileinitseed:
                if k not in newd:
                    newd[k] = fileinitseed[k]
                #
            ##
            ret.append(newd)
        ##

        return ret
    ###
####

def main(args=None):
    from locus.main import main as smain
    smain(OpenTunerTool, argparser, args)
###
