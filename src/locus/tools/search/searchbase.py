'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, os, time, math, copy
from datetime import datetime

from locus.util.constants import ICE_OUTPUT_MODE_STDOUT,\
ICE_OUTPUT_MODE_INPLACE, ICE_OUTPUT_MODE_SUFFIX, ICE_CLR_INITSEED, bcolors
from locus.frontend.optlang.locus.locusast import OrStmt, OptionalStmt, IfStmt,\
    OrBlock, SearchEnum, SearchPowerOfTwo, SearchInteger, SearchPerm, RangeExpr,\
    Constant, AssignStmt, ID, ListMaker, FuncCall, processCallArgList,\
    Scope, Argument, OptSeqDef, _assignID, _lookupID, LocusNode
from locus.frontend.optlang.locus.locusvisitor import GenericVisitorWScope,\
     GenericVisitorUpdateId, NodePathVisitor
from locus.frontend.optlang.locus.optimizer import findScopeIdNew,\
    DeadCodeEliminationConsts, ConstantPropagation, ConstantFolding
from locus.backend.genlocus import GenLocus
from locus.util.misc import getTimingFunc
from .expandrec import recCheckPrintStack, checkIfs, allArgsEqual,\
     replaceVarsOtherCalls, replaceVars, argsAssignStmts
from .expandrecIter import createOPTRecursionTree 

log = logging.getLogger(__name__)

TIME_CHK = 'checkcmd'
TIME_RUN = 'runcmd'
TIME_BLD = 'buildcmd'
TIME_GEN = 'gencode'
TIME_SRCH = 'search'

EVAL_OK      = 0
EVAL_TIMEOUT = 1
EVAL_ERROR   = 2

def convertDesiredResult(searchnodes, cfgint, revertsearch):
    log.info("start")
    for key, val in searchnodes.items():
        p, n, scope = val
        if n.unqid in cfgint:

            #self.convertdebug.write("### About to replace {} {}.\n".format(n.unqid,type(n)))
            #for ent in self.optuni.entities():
                #ent.show(buf=self.convertdebug)

            # The OrBlock, OrStmt, SearchEnum replace by position given
            if type(n) is OrBlock or\
                type(n) is OrStmt or\
                type(n) is SearchEnum:
                iternode = iter(n)
                t = next(iternode)
                for i in range(cfgint[n.unqid]):
                    t = next(iternode)
                revertsearch[n.unqid] = t
                #print "@@@ ***",t
                #print "@@@ ---"
                #print "$$$ ***",p
                #print "$$$ ---"
                p.replace(n, t)

            elif type(n) is OptionalStmt:
                if cfgint[n.unqid]:
                    #get the stmt child of OptionalStmt
                    t = next(iter(n))
                    p.replace(n, t)
                else:
                    t = Constant(type(None), None)
                    p.replace(n, t)
                revertsearch[n.unqid] = t

            elif isinstance(n, SearchPowerOfTwo):
                t = Constant(int, cfgint[n.unqid])
                p.replace(n, t)
                revertsearch[n.unqid] = t

            elif isinstance(n, SearchPerm):
                # Create a listmaker a convert the items to whatever type it is.
                #print "convertDesiredResult", len(cfgint[n.unqid]), cfgint[n.unqid]
                newlist = []
                for el in cfgint[n.unqid]:
                    newlist.append(Constant(type(el),el))
                t = ListMaker(newlist)
                p.replace(n, t)
                revertsearch[n.unqid] = t

            else:
                raise RuntimeError("Type {} not identified.".format(repr(n)))
            #endif type

            #self.convertdebug.write("&&& Replaced {}\n".format(n.unqid))
            #for ent in self.optuni.entities():
                #ent.show(buf=self.convertdebug)

        else:
            # In hierarquical spaces, only selected variables are on the final
            # space expressed by the cfgint.
            log.info("Node {} not in cfgint.".format(n.unqid))
            #raise RuntimeError("Could not find in the cfg node id "
            #    "{}.".format(n.unqid))
        #endif n.unqid
    #endfor p, n
#enddef

def convertOnlyORDesiredResult(searchnodes, cfgint, revertsearch):
    log.info("start")
    orlist = {}
    for key, val in searchnodes.items():
        p, n, scope = val
        if n.unqid in cfgint:
            # The OrBlock, OrStmt, SearchEnum replace by position given
            if type(n) is OrBlock:
                orlist[n.unqid] = cfgint[n.unqid]
                iternode = iter(n)
                t = next(iternode)
                for i in range(cfgint[n.unqid]):
                    t = next(iternode)
                revertsearch[n.unqid] = t
                #print "@@@ ***",t
                #print "@@@ ---"
                #print "$$$ ***",p
                #print "$$$ ---"
                p.replace(n, t)
        else:
            # In hierarquical spaces, only selected variables are on the final
            # space expressed by the cfgint.
            log.info("Node {} not in cfgint.".format(n.unqid))
            #raise RuntimeError("Could not find in the cfg node id "
            #    "{}.".format(n.id))
        #endif n.id
    #endfor p, n
    return orlist
#enddef

def convergenceCriteria(starttime, stopafter):
    elapsed = (datetime.now() - starttime).total_seconds()
    if elapsed >= (stopafter * 60):
        return True

    return False
#enddef

def revert2origsearchnodes(searchnodes, revertsearch):
    log.debug("Reverting optspace to the original state.")
    #self.convertdebug.write("=== Reverting \n".format())
    for _id in revertsearch:
        if _id in searchnodes:
            p, n, scope = searchnodes[_id]
            t = revertsearch[_id]
            #self.convertdebug.write("@@ {} -> {}\n".format(t.id,_id))
            #t.show(buf=self.convertdebug)
            p.replace(t, n)
        else:
            raise RuntimeError("{} id no find in original "
                    "searchnodes.".format(_id))
        #endif

    #for ent in self.optuni.entities():
    #    ent.show(buf=self.convertdebug)
    #self.convertdebug.write("=== End REVERTING \n".format())

    revertsearch.clear()
#enddef

# printempty: decides if printing all search primitives even if 
# they are not part of the current configuration.
def prettyprintcfg(searchnodes, cfgint=None, sep=", ", printempty=False):
    ret = []
    localcfg = cfgint or {}
    #print only the space
    for _id in sorted(searchnodes):
        p,n,s = searchnodes[_id]
        #log.info("{}".format(n.id))
        foundval = None
        if _id in localcfg:
            rescfg = localcfg[_id]
            foundval = str(rescfg)
            if isinstance(n, SearchEnum):
                #log.debug(f"{n.unqid}: {n.values}")
                foundval = str(n.values[rescfg])

        if isinstance(p, AssignStmt):
            val = p.lvalue.name
        elif isinstance(n, OrStmt):
            val = "OrStmt"
        elif isinstance(n,OptionalStmt):
            val = "OptionalStmt"
        elif isinstance(n, OrBlock):
            val = "OrBlock"
        else:
            val = str(n)

        if foundval is not None:
            ret.append(str(_id)+": "+val+" = "+foundval)
        elif printempty:
            ret.append(str(_id)+": "+val)
        #endif
    return sep.join(ret)
#enddef

def variantEpilog(undoList, outputmode):
    # Undo the if changes are in place for next tests.
    if outputmode == ICE_OUTPUT_MODE_INPLACE:
        log.debug("undolist after each run: {}".format(undoList))
        for ud in undoList:
            unwindBackup(ud[1], ud[0])
        #endfor
    #endif
#enddef

def getMetricFromResult(stdout, stderr, tfunc):
    metric = None
    if tfunc is not None:
        timingfunc = getTimingFunc(tfunc)
        if timingfunc and stdout is not None and stderr is not None:
            log.info("Using external function to get kernel timing: {}".format(timingfunc))
            try:
                metric = timingfunc(stdout, stderr)
            except Exception as e:
                log.error("Error: {}. Problem parsing result by {}.\nStd\n {} \nErr:\n {}".format(e, tfunc,
                    stdout,
                    stderr))
                raise RuntimeError(e)
            #end try

            log.debug("Output:\n {} \nErr:\n {}".format(stdout, stderr))
        #endif
    #endif tfunc not none

    return metric
#endif

# calc timeout value
def calctimeout(timeout, elapsedmin, upperlimit, timeoutfactor):
    # Assuming that elapsedmin starts as None and if upper-limit is provided it
    # will be used first.
    if timeout and upperlimit is not None:
        if elapsedmin is None:
            rettimeout = upperlimit
        else:
            rettimeout = min(upperlimit, elapsedmin * timeoutfactor)
        log.debug("Timeout (sec) on next assessment: {}".format(timeout))
    elif timeout and elapsedmin is not None:
        rettimeout = elapsedmin * timeoutfactor
        log.debug("Timeout (sec) on next assessment: {}".format(timeout))
    else:
        rettimeout = None
        log.debug("NO timeout on next assessment!")
    #endif

    return rettimeout
#enddef

def printTimingReport(timedict):
    log.info("Time Report:")
    for key, val in timedict.items():
        if val:
            _sum = sum(val)
            _max = max(val)
            _min = min(val)
            _mean = _sum / len(val)
            _var = sum((xi - _mean) ** 2 for xi in val) / len(val)
            _stdev = math.sqrt(_var)
            log.info("{} (seconds) = sum: {:.3f} max: {:.3f} min: {:.3f} mean: {:.3f} var: {:.3f} stddev: {:.3f}".format(key,
                _sum, _max, _min, _mean, _var, _stdev))
        else:
            log.info("{} (seconds) = no time measurements.".format(key))
        #endif
    #endfor
#enddef

def empiricalEvaluation(ctx, bldcmd, evalcmd, chkcmd, cfg):

    # build, compile
    bldres = ctx.bldhand.run(bldcmd)
    ctx.timing[TIME_BLD].append(bldres['time'])

    # Calc timeout
    timeout = calctimeout(ctx.iceargs.timeout, 
                          ctx.elapsedmin,
                          ctx.iceargs.upper_limit,
                          ctx.iceargs.timeoutfactor)

    # run
    evalres = ctx.evalhand.run(evalcmd, timeout, _raise_=ctx.iceargs.evalfail)
    ctx.timing[TIME_RUN].append(evalres['time'])

    metric = float("inf")
    metrictfn = None

    # timed out? not need to continue
    if evalres['timeout'] == True:
        retstatus = EVAL_TIMEOUT
        return metric, retstatus
    #endif

    # get metric
    if evalres['stdout'] is not None and\
       evalres['stderr'] is not None and\
       evalres['returncode'] == 0 and\
       ctx.iceargs.tfunc is not None:
        # Can return a metric or a dictionary with more info
        metrictfn = getMetricFromResult(evalres['stdout'], 
                                     evalres['stderr'], 
                                     ctx.iceargs.tfunc)
        if type(metrictfn) is dict:
            metric = metrictfn['metric']
        elif metrictfn is not None:
            metric = metrictfn
        #endif
    #endif

    #log.info(f"metric returned: {type(metric)} {metric}")

    # calculation to update the max time of a an experiment(timeout)
    # the elapsedmin is taken from the fastest valid experiment
    if metric < ctx.metricmin:
        ctx.elapsedmin = evalres['time']
        ctx.metricmin = metric
    #endif

    retstatus = EVAL_ERROR
    evalretcode = evalres['returncode']
    chkres = None
    if chkcmd is not None and \
       evalretcode is not None and \
       evalretcode == 0:
        chkres = ctx.chkhand.run(chkcmd, _raise_=False)
        ctx.timing[TIME_CHK].append(chkres['time'])

        # If check failed, return error
        if chkres['returncode'] != 0:
            retstatus = EVAL_ERROR
        else:
            retstatus = EVAL_OK
        #

    elif chkcmd is None and \
         evalretcode is not None and \
         evalretcode == 0:
        retstatus = EVAL_OK

    elif evalretcode is None or \
         evalretcode is not None and \
         evalretcode != 0:
        retstatus = EVAL_ERROR
    #endif

    if type(metrictfn) is dict:
        log.debug(f"Created metricinfo: {metrictfn} ")
        metric_kwargs = {'metricinfo': metrictfn}
    else:
        metric_kwargs = {}
    #
    ctx.reshandobj.run(cfg, bldres, evalres, chkres, 
                       metric, prettyprintcfg, ctx.searchstarttime, 
                       **metric_kwargs)

    return metric, retstatus
#enddef

# Replace the calls on all CodeRegs of a optseq that is recursive.
# The tree generated is the replacement on all call places.
# It needs the values that limits the recursion, but these values are suposed to be known up to this point.
# The replacements is done before the search. 
#def replaceRecursion():
#    log.info("Start")
#    log.info("End")

space = 0
#input: optimization universe, list with recursive optseqs 
#output: recursion tree for each optsew  
#Caveats: this does not treat indirect recursion
def createRecursionTree(optuni, recoptseqs, iceargs):
    log.info("Start")

    funccalls = [] # all funccalls excepts the ones that represent recursion.
    #selfcalls = {} # recursive calls inside the function definition
    #ifstmts = {}

    # find all func calls
    for ent in optuni.entities():
        senodes = IfStmt, FuncCall 
        visitor = GenericVisitorWScope(senodes)
        visitor.visit(ent)
        # Remove the recursion calls
        nonselfcalls = [(fcpar, fcnode, fcscop) for fcpar, fcnode, fcscop in visitor.values 
                if isinstance(fcnode, FuncCall) and ent.id.name != fcnode.name.name]
        #secalls = [(fcpar, fcnode, fcscop) for fcpar, fcnode, fcscop in visitor.values 
        #        if isinstance(fcnode, FuncCall) and ent.id.name == fcnode.name.name]
        #ifnodes = [(stpar, stmt, stscop) for stpar, stmt, stscop in visitor.values
        #        if isinstance(stmt, IfStmt)]
        #print nonselfcalls, visitor.values
        funccalls.extend(nonselfcalls)
        #selfcalls[ent] = secalls
        #ifstmts[ent] = ifnodes
    #endfor

    optuni.topblk.execute(optuni.topblk.scope, inplace=True)
    rectress = []
    for ts in recoptseqs:
        #print ts.args
        optseqname = ts.id.name

        # Algo to create recursion tree:
        # 1. Get the starting argument values of the call
        # 2. Get the formula that these recursive calls apply to
        # the arguments and all the places these parameters are
        # used to know when to stop.
        # Find the return expression used to finish execution.
        # 3. Extrapole the expression and create the tree

        #print "ts.body"
        #ts.body.show()

        #TODO expand the search variables required on the recursion to OR blocks and then do rest as before!

        ######
        # Create block for interior nodes of the recursion, 
        # with no ifs. 
        interbody = copy.deepcopy(ts.body) 
 
        senodes = IfStmt, FuncCall, OrBlock #Return 
        visitor = GenericVisitorWScope(senodes)
        visitor.visit(interbody)
        ifnodes = [(stpar, stmt, stscop) for stpar, stmt, stscop in visitor.values
                if isinstance(stmt, IfStmt)]
        allcas = [(stpar, stmt, stscop) for stpar, stmt, stscop in visitor.values
                if isinstance(stmt, FuncCall)]
        selfcas = [(stpar, stmt, stscop) for stpar, stmt, stscop in allcas
                if ts.id.name == stmt.name.name]
        othercas = [(stpar, stmt, stscop) for stpar, stmt, stscop in allcas
                if ts.id.name != stmt.name.name]
        ornodes = [(stpar, stmt, stscop) for stpar, stmt, stscop in visitor.values
                if isinstance(stmt, OrBlock)]

        if ifnodes:
            ifpar, ifstmt, _ = ifnodes[0] # 1st item of the list, 2nd item of the tuple, and 1st field of the ifblks.
            retexpr = ifstmt.ifblks[0][0]
            log.debug("retexpr: {}".format(retexpr))
            ifpar.remove(ifstmt)
        else:
            raise RuntimeError("Could not find a If stmt!")
        #endif

        if ornodes:
            orblkpar, orblk, _ = ornodes[0]
        else:
            raise RuntimeError("Could not find OrBlock!")
        #endif

        ######
        # Visit e get the recursion call's ast path from the OptSeq path.
        # Create a separate scope.
        # Clean it up by removing any call before or after.
        pathnodesreccas = []
        for rcpar, rcnode, rcscop in selfcas:
            #print id(rcnode)
            visitor = NodePathVisitor(rcnode)
            visitor.visit(interbody)
            #print visitor.pathnodes
            #ret = checkIfs(rcnode, visitor.pathnodes, scope)
            #print rcnode, ret
            pathnodesreccas.append(visitor.pathnodes)
        #endfor

        mergedselfcas = [x + (y,) for x,y in zip(selfcas, pathnodesreccas)]

        ######
        # Create block for leaves of the recursion, no if and no
        # recurive calls.
        blksidtodel = {} #set()

        # get index of block in the OrBlock that contains recursive calls.
        for torpar, torblk, _ in ornodes:
            blksidtodel[torblk.unqid] = []
            for blkidx, blk in enumerate(torblk):
                #for scpar, scstmt, scscop in selfcas:
                for scpar, scstmt, scscop, pathnodes in mergedselfcas:
                    #if blk in pathnodes and b ... 
                    tmp = [x for _, x, _ in pathnodes]
                    #if blk == scpar and blkidx not in blksidtodel[torblk.unqid]:
                    if blk in tmp and blkidx not in blksidtodel[torblk.unqid]:
                        blksidtodel[torblk.unqid].append(blkidx)
        #endfor

        # Separate the recursive calls according to the block.
        # They must handled separetely when
        #selfcasperblk = {}
        #for scpar, scstmt, scscop in selfcas:
        #    if scpar.unqid not in selfcasperblk:
        #        selfcasperblk[scpar.unqid] = []
        #    #endif
        #    selfcasperblk[scpar].append((scpar,scstmt,scscop))
        ##endfor

        leafbody = copy.deepcopy(interbody) 

        senodes = OrBlock
        visitor = GenericVisitorWScope(senodes)
        visitor.visit(leafbody)
        orndleaf = [(stpar, stmt, stscop) for stpar, stmt, stscop in visitor.values
                if isinstance(stmt, OrBlock)]

        if orndleaf:
            for _, orblkleaf,_ in reversed(orndleaf):

                # Based on the index get the blocks of the OrBlock that 
                # has recursive calls and we want to remove.
                if blksidtodel[orblkleaf.unqid]:
                    blktodel = []
                    for blkid in reversed(list(blksidtodel[orblkleaf.unqid])):
                        #blktodel.add(orblkleaf[blkid])
                        blktodel.append(orblkleaf[blkid])
                    #endfor

                    # Remove the blocks on the OrBlock.
                    for blk in blktodel:
                        orblkleaf.remove(blk) 
                    #endfor
                #endif
            #endfor
        else:
            raise RuntimeError("Expected a OrBlock node.")
        #endif

        ######


        # The funccall in CodeReg for the recursive OptSeq
        # is where to place for tree.
        for fcpar, fcnode, fcscop in funccalls:
            fcallname = fcnode.name.name
            # Is this fuction on the recursion list?
            if fcallname == optseqname:
                #ts.show()
                log.info("executing {} args: {}".format(optseqname, fcnode.arglist))

                #print the arguments of the funccall 
                #print "Func call arguments", fcnode.arglist
                #print the argument of the recursive calls
                #for recalls in selfcalls[ts]:
                #    print "RecCall",recalls[1].name,recalls[1].arglist
                #    recalls[1].arglist.show()

                scope = ts.scope.customCopy() #Scope()
                processCallArgList(fcnode.arglist, fcscop, ts.scope, scope, ts.args.args)
                #print ",".join(str(k)+":"+str(v) for k,v in scope.iteritems())

                #print orblk #.show()
                #print ("Interior:")
                #print (orblk)
                #orblk.show()
                #print("Leaf:")
                #orblkleaf.show()
                #print (orblkleaf)

                # Evaluate and expand recursive tree base on the OptSeq structure code.
                # For each recursive calls on the same block.
                node = recCheckPrintStack(retexpr, mergedselfcas, othercas, scope, orblk,
                        orblkleaf, ts)
                ##print "%%%% What:"
                ##node.show()
                #visitor = GenericVisitorUpdateId()
                #visitor.visit(node)

                #print "%%%% Eita:"
                #node.show()

                # Adding the function call variables here
                argsAssignStmts(_, ts.args.args, fcnode.arglist, fcpar, fcnode)
                #for formarg, actarg in zip(ts.args.args, fcnode.arglist):
                #    if actarg.id is not None:
                #        print "actarg id:",type(actarg.id)
                #    if actarg.expr is not None:
                #        print "actarg expr:",type(actarg.expr)
                #
                #    print "form:", formarg, type(formarg), "act:",actarg, type(actarg)
                #endfor

                # Replace the function call.
                fcpar.replace(fcnode, node)

            #endif
        #endfor
    #endfor

    # After so much change reset the node unqid. 
    # It matters for the optimizations ahead.
    LocusNode.resetidcounter()
    splst = [(n, s) for n, s in optuni.allspaces()]
    for name, sp in sorted(splst, key=lambda tup: tup[0]):
        visitor = GenericVisitorUpdateId()
        visitor.visit(sp)
        log.info("Reset nodes id: {} id: {}".format(name, sp.unqid))
    #endfor

    # Recalculate the scope. not sure is the correct place if there are multiple
    # calls to a recursive optset.
    log.info("Recreating the scope to account for all the changes...")
    optuni.createscope()
    log.info("Done scope.")

    if iceargs.debug or iceargs.saveexprec:
        exprecfile = iceargs.rundirpath+"/"+"bef_expandedrec.locus"
        log.debug("Generating expanded recursion in {} ...".format(exprecfile))
        GenLocus.searchmode(optuni, exprecfile)
        log.info("Generated expanded recursion in {} .".format(exprecfile))
    #endif

    #for sp in optuni.optspaces():
    #    sp.show()

    optuni.topblk.execute(optuni.topblk.scope, inplace=True)
    log.info("Compiler optimizations...")
    #log.info("{}".format(optuni.topblk.scope))

    ttlncprop, ttlncfold, ttldcodel = 0, 0, 0

    applyOpt = True
    limitExpr = 50
    limitIf = 50

    if applyOpt:
        for sp in optuni.optspaces():
            log.info("  on {}".format(sp.name))
            npass = 0
            while True:
                cprop = ConstantPropagation(sp, optuni.topblk.scope)
                ncprop, reachlimitExpr, reachlimitIf = cprop.execute(limitExpr,
                        limitIf)

                cfold = ConstantFolding(sp)
                ncfold = cfold.execute()

                dcodel = DeadCodeEliminationConsts(sp)
                ndcodel = dcodel.execute()

                ttlncprop += ncprop
                ttlncfold += ncfold
                ttldcodel += ndcodel

                npass += 1
                if ncprop == 0 and ncfold == 0 and ndcodel == 0 and \
                        not reachlimitExpr and not reachlimitIf:
                    break

                if reachlimitExpr and ncprop == 0:
                    limitExpr += 50
                if reachlimitIf and ncprop == 0:
                    limitIf += 50
            #endwhile
            log.info("Done cprop: {} cfold: {} dcodel: {} in {} steps.".format(
                ttlncprop, ttlncfold, ttldcodel, npass))
        #endfor

        # After deadcode elimination it might be good to do it again.
        log.info("Recreating (2nd time) the scope to account for all the changes...")
        optuni.createscope()
        log.info("Done scope.")

        if iceargs.debug or iceargs.saveexprec:
            exprecfile = iceargs.rundirpath+"/"+"aft_expandedrec.locus"
            log.debug("Generating expanded recursion in {} ...".format(exprecfile))
            GenLocus.searchmode(optuni, exprecfile)
            log.info("Generated expanded recursion in {} .".format(exprecfile))
        #endif
    #endif

    log.info("End")
#enddef

def applyDFAOpts(optuni, iceargs):
    import locus.frontend.optlang.locus.locuscfg as lcfg
    import locus.frontend.optlang.locus.dataflowopts as dfw

    log.info("Apply DFA Compiler optimizations...")
    #log.info("{}".format(optuni.topblk.scope))

    if iceargs.debug:
        optfile = iceargs.rundirpath+"/"+"bef_dfaopt.locus"
        log.debug("Generating Locus before DFA in {} ...".format(optfile))
        GenLocus.searchmode(optuni, optfile)
        log.info("Generated Locus before DFA in {} .".format(optfile))
    #endif

    #
    # optimize the topblk before anything else
    #
    dfw.combinedCfgAstOpts(optuni.topblk)

    # get reachdef of exit node
    topentry = lcfg.LocusCFG.ast2cfg(optuni.topblk)
    dfw.ReachingDefs.exec(topentry)
    bbdict = lcfg.LocusCFG.dictfycfg(topentry)
    exitnd = None
    for nd in bbdict.values():
        if nd.isexit():
            exitnd = nd
            break
    ##

    topblkOUT = set()
    if nd is not None:
        #print(len(exitnd.stmtsIN))
        log.debug("Exit nd OUT: "+"; ".join([str(a) for a in nd.OUT]))
        topblkOUT = nd.OUT
    #print("Exit nd IN: ", exitnd.stmtsIN[0]) 

    for sp in optuni.optspaces():
        log.info("  on {}".format(sp.name))

        tmpnextbody = sp.body
        props, folds, deaths = dfw.combinedCfgAstOpts(tmpnextbody, entryout=topblkOUT)

        log.info("Done cprop: {} cfold: {} dcodel: {}.".format(
            props, folds, deaths))
    #endfor

    # After deadcode elimination it might be good to do it again.
    log.info("Recreating (2nd time) the scope to account for all the changes...")
    optuni.createscope()
    log.info("Done scope.")

    if iceargs.debug:
        optfile = iceargs.rundirpath+"/"+"aft_dfaopt.locus"
        log.debug("Generating optimized Locus in {} ...".format(optfile))
        GenLocus.searchmode(optuni, optfile)
        log.info("Generated optimized Locus in {} .".format(optfile))
    #endif
#enddefine


#DEPRECATED!!!! Used the one in expandrec.py
space = 0
def recCheck(retexpr, reccalls, othercalls, scope, interiorbody, leafbody, ts):
    global space

    log.debug("Starting recCheck {}".format(scope))

    #print retexpr, scope
    stop = False if retexpr is None else retexpr.execute(scope)
    #print "EITA", type(stop), stop, scope 
    log.debug("stop: {} {} {}".format(type(stop), stop, scope))

    if not stop:
        nodestomove = []
        for rcpar, rcnode, rcscop, pathnodes in reccalls:
            #print " "*space,"### Entering ", id(rcnode)
            space += 1
            #print "reccall scope", rcscop, " vs pscope",scope,"vs ts.scope", ts.scope
            #newscope = copy.deepcopy(scope)
            #tmprcscop = copy.deepcopy(rcscop)
            #tmprcscop.parent = scope.parent
            #newscope = tmprcscop.customCopy()
            #newscope.parent = scope
            newscope = ts.scope.customCopy()
            tmprcscop = rcscop.customCopy()
            #print "newscope",id(newscope),newscope
            #print "tmprcscop",id(tmprcscop), tmprcscop

            # Reset the parent scopes, so they are clean.
            # pathnodes does not include the globalscope. It
            # includes from the OptSeq scope and down.
            for par, node, scp in pathnodes:
                #print id(node), type(node), scp, id(scp)
                for key, val in scp.items():
                    scp[key] = None
                #endfor
            #endfor

            #print "ts.scope --",  ts.scope, id(ts.scope)
            # Sync the scope received and the one copied.
            # Here the parameters of the OptSeq are assigned to
            # the scope, so it can execute its expressions.
            for key, val in scope.items():
                #print "updating", key, val
                _assignID(ID(key), val, tmprcscop) 
            #endif

            inpcheck = []
            # find var
            # Assuming arguments of type <id>
            for i, arg in enumerate(rcnode.arglist):
                #actargexp = arg.expr
                actargexp = arg.id
                #print "arg ", actargexp,"...", id(tmprcscop), tmprcscop
                res = findScopeIdNew(actargexp, tmprcscop)
                #print "arg ", actargexp," = ", res, id(tmprcscop), tmprcscop
                inpcheck.append(res)
            #endfor

            # If the arguments of the recursion call matches the parent call: Stop recursion!
            argsmatch = True

            # if the recfunction arguments have the same value
            for i, formarg in enumerate(ts.args.args):
                if formarg.id.name in scope and scope[formarg.id.name] is not None:
                    #print i,":",formarg.id.name,"=",scope[formarg.id.name]," ? ",inpcheck[i]
                    if scope[formarg.id.name] != inpcheck[i]:
                        argsmatch = False
                        break
                else:
                    RuntimeError("Argument {} supposed to be in scope.".format(formag.id.name))
                #endif
            #endfor

            ifret = checkIfs(rcnode, pathnodes, scope)
            #print "checkIfs", ifret

            # Backup just the dictionary of the scope.
            savedcurscp = [copy.deepcopy(scp.table) for par, node, scp in pathnodes]

            # Had to make a full copy because the it's used a
            # customCopy.
            #savedtmprcscop = copy.deepcopy(tmprcscop)

            if not argsmatch and ifret:
                # add the func block to the tree
                #processCallArgList(rcnode.arglist, scope, ts.scope, newscope, ts.args.args)
                processCallArgList(rcnode.arglist, tmprcscop, scope, newscope, ts.args.args)
                #processCallArgList(rcnode.arglist, rcscop, ts.scope, newscope, ts.args.args)
                #print "Start",",".join(str(k)+":"+str(v) for k,v in newscope.iteritems())
                #print "newscope",newscope
                child = recCheck(retexpr, reccalls, othercalls, newscope,
                        interiorbody, leafbody, ts)
                #if child is None:
                #    print "Child None"
                #nodestomove.append((orndchi, rcpar, child, rcnode, savedtmprcscop))
                #nodestomove.append((pathnodes, rcpar, child, rcnode, savedtmprcscop))
                nodestomove.append((pathnodes, rcpar, child, rcnode, None))
                #endif
            else:
                log.debug("Recursive call the same as the parent call. Avoided infinite recursion.")
                #nodestomove.append((pathnodes, rcpar, None, rcnode, savedtmprcscop))
                nodestomove.append((pathnodes, rcpar, None, rcnode, None))
            #endif

            # After the recursion needs to put back of the
            # pathnodes of the rcnode.
            for (_, _, scp), bkpscp in zip(pathnodes, savedcurscp):
                for key, val in scp.items():
                    scp[key] = bkpscp[key]
                #endfor
            #endfor

            space -= 1
            #print " "*space,"$$$ Leaving ", id(rcnode)
        #endfor

        dictparnone = {}
        dictpartlt  = {}
        dictpar = {}

        # separate the nodestomove by parent. If all the nodes of the same
        # parent are to be moved, the full parent block is to be removed.
        #for orchi, par, chi, no, _ in nodestomove:
        for pathnodes, par, chi, no, _ in nodestomove:
            # 1st node of pathnodes is the block of the OptSeq,
            # 2nd is the OrBlokc, 3rd the block we need.
            _, orchi, _ = pathnodes[2]

            idx = orchi
            if idx not in dictpar:
                dictpar[idx]     = []
            if idx not in dictparnone:
                dictparnone[idx] = 0
            if idx not in dictpartlt:
                dictpartlt[idx]  = 0

            dictpartlt[idx] += 1

            if chi is None:
                dictparnone[idx] += 1

            dictpar[idx].append((orchi, par, chi, no))
        #endfor

        #print "dictpar:",dictpar
        #print "dictparnone",dictparnone
        #print "dictpartlt",dictpartlt

        # If None replace the call with None; stmt.
        # This list holds them.
        noneconstlst = []

        #for k, v in scope.iteritems():
        #    for _, _, _, _, sc in nodestomove:
        #        v2 = _lookupID(ID(k), sc)
        #        if v != v2:
        #            raise RuntimeError("Scope inconsistent!!!")
        #endfor

        # After getting the children, gen replace
        # on the block
        for _, par, chi, no, _ in nodestomove:
            if chi is None:
                ah = Constant(type(None), None)
                par.replace(no, ah)
                noneconstlst.append(ah)
            else:
                par.replace(no, chi)
        #endfor

        remoblks = []

        # if all the rec calls are to None, remove the entire block from the OrBlock.
        for idxblk in dictpar:
            if dictparnone[idxblk] == dictpartlt[idxblk]:
               parchildren = dictpar[idxblk] 
               #orchi, _, _, _ = parchildren[0]
               posblk = interiorbody.blocks.index(idxblk)
               #remoblks.append((posblk,orchi))
               remoblks.append((posblk,idxblk))
        #endif

        # First, get the parents and their position, then actually remove.
        for pos, par in remoblks:
            log.debug("Removing block...")
            interiorbody.remove(par)
        #endfor

        # Change values of other calls, according to scope.
        replvarot = []
        #replaceVarsOtherCalls(nodestomove, othercalls, replvarot)

        # Copied the original block
        newblock = copy.deepcopy(interiorbody)

        # Put other calls arguments back to orinial value
        for parot, arg in replvarot:
            #print "Putting back arg", arg.id #, parot
            arg.expr = None
        #endfor

        # Replace the variables by their values in the scope.
        # Replace only the ones not set yet. 
        for k, v in scope.items():
            log.debug("[Interior] Replace scope:  {} <= {} ".format(k, v))
            replaceVars(newblock, k, v)
        #endfor

        # Put the removed blocks back. 
        #sortedremoblks = sorted(remoblks, reverse=True, key=lambda tup: tup[0])
        sortedremoblks = sorted(remoblks, key=lambda tup: tup[0])

        for pos, par in sortedremoblks:
            log.debug("Adding back block...")
            interiorbody.blocks.insert(pos, par)
        #endfor

        # Return the block to the original state
        for _, par, chi, no, _ in nodestomove:
            if chi is None:
                par.replace(noneconstlst.pop(0), no)
            else:
                par.replace(chi, no)
        #endfor

        return newblock

    else:
        allequal = allArgsEqual(scope, ts.args.args)  

        # If all the arguments of the recursive optset are equal avoid it!
        if allequal:
            tmpbody = None
        else:
            tmpbody = copy.deepcopy(leafbody) 

            # Replace the variables by a their values in the scope.
            for k, v in scope.items():
                log.debug("[Leaf] Replace scope:  {} <= {} ".format(k, v))
                replaceVars(tmpbody, k, v)
            #endfor
        #endif

        #print "returning a tmpbody:",tmpbody
        #print "-------------------"
        return tmpbody
    #endif
#enddef

#input: list of OptSeqs
#output: list of OptSeqs with recursion
def findRecursions(optseqs):
    recursions = []
    for ts in optseqs:
        optseqname = ts.id.name
        log.debug("optseq _ {} _ has recursion?".format(optseqname))
        senodes = FuncCall 
        visitor = GenericVisitorWScope(senodes)
        visitor.visit(ts)
        for par, call, scop in visitor.values:
            log.debug("call {}?".format(call.name))
            if call.name.name == optseqname:
                recursions.append(ts)
                log.debug("YES!")
                break
            #endif
        #endfor
    #endfor

    return recursions
#enddef

def expandRecursion(optuni, iceargs):
    # Find, generate the tree representing recursion and replace
    # them in optuni. Recursion can only happen on OptSeqs.
    optseqslist = [os for os in optuni.entities() if isinstance(os, OptSeqDef)]
    recursions = findRecursions(optseqslist)
    if recursions: 
        log.info("recursions len {}".format(len(recursions)))
        #createRecursionTree(optuni, recursions, iceargs)
        # only the optseqs expanded should be removed 
        # to make the optimization space less complicated...
        optseqsexpanded = createOPTRecursionTree(optuni, recursions, iceargs)
        # remove the optseq from the optuni 
        log.info("Removing recursive Optseq from the opt space...")
        for rec in optseqsexpanded:
            name = rec.id.name
            del optuni[name]
            log.info("... {} removed".format(name))
        #endfor
    #endif
#endif

#
# Expect a list of configuration seeds. 
# Each cfg seed is represented as a dictionary.
#
def seedcfgsstr(seeds, preamble=None, prcolor=ICE_CLR_INITSEED):
    nl = '\n'
    ret = f'{prcolor}{preamble}{bcolors.ENDC}'
          #f' {nl+"-" if seeds else "None"}'
    if seeds:
        #ret += f' {nl+"-"}'
        for cfg in seeds:
            strseed = ", ".join([str(k)+": "+str(cfg[k]) for k in
                    sorted(cfg, key=lambda x: int(x))])
            ret += f'{nl+"-"} {{{strseed}}}'
        ##
    else:
        ret += " None"
    #

    return ret
#

def buildSearchNodes(ent, searchnodes):
     for ty in OrBlock, OptionalStmt,\
            OrStmt, SearchEnum,\
            SearchPerm, \
            SearchPowerOfTwo:

        visitor = GenericVisitorWScope(ty)
        visitor.visit(ent)
        dictid = {n.unqid:(p,n,s) for p,n,s in visitor.values}
        searchnodes.update(dictid)
    #endfor ty
#enddef

