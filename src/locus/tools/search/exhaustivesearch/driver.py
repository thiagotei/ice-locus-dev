'''

@author: thiago
'''

import logging, argparse
import itertools

from locus.tools.search.searchtoolinterface import AbstractSearchTool, argparser as absstparser

argparser = argparse.ArgumentParser(add_help=False,
                                    parents=[absstparser])

logice = logging.getLogger(__name__)

class ExhaustiveSearch():

    def __init__(self):
        pass

    def exec(self, objfn, space, max_evals=None): 
        logice.debug("Start")

        for count, x in enumerate(itertools.product(*space)):
            if max_evals is not None and count > max_evals:
                logice.info("Reached the max_evals {}".format(count))
                break
            #

            # Assumes that ther order of the search parameters in point space x is the same as 
            # the one in the input space list
            inp = {}
            for k,v in zip(space, x):
               inp[k.paramkey] = v 
            #

            objfn(inp)
        #
        logice.debug("End")
    ##
###
