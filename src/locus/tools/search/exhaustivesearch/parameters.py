'''

@author: thiago
'''

import logging, itertools, math

log = logging.getLogger(__name__)

class Parameter():

    def __init__(self, paramkey):
        self.paramkey = paramkey
    ##

    def __iter__(self):
        return self
    ##

    def __str__(self):
        return str(self.__class__)+" key: "+self.paramkey
    ##
##

class NumericParameter(Parameter):

    def __init__(self, key, _min, _max):
        super().__init__(key)
        if _min < 0:
            raise ValueError('min should be >= 0 : {} < 0'.format(_min))

        if _max <= 0:
            raise ValueError('max should be > 0 : {} <= 0'.format(_max))

        if _min >= _max:
            raise ValueError('min is bigger than max: {} >= {} '.format(_min, _max))

        self.min = int(_min)
        self.max = int(_max)
        self._cur = self.min
    ##
##

class PowerOfTwoParameter(NumericParameter):

    def __init__(self, key, _min, _max):
        super().__init__(key, _min, _max)

        if (_min & _min-1) > 0:
            raise ValueError("{} is not power of two.".format(_min))

        if (_max & _max-1) > 0:
            raise ValueError("{} is not power of two.".format(_max))
    ##

    def __next__(self):
        if self._cur > self.max:
            raise StopIteration
        else:
            ret = self._cur
            self._cur *= 2
            return ret
    ##

    def search_space_size(self):
        return int(math.log(self.max,2)) - int(math.log(self.min, 2)) + 1
    ##
##

class IntegerParameter(NumericParameter):

    def __init__(self, key, _min, _max, _step=1):
        super().__init__(key, _min, _max)
        if _step < 1:
            raise ValueError('Step should be at lest 1: {} < 1'.format(_step))
        
        self.step = _step
    ##

    def __next__(self):
        if self._cur > self.max:
            raise StopIteration
        else:
            ret = self._cur
            self._cur += self.step
            return ret
        #
    ##

    def search_space_size(self):
        return int((self.max - self.min)/self.step) + 1
##

class BooleanParameter(Parameter):

    def __init__(self, key):
        super().__init__(key)
        self.__next = 2 
    ##

    def __next__(self):
        if self.__next == 2:
            self.__next = 1
            return True
        elif self.__next == 1:
            self.__next = 0
            return False
        else:
            raise StopIteration
        #
    ##

    def search_space_size(self):
        return 2
    ##
##

class EnumParameter(Parameter):

    def __init__(self, key, vallist):
        super().__init__(key)
        self._vallist = vallist
        self.__idx = 0
    ##


    def __next__(self):
        try:
            item = self._vallist[self.__idx]
        except IndexError:
            raise StopIteration()
        self.__idx += 1
        return item
    ##

    def search_space_size(self):
        return max(1, len(self._vallist))
    ##
##

class PermutationParameter(Parameter):
    
    def __init__(self, key, vallist):
        super().__init__(key)
        self._vallist = vallist
        log.info("list: {} {}".format(type(self._vallist), self._vallist))
    ##

    def __iter__(self):
        self.__myiter = iter(itertools.permutations(self._vallist))
        return self

    def __next__(self):
        return next(self.__myiter)
    ##

    def search_space_size(self):
        return math.factorial(max(1, len(self._vallist))) 
    ##
##


