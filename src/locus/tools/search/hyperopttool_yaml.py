'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys
import argparse, copy
import adddeps
try:
    from hyperopt import hp, fmin, tpe, STATUS_OK as HP_STATUS_OK, STATUS_FAIL as \
                    HP_STATUS_FAIL, Trials
except ImportError as e:
    print("You need to install hyperopt (I've used pip for it)!")
    raise

from locus.backend.codegen import CodeGen
from .searchtoolinterface import AbstractSearchTool
from locus.util.cmds import Commands
from locus.util.misc import getDefaultTiming, unwindBackup, getTimingFunc
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo, Boolean, \
                    Enum
from locus.tools.search.opentunertool import ToolSpec
from locus.util.constants import ICE_OUTPUT_MODE_STDOUT,\
                                ICE_OUTPUT_MODE_INPLACE

argparser = argparse.ArgumentParser(add_help=False)

logice = logging.getLogger("HyperOptTool")

class HyperOptTool(AbstractSearchTool):
    """ HyperOpt driver to search optimization space """

    def __init__(self, iceargs):
        self.iceargs = iceargs
        self.optuni = None
        self.hyopspace = None
        self.parsedsrcfiles = None
        # Results are saved here
        self.trials = None

        logice.debug("HyperOptTool init")

        # Used to save info about the optimizations that are used later.
        self.otparamdict = {}


    def convertOptUni(self, optuni):
        logice.debug("start")
        self.optuni = optuni
        #
        # Describe the optimization space
        # Example: 
        # hyopspace = { 
        #  'matmul_pips_tiling_0': hp.choice('choice',[
        #                       [], # Represents not using
        #                       { 'factor1': hp.quniform('factor1_int', v1.min,
        #                       v1.max, 1),
        #                         'factor2': hp.quniform('factor2_int', v2.min,
        #                       v2.max, 1)}
        #}
        self.hyopspace = {}

        #
        # Going through the optimization space in optimization universe.
        #
        for space in self.optuni:
            ind_opt = 0
            for opt in space.traverse():
                logice.debug("opt %s",opt.val.name)

                # Getting the parameters about the search and the optimization
                # to give to OpenTuner.
                optclass = opt.val.classptr # get the optimization class
                optname = optclass.getOptName()
                searchparamsspec = optclass.getSearchParams() # Types of parameters required by the tool
                searchparamstype = optclass.getSearchTypes() # Types for the search
                toolname = optclass.getToolName()

                hyopname = "{}_{}_{}_{}".format(space.name, toolname, optname,
                        ind_opt)

                if hyopname not in self.otparamdict:
                    self.otparamdict[hyopname] = {}

                if 'info' not in self.otparamdict[hyopname]:
                    self.otparamdict[hyopname]['info'] = {'spacename':space.name,'toolname': toolname,
                            'optname': optname, 'indopt': ind_opt}

                # TODO check if it is case of not having this optmizations
                # (check * and +)
                # None here means it put on the space of optimization the non
                # use of this optimization.
                hyopparams = {}
                # Going through the search params of current optimization
                for p, v in opt.val.params.items():
                    logice.debug("sp {} = {} type {}".format(p,v,type(v)))
                    # Check if there is any search params
                    #TODO check if the attribute is a star, which means that the instruction 
                    # So far star is not used with opentuner
#                    if not searchparamsspec:
#                        self.__manipulator
#                    else:
#                    paramname = "{}_{}".format(hyopname,p)
                    if p in searchparamsspec:
                        obj = ToolSpec(space.name, toolname, optname, ind_opt,\
                                p, None, ToolSpec.OPTTOOL)
                        paramname = repr(obj)
                        self.otparamdict[hyopname][paramname] = obj

                        if type(v) == PowerOfTwo:
                            hyopparams[paramname] = hp.quniform("{}_param".format(paramname), v.min, v.max, 1)

                        elif type(v) == Integer:
                            hyopparams[paramname] = hp.quniform("{}_param".format(paramname), v.min, v.max, 1)

                        elif type(v) == Boolean:
                            hyopparams[paramname] = hp.choice("{}_param".format(paramname), [True, False])

                        elif type(v) == Enum:
                            hyopparams[paramname] = hp.choice("{}_param".format(paramname), v.options)

                        elif type(v) == str or type(v) == int:
                            # Here the searchable parameter have only option,
                            # which means they dont need to be searchable.
                            # add them to the paramtup so they will be using
                            # when optimizing tool is called.
                            logice.warning("searchable param - {} = {} - do not require "
                                    "search. ".format(p,v))
                        else:
                            logice.error("Could not find the correct type for the "
                                "param - {} - in opt {} in "
                                "{}!".format(opt.val.name, space.name))
                        #endif
                    #endif searchparamsspec
                #endfor

                if not hyopparams:
                    #FIXME if the instruction must be executed this need to be
                    # something else
                    hyopparams = True

                self.hyopspace[hyopname] = hp.choice('{}_choice'.format(hyopname),
                            [False,hyopparams])

                ind_opt = ind_opt + 1
            #endfor opt
        #endfor space

        # FIXME Put availability to other types of params such as enum and check
        # if name of params do not coiincide with names on the code regions.
        if self.optuni.search.buildoptions and 'params' in self.optuni.search.buildoptions:
            params_dict = self.optuni.search.buildoptions['params']
            # Iterate over the params
            for key, val in params_dict.items(): 
                if key in self.hyopspace:
                    #TODO raise and exception instead
                    logice.error("Code region has the name of a param!")
                    sys.exit(1)

                self.hyopspace[key] = h.quniform(key+"_int",val['min'],val['max'],1)

        # Check if there is anything to search!
        if not self.hyopspace:
            return False
        else:
            logice.info("Search space size: {} keys".format(len(self.hyopspace)))
            return True

    def convertCurrentCfg(self, cfg):

        cfgoptuni = copy.deepcopy(self.optuni)

        #logice.debug("cfg: {}".format(cfg))

        sortedcfg = {}

        #
        # Put cfg in a dict ordered by space and position
        # First level is the optimization and the 2nd the params of the
        # optimization.
        #
        for key, val in cfg.items():

            try:
                info = self.otparamdict[key]['info']
                optspacename = info['spacename']
                indopt = info['indopt']
            except KeyError as e:
                raise KeyError("Info field from otparamdict not correct.")

            if optspacename not in sortedcfg:
                sortedcfg[optspacename] = {}
            if optspacename not in sortedcfg[optspacename]:
                sortedcfg[optspacename][indopt] = {}

            # val true means that this optimization should be executed.
            if val:
                # here it can be a dictionary or a True.
                # dict means parameters searched. True means just execute this
                # optimization.
                if isinstance(val, dict):
                    for kpar, vpar in val.items():
                        obj = self.otparamdict[key][kpar]
                        if vpar:
                            sortedcfg[optspacename][indopt][obj.paramname] = (obj, vpar)
                else:
                    sortedcfg[optspacename][indopt] = (key, True)
            else:
                sortedcfg[optspacename][indopt] = (key, False)


        logice.debug("sortedcfg: {}".format(sortedcfg))

        #
        # Traverse a copy of the optimization space filling the gaps of the
        # selected parameters and optimizations.
        # This is done so the parameters that are not searched are already in
        # place.
        #
        for space in cfgoptuni:
            ind_opt = 0
            nodeToDel = []
            for opt in space.traverse():
                logice.debug("opt {}".format(opt.val.name))

                # Check if it has been in the search.
                if space.name in sortedcfg and ind_opt in sortedcfg[space.name]:

                    if isinstance(val, dict):
                        # Go through all parameters for the same optimization that
                        # were searched.
                        for p in sortedcfg[space.name][ind_opt]:
                            # here p can be none which means no search param, but
                            # the optimization should be executed.
                            if p:
                                logice.debug("updating a parameter: {}".format(p))
                                obj = sortedcfg[space.name][ind_opt][p][0]
                                val = sortedcfg[space.name][ind_opt][p][1]

                                if obj.paramname in opt.val.params:
                                    opt.val.params[obj.paramname] = val
                                else:
                                    logice.error("Could not find the parameter: {} opt: {} tool: {} space: {}".format(obj.paramname, opt.val.name, "X", space.name))
                                    raise KeyError("The param was not where it was"
                                            "supposed to be")
                                    sys.exit(1)
                                #endif
                            #endif p
                        #endfor sortedcfg
                    else:
                        tmpkey, useit = sortedcfg[space.name][ind_opt]
                        if not useit:
                            nodeToDel.append((tmpkey,opt))

                    #endif isinstance
                #endif
                ind_opt += 1
            #endfor space.traverse

            # Remove the opts that were selected to not be used.
            for tmpkey, nd in nodeToDel:
                space.delOptNode(nd)
                logice.debug("Deleting opt of current cfg: {}".format(tmpkey))

        #endfor cfgoptuni
        logice.debug("*** End")
        return cfgoptuni

    def compile_and_run(self, cfg):
        cfg_id = self.totalcfgs
        self.totalcfgs += 1
        logice.info("start {}".format(cfg_id))

        logice.debug("cfg: {}".format(cfg))

        try:
            cfgoptuni = self.convertCurrentCfg(cfg)

            logice.debug("optuni from cfg: {}".format(cfgoptuni))

            undoList = CodeGen.genCode(self.parsedsrcfiles, cfgoptuni,\
                self.iceargs, cfg_id)
        except Exception as e:
            logice.exception("Error while converting and generating code. {}".format(e))
            # Code generation failed, go to next!
            # Fixed What to return to HyperOpt when experiment fail?
            return {'loss':sys.float_info.max, 'status': HP_STATUS_FAIL}

        #TODO define this properly with compiler selection and different types of flags
        tmpbuildcmd = self.optuni.search.buildcmd

        if self.optuni.search.buildcmd and \
            self.iceargs.output != ICE_OUTPUT_MODE_STDOUT:

            logice.debug("buildcmd: {}".format(tmpbuildcmd))
            try:
                stdout, stderr = Commands.callCmd(tmpbuildcmd)

            except RuntimeError as e:
                logice.error("{}".format(e))
                return {'status': HP_STATUS_FAIL}

            except Exception as e:
                logice.exception("{}".format(e))
                return {'status': HP_STATUS_FAIL}
            else:
                logice.debug("buildcmd stdout: {}\nstderr: {}".format(stdout, stderr))

        if self.optuni.search.runcmd and \
                self.iceargs.output != ICE_OUTPUT_MODE_STDOUT:
            logice.debug("runcmd: {}".format(tmpbuildcmd))
            try:
                stdout, stderr = Commands.callCmd(self.optuni.search.runcmd)

            except RuntimeError as e:
                logice.error("{}".format(e))
                return {'status': HP_STATUS_FAIL}

            except Exception as e:
                logice.exception("{}".format(e))
                return {'status': HP_STATUS_FAIL}

            else:
                logice.debug("runcmd stdout: {}\nstderr: {}".format(stdout, stderr))

        if self.iceargs.output == ICE_OUTPUT_MODE_INPLACE:
            logice.debug("undolist after each run: {}".format(undoList))
            for ud in undoList:
                unwindBackup(ud[1], ud[0])

        timingfunc = getTimingFunc(self.iceargs.tfunc)
        if timingfunc:
            metric = timingfunc(stdout, stderr)
            logice.debug("Using external function to get kernel timing: {}".format(timingfunc))
            logice.debug("Output:\n {} \nErr:\n {}".format(stdout, stderr))
        else:
            metric = getDefaultTiming(stdout, stderr)

        logice.info("Run cfg_id: {} time: {} cc: {} cfg: {}".format(cfg_id, metric,
            "?", cfg))

        return {'loss': metric, 'status': HP_STATUS_OK}

    def search(self, parsedsrcfiles):
        logice.debug("{}".format(self.hyopspace))

        self.totalcfgs = 0

        self.parsedsrcfiles = parsedsrcfiles

        self.trials = Trials()
        best = fmin(self.compile_and_run,
                space=self.hyopspace,
                algo=tpe.suggest,
                max_evals=self.iceargs.ntests,
                trials=self.trials)

        logice.info("Best: {}".format(best))
