'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo,\
Permutation, Boolean, Enum
from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
from locus.util.misc import readBuffer
from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_F, ICE_CODEFILE_CXX

log = logging.getLogger(__name__)

class Pragma(AbstractOptTool):

    def __init__(self):
        pass

    @staticmethod
    def genPragma(filetype, prtype):

        if filetype == ICE_CODEFILE_C or filetype == ICE_CODEFILE_CXX:
            ret = " " + prtype
        else:
            log.warning("filetype not recognized.")
            ret = ""

        return ret

    class Base(AbstractOptToolOpt):

        @staticmethod
        def getParams():
            return {'loop': str}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "Pragma"

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            log.debug("start {}".format(params))

            if 'loop' in params:
                paramloop = params['loop']
            else:
                raise ValueError("Expecting a loop param."
                                "Recved {}".format(params))

            if paramloop == "0":
                srccode.replaceCoregWithPragma(regiongoalname,
                    Pragma.genPragma(srccode.filetype, self._PRTYPE))
            else:
                srccode.addPragmaLoop(regiongoalname,
                        Pragma.genPragma(srccode.filetype, self._PRTYPE),
                        paramloop)
            #endif
            return srccode
        #end def

    class Ivdep(Base):
        _PRTYPE = "ivdep"

        @staticmethod
        def getOptName():
            return "Ivdep"

    class Vector(Base):
        _PRTYPE = "vector always"

        @staticmethod
        def getOptName():
            return "Vector"

    class VectorAligned(Base):
        _PRTYPE = "vector aligned"

        @staticmethod
        def getOptName():
            return "Vector"

