'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy


from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.frontend.srccode.cparser.sourcecode import ForLoopDepthVisitor,ListInnerLoopsVisitor,\
        IsPerfectLoopNest
from locus.frontend.srccode.cparser.sourcecode \
        import SourceCode as SourceCodeCparser
from locus.frontend.optlang.locus.locusast import Constant, TupleMaker
from locus.tools.opts.roseuiuc import RoseUiuc
from locus.tools.opts.roselore import RoseLore
from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX, ICE_CODEFILE_F
from locus.util.cmds import Commands
from locus.util.misc import getBasenameFile

log = logging.getLogger("Query")

class Query(AbstractOptTool):

    class LoopNestInfo(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "Query"

        @staticmethod
        def getOptName():
            return "LoopNestInfo"

        # Look for the 1st code region!
        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):

            if not isinstance(srccode, SourceCodeCparser):
                raise NotImplementedError("Cannot use ref version without a proper parsers!")

            #FIXME: this loop should not exist. It should receive the code region..
            # get the code region to start the visit
            for cr in srccode:
                if cr.label == regiongoalname:
                    lv = ForLoopDepthVisitor()
                    lv.visit(cr.startnode)
                    #print "loop depth",lv.ldepth
                    log.info("LoopNestInfo returns {}.".format(lv.ldepth))
                    return Constant(int,lv.ldepth)

    class IsPerfectLoopNest(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "Query"

        @staticmethod
        def getOptName():
            return "IsPerfectLoopNest"

        # Look for the 1st code region!
        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):

            if not isinstance(srccode, SourceCodeCparser):
                raise NotImplementedError("Cannot use ref version without a proper parsers!")

            # get the code region to start the visit
            #FIXME: this loop should not exist. It should receive the code region..
            for cr in srccode:
                if cr.label == regiongoalname:
                    lv = IsPerfectLoopNest()
                    ret = lv.visit(cr.startnode)
                    log.info("IsPerfectLoopNest returns {}.".format(ret))
                    return Constant(bool, ret)

    class IsDepAvailable(AbstractOptToolOpt):
        _PRAGMA = "uiuc_loredepavailable"

        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "Query"

        @staticmethod
        def getOptName():
            return "LoreDepAvailable"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            log.debug("start")

            try:
                praginfo = cls._PRAGMA
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            # Copy and any changes will be disregarded.
            querysrccode  = copy.deepcopy(srccode)

            newsrccode = cls.transform(querysrccode, praginfo, regiongoalname,\
                    combid, optid, cls.__name__,\
                    RoseLore.toolPath('loop'+cls.getOptName()), args)

            log.info("IsDepAvailable returns {}.".format(newsrccode))
            return newsrccode

        @staticmethod
        def transform(srccode, praginfo, regiongoalname, combind, optid, optname,  binarypath, args):
            file_dir = args.rundirpath #"."

            # Add the loop annot required by the RoseUiuc tool
            srccode.replaceCoregWithPragma(regiongoalname, praginfo)

            basename = getBasenameFile(srccode.srcfilename)

            tmpfilename = RoseUiuc.tmp_file_name+'-Query-'+\
                        args.suffixinfo+'-'+basename+\
                        '-'+regiongoalname+'-comb-'+\
                        str(combind)+'-'+str(optid)+'-'+optname

            if srccode.filetype == ICE_CODEFILE_C:
                tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_c
            elif srccode.filetype == ICE_CODEFILE_CXX:
                tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_cxx
            elif srccode.filetype == ICE_CODEFILE_F:
                tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_f

            # Write the code with the required info added
            srccode.writetofile(file_dir+'/'+tmpfilename)

            # Look for .dep file 
            # If it exists use it appended to rose command
            depcontent = RoseUiuc.readContent(srccode.srcfilename, '.dep')

            # Call Rose command
            #str_command = binarypath+' '+depcontent+' '+file_dir+'/'+tmpfilename
            str_command = binarypath+' '+depcontent+' '+file_dir+'/'+tmpfilename+ \
                    ' -rose:o '+file_dir+'/rose_'+tmpfilename
            log.debug("calling rose: {}".format(str_command))

            ret = None
            try:
                std, error, retcode, elapsed, ht = Commands.callCmd(str_command)

                if retcode == 0:
                    ret = True
                else:
                    ret = False

            except Exception as e:
                #log.error("Calling RoseUIUC resulted in error {}".format(e))
                log.error("std:\n{}\n{}".format(str_command, e))
                raise

            else:
                log.debug("result of runnning:\n\tstd:\n{}\n-----".format(std))

                if error != "":
                    log.debug("result of runnning:\n\terror:\n{}\n-----".format(error))

            finally:
                # Rollback everything and for now returns the nonoptimized input.
                if not args.debug:
                    RoseUiuc.removeTmpFiles(tmpfilename, file_dir)
            #end try

            if ret is None:
                raise RuntimeError("Could not get the result of the IsDepAvailable!")

            return Constant(bool, ret)
        #enddef
    #endclass

    class ListInnerLoops(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "Query"

        @staticmethod
        def getOptName():
            return "ListInnerLoops"

        # Look for the 1st code region!
        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):

            if not isinstance(srccode, SourceCodeCparser):
                raise NotImplementedError("Cannot use ref version without a proper parsers!")

            # get the code region to start the visit
            #FIXME: this loop should not exist. It should receive the code region..
            for cr in srccode:
                if cr.label == regiongoalname:
                    lv = ListInnerLoopsVisitor()
                    lv.visit(cr.startnode)
                    #print lv.innerloops
                    #for inn in lv.innerloops:
                    #    if inn[1]:
                            #inn[1].show()
                    #        print "=============="
                    #srccode.ast.show()
                    #print "type apply",type(lv.innerloops)
                    #retlist = [(cr,lp)  for lp in lv.innerloops]
                    return cr, lv.innerloops 
                #
            ##
        ###
    ####

    class SpMatInfo(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "Query"

        @staticmethod
        def getOptName():
            return "SpMatInfo"

        # Look for the 1st code region!
        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            import scipy.io as sio
            try:
                matname = params.get('mat')
                log.debug("matname: {}".format(matname))
            except KeyError as e:
                log.error("Could not find the mat name required. {}".format(e))
            #
            #m = sio.mmread(matname)
            m = sio.mminfo(matname)
            #print(m)
            info = (Constant(int, m[0]), Constant(int,m[1]),Constant(bool,m[5]=='symmetric'))
            #print(info)
            s = TupleMaker(info)
            #print(s)
            return s
        ###
    ####

    class SpMatDim(SpMatInfo):
        # Look for the 1st code region!
        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            import scipy.io as sio
            try:
                matname = params.get('mat')
                dim = params.get('dim')
                patch = params.get('patch', 1)
                log.debug("matname: {}".format(matname))
            except KeyError as e:
                log.error("Could not find the mat name required. {}".format(e))
            #
            #m = sio.mmread(matname)
            m = sio.mminfo(matname)
            if dim == 'row':
                v = m[0]
            elif dim == 'col':
                v = m[1]
            else:
                raise ValueError
            #
            if (v % patch):
                v += (patch - (v % patch))
            #

            return Constant(int, v)
        ###

