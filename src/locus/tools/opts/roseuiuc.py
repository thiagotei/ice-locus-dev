'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy, os

from .opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot, Annot
from locus.frontend.srccode.regex.sourcecode \
        import SourceCode as SourceCodeRegex
from locus.frontend.srccode.cparser.sourcecode \
        import SourceCode as SourceCodeCparser
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo,\
Permutation, Boolean
from locus.util.exceptions import NotLegalOptimizationError, OptExecError
from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX, ICE_CODEFILE_F, ICE_ROSE_END_MARK,\
        ICE_C_SUFFIX, ICE_CXX_SUFFIX, ICE_FORTRAN_SUFFIX, ICE_TOOL_PATH_CMPOPTS
from locus.util.cmds import Commands
from locus.util.misc import getBasenameFile

log = logging.getLogger("RoseUiuc")

class RoseUiuc(AbstractOptTool):
    """ The class has nested classes that represent the optimizations.
        The Params of the optimizaton class represents info to the optimization
        that are fixed. The SearchParams represents infos that can be searched
        through.
        They have a method with a dict that inform types for each case.
        The SearchParams type must be a list a must have  a field 'type' with
        the possible types of the search parameters. 
        The Param type must be unique (no in a list).
    """

    __file_dir = "." 
    tmp_file_name = "tmp"
    tmp_file_extension_f = ICE_FORTRAN_SUFFIX[0] #"f90"
    tmp_file_extension_c = ICE_C_SUFFIX[0] #"c"
    tmp_file_extension_cxx = ICE_CXX_SUFFIX[0] #"cpp"
    #__tool_dir = "rose"

    def __init__(self):
#        AbstractOptTool.__init__(self, "RoseUiuc")
        log.debug("RoseUiuc init")

    class RoseClassBaseFactor(AbstractOptToolOpt):
        def __init__(self):
            log.debug("{}".format("RoseClassBaseFactor"))

        # This class cannot be instantiated. This is not an optimization.
        def __new__(cls, *args, **kwargs):
            if cls is RoseUiuc.RoseClassBaseFactor:
                raise TypeError("base class may not be instantiated.")
            return AbstractOptToolOpt.__new__(cls, *args, **kwargs)

        @staticmethod
        def toolPath(name):
            return RoseUiuc.toolPath(name)

        @staticmethod
        def getParams():
            """There are mandatory parameters of the optimizations\
            They do not have anything to search or optimize. 
            The 'type' key here is to specify how the search tool will expose
            the 'factor' parameter to the search."""
            return {'loop': int, 'type': str}

        @staticmethod
        def getSearchParams():
            """There are parameters related to the search.\
            It depends on the search tool which one is mandatory.
            This is the type required by the be the optimization tool.
            For instance, the Rose optimizations require factor as an integer.
            """
            return {'factor': int}

        @staticmethod
        def getSearchTypes():
            """This is the type used to decribe the optimization space."""
            """1st one is the defaut."""
            return {'factor': [Integer, PowerOfTwo]}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")
            try:
                praginfo = cls._PRAGMA + " " + str(params.get('loop')) + " " +\
                str(params.get('factor'))
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            newsrccode = RoseUiuc.transform(srccode, praginfo, regiongoalname,\
                    combind, optid, cls.__name__,\
                    cls.toolPath('loop'+cls.getOptName()), args)
            return newsrccode

        @staticmethod
        def getToolName():
            return "RoseUiuc"

    class LICM(AbstractOptToolOpt):
        """Loop Invariant Code Motion"""
        _PRAGMA = "uiuc_licm"

        def __init__(self):
            log.debug("LICM")

        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            # The param order should come as string but the search is
            # used as boolean.
            return {}
            #return {'order': Integer, 'type': [Permutation]}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                praginfo = cls._PRAGMA
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            newsrccode = RoseUiuc.transform(srccode, praginfo, regiongoalname,\
                    combind, optid, cls.__name__,\
                    RoseUiuc.toolPath('loop'+cls.getOptName()), args)
 
            return newsrccode

        @staticmethod
        def getToolName():
            return "RoseUiuc"

        @staticmethod
        def getOptName():
            return "ICM"

    class Interchange(AbstractOptToolOpt):
        _PRAGMA = "uiuc_interchange"

        def __init__(self):
            log.debug("Interchange")

        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            # The param order should come as string but the search is
            # used as boolean.
            return {'order': str}
            #return {'order': Integer, 'type': [Permutation]}

        @staticmethod
        def getSearchTypes():
            return {'order': [Boolean]}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                paramorder = params.get('order')
                if isinstance(paramorder, str):
                    order = paramorder
                else:
                    try:
                        order = ",".join(str(x) for x in paramorder)
                    except Exception as e:
                        log.exception("Type {} of parameter order."
                                     "Must be str or list-style container. {}".format(type(paramorder),e))
                    #endtry
                #endif

                praginfo = cls._PRAGMA + " " + order
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            newsrccode = RoseUiuc.transform(srccode, praginfo, regiongoalname,\
                    combind, optid, cls.__name__,\
                    RoseUiuc.toolPath('loop'+cls.getOptName()), args)
 
            return newsrccode

        @staticmethod
        def getToolName():
            return "RoseUiuc"

        @staticmethod
        def getOptName():
            return "Interchange"

    class StripMine(RoseClassBaseFactor):
        _PRAGMA = "uiuc_stripmine"

        def __init__(self):
            log.debug("{}".format(self.__class__.__name__))
            super(RoseUiuc.StripMine, self).__init__()

        @staticmethod
        def getOptName():
            return "StripMine"

    class UnrollAndJam(RoseClassBaseFactor):
        _PRAGMA = "uiuc_unrollandjam"

        def __init__(self):
            log.debug("{}".format(self.__class__.__name__))
            super(RoseUiuc.UnrollAndJam, self).__init__()

        @staticmethod
        def getOptName():
            return "UnrollAndJam"

    class Unroll(RoseClassBaseFactor):
        _PRAGMA = "uiuc_unroll"

        def __init__(self):
            log.debug("{}".format(self.__class__.__name__))
            super(RoseUiuc.Unroll, self).__init__()

        @staticmethod
        def getOptName():
            return "Unroll"

    @staticmethod
    def transform(srccode, praginfo, regiongoalname, combind, optid, optname,  binarypath, args):

        # Add the loop annot required by the RoseUiuc tool
        srccode.replaceCoregWithPragma(regiongoalname, praginfo)

        # Extract srcfilename without the extension
        basename = getBasenameFile(srccode.srcfilename)

        RoseUiuc.__file_dir = args.rundirpath

        tmpfilename = RoseUiuc.tmp_file_name+'-'+\
                    args.suffixinfo+'-'+basename+\
                    '-'+regiongoalname+'-comb-'+\
                    str(combind)+'-'+str(optid)+'-'+optname

        if srccode.filetype == ICE_CODEFILE_C:
            tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_c
        elif srccode.filetype == ICE_CODEFILE_CXX:
            tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_cxx
        elif srccode.filetype == ICE_CODEFILE_F:
            tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_f

        # Write the code with the required info added
        srccode.writetofile(RoseUiuc.__file_dir+'/'+tmpfilename)

        # Look for .dep file 
        # If it exists use it appended to rose command
        depcontent = RoseUiuc.readContent(srccode.srcfilename, '.dep')

        # Call Rose command
        str_command = binarypath+' '+depcontent+' '+RoseUiuc.__file_dir+'/'+tmpfilename+ \
                ' -rose:o '+RoseUiuc.__file_dir+'/rose_'+tmpfilename

        log.debug("calling rose: {}".format(str_command))

        try:
            std, error, retcode, elapsed, ht = Commands.callCmd(str_command)

            if retcode == 2:
                raise NotLegalOptimizationError("Optimization not legal {} .".format(str_command),std,error,retcode)
            elif retcode != 0:
                raise OptExecError("Error on optimization {} .".format(str_command),std,error,retcode)

            # Read the file
            # Parse transformed regions

            # Read it back the resulting optimized file
            transfsrcode = type(srccode)(RoseUiuc.__file_dir+'/rose_'+tmpfilename, srccode.filetype).readandparse(args.preproc)

        except Exception as e:
            #log.error("Calling RoseUIUC resulted in error {}".format(e))

            log.exception("problem on optimizing with Rose: {}".format(e)) 
            raise

        else:
            log.debug("result of runnning:\n\tstd:\n{}\n-----".format(std))

            if error != "":
                log.debug("result of runnning:\n\terror:\n{}\n-----".format(error))

        finally:
            # Rollback everything and for now returns the nonoptimized input.
            if not args.debug:
                RoseUiuc.removeTmpFiles(tmpfilename, RoseUiuc.__file_dir)
        #endtry

        if isinstance(transfsrcode, SourceCodeRegex):
            transfsrcode = RoseUiucParser.parse(transfsrcode, regiongoalname)
            #transfsrcode = ReadSrcFile.parseSrcFile("./tmp-22680-comb-2323-1818-Stripmine.c", srccode.filetype)

            # Put back the info in the structure of the optimized code.
            # This is done to the next loop optimizations.
            # On the same loop, see the same keys.
            transfsrcode.makeMetadataCoherent(srccode)

        elif isinstance(transfsrcode, SourceCodeCparser):
            transfsrcode.replacePragmaWithCoreg(praginfo, "@ICE loop="+regiongoalname)

            transfsrcode.adjustLoopCoreg(regiongoalname)

            #transfsrcode.cleanUpSingleCmpd()

        else:
            raise NotImplementedError("Source code type not accepted!")

        transfsrcode.srcfilename = srccode.srcfilename

        return transfsrcode

    @staticmethod
    def removeTmpFiles(tmpfilename, basedir):
        try:
            tmpname = basedir+'/'+tmpfilename

            # remove the tmp file input for rose
            if os.path.isfile(tmpname):
                os.remove(tmpname)

            titmpname, file_extension = os.path.splitext(tmpfilename)
            tmpname = basedir+'/'+titmpname+".ti"

            # remove the tmp file with .ti extension that is generated by def-use LICM
            if os.path.isfile(tmpname):
                os.remove(tmpname)

            tmpname = basedir+'/rose_'+tmpfilename

            # remove the file tmp output of rose already read and parsed.
            if os.path.isfile(tmpname):
                os.remove(tmpname)

        except Exception as e:
            log.error("Error removing tmp files. {}".format(e))

    @staticmethod
    def printCodeWithAnnot(newsrccode, args, tmpfilename):
#        Writer.writetmpsrccode(newsrccode, tmpfilename)
        newsrccode.writetofile(tmpfilename)

    @staticmethod
    def toolPath(name):
        tool_dir = os.getenv(ICE_TOOL_PATH_CMPOPTS, None)
        #reltoolspath = os.path.dirname(__file__)+"/"+RoseUiuc.__tool_dir+"/"+name+"/"+name
        if tool_dir is None:
            raise OptExecError(f"{ICE_TOOL_PATH_CMPOPTS} not defined!")
        reltoolspath = tool_dir+"/bin/"+name
        abstoolpath = os.path.abspath(reltoolspath)
        return abstoolpath

    @staticmethod
    def readContent(srcfilename, extension):
        log.debug("Init {} {} .".format(srcfilename, extension))

        result = ""

        # Extract srcfilename without the extension
        basename = os.path.splitext(srcfilename)[0]

        # Add the extension given
        newname = basename + extension
        try: 
            # Open it to read
            with open(newname, 'r') as infile:
                for line in infile:
                    result = result + line
        except IOError:
            log.warning("File - {} - not found! Maybe it wasn't necessary.".format(newname))

        return result.strip()

class RoseUiucParser(object):
    """  Parse the result generated from transformations from Rose. """
    import re

    __annot_base_c = '^[ \t]*#pragma[ \t]+'
    __annot_base_cxx = __annot_base_c 
    __annot_base_f = '^[ \t]*![ \t]*'

    __annot_transf = 'uiuc_\w+'
    __annot_transf_end = ICE_ROSE_END_MARK #'endlooptransformation'

    __annot_transf_begin_c = r''+__annot_transf+'[ \t\w,.]*\n'
    __annot_transf_end_c = r'^[ \t]*//[ \t]+'+__annot_transf_end+'[ \t]*\n'

    __annot_transf_begin_f = r'\$[ \t]+'+__annot_transf+'[ \t\w,.]*'
    __annot_transf_end_f = r'[ \t]+'+__annot_transf_end+'[ \t]*'

    __annot_begin_f = r''+__annot_base_f+'('\
    +__annot_transf_begin_f+'|'\
    +__annot_transf_end_f+')\n'

    __annot_begin_c = r'('+__annot_base_c+'('\
    +__annot_transf_begin_c+')|'\
    +__annot_transf_end_c+')'

    __annot_transf_re = re.compile(__annot_transf, flags=re.MULTILINE)
    __annot_transf_end_re = re.compile(__annot_transf_end, flags=re.MULTILINE)

    __annot_begin_f_re = re.compile(__annot_begin_f, flags=re.MULTILINE)    
    __annot_begin_c_re = re.compile(__annot_begin_c, flags=re.MULTILINE)    

    def __init__(self):
        pass

    @staticmethod
    def parse(srccode, regiongoalname):
        """ This assumes parsing back the results of a one or more nonoverlapping code regions with the same id. """

        newsrccode = copy.deepcopy(srccode)

        lineno = 1
        FOUND_NOTHING = 0
        FOUND_TRANSF_BLOCK = 1
        FOUND_TRANSF_ENDBLOCK = 2
        lastannfound = FOUND_NOTHING

        replmntDict = {}

        for coreg in newsrccode:
            codebuf = copy.deepcopy(coreg.buffer) # copying the buf for safety reasons
            while True:
                if newsrccode.filetype == ICE_CODEFILE_C or \
                    newsrccode.filetype == ICE_CODEFILE_CXX:
                    matchobj = RoseUiucParser.__annot_begin_c_re.search(codebuf)
                elif newsrccode.filetype == ICE_CODEFILE_F:
                    matchobj = RoseUiucParser.__annot_begin_f_re.search(codebuf)

                #log.debug("{}".format(codebuf))

                if not matchobj:
                    log.debug("Couldnt find anything!")
                    if codebuf != '' and coreg in replmntDict:
                        replmntDict[coreg].append(CodeRegionAnnot(codebuf, False, lineno, 0, None))
                    break

                indexnewl = 0
                # index_newl = code[match_obj.end():].find('\n')
                # print "index %d " % index_newl
                ann = codebuf[matchobj.start():matchobj.end()+indexnewl]
                annlineno = lineno + codebuf[:matchobj.start()].count('\n')
                log.debug("Ann found {} line num {}".format(ann, annlineno))

                #parse the annotation  
                if(RoseUiucParser.__annot_transf_re.search(ann)):
                    log.debug("Found uiuc transf open annotation!")
                    annfound = FOUND_TRANSF_BLOCK

                elif(RoseUiucParser.__annot_transf_end_re.search(ann)):
                    log.debug("Found uiuc transf close annotation!")
                    annfound = FOUND_TRANSF_ENDBLOCK
                    if lastannfound != FOUND_TRANSF_BLOCK:
                        log.error("Found an endtransf without an transf annotation!")

                #get the leading non-annotation if that inside a opening block
                if annfound == FOUND_TRANSF_BLOCK:
                    non_ann = codebuf[:matchobj.start()]
                    non_annot_line_no = lineno
                    replmntDict[coreg] = []

                    if non_ann != '':
                        #code_seq.append((non_ann, non_annot_line_no, False, None))
                        replmntDict[coreg].append(CodeRegionAnnot(non_ann, False, non_annot_line_no, 0, None)) # assuming that RoseUiuc only performs loop opts.
                        log.debug("found_transf_block non_ann: {}".format( non_ann))

                    #codeann = ann
                    codeann = ""

                elif annfound == FOUND_TRANSF_ENDBLOCK:
                    codeann += codebuf[:matchobj.start()]
                    replmntDict[coreg].append(CodeRegionAnnot(codeann, True,
                        annlineno, 0, Annot(regiongoalname, CodeRegionAnnot.LOOP, {})))
                    log.debug("found_transf_endblock codeann: {}".format( codeann))


                lineno += codebuf[:matchobj.end()].count('\n')
                codebuf = codebuf[matchobj.end()+indexnewl:]

                lastannfound = annfound
            #end while true
        #end for codereg

        # Add new parsed code regions to the newsrccode
        for origcoreg, newcoregs in replmntDict.items():
            # newcoderegs are in a list
            for newcrg in newcoregs:
                newsrccode.addBeforeCodeRegion(newcrg, origcoreg)
            # endfor
            newsrccode.deleteCodeRegion(origcoreg)
        # endfor

        return newsrccode
