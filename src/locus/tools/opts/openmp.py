'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo,\
Permutation, Boolean, Enum
from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
from locus.util.misc import readBuffer
from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_F, ICE_CODEFILE_CXX

log = logging.getLogger("OpenMP")

class OpenMP(AbstractOptTool):

    def __init__(self):
        pass

    @staticmethod
    def genPragma(filetype, omptype, clauses):
        """ filetype:c, c++, fortran """
        """ omptype: for, parallel """
        """ clauses: given by user that omptypes can accept."""

        formclauses = clauses
#        for c in clauses.values():
#            formclauses = formclauses + " " + str(c)

        if filetype == ICE_CODEFILE_C or filetype == ICE_CODEFILE_CXX:
            ret = "omp " + omptype + " " + formclauses
        elif filetype == ICE_CODEFILE_F:
            ret = "!$OMP " + omptype + " " + formclauses + "\n"
        else:
            log.warning("filetype not recognized.")
            ret = ""

        return ret

    class OMPBase(AbstractOptToolOpt):

        @staticmethod
        def getParams():
            return {'loop': str, 'clauses' : str}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "OpenMP"

    class OMPFor(OMPBase):
        _OMPTYPE = "parallel for"

        @staticmethod
        def getSearchParams():
            return {'schedule': ['static', 'dynamic', 'guided', 'runtime',
                'auto'], 'chunk': int}

        @staticmethod
        def getSearchTypes():
            return {'schedule' : Enum, 'chunk' : Integer}

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            log.debug("start {}".format(params))

            if 'loop' in params:
                paramloop = params['loop']
            else:
                raise ValueError("Expecting a loop param."
                                "Recved {}".format(params))

            paramlist = []
            if 'schedule' in params and 'chunk' in params:
                paramlist.append("schedule("+params['schedule']+\
                        ","+str(params['chunk'])+")")
            elif 'schedule' in params:
                paramslist.append("schedule("+params['schedule']+")")
            if 'clauses' in params:
                paramlist.append(params['clauses'])

            paramsstr = " ".join(paramlist)
            if paramloop == "0":
                srccode.replaceCoregWithPragma(regiongoalname,
                    OpenMP.genPragma(srccode.filetype, self._OMPTYPE, paramsstr))
            else:
                srccode.addPragmaLoop(regiongoalname,
                        OpenMP.genPragma(srccode.filetype, self._OMPTYPE, paramsstr),
                        paramloop)
            #endif

            return srccode

        @staticmethod
        def getOptName():
            return "OMPFor"

    class OMPParallel(OMPBase):
        _OMPTYPE = "parallel"

        @staticmethod
        def apply(srccode, params, orighashes, regiongoalname, args, combid, optid):
            log.debug("start {}".format(params))

            srccode.replaceCoregWithPragma(regiongoalname, 
                    OpenMP.genPragma(srccode.filetype, self._OMPTYPE, params))

            return srccode

        @staticmethod
        def getOptName():
            return "OMPParallel"

