'''
@author: thiago
'''
import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.backend.gencache import GenCache 

log = logging.getLogger("callmethodeigen")

class CallMethodEigen(AbstractOptTool):
    _leadsp = 0
    _incsp  = 2
    _lvl = 0

    def __init__(self):
        pass


    class BaseEigen(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {'buffer': str, 'mS': str, 'mE': str }

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethodEigen"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                #nS, nE = params.get('nS'), params.get('nE')
                ldz = params.get('ldz')
                #res = params.get('outMat')
                inline= params.get('inline', False)
                omp = params.get('omp', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                line = ""

                #
                #line += cls._CALLSTR + "(" + str(mS) + ", " + str(mE) +\
                #                     ", " + str(nS) + ", " + str(nE) +\
                #                     ", N, K, A, B, "+str(res)+");\n"
                mlen = mE-mS
                sp = ' '*CallMethodEigen._leadsp
                if inline:
                    line += "#pragma forceinline\n"
                #
                #if omp:
                #    line += f"{sp}#pragma omp task firstprivate(rho,cutpnt)\n"
                ##
                if omp:
                    line += f"{sp}#pragma omp task"
                    if CallMethodEigen._lvl > 0:
                        line += f" firstprivate(rho,cutpnt)\n"
                    else:
                        line += f"\n"
                    #
                #
                # the leafs need to call this!
                line += f"{sp}{{\n"
                line += f"{sp}  #ifdef DEBUG_OMP\n"
                line += f"{sp}  #pragma omp critical\n"
                line += f"{sp}  cerr << \"It: \" << it  << \" mS-mE \" << {mS} << \"-\" << {mE} << \" Thread id: \" << omp_get_thread_num() << endl;\n"
                line += f"{sp}  #endif\n"
                line += f"{sp}  fillIndxq(&indxq[{mS}],{mlen});\n"
                line += sp+"  "+cls._CALLSTR + "(&D["+str(mS)+"], &Z["+str(((mS*ldz)+mS))+"], &E["+str(mS)+"], "+\
                                     str(mlen)+", ldz);\n";
                line += f"{sp}}}\n"
                buf.write(line)

            return srccode
        #enddef
    ####

    class QRTD(BaseEigen):
        _CALLSTR = "QRTD"
    ####

    class Bisection(BaseEigen):
        _CALLSTR = "Bisection"
    ####

    class QRTDBase(BaseEigen):
        _CALLSTR = "QRTD"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                #nS, nE = params.get('nS'), params.get('nE')
                ldz = params.get('ldz')
                #res = params.get('outMat')
                inline= params.get('inline', False)
                omp = params.get('omp', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))
            ##

            with open(buffername, 'a') as buf:
                line = cls._CALLSTR+f"(D, Z, E, n, ldz);\n"
                buf.write(line)
            ##

            return srccode
        ###
    ####

    class BisectionBase(QRTDBase):
        _CALLSTR = "Bisection"
    ####

    class LapackDCBase(QRTDBase):
        _CALLSTR = "LapackDC"
    ####

    class DCProlog(BaseEigen):
        _CALLSTR = "DCProlog"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                #nS, nE = params.get('nS'), params.get('nE')
                ldz = params.get('ldz')
                #res = params.get('outMat')
                inline= params.get('inline', False)
                omp = params.get('omp', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                mlen = mE-mS
                cutpnt = mS+(mlen//2)
                line = ""
                #
                if omp and CallMethodEigen._lvl == 0:
                    sp = ' '*CallMethodEigen._leadsp
                    line += f"{sp}#pragma omp parallel\n"
                    line += f"{sp}{{\n"  
                    line += f"{sp}  #pragma omp single nowait\n"
                    line += f"{sp}  {{\n"
                    CallMethodEigen._leadsp += 2*CallMethodEigen._incsp
                #
                sp = ' '*CallMethodEigen._leadsp
                if omp:
                    line += f"{sp}#pragma omp task"
                    if CallMethodEigen._lvl > 0:
                        line += f" firstprivate(rho,cutpnt)\n"
                    else:
                        line += f"\n"
                    #
                #
                line += f"{sp}{{\n"
                line += f"{sp}  int cutpnt = {cutpnt};\n"
                line += f"{sp}  TYPE rho = E[cutpnt-1];\n"
                #line += f"  TYPE * q1;\n"
                #line += f"  TYPE * q2;\n"
                line += f"{sp}  D[cutpnt-1] = D[cutpnt-1] - fabs(rho);\n"
                line += f"{sp}  D[cutpnt]   = D[cutpnt]   - fabs(rho);\n"
                buf.write(line)
                CallMethodEigen._leadsp += CallMethodEigen._incsp
                CallMethodEigen._lvl += 1

            return srccode
        #enddef
    ####

    class DCEpilog(BaseEigen):
        _CALLSTR = "DCEpilog"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                #nS, nE = params.get('nS'), params.get('nE')
                ldz = params.get('ldz')
                #res = params.get('outMat')
                inline= params.get('inline', False)
                omp = params.get('omp', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                mlen = mE-mS
                n1 = mlen//2
                n2 = mlen-n1
                CallMethodEigen._leadsp -= CallMethodEigen._incsp
                CallMethodEigen._lvl -= 1
                sp = ' '*CallMethodEigen._leadsp
                line = ""
                if omp:
                    line += f"{sp}  #pragma omp taskwait\n"
                #
                line += f"{sp}  {{\n"
                line += f"{sp}    #ifdef DEBUG_OMP\n"
                line += f"{sp}    #pragma omp critical\n"
                line += f"{sp}    cerr << \"Reduce=> It: \" << it  << \" mS-mE \" << {mS} << \"-\" << {mE} << \" Thread id: \" << omp_get_thread_num() << endl;\n"
                line += f"{sp}    #endif\n"
                line += f"{sp}    TYPE * workDC = new TYPE[{mlen*mlen}+4*{mlen}];\n"
                line += f"{sp}    if(int ret = ComputeEig(&D[{mS}], &Z[{(mS*ldz)+mS}], workDC, &indxq[{mS}], rho, {n1}, {n2}, ldz)){{\n"
                #line += f"  if(int ret = ComputeEig(D, Z, workDC, q1, q2, rho, cutpnt, n-cutpnt, ldz)){{\n"
                line += f"{sp}      cerr << \"[Error] ComputeEig {mS}-{mE} failed! ret error \" << ret << endl;\n"
                #line += f"{sp}      return ret;\n"
                line += f"{sp}    }}\n"
                line += f"{sp}    delete [] workDC;\n"
                line += f"{sp}  }}\n"
                #line += f"  delete [] tmp;\n"
                line += f"{sp}}}\n"
                #log.info(f"######### {omp} {CallMethodEigen._lvl}")
                if omp and CallMethodEigen._lvl == 0:
                    CallMethodEigen._leadsp -= 2*CallMethodEigen._incsp
                    sp = ' '*CallMethodEigen._leadsp
                    line += f"{sp}  }}\n"
                    line += f"{sp}}}\n"  
                #
                buf.write(line)
            ##

            return srccode
        #enddef
    ####


####
