'''
@author: thiago
'''
import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.backend.gencache import GenCache 

log = logging.getLogger("callmethod")

class CallMethodSpMV(AbstractOptTool):
    _leadsp = 0
    _incsp  = 2
    _lvl = 0
    invoklst = []

    def __init__(self):
        pass


    class BaseSpMV(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {'begin': str, 'end' : str, 'buffer': str }

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethodSpMV"

        @classmethod
        def convProlog(cls, sp):
            line = ""
            line += f"{sp}{{\n"
            line += f"{sp}  sparse_operation_t op = SPARSE_OPERATION_NON_TRANSPOSE;\n"
            line += f"{sp}  printf({cls._MSG1}\\n\");\n"
            return line
        ###

        @classmethod
        def convEpilog(cls, sp):
            line = ""
            line += f"{sp}  if(stat != SPARSE_STATUS_SUCCESS) {{\n"
            line += f"{sp}    fprintf(stderr,\"MKL coo->{cls._DSTCONV} Failed!\");\n"
            line += f"{sp}    exit(1);\n"
            line += f"{sp}  }}\n"
            line += f"{sp}}}\n"
            return line

        ###

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                #beginv = params.get('begin')
                #endv = params.get('end')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                #line = cls._CALLSTR + "(" + str(beginv) + "," + str(endv) + "- 1, &inp[0]);\n"
                sp = ' '*CallMethodSpMV._leadsp
                line = ""
                line += cls.convProlog(sp)
                line += f"{sp}  sparse_status_t stat = mkl_sparse_convert_{cls._DSTCONV}(Acoo, op, &A);\n"
                line += cls.convEpilog(sp)
                buf.write(line)

            return srccode
        #enddef
    ####

    class ConvCSR(BaseSpMV):
        _MSG1 = "\"Converting from COO to CSR ..."
        _DSTCONV = "csr"
    ####

    class ConvBSR(BaseSpMV):
        _MSG1 = "\"Converting from COO to BSR ..."
        _DSTCONV = "bsr"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                block = params.get('blocksize', 2);
                #beginv = params.get('begin')
                #endv = params.get('end')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethodSpMV._leadsp
                line = ""
                line += cls.convProlog(sp)
                line += f"{sp}  sparse_layout_t blay = SPARSE_LAYOUT_ROW_MAJOR;\n"
                line += f"{sp}  sparse_status_t stat = mkl_sparse_convert_{cls._DSTCONV}(Acoo, {block}, blay, op, &A);\n"
                line += cls.convEpilog(sp)
                buf.write(line)
            ##
            return srccode
        ###
    ####

    class Block(BaseSpMV):
        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                fmt = params.get('fmt')
                fmtblk = params.get('fmtblk', 0);
                mS, mE = params.get('mS'), params.get('mE')
                nS, nE = params.get('nS'), params.get('nE')
                #beginv = params.get('begin')
                #endv = params.get('end')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethodSpMV._leadsp
                line = ""
                line += f"{sp} convA({mS}, {mE}, {nS}, {nE}, nz, I, J, val, x, y, {fmt}, {fmtblk}, isReallySymmetric, t_it, niter, argv[n]);\n"

                buf.write(line)
            #
            return srccode
    ####

    class Conv2t3(BaseSpMV):
        SUF = 0

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                fmt = params.get('fmt')
                fmtblk = params.get('fmtblk', 0);
                mS, mE = params.get('mS'), params.get('mE')
                nS, nE = params.get('nS'), params.get('nE')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethodSpMV._leadsp
                line = ""
                line += f"{sp} sparse_matrix_t A{cls.SUF} = convA2({mS}, {mE}, {nS}, {nE}, nz, I, J, val, {fmt}, {fmtblk}, op);\n"
                buf.write(line)
            #

            CallMethodSpMV.invoklst.append((mS, nS, cls.SUF))
            cls.SUF += 1
            return srccode
    ####

    class GenInvoc(BaseSpMV):
        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethodSpMV._leadsp

                line = f"{sp} for (int it = 0; it < niter; it++) {{\n"
                line += f"{sp}   memset(y, 0, sizeof(double) * M);\n"
                line += f"{sp}   mygettime(&t_start);\n"

                for mS, nS, suf in CallMethodSpMV.invoklst:
                    line += f"{sp}   if(A{suf} != NULL) execSpMV2(op, alpha, A{suf}, descr, &x[{nS}], beta, &y[{mS}]);\n"
                ##

                line += f"{sp}   mygettime(&t_end);\n"
                line += f"{sp}   t_it[it] = mydifftimems(&t_start, &t_end);\n"
                line += f"{sp} }}\n"

                for _, _, suf in CallMethodSpMV.invoklst:
                    line += f"{sp} if(A{suf} != NULL) mkl_sparse_destroy(A{suf});\n"
                ##

                buf.write(line)
                CallMethodSpMV.invoklst = []
                CallMethodSpMV.Conv2t3.SUF = 0
            #

            return srccode
        ###
    ####
####

