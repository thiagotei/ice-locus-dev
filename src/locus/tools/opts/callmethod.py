'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.backend.gencache import GenCache 
#from locus.optspace.parameters import Parameter, Integer, PowerOfTwo,\
#Permutation, Boolean, Enum
#from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
#from locus.util.misc import readBuffer
#from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_F, ICE_CODEFILE_CXX

log = logging.getLogger("callmethod")

class CallMethod(AbstractOptTool):
    _leadsp = 0
    _incsp  = 2
    _lvl = 0

    def __init__(self):
        pass

    class BaseSorting(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {'begin': str, 'end' : str, 'buffer': str }

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethod"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                beginv = params.get('begin')
                endv = params.get('end')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                line = cls._CALLSTR + "(" + str(beginv) + "," + str(endv) + "- 1, &inp[0]);\n"
                buf.write(line)

            return srccode
        #enddef

    class SelectionSort(BaseSorting):
        _CALLSTR = "selectionsort"

    class InsertionSort(BaseSorting):
        _CALLSTR = "insertionsort"

    class QuickSort(BaseSorting):
        _CALLSTR = "quicksort"

    class RadixSort(BaseSorting):
        _CALLSTR = "radixsort"

    class RadixSort16(BaseSorting):
        _CALLSTR = "radixsort16"

    class QKPivot(BaseSorting):
        _CALLSTR = "partitionPiv"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                beginv = params.get('begin')
                endv = params.get('end')
                pivot = params.get('pivot')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                line = cls._CALLSTR + "(&inp[0], " + str(beginv) + ", " + str(endv) + "-1, " +\
                        str(pivot) + ");\n"
                buf.write(line)

            return srccode
        #enddef

    class BubbleSort(BaseSorting):
        _CALLSTR = "bubblesort"

    class Merge(BaseSorting):
        _CALLSTR = "merge"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                beginv = params.get('begin')
                endv = params.get('end')
                pivot = params.get('pivot')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                #pivot = str(((endv-beginv)/2)+beginv)
                line =' '*CallMethod._leadsp + cls._CALLSTR + "(" + str(beginv) + ", " + str(endv) + "-1, " +\
                        str(pivot) + ", &inp[0]);\n"
                buf.write(line)

            return srccode
        #enddef

    class CreateFile(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {'buffer': str}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethod"

        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'w') as buf:
                if orighashes:
                    for c, hex_v in orighashes:
                        if regiongoalname == c:
                            break
                        #endif
                    #endfor
                    hashline = GenCache.creathashsyntax(hex_v, srccode.filetype)
                    buf.write(hashline)
                else:
                    raise RuntimeError("Hashes are empty! Maybe because you have a C file." 
                                       "Hashes are not implemented for cparser.")
                #endif
            #endwith

            return srccode
        #enddef

    class BaseGemm(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {'buffer': str, 'mS': str, 'mE': str, 
                    'nS': str, 'nE': str, 
                    'kS': str, 'kE': str }

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethod"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                nS, nE = params.get('nS'), params.get('nE')
                kS, kE = params.get('kS'), params.get('kE')
                res = params.get('outMat')
                omp = params.get('omp', False)
                inline= params.get('inline', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                line = ""
                if inline:
                    line += "#pragma forceinline\n"
                #
                sp = ' '*CallMethod._leadsp
                if omp:
                    line += f"{sp}#pragma omp task\n"
                #
                line += sp + cls._CALLSTR + "(" + str(mS) + ", " + str(mE) +\
                                     ", " + str(nS) + ", " + str(nE) +\
                                     ", " + str(kS) + ", " + str(kE) +\
                                     ", N, K, A, B, "+str(res)+");\n"
                buf.write(line)

            return srccode
        #enddef

    class BaseGemmNew(BaseGemm):
        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mlen = params.get('M')
                nlen = params.get('N')
                klen = params.get('K')
                lda = params.get('lda')
                ldb = params.get('ldb')
                ldc = params.get('ldc')
                matA = params.get('matA')
                matB = params.get('matB')
                res = params.get('outMat')
                omp = params.get('omp', False)
                inline= params.get('inline', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                line = ""
                if inline:
                    line += "#pragma forceinline\n"
                #
                sp = ' '*CallMethod._leadsp
                if omp:
                    line += f"{sp}#pragma omp task\n"
                #
                line += sp + cls._CALLSTR + "(" + str(mlen) + ", " + str(nlen) +\
                                     ", " + str(klen) + ", " + str(matA) + ", " + str(lda) +\
                                     ", " + str(matB) +", " + str(ldb) + ", " + str(res) +\
                                     ", " + str(ldc)+");\n"
                buf.write(line)

            return srccode
        #enddef
    ####

    class GemmNaiveIJK(BaseGemmNew):
        _CALLSTR = "naiveIJK"

    class GemmNaiveIJKRegTileJ4(BaseGemmNew):
        _CALLSTR = "naiveIJKRegTileJ4"

    class GemmNaiveIJKRegTileJ8(BaseGemmNew):
        _CALLSTR = "naiveIJKRegTileJ8"

    class GemmNaiveIKJ(BaseGemmNew):
        _CALLSTR = "naiveIKJ"

    class GemmNaiveIKJRegTileJ4(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileJ4"

    class GemmNaiveIKJRegTileK4(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileK4"

    class GemmNaiveIKJRegTileK8(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileK8"

    class GemmNaiveIKJRegTileK4J4(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileK4J4"

    class GemmNaiveIKJRegTileK8J4(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileK8J4"

    class GemmNaiveIKJRegTileI4K4(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileI4K4"

#        @classmethod
#        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
#            try:
#                buffername = params.get('buffer')
#                mlen = params.get('M')
#                nlen = params.get('N')
#                klen = params.get('K')
#                lda = params.get('lda')
#                ldb = params.get('ldb')
#                ldc = params.get('ldc')
#                matA = params.get('matA')
#                matB = params.get('matB')
#                res = params.get('outMat')
#                omp = params.get('omp', False)
#                inline= params.get('inline', False)
#            except KeyError as e:
#                log.error("Could not find the info required. {}".format(e))
#
#            with open(buffername, 'a') as buf:
#                line = ""
#                if inline:
#                    line += "#pragma forceinline\n"
#                #
#                sp = ' '*CallMethod._leadsp
#                if omp:
#                    line += f"{sp}#pragma omp task\n"
#                #
#                line += sp + cls._CALLSTR + "(" + str(mlen) + ", " + str(nlen) +\
#                                     ", " + str(klen) + ", " + str(matA) + ", " + str(lda) +\
#                                     ", " + str(matB) +", " + str(ldb) + ", " + str(res) +\
#                                     ", " + str(ldc)+");\n"
#                buf.write(line)
#
#            return srccode
#        #enddef
    ####

    class GemmNaiveIKJRegTileI8K8(BaseGemmNew):
        _CALLSTR = "naiveIKJRegTileI8K8"

    class CreateTmpArray(BaseGemm):
        _suffix = 1

        @staticmethod
        def getParams():
            return {'mS': str, 'mE': str, 
                    'nS': str, 'nE': str}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                nS, nE = params.get('nS'), params.get('nE')
                tmpname = params.get('tmpname')

            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethod._leadsp
                line = sp + "MATTYPE "+tmpname+" = allocaTmp("+\
                        str(mS)+","+str(mE)+","+str(nS)+","+str(nE)+");\n"

                buf.write(line)

            return srccode
       #enddef

    class DestroyTmpArray(BaseGemm):
        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                tmpname = params.get('tmpname')

            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethod._leadsp
                line = sp + "free("+tmpname+");\n"

                buf.write(line)

            return srccode
       #enddef

    class MatrixAddandFree(BaseGemm):
        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                #mS, mE = params.get('mS'), params.get('mE')
                #nS, nE = params.get('nS'), params.get('nE')
                mlen, nlen = params.get('M'), params.get('N')
                ldc, ldt = params.get('ldc'), params.get('ldt')
                tmpname = params.get('tmpname')
                res = params.get('outMat')
                omp = params.get('omp', False)

            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethod._leadsp
                line = ""
                if omp:
                    line += f"{sp}#pragma omp taskwait\n"
                    line += f"{sp}{{\n"
                    CallMethod._leadsp += CallMethod._incsp
                #
                sp = ' '*CallMethod._leadsp
                #line += sp + "MatrixAdd" + "(" + str(mS) + ", " + str(mE) +\
                #                     ", " + str(nS) + ", " + str(nE) +\
                #                     ", N, "+res+", "+str(tmpname)+");\n"
                line += sp + "MatrixAdd" + "(" + str(mlen) + ", " + str(nlen) +\
                                     ", " + str(res) + ", " + str(ldc) +\
                                     ", " + str(tmpname) + ", " + str(ldt) + ");\n"
                line += sp + "free("+tmpname+");\n"
                if omp:
                    CallMethod._leadsp -= CallMethod._incsp
                    sp = ' '*CallMethod._leadsp
                    line += f"{sp}}}\n"
                #
                buf.write(line)

            return srccode
       #enddef

    class DCProlog(BaseGemm):
        """ Gemm to create a new block of code """

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                comment = params.get('comment',"")
                omp = params.get('omp', False)

            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                sp = ' '*CallMethod._leadsp
                line = ""
                if comment:
                    line += f"{sp}{comment}\n"
                #
                if omp:
                    line += f"{sp}#pragma omp task\n"
                #
            #
                line += f"{sp}{{\n"
                buf.write(line)
                CallMethod._leadsp += CallMethod._incsp
                CallMethod._lvl += 1

            return srccode
       #enddef
    ####

    class DCEpilog(BaseGemm):
        """ Gemm to create a new block of code """

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                comment = params.get('comment',"")

            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                CallMethod._leadsp -= CallMethod._incsp
                CallMethod._lvl -= 1
                sp = ' '*CallMethod._leadsp
                line = ""
                if comment:
                    line += f"{sp}{comment}\n"
                #
                line += f"{sp}}}\n"
                buf.write(line)

            return srccode
       #enddef
    ####

    class BaseMtransp(AbstractOptToolOpt):

        @staticmethod
        def getParams():
            return {'buffer': str, 'mS': str, 'mE': str, 
                    'nS': str, 'nE': str}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethod"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                mS, mE = params.get('mS'), params.get('mE')
                nS, nE = params.get('nS'), params.get('nE')
                inline= params.get('inline', False)
                omp = params.get('omp', False)
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                line = ""
                if inline:
                    line += "#pragma forceinline\n"
                #

                if omp:
                    sp = ' '*CallMethod._leadsp
                    #line += f"{sp}#pragma omp task mergeable\n"
                    line += f"{sp}#pragma omp task\n"
                #

                line += ' '*CallMethod._leadsp + cls._CALLSTR + "(" + str(mS) + ", " + str(mE) +\
                                     ", " + str(nS) + ", " + str(nE) +\
                                     ", M, N, A, B);\n"
                buf.write(line)

            return srccode
        #enddef

    class OMPprolog(BaseMtransp):

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))
            ##

            with open(buffername, 'a') as buf:
                sp = " "*CallMethod._leadsp
                line = ""
                line += f"{sp}#pragma omp parallel\n"
                line += f"{sp}{{\n"  
                #line += f"{sp}  #pragma omp single nowait\n"
                line += f"{sp}  #pragma omp single\n"
                line += f"{sp}  {{\n"

                buf.write(line)
                CallMethod._leadsp += 2*CallMethod._incsp
            ##
            return srccode
        ###
    ####

    class OMPepilog(BaseMtransp):

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))
            ##

            with open(buffername, 'a') as buf:
                CallMethod._leadsp -= 2*CallMethod._incsp
                sp = " "*CallMethod._leadsp
                line = ""
                line += f"{sp}  }}\n"
                line += f"{sp}}}\n"  

                buf.write(line)
            ##
            return srccode
        ###
    ####

    class MtranspNaiveIJ(BaseMtransp):
        _CALLSTR = "naiveIJ"

    class MtranspNaiveIJRegTileI4(BaseMtransp):
        _CALLSTR = "naiveIJRegTileI4"

    class MtranspNaiveIJRegTileI8(BaseMtransp):
        _CALLSTR = "naiveIJRegTileI8"
