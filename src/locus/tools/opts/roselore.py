'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy, os

from locus.optspace.parameters import Integer, PowerOfTwo
from .opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.frontend.optlang.locus.locusast import ListMaker
from .roseuiuc import RoseUiuc

log = logging.getLogger("RoseLore")

class RoseLore(RoseUiuc):

    tool_dir = "roselore"

    def __init__(self):
#        AbstractOptTool.__init__(self, "RoseLore")
        #log.debug("RoseLore init")
        pass

    class RoseLoreClassBase(RoseUiuc.RoseClassBaseFactor):

        @staticmethod
        def toolPath(name):
            return RoseLore.toolPath(name)

    @staticmethod
    def toolPath(name):
        #reltoolspath = os.path.dirname(__file__)+"/"+RoseLore.tool_dir+"/"+name+"/"+name
        #abstoolpath = os.path.abspath(reltoolspath)
        #return abstoolpath
        return RoseUiuc.toolPath(name)

    class Distribute(AbstractOptToolOpt):
        _PRAGMA = "uiuc_loredistribute"

        def __init__(self):
            #log.debug("LoreDistribute")
            pass

        @staticmethod
        def getParams():
            return {'loop': int}

        @staticmethod
        def getSearchParams():
            # The param order should come as string but the search is
            # used as boolean.
         #   return {'loop': int}
            return {}
            #return {'order': Integer, 'type': [Permutation]}

        @staticmethod
        def getSearchTypes():
            #return {'order': [Boolean]}
            return {}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                praginfo = self._PRAGMA + " " + params.get('loop')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            newsrccode = RoseUiuc.transform(srccode, praginfo, regiongoalname,\
                    combind, optid, cls.__name__,\
                    RoseLore.toolPath('loop'+getOptName()), args)

            return newsrccode

        @staticmethod
        def getOptName():
            return "LoreDistribute"

        @staticmethod
        def getToolName():
            return "RoseLore"

    class Interchange(AbstractOptToolOpt):
        _PRAGMA = "uiuc_loreinterchange"

        def __init__(self):
            log.debug("LoreInterchange")

        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            # The param order should come as string but the search is
            # used as boolean.
            return {'order': str}
            #return {'order': Integer, 'type': [Permutation]}

        @staticmethod
        def getSearchTypes():
            return {'order': [Boolean]}

        def apply(self, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                order = params.get('order')
                #print order, type(order)
                if isinstance(order, list) or\
                    isinstance(order, ListMaker):
                    liststr = ",".join(map(str,order))
                elif isinstance(order, str):
                    liststr = order
                else:
                    raise RuntimeError("order in not recognizable format.")
                praginfo = self._PRAGMA + " " + liststr
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            newsrccode = RoseUiuc.transform(srccode, praginfo, regiongoalname,\
                    combind, optid, self.__class__.__name__,\
                    RoseLore.toolPath('loop'+self.getOptName()), args)

            return newsrccode

        @staticmethod
        def getToolName():
            return "RoseLore"

        @staticmethod
        def getOptName():
            return "LoreInterchange"

    class Tiling(RoseLoreClassBase):
        _PRAGMA = "uiuc_loretiling"

        def __init__(self):
            log.debug("{}".format(self.__class__.__name__))
            super(RoseLore.Tiling, self).__init__()

        @staticmethod
        def getOptName():
            return "LoreTiling"

    class UnrollAndJam(RoseLoreClassBase):
        _PRAGMA = "uiuc_loreunrollandjam"

        def __init__(self):
            log.debug("{}".format(self.__class__.__name__))
            super(RoseLore.UnrollAndJam, self).__init__()

        @staticmethod
        def getOptName():
            return "LoreUnrollAndJam"

    class Unroll(AbstractOptToolOpt):
        _PRAGMA = "uiuc_loreunroll"

        def __init__(self):
            log.debug("LoreUnroll")

        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            # The param order should come as string but the search is
            # used as boolean.
            return {'factor': int}
            #return {'order': Integer, 'type': [Permutation]}

        @staticmethod
        def getSearchTypes():
            #return {'order': [Boolean]}
            return {'factor': [Integer, PowerOfTwo]}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                praginfo = cls._PRAGMA + " " + params.get('factor')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            newsrccode = RoseUiuc.transform(srccode, praginfo, regiongoalname,\
                    combind, optid, cls.__name__,\
                    RoseLore.toolPath('loop'+getOptName()), args)

            return newsrccode

        @staticmethod
        def getOptName():
            return "LoreUnroll"

        @staticmethod
        def getToolName():
            return "RoseLore"

