'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.ice import ICE_CODEFILE_C, ICE_CODEFILE_F, ICE_CODEFILE_CXX


log = logging.getLogger("Moya")

class Moya(AbstractOptTool):

    def __init__(self):
        pass

    @staticmethod
    def genPragma(filetype, moyatype, formclauses):
        """ filetype:c, c++, fortran """
        """ moyatype: specialize, declare"""
        """ clauses: given by user that omptypes can accept."""
        if filetype == ICE_CODEFILE_C or inputtype == ICE_CODEFILE_CXX:
            ret = "moya " + moyatype + " " + formclauses
        elif filetype == ICE_CODEFILE_F:
            ret = "!$moya " + moyatype + " " + formclauses + "\n"
        else:
            log.warning("filetype not recognized.")
            ret = ""
        formclauses = ")"

        return ret

    class MoyaBase(AbstractOptToolOpt):

        @staticmethod
        def getParams():
            return {}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @staticmethod
        def getToolName():
            return "Moya" #Moya.__class__.__name__


    class Specialize(MoyaBase):
        _MOYATYPE = "specialize"

        @staticmethod
        def getParams():
            return {'ignore' : str, 'max': int}

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            log.debug("start {}".format(params))

            #Moya can only be applied to functions
            srccode.replaceCoregWithPragma(regiongoalname,
                    Moya.genPragma(srccode.filetype, self._MOYATYPE,
                        self.genClauses(params)) )

            return srccode

        @staticmethod
        def genClauses(params):
            formclauses = ""
            for k, c in params.items():
                formclauses = formclauses + " " + k + "("+c+")"

            return formclauses

        @staticmethod
        def getOptName():
            return "Specialize"

    class Declare(MoyaBase):
        _MOYATYPE = "declare"

        @staticmethod
        def getParams():
#            return {'clauses' : ['allocator', 'memsafe', 'noanalyze']}
            return {'clauses' : str}


        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            log.debug("start {}".format(params))

            #Moya can only be applied to functions
            srccode.replaceCoregWithPragma(regiongoalname,
                    Moya.genPragma(srccode.filetype, self._MOYATYPE,
                        self.genClauses(params)))

            return srccode

        @staticmethod
        def genClauses(params):
            formclauses = ""
            for v in list(params.values()):
                formclauses = formclauses + " " + v + " "

            return formclauses

        @staticmethod
        def getOptName():
            return "Declare"

