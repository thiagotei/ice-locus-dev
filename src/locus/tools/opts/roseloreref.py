'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy, os

from locus.frontend.srccode.cparser.sourcecode \
        import SourceCode as SourceCodeCparser
from .opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_CXX, ICE_CODEFILE_F
from locus.util.cmds import Commands
from locus.util.exceptions import OptExecError, NotLegalOptimizationError
from .roseuiuc import RoseUiuc
from .roselore import RoseLore
from locus.util.misc import getBasenameFile

log = logging.getLogger("RoseLoreRef")

class RoseLoreRef(AbstractOptTool):
    """ The class has nested classes that represent the optimizations.
        The Params of the optimizaton class represents info to the optimization
        that are fixed. The SearchParams represents infos that can be searched
        through.
        They have a method with a dict that inform types for each case.
        The SearchParams type must be a list a must have  a field 'type' with
        the possible types of the search parameters. 
        The Param type must be unique (no in a list).
    """

    __file_dir = "." 

    def __init__(self):
#        AbstractOptTool.__init__(self, "RoseLoreRef")
        log.debug("RoseLoreRef init")

    class RoseLoreRefClassBase(AbstractOptToolOpt):
        def __init__(self):
            log.debug("{}".format("RoseClassBaseFactor"))

        # This class cannot be instantiated. This is not an optimization.
        def __new__(cls, *args, **kwargs):
            if cls is RoseLoreRef.RoseLoreRefClassBase:
                raise TypeError("base class may not be instantiated.")
            return AbstractOptToolOpt.__new__(cls, *args, **kwargs)

        @staticmethod
        def toolPath(name):
            return RoseLoreRef.toolPath(name)

        @staticmethod
        def getParams():
            """There are mandatory parameters of the optimizations\
            They do not have anything to search or optimize. 
            The 'type' key here is to specify how the search tool will expose
            the 'factor' parameter to the search."""
            return {'loop': int, 'type': str}

        @staticmethod
        def getSearchParams():
            """There are parameters related to the search.\
            It depends on the search tool which one is mandatory.
            This is the type required by the be the optimization tool.
            For instance, the Rose optimizations require factor as an integer.
            """
            return {'factor': int}

        @staticmethod
        def getSearchTypes():
            """This is the type used to decribe the optimization space."""
            """1st one is the defaut."""
            return {'factor': [Integer, PowerOfTwo]}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")
            try:
                crref, tgtref = params.get('loop')
                #praginfo = cls._PRAGMA + " " + str(params.get('loop')) + " " +\
                praginfo = cls._PRAGMA + " " + "1" + " " +\
                str(params.get('factor'))
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            #def transform(srccode, praginfo, coregionname, coregionref, tgtref, args, combind, optid):

            newsrccode = RoseLoreRef.transform(srccode, praginfo, regiongoalname,\
                    crref, tgtref, \
                    combind, optid, cls.__class__.__name__,\
                    cls.toolPath('loop'+cls.getOptName()), args)
            return newsrccode

        @staticmethod
        def getToolName():
            return "RoseLoreRef"

    @staticmethod
    def toolPath(name):
        #reltoolspath = os.path.dirname(__file__)+"/"+RoseLore.tool_dir+"/"+name+"/"+name
        #abstoolpath = os.path.abspath(reltoolspath)
        #return abstoolpath
        return RoseUiuc.toolPath(name)

    class Unroll(RoseLore.Unroll):
        _PRAGMA = "uiuc_loreunroll"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                crref, tgtref = params.get('loop')
                #praginfo = cls._PRAGMA + " " + str(params.get('loop')) + " " +\
                praginfo = cls._PRAGMA + " " + str(params.get('factor'))
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))
                raise

            log.debug("calling tranform")
            newsrccode = RoseLoreRef.transform(srccode, praginfo, regiongoalname,\
                    crref, tgtref, \
                    combind, optid, cls.__name__,\
                    RoseLoreRef.toolPath('loop'+cls.getOptName()), args)

            return newsrccode

    class Distribute(RoseLore.Distribute):

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combind, optid):
            log.debug("start")

            try:
                crref, tgtref = params.get('loop')
                praginfo = cls._PRAGMA + " 1"

            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))
                raise

            newsrccode = RoseLoreRef.transform(srccode, praginfo, regiongoalname,\
                    crref, tgtref, \
                    combind, optid, cls.__name__,\
                    RoseLoreRef.toolPath('loop'+cls.getOptName()), args)

            return newsrccode

    @staticmethod
    def transform(srccode, praginfo, regiongoalname, coregionref, tgtref, combind, optid, optname,  binarypath, args):
        """ This receives and reference of the ast node instead of the name of the code region to be optimized. """

        log.debug("start")

        if not isinstance(srccode, SourceCodeCparser):
            raise NotImplementedError("Cannot use ref version without a proper parsers!")

        #print "LoreRef",type(coregionref), type(tgtref), tgtref

        # This is used if the target node is the same as the coregionref because
        # the ICE pragma will change be changed.
        # if they are not the same, a new pragma for the tool must be added and then removed.
        replacePragma = False

        # For inner loops can be a list of target references (tgtref).
        for refs in tgtref:
            tgtloopparent, tgtloop = refs
            if coregionref.startnode == tgtloop:
                replacePragma = True
            srccode.replaceCoregRefWithPragma(coregionref, refs, praginfo)
        #endif

        # Extract srcfilename without the extension
        basename = getBasenameFile(srccode.srcfilename)

        RoseLoreRef.__file_dir = args.rundirpath

        tmpfilename = RoseUiuc.tmp_file_name+'-'+\
                    args.suffixinfo+'-'+basename+\
                    '-'+regiongoalname+'-comb-'+\
                    str(combind)+'-'+str(optid)+'-'+optname

        if srccode.filetype == ICE_CODEFILE_C:
            tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_c
        elif srccode.filetype == ICE_CODEFILE_CXX:
            tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_cxx
        elif srccode.filetype == ICE_CODEFILE_F:
            tmpfilename = tmpfilename +'.'+ RoseUiuc.tmp_file_extension_f

        # Write the code with the required info added
        #REMOVE RoseLoreRef.printCodeWithAnnot(newsrccode, args, tmpfilename)
        srccode.writetofile(RoseLoreRef.__file_dir+'/'+tmpfilename)

        # Look for .dep file 
        # If it exists use it appended to rose command
        depcontent = RoseUiuc.readContent(srccode.srcfilename, '.dep')

        # Call Rose command
        str_command = binarypath+' '+depcontent+' '+RoseLoreRef.__file_dir+'/'+tmpfilename+ \
                ' -rose:o '+RoseLoreRef.__file_dir+'/rose_'+tmpfilename

        log.debug("calling rose: {}".format(str_command))

        #std, error = ("std output", "err output")#Commands.callCmd(str_command)
        try:
            std, error, retcode, elapsed, ht = Commands.callCmd(str_command)

            if retcode == 2:
                raise NotLegalOptimizationError("Optimization not legal {} .".format(str_command),std,error,retcode)
            elif retcode != 0:
                raise OptExecError("Error on optimization {} .".format(str_command),std,error,retcode)

            # Read the file
            # Parse transformed regions

            # Read it back the resulting optimized file
            transfsrcode = type(srccode)(RoseLoreRef.__file_dir+'/rose_'+tmpfilename, srccode.filetype).readandparse(args.preproc)

        except Exception as e:
            #log.error("Calling RoseLoreRef resulted in error {}".format(e))

            log.exception("problem on optimizing with RoseLoreRef: {}".format(e)) 
            raise

        else:
            log.debug("result of runnning:\n\tstd:\n{}\n-----".format(std))

            if error != "":
                log.debug("result of runnning:\n\terror:\n{}\n-----".format(error))

        finally:
            # Rollback everything and for now returns the nonoptimized input.
            if not args.debug:
                RoseUiuc.removeTmpFiles(tmpfilename, RoseLoreRef.__file_dir)
        #end try

        if isinstance(transfsrcode, SourceCodeCparser):

            if replacePragma:
                transfsrcode.replacePragmaWithCoreg(praginfo, "@ICE loop="+regiongoalname)
                # Only when the ICE pragma was replaced we need to adjust the code region pragma.
                # Otherwise the pragma will be removed anyway, so no need to fix it.
                transfsrcode.adjustLoopCoreg(regiongoalname)
            #endif

            transfsrcode.removePragmas(praginfo)

            #transfsrcode.cleanUpSingleCmpd()

            if len(srccode) != len(transfsrcode):
                raise RuntimeError("The number of Code regions after transformations is incorrect.")

        else:
            raise NotImplementedError("Source code type not accepted!")

        transfsrcode.srcfilename = srccode.srcfilename

        return transfsrcode

