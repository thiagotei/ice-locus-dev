'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy, shutil

log = logging.getLogger("Pips")

try:
    from pyps import *
except ImportError as e:
    print("You need to run Pips script (I've done . ~/MYPIPS/pipsrc.sh for it)!")
    raise

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.tools.opts.roseuiuc import RoseUiuc
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo,\
Permutation, Boolean, Enum
from locus.frontend.srccode.regex.sourcecode \
        import SourceCode as SourceCodeRegex
from locus.frontend.srccode.cparser.sourcecode \
        import SourceCode as SourceCodeCparser
from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
from locus.util.misc import readBuffer
from locus.util.constants import ICE_CODEFILE_C, ICE_CODEFILE_F, ICE_CODEFILE_CXX
from locus.util.misc import getBasenameFile


class Pips(AbstractOptTool):

    _tmp_file_name = "pips-result"
    _tmp_file_extension_f = "f90"
    _tmp_file_extension_c = "c"
    _tmp_file_extension_cxx = "cpp"
    _initial_file_mark = "PIPS include guard end:"
    _base_codereg_mark = "coregopt"
    _initial_loop_mark = _base_codereg_mark+"begin"
    _final_loop_mark = _base_codereg_mark+"end"
    _annotpreamble = ""

    def __init__(self):
        pass

    @staticmethod
    def findLoopWithLabel(loops, label):
        """This function is to be used with loops type from the tools Pips.
        It recursively look for a label on the loop nests. It assumes each label
        is unique in each function."""

        for l1 in loops:
#            log.debug("Loop {}".format(l1.label))
            if l1.label == label:
#                log.debug("Found!!! {}".format(l1.label))
                return l1
            else:
#                log.debug("Down one level. ".format())
                ret = Pips.findLoopWithLabel(l1.loops(), label)
#                log.debug("Up one level. Ret {}".format(None if ret == None else ret.label))
                if ret:
                    return ret
        return None

    #end findLoopWithLabel

    @staticmethod
    def removeTmpFiles(tmpfilename):
        try:
            # remove the tmp file input for rose
            os.remove(tmpfilename)

        except Exception as e:
            log.error("Error removing tmp files. {}".format(e))

    #end removeTmpFiles

    class LoopOpt(AbstractOptToolOpt):

        @staticmethod
        def getParams():
            """No search on these fields. """
            return {'loop' : int}

        @classmethod
        def getToolName(cls):
            return "Pips"

        @classmethod
        def applyLoopOpt(cls, srccode, params, pipsfunc, optParams, regiongoalname, args, combid, optid):
            log.debug("start {}".format(params))

            maindir   = os.getcwd() # dir where ICE is executing.
            runabsdir = os.path.join(maindir, args.rundirpath) #rundirpath is a relative path
            try:
                # Could not generate the pips files by adding the run-path.
                # So, move to the run-path, execute pips, and then move back.
                os.chdir(runabsdir)

                tmpfilenameext = "Notdefined.txt"

                if 'loop' not in params:
                    raise ValueError("Expecting a loop param."
                        "Recved {}".format(params))

                paramloop = params['loop']
                log.debug("LoopOpt {}".format(paramloop))

                # pips needs the labels and cannot have a pragma ICE before. 
                # So we need to add the label and not have ICE pragma on
                # printing out code for Pips.
                nlabels = srccode.replaceCoregWithLabel(regiongoalname,
                        Pips._base_codereg_mark, paramloop)

                if nlabels == 0:
                    raise RuntimeError("Added {} labels which means could not"
                            " find the code regions.".format(nlabels))

                # Extract srcfilename without the extension, and replace any
                # periods (Pips does not like it, dont know why.)
                basename = getBasenameFile(srccode.srcfilename).replace(".","-")

                tmpfilename = Pips._tmp_file_name+'-'+\
                    args.suffixinfo+'-'+basename+\
                    '-'+regiongoalname+'-comb-'+\
                    str(combid)+'-'+str(optid)+'-'+cls.getOptName()


                if srccode.filetype == ICE_CODEFILE_C:
                    tmpfilenameext = tmpfilename +'.'+ Pips._tmp_file_extension_c
                elif srccode.filetype == ICE_CODEFILE_CXX:
                    tmpfilenameext = tmpfilename +'.'+ Pips._tmp_file_extension_cxx
                elif srccode.filetype == ICE_CODEFILE_F:
                    tmpfilenameext = tmpfilename +'.'+ Pips._tmp_file_extension_f

#                with open("./Thiago"+tmpfilenameext, 'w') as f:
                    #sr.unparseandwrite(lambda x: x)
#                    srccode.unparseandwrite(f.write)
                #dirtmpfilename    = args.rundirpath+'/'+tmpfilename
                #dirtmpfilenameext = args.rundirpath+'/'+tmpfilenameext

                srccode.writetofile(tmpfilenameext)

                # removing previous output
                if os.path.isdir(tmpfilename+".database"):
                    shutil.rmtree(tmpfilename+".database", True)

            except Exception as e:
                #  return the original
                log.error("problem on preparing input for Pips: {}".format(e)) 

                # Remove temporary information generated by rose
                if not args.debug:
                    Pips.removeTmpFiles(tmpfilenameext)

                os.chdir(maindir)
                raise

            try:
                os.chdir(runabsdir)

                #
                # Implementation of workspace can be found at
                # $PIPPATH/MYPIPS/prod/pips/lib/python/pips/pypsbase.py

                #add the content of the .dep file to the cppflags
                depcontent = RoseUiuc.readContent(srccode.srcfilename, '.dep')
                pipscppflags = "-I"+maindir+" "+depcontent

                if pipscppflags:
                    log.info("Pips cppflags: {}".format(pipscppflags))
                #endif

                ws = workspace(tmpfilenameext, verbose=False, name=tmpfilename,
                        deleteOnClose=not args.debug, cppflags=pipscppflags)
                #ws = workspace(tmpfilenameext, verbose=False,
                #        name=dirtmpfilename, deleteOnClose=not args.debug,
                #        recoverInclude=False)

                if not ws:
                    raise RuntimeError("Could not create Pips workspace.")

            except Exception as e:
                log.error("problem Pips workspace: {}".format(e)) 

                # Remove temporary information generated by rose
                if not args.debug:
                    Pips.removeTmpFiles(tmpfilenameext)

                os.chdir(maindir)
                raise

            try:
                os.chdir(runabsdir)

                ws.props.USER_LOG_P = False
                ws.props.PRETTYPRINT_DIV_INTRINSICS = False
                ws.props.ABORT_ON_USER_ERROR = False
                ws.props.MEMORY_EFFECTS_ONLY = False
                ws.props.PARTIAL_LOOP_TILING = True
                ws.props.PREPROCESSOR_MISSING_FILE_HANDLING = "generate"
                #ws.props.NO_USER_WARNING = True
                ws.activate("MUST_REGIONS")
                ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")
                # Tell Pips that some labels are for marking code regions and
                # cannot be remove.
                if paramloop == "0":
                    log.debug("Setting LABEL_EXCEPTION to %s.",
                            Pips._base_codereg_mark)
                    ws.props.LABEL_EXCEPTION = Pips._base_codereg_mark
                else:
                    log.debug("No LABEL_EXCEPTION.")
                    ws.props.LABEL_EXCEPTION = ""

                #Different labels for Pips if the same
                #code regions appears more than once in the same file
                for cur in range(nlabels):
                    curlabel = Pips._initial_loop_mark+str(cur)
                    for fct in ws.all_functions:
                        log.debug("Function {}".format(fct))
                        loop = Pips.findLoopWithLabel(fct.loops(),\
                                curlabel)
                        if loop:
                            #loop.loop_tiling(matrix=matrixstr)
                            pipsfunc(fct, ws, loop, curlabel, optParams)
                        else:
                            log.warning("Function {} no loop with "
                                    "label {}.".format(fct.name, curlabel))
                        #fct.display()

                # Remove all the labels in the code that are not on LABEL_EXCEPTION
                # pattern
                for fct in ws.all_functions:
                    fct.clean_labels()

                # Reunite the optmized file and all functions as a single file.
                ws.all.unsplit()

                transffilename = tmpfilename+".database/Src/"+tmpfilenameext

                # Removes the headers info that Pips adds after optimizes the file.
                PipsParser.fixPipsHeader(transffilename, srccode.filetype)

                preprocinfo = [maindir]
                if args.preproc:
                    preprocinfo.extend(args.preproc)
                #endif

                # Construct the object with the same type from the input (e.g.
                # cparser, regex)
                tranfsrccode = type(srccode)(transffilename,
                        srccode.filetype).readandparse(preprocinfo)

                # Put back the ICE pragma
                if paramloop == "0":
                    tranfsrccode.replaceLabelWithCoreg(Pips._base_codereg_mark,
                        regiongoalname)

                # Needs to put orignal name file, otherwise it write on the file
                # result from Pips.
                tranfsrccode.srcfilename = srccode.srcfilename

            except Exception as e:
                #  return the original
                log.exception("problem on optimizing with Pips: {}".format(e)) 
                raise

            finally:
                log.debug("Closing")

                # Close Pips workspace
                ws.close()

                # Remove temporary information generated by rose
                if not args.debug:
                    Pips.removeTmpFiles(tmpfilenameext)

                os.chdir(maindir)
            #end try

            return tranfsrccode
    #endclass LoopOpt

    class Unroll(LoopOpt):

        @staticmethod
        def getSearchParams():
            """factor is a searchable parameter. """
            return {'factor' : int}

        @staticmethod
        def getSearchTypes():
            """ It can be searched by as an integer values or only on the power
            of two values in that range. The latter may improve speed of the
            search.
            """
            return {'factor' : [Integer, PowerOfTwo]} 

        @staticmethod
        def getOptName():
            return "Unroll"

        @staticmethod
        def unrollopts(fct, ws, loop, curlabel, optParams):
            loop.unroll(**optParams)

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):

            if 'factor' not in params or 'loop' not in params:
                raise ValueError("Expecting a factor param."
                        "Recved {}".format(params))

            log.debug("{}".format(params))

            optParams = {}
            optParams['UNROLL_RATE'] = params['factor']

            return self.applyLoopOpt(srccode, params, Pips.Unroll.unrollopts, optParams, regiongoalname, args, combid, optid)
    #endclass unroll

    class Tiling(LoopOpt):

        @staticmethod
        def getSearchParams():
            return {'factor' : list} # list of integers

        @staticmethod
        def getSearchTypes():
            return {'factor' : [[Integer, PowerOfTwo]]} 

        @classmethod
        def getOptName(cls):
            return "Tiling"

        @staticmethod
        def matrix(factors):
            n = len(factors)
            lmatrix = [[str(factors[i]) if j == i else 0
                        for j in range(n)]
                            for i in range(n)]
            a = [" ".join(map(str,row)) for row in lmatrix]
            a = ",".join(a)
            return a

        @staticmethod
        def tilingopts(fct, ws, loop, curlabel, optParams):
            loop.loop_tiling(**optParams)
#            fct.partial_eval()

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):

            if 'factor' not in params or 'loop' not in params:
                raise ValueError("Expecting a factor param."
                        "Recved {}".format(params))

            try:
                tfactor = [x for x in params['factor']] 
            except Exception as e:
                log.exception("In {} expecting list-style container. {}".format(self.getOptName(),e))
            #endtry

            matrixstr = self.matrix(tfactor)
            log.debug("matrix {}, loop {}".format(matrixstr, loop))

            optParams = {}
            optParams['matrix'] = matrixstr

            return self.applyLoopOpt(srccode, params, Pips.Tiling.tilingopts, optParams, regiongoalname, args, combid, optid)
    #endclass tiling

    class DiamondTiling(Tiling):

        @classmethod
        def getOptName(cls):
            return "DiamondTiling"

        @classmethod
        def matrix(cls, factors):
            """ Only works for 2D or 3D in space and 1D in time."""
            if len(factors) == 1:
                x = factors[0]
                matrix = [[1,0,0],[1,x,x],[1,x,-x]]
            elif len(factors) == 2:
                x = factors[0]
                y = factors[1]
                matrix = [[1,0,0,0],[0,y,0,y],[0,x*y,x,-x*y],[0,-x*y,x,xy]]
            else:
                raise RuntimeError("Not implemented DiamondTiling for len "
                        "{}.".format(len(factors)))

            a = [" ".join([str(col) for col in row]) for row in matrix]
            a = ",".join(a)
            return a
    #endclass DiamondTiling

    class GenericTiling(Tiling):

        @classmethod
        def getOptName(cls):
            return "GenericTiling"

        @classmethod
        def matrix(cls, matrix):
            #print matrix
            a = [" ".join([str(col) for col in row]) for row in matrix]
            a = ",".join(a)
            #print a
            return a
    #endclass GenericTiling

    class ParallelGenericTiling(Tiling):

        @classmethod
        def getOptName(cls):
            return "ParallelGenericTiling"

        @staticmethod
        def tilingopts(fct, ws, loop, curlabel, optParams):
            if 'tiledir' in optParams:
                ws.props.TILE_DIRECTION = optParams['tiledir']
            else:
                ws.props.TILE_DIRECTION = "TI"

            if 'localdir' in optParams:
                ws.props.LOCAL_TILE_DIRECTION = optParams['localdir']
            else:
                ws.props.LOCAL_TILE_DIRECTION = "LI"

            if 'checklegal' in optParams:
                ws.props.CHECK_TRANSFORMATION_LEGALITY = optParams['checklegal']

            ws.props.LOOP_LABEL = curlabel
            ws.props.LOOP_TILING_MATRIX = optParams['matrix']
            fct.parallel_loop_tiling()

        @classmethod
        def matrix(cls, matrix):
            #print matrix
            a = [" ".join([str(col) for col in row]) for row in matrix]
            a = ",".join(a)
            #print a
            return a

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):

            if 'factor' not in params or 'loop' not in params:
                raise ValueError("Expecting a factor param."
                        "Recved {}".format(params))

            try:
                tfactor = [x for x in params['factor']] 
            except Exception as e:
                log.exception("In {} expecting list-style container. {}".format(self.getOptName(),e))
            #endtry

            optParams = {}
            if 'tiledir' in params:
                optParams['tiledir'] = params['tiledir']

            if 'localdir' in params:
                optParams['localdir'] = params['localdir']

            if 'checklegal' in params:
                optParams['checklegal'] = params['checklegal']

            #matrixstr = self.matrix(params['factor'])
            matrixstr = self.matrix(tfactor)
            log.debug("matrix {}, loop {}".format(matrixstr, loop))

            optParams['matrix'] = matrixstr

            return self.applyLoopOpt(srccode, params, Pips.ParallelGenericTiling.tilingopts, optParams, regiongoalname, args, combid, optid)

    #endclass GenericTiling

    class LoopFusion(LoopOpt):
        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @staticmethod
        def getOptName():
            return "LoopFusion"

        @staticmethod
        def loopfusionopts(fct, ws, loop, curlabel, optParams):
            fct.loop_fusion()

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            if 'loop' not in params:
                params['loop'] = "0"

            optParams = {}
            return self.applyLoopOpt(srccode, params,
                    Pips.LoopFusion.loopfusionopts, optParams, regiongoalname, args, combid, optid)
    #endclass LoopFusion

    class HorizontalUnrolling(LoopOpt):
        @staticmethod
        def getSearchParams():
            """factor is a searchable parameter. """
            return {'factor' : int}

        @staticmethod
        def getSearchTypes():
            """ It can be searched by as an integer values or only on the power
            of two values in that range. The latter may improve speed of the
            search.
            """
            return {'factor' : [Integer, PowerOfTwo]} 

        @staticmethod
        def getOptName():
            return "HorizontalUnrolling"

        @staticmethod
        def horiunrollopts(fct, ws, loop, curlabel, optParams):
            loop.unroll(**optParams)
            fct.partial_eval()
            fct.simplify_control()
            fct.dead_code_elimination()
            fct.loop_fusion()
            fct.scalarization()
            fct.split_initializations()
            i = 0
#            while i <= optParams['UNROLL_RATE']+1:
            fct.forward_substitute()
            fct.forward_substitute()
            fct.forward_substitute()
            fct.forward_substitute()
            fct.forward_substitute()
#                i += 1
            fct.simplify_control()
            fct.dead_code_elimination()
            #fct.partial_eval()

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            if 'factor' not in params or 'loop' not in params:
                raise ValueError("Expecting a factor param."
                        "Recved {}".format(params))

            log.debug("{}".format(params))

            optParams = {}
            optParams['UNROLL_RATE'] = params['factor']

            return self.applyLoopOpt(srccode, params,
                    Pips.HorizontalUnrolling.horiunrollopts, optParams, regiongoalname, args, combid, optid)
    #endclass HorizontalUnrolling

    class UnrollAndJam(LoopOpt):

        @staticmethod
        def getSearchParams():
            """factor is a searchable parameter. """
            return {'factor' : int}

        @staticmethod
        def getSearchTypes():
            """ It can be searched by as an integer values or only on the power
            of two values in that range. The latter may improve speed of the
            search.
            """
            return {'factor' : [Integer, PowerOfTwo]} 

        @staticmethod
        def getOptName():
            return "UnrollAndJam"

        @staticmethod
        def unrollandjamopts(fct, ws, loop, curlabel, optParams):
            loop.unroll(**optParams)
            fct.loop_fusion()
            fct.partial_eval()
            fct.simplify_control()
            fct.dead_code_elimination()
            fct.display()
            fct.scalarization()

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            if 'factor' not in params or 'loop' not in params:
                raise ValueError("Expecting a factor param."
                        "Recved {}".format(params))

            log.debug("{}".format(params))

            optParams = {}
            optParams['UNROLL_RATE'] = params['factor']

            return self.applyLoopOpt(srccode, params,
                    Pips.UnrollAndJam.unrollandjamopts, optParams, regiongoalname, args, combid, optid)
    #endclass UnrollAndJam

#endclass Pips

class PipsParser(object):
    """  Parse the result generated from transformations from Pips. """
    import re

    __annot_init_file_c = r'^[ \t]*//[ \t]+' + Pips._initial_file_mark + \
    '[ \t]*(#\w+[ \t]*<.*>)[ \t]*\n'
    __annot_init_file_f = r'^\$[ \t]+' + Pips._initial_file_mark + '[ \t]*\n'

    __annot_initfile_f_re = re.compile(__annot_init_file_f, flags=re.MULTILINE)
    __annot_initfile_c_re = re.compile(__annot_init_file_c, flags=re.MULTILINE)

    def __init__(self):
        pass

    @staticmethod
    def fixPipsHeader(srcfilename, filetype):
        """ Find all includes that Pips replaced and put them back in
            proper way.
        """
        log.debug("filename: %s",srcfilename)

        with open(srcfilename, 'r') as f:
            codebuf = f.read()

        newcodebuf = ''
        while True:
            if filetype == ICE_CODEFILE_C or \
                    filetype == ICE_CODEFILE_CXX:
                matchobj = PipsParser.__annot_initfile_c_re.search(codebuf)

            elif filetype == ICE_CODEFILE_F:
        #        matchobj = PipsParser.__annot_initfile_f_re.search(codebuf)
                raise NotImplementedError()

            if not matchobj:
                log.debug("No more matches found!")
                if codebuf != '':
                    #newcodebuf += _pips_function_h+"\n\n"
#                    newcodebuf += "#include <pips_runtime.h>\n"
                    newcodebuf += "#define MAX0(x,y) ((x<y)? 0:x)\n"
                    newcodebuf += "#define MOD(x,y) (x%y)\n"
                    newcodebuf += codebuf
                break

            strinclude = matchobj.group(1)+'\n'
            newcodebuf += strinclude
            codebuf = codebuf[matchobj.end():]

        with open(srcfilename, 'w') as f:
            f.write(newcodebuf)

#endclass PipsParser

_pips_function_h = r"""#ifndef PIPS_HEADER_
#define PIPS_HEADER_
/* PIPS run-time support for C code generation
 *
 * Low-level functions used by the PIPS code generation algorithms:
 * * pips_min(int x1, in x2, int x3...)
 * * pips_max(int x1, int x2, in x3...)
 * * idiv(x,y)
 * * ...
 *
 * Some of these overloaded functions are available as intrinsics MIN
 * and MAX.
 *
 * For pips_min() and pips_max(), the first argument is the number of
 * effective arguments, the second argument is the first effective
 * argument and other arguments are passed as varargs. For instance:
 *
 * pips_min(5, 1, 2, 3, 4, 5);
 *
 * Initially, the source code for these two operators has been available in
 * validation/Hyperplane/run_time.src/pips_run_time.c
 */

//#define PIPS_C_MIN_OPERATOR_NAME        "pips_min"
//#define PIPS_C_MAX_OPERATOR_NAME        "pips_max"

// Old implementation inefficient for execution time, but can have "infinite" number of arguments
// #include <stdarg.h>
//
// int pips_min(int count, int first,  ...)
// {
//     va_list ap;
//     int j;
//     int min = first;
//     va_start(ap, first);
//     for(j=0; j<count-1; j++) {
//         int next =va_arg(ap, int);
//         min = min<next? min : next;
//     }
//     va_end(ap);
//     return min;
// }
//
// int pips_max(int count, int first,  ...)
// {
//     va_list ap;
//     int j;
//     int max = first;
//     va_start(ap, first);
//     for(j=0; j<count; j++) {
//         int next =va_arg(ap, int);
//         max = max>next? max : next;
//     }
//     va_end(ap);
//     return max;
// }


/* Alternative implementation provided by Fabien Coelho for up to 6 arguments.
 * Some improve by NL to try to make them more generic.
 * There is a ternaire test version (commented) and a if/then/else test version.
 * inline to be considered need to be compiled with minimum of -O1 or -Ofast
 *   -O0 or without -O no inlining is done
 * It is supposed to be as efficient as Fortran MIN and MAX intrinsics
 */

// type of pips_min/pips_max functions
#define mytype float

#define pips_min(n, ...)                    \
  pips_min_ ## n(__VA_ARGS__)
#define pips_max(n, ...)                    \
  pips_max_ ## n(__VA_ARGS__)

// To generate a list of argument with mytype, up to 6 args
#define argument_var1(mytype, a, ...) mytype a
#define argument_var2(mytype, a, ...) mytype a, argument_var1(mytype, __VA_ARGS__)
#define argument_var3(mytype, a, ...) mytype a, argument_var2(mytype, __VA_ARGS__)
#define argument_var4(mytype, a, ...) mytype a, argument_var3(mytype, __VA_ARGS__)
#define argument_var5(mytype, a, ...) mytype a, argument_var4(mytype, __VA_ARGS__)
#define argument_var6(mytype, a, ...) mytype a, argument_var5(mytype, __VA_ARGS__)

//definition of the body of pips_min and pips_max function
// ternaire version
// #define pips_min_return(mytype, m, a1, a2, ...) \
//   return a1 < a2 ? pips_min(m, a1, __VA_ARGS__) :  pips_min(m, a2, __VA_ARGS__);
// if/then/else version
#define pips_min_return(mytype, m, a1, a2, ...) \
  {                                     \
    mytype r;                           \
    if (a1<a2)                          \
      r=pips_min(m, a1, __VA_ARGS__);   \
    else                                \
      r=pips_min(m, a2, __VA_ARGS__);   \
    return r;                           \
  }
// #define pips_max_return(mytype, m, a1, a2, ...) \
//   return a1 > a2 ? pips_max(m, a1, __VA_ARGS__) :  pips_max(m, a2, __VA_ARGS__);
#define pips_max_return(mytype, m, a1, a2, ...) \
  {                                     \
    mytype r;                           \
    if (a1>a2)                          \
      r=pips_max(m, a1, __VA_ARGS__);   \
    else                                \
      r=pips_max(m, a2, __VA_ARGS__);   \
    return r;                           \
  }

//declaration of pips_min_n function, n is the number of variables to compare
// m must be n-1 value
#define pips_min_decl(mytype, n, m, ...) \
  __inline__ mytype pips_min_ ## n(argument_var ## n(mytype, __VA_ARGS__)) { \
    pips_min_return(mytype, m, __VA_ARGS__) \
  }
#define pips_max_decl(mytype, n, m, ...) \
  __inline__ mytype pips_max_ ## n(argument_var ## n(mytype, __VA_ARGS__)) { \
    pips_max_return(mytype, m, __VA_ARGS__) \
  }

// X macro to declare pips_min_n/pips_max_n functions
#define X_pips_decl(mytype, _) \
  _(mytype, 3, 2, i1, i2, i3) \
  _(mytype, 4, 3, i1, i2, i3, i4) \
  _(mytype, 5, 4, i1, i2, i3, i4, i5) \
  _(mytype, 6, 5, i1, i2, i3, i4, i5, i6)

//The basic pips_min_2/pips_max_2 must be manually declared
// __inline__ mytype pips_min_2(mytype i1, mytype i2)
// {
//   return i1 < i2? i1: i2;
// }
//__inline__ mytype pips_min_2(mytype i1, mytype i2)
 mytype pips_min_2(mytype i1, mytype i2)
{
  mytype r;
  if (i1<i2)
    r=i1;
  else
    r=i2;
  return r;
}
// __inline__ mytype pips_max_2(mytype i1, mytype i2)
// {
//   return i1 > i2? i1: i2;
// }
//__inline__ mytype pips_max_2(mytype i1, mytype i2)
mytype pips_max_2(mytype i1, mytype i2)
{
  mytype r;
  if (i1>i2)
    r=i1;
  else
    r=i2;
  return r;
}

// declaration of pips_min_n functions
X_pips_decl(mytype, pips_min_decl)
// declaration of pips_max_n functions
X_pips_decl(mytype, pips_max_decl)
#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))
#define  pips_div(x,y) ((x<0)?((x-y+1)/y):((x)/y))
#endif
"""
