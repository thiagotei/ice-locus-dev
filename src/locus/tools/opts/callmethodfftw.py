'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.util.exceptions import OptExecError, NotLegalOptimizationError
from locus.backend.gencache import GenCache

log = logging.getLogger("callmethodfftw")

class CallMethodFftw(AbstractOptTool):

    def __init__(self):
        pass

    class BaseFftw(AbstractOptToolOpt):
        @staticmethod
        def getParams():
            return {'id': str, 'nextid' : str, 'buffer': str }

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def getToolName(cls):
            return "CallMethodFftw"

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            try:
                buffername = params.get('buffer')
                curid = params.get('idarg')
                childid = params.get('childid')
                parid = params.get('paridarg')
                #beginv = params.get('begin')
                #endv = params.get('end')
            except KeyError as e:
                log.error("Could not find the info required. {}".format(e))

            with open(buffername, 'a') as buf:
                #print("BaseFftw curid:"+str(curid) + " nextid:"+str(nextid))
                line = cls.createstr(parid, curid, childid);
                buf.write(line)

            return srccode
        #enddef

        @classmethod
        def createstr(cls, parid, curid, childid):
            #print "createstr", curid, nextid, type(curid), type(nextid), cls._CODESTR
            return cls._CODESTR.format(str(parid), str(curid), str(childid))
        #enddef
    #endclass

    class Iterative(BaseFftw):
#        _CODESTR = r"""solver *s{1} = NULL;
#if(!s{1}){{fprintf(stderr, "Solver s{1} not found!\n"); exit(1);}}
#plan *cld{1} = s{1}->adt->mkplan(s{1}, pbl{1}, ego);
#if(!cld{1}){{fprintf(stderr, "CLD mkplan failed!\n"); exit(1);}}
#"""

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            std = ""
            error = "CallMethodFftw.Iterative not implemented, it "\
                    "have would've resulted in slowdonw anyway!!!!"
            retcode = 1
            raise NotLegalOptimizationError("Optimization not implemented!",std,error,retcode)
    #endclass

    ##########################
    # hints on writing code generator for solvers:
    # variable pbl -> lhs = {1}; rhs = {0};
    #          cld -> lhs = {1}; rhs = {2};
    #          cldw -> lhs = {1}; rhs = {1};


    ##########################
    # Creates the plan, 1st to be called.
    class ProlApiplan(BaseFftw):
        _CODESTR = r"""pldesc[flagidx] = "LOCUS";
int howmany = 1, rank = 1, istride = 1, idist = 1, ostride = 1, odist = 1;
const int *inembed = 0;
const int *onembed = 0;

R *ri, *ii, *ro, *io;
if (!fftw_many_kosherp(rank, &n, howmany)){{printf("Eita something weird!\n");exit(1);}}

EXTRACT_REIM(FFTW_FORWARD, in, &ri, &ii);
EXTRACT_REIM(FFTW_FORWARD, out, &ro, &io);

tensor *sz = fftw_mktensor_rowmajor(rank, &n, N0(inembed), N0(onembed), 2 * istride, 2 * ostride);
tensor *vecsz = fftw_mktensor_1d(howmany, 2 * idist, 2 * odist);
problem * pbl{1} = fftw_mkproblem_dft_d(sz, vecsz,
                                    TAINT_UNALIGNED(ri, flags),
                                    TAINT_UNALIGNED(ii, flags),
                                    TAINT_UNALIGNED(ro, flags),
                                    TAINT_UNALIGNED(io, flags));

planner *ego = fftw_mkapiplan_locus_prol(flags, pbl{1});
if(!ego){{fprintf(stderr, "Error creating planner!\n");exit(1);}};
"""
    #endclass

    # Finalizes plan creation. Last to be called.
    class EpilApiplan(BaseFftw):
        _CODESTR = r"""fftw_plan theplan = fftw_mkapiplan_locus_epil(sign, ego, cld{2}, pbl{1});
"""
    #endclass
    ##########################

    ##########################
    # Creates a VT plan
    class ProlVT(BaseFftw):
        _CODESTR = r"""/* ProlVT {1}  */
solver *s{1} = fftw_findSolver(ego, "fftw_dft_vrank_geq1_register", 0);
if(!s{1}) {{fprintf(stderr, "Solver s{1} not found!\n");exit(1);}}
vrankinfo *inf{1} = fftw_mkplan_vrankgeq1_prol(s{1}, pbl{0}, ego);
if(!inf{1}) {{fprintf(stderr, "vrankinfo inf{1} not applicable!\n"); exit(1);}}
const problem * pbl{1} = inf{1}->cld_prb;
"""
    #endclass

    # Finalizes the creation a VT plan
    class EpilVT(BaseFftw):
        _CODESTR = r"""plan *cld{1} = fftw_mkplan_vrankgeq1_epil(s{1}, cld{2}, inf{1});
fftw_destroy_vrank_info(inf{1});
/* EpilVT {1}  */
"""
    #endclass
    ##########################

    ##########################
    # Creates a CT plan.
    class ProlDftCt(BaseFftw):
        _CODESTR = r"""/* ProlDftCt code {1} */
//const problem *pbl{1} = pbl{0};  /* This for the cld recursion... the cldw recursion uses pbl{1} */
"""
    #endclass

    # Finish the creation of a CT plan.
    class EpilDftCt(BaseFftw):
        _CODESTR = r"""plan *cld{1} = fftw_mkplan_ctdit_epil(sW{1}, cldw{1}, cld{2}, inf{1});
if(!cld{1}) {{fprintf(stderr, "CT cld{1} plan is null!\n"); exit(1);}}
fftw_destroy_ctdit_info(inf{1});
/* EpilDftCt code {1}  */
"""
    #endclass
    ##########################

    ##########################
    # Indirect before solver (idx 0; after solver is idx 1).
    class ProlIndirect(BaseFftw):
        _CODESTR = r"""/* ProlIndirect code {1}  */
solver *s{1} = fftw_findSolver(ego,"fftw_dft_indirect_register", 0);
if(!s{1}) {{fprintf(stderr, "Solver s{1} not found!"); exit(1);}}
indinfo *inf{1} = fftw_mkplan_indir_prol(s{1}, pbl{0}, ego);
if(!inf{1}) {{fprintf(stderr, "indirect inf{1} solver not applicable!\n"); exit(1);}}
const problem *pbl{1} = (const problem *) inf{1}->cld_prb;
"""

    class MiddleIndirect(BaseFftw):
        _CODESTR = r"""plan *tmpcld{1} = cld{2};
pbl{1} = (const problem *) inf{1}->cldcpy_prb;
"""
    #endclass

    class EpilIndirect(BaseFftw):
        _CODESTR = r"""plan *cld{1} = fftw_mkplan_indir_epil(s{1}, cld{2}, tmpcld{1});
if(!cld{1}){{fprintf(stderr, "Indirect cld{1} plan is null!\n"); exit(1);}}
fftw_destroy_indir_info(inf{1});
/* EpilIndirect code {1} */
"""
    #endclass
    ##########################

    # Codelets base class for codelets (T for dftw, N for dft)
    class BaseCodelets(BaseFftw):
        _CODELET_SUFIX = "_avx"
        _CODELET_INDEX = "0"

        @classmethod
        def createstr(cls, parid, curid, childid):
            return cls._CODESTR.format(str(parid), str(curid), str(childid),
                    cls._CODELET_NAME, cls._CODELET_SUFIX, cls._SOLVER_VAR,
                    cls._CODELET_INDEX)
        #enddef
    #endclass

    class BaseCldCodelets(BaseCodelets):
        _SOLVER_VAR = "s"
        _CODESTR = r"""solver *{5}{1} = fftw_findSolver(ego, "{3}{4}", {6});
if(!{5}{1}){{fprintf(stderr, "Solver {5}{1} not found!\n"); exit(1);}}
plan *cld{1} = s{1}->adt->mkplan({5}{1}, pbl{0}, ego);
if(!cld{1}){{fprintf(stderr, "CLD cld{1} mkplan failed!\n"); exit(1);}}
"""
#        @classmethod
#        def createstr(cls, curid, nextid):
#            return cls._CODESTR.format(str(curid), str(nextid),
#                    cls._CODELET_NAME, cls._CODELET_SUFIX, cls._SOLVER_VAR)
#        #enddef
    #endclass

    # DIT (Decimation in time)
    class BaseCldwCodelets(BaseCodelets):
        _SOLVER_VAR = "sW"
        _CODESTR = r"""solver *{5}{1} = fftw_findSolver(ego, "{3}{4}", {6});
if(!{5}{1}){{fprintf(stderr, "Solver {5}{1} not found!\n"); exit(1);}}
ctditinfo *inf{1} = fftw_mkplan_ctdit_prol(sW{1}, pbl{0}, ego);
if(!inf{1}) {{fprintf(stderr, "CT inf{1} is not applicable.\n" ); exit(1);}}
const problem *pbl{1} = (const problem *) inf{1}->cld_prb;
plan *cldw{1} = ((const ct_solver *) {5}{1})->mkcldw((const ct_solver*) {5}{1},
           inf{1}->r, inf{1}->m * inf{1}->d[0].os, inf{1}->m * inf{1}->d[0].os,
           inf{1}->m, inf{1}->d[0].os,
           inf{1}->v, inf{1}->ovs, inf{1}->ovs,
           0, inf{1}->m,
           inf{1}->p->ro, inf{1}->p->io, ego);
if(!cldw{1}) {{fprintf(stderr, "No cldw{1} plan!\n"); exit(1);}}
"""
    #endclass


    # DIF (Decimation in frequency)
    class BaseCldwCodeletsDIF(BaseCodelets):
        _SOLVER_VAR = "sW"
        _CODESTR = r"""solver *{5}{1} = fftw_findSolver(ego, "{3}{4}", {6});
if(!{5}{1}){{fprintf(stderr, "Solver {5}{1} not found!\n"); exit(1);}}
ctditinfo *inf{1} = fftw_mkplan_ctdit_prol(sW{1}, pbl{0}, ego);
if(!inf{1}) {{fprintf(stderr, "CT inf{1} is not applicable.\n" ); exit(1);}}
const problem *pbl{1} = (const problem *) inf{1}->cld_prb;
plan *cldw{1} = ((const ct_solver *) {5}{1})->mkcldw((const ct_solver*) {5}{1},
           inf{1}->r, inf{1}->m * inf{1}->d[0].is, inf{1}->cors,
           inf{1}->m, inf{1}->d[0].is,
           inf{1}->v, inf{1}->ivs, inf{1}->covs,
           0, inf{1}->m,
           inf{1}->p->ri, inf{1}->p->ii, ego);
if(!cldw{1}) {{fprintf(stderr, "No cldw{1} plan!\n"); exit(1);}}
"""
    #endclass

    ##########################
    # DFT R2HC rdft-rank0-tiled
    class ProlR2HC(BaseFftw):
        _CODESTR = r"""/* ProlR2HC {1} */
solver *s{1} = fftw_findSolver(ego, "fftw_dft_r2hc_register", 0);
if(!s{1}) {{fprintf(stderr, "Solver s{1} not found!\n"); exit(1);}}
r2hcinfo *inf{1} = fftw_mkplan_r2hc_prol(s{1}, pbl{0}, ego);
if(!inf{1}) {{fprintf(stderr, "r2hcinfo inf{1} not applicable!\n"); exit(1);}}
const problem *pbl{1} = inf{1}->cld_prb;
"""
    #endclass

    class EpilR2HC(BaseFftw):
        _CODESTR = r"""plan *cld{1} = fftw_mkplan_r2hc_epil(s{1}, cld{2}, inf{1});
fftw_destroy_r2hc_info(inf{1});
/* EpilR2HC {1} */
"""
    #endclass
    ##########################

    ##########################
    # Dftw-genericbuf only showed up on fft length >= 512K elements.
    # there are 7x5 dftw-genericbuf kernels. look at dftw-genericbuf source.
    class ProlWGenBuf(BaseCldwCodelets):
        _CODELET_SUFIX = ""
        _CODELET_NAME  = "fftw_ct_genericbuf_register"
        _SOLVER_VAR = "sW" #"sGenW"
        _CODESTR = r"""/* ProlWGenBuf {1}  */
solver *{5}{1} = fftw_findSolver(ego, "{3}{4}", {6});
if(!{5}{1}){{fprintf(stderr, "Solver {5}{1} not found!\n"); exit(1);}}
ctditinfo *inf{1} = fftw_mkplan_ctdit_prol({5}{1}, pbl{0}, ego);
if(!inf{1}) {{fprintf(stderr, "CT inf{1} is not applicable.\n" ); exit(1);}}
wgenbufinfo *infgb{1} = fftw_mkcldw_wgenbuf_prol((const ct_solver*){5}{1},
           inf{1}->r, inf{1}->m * inf{1}->d[0].os, inf{1}->m * inf{1}->d[0].os,
           inf{1}->m, inf{1}->d[0].os,
           inf{1}->v, inf{1}->ovs, inf{1}->ovs,
           0, inf{1}->m,
           inf{1}->p->ro, inf{1}->p->io, ego);
if(!infgb{1}) {{fprintf(stderr, "No ProlWGenBuf infgb{1} plan!\n"); exit(1);}}
const problem *pbl{1} = infgb{1}->cld_prb;
"""
    #endclass

    class EpilWGenBuf(BaseCldwCodelets):
        _CODELET_SUFIX = ""
        _CODELET_NAME  = "fftw_ct_generic_register"
        _SOLVER_VAR = "sW" #"sGenW"
        _CODESTR = r"""plan *cldw{1} = fftw_mkcldw_wgenbuf_epil((const ct_solver *) {5}{1}, cld{2}, infgb{1});
if(!cldw{1}) {{fprintf(stderr, "dftw-genericbuf {1} plan is null!\n"); exit(1);}}
fftw_destroy_wgenbuf_info(infgb{1});
/* EpilWGenBuf {1}  */
pbl{1} = inf{1}->cld_prb; //This cld of the ct needs the data from ctditinf.
"""
    #endclass
    ##########################

    # T Codelets
    class T1fv_2(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t1fv_2"

    class T2fv_2(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t2fv_2"

    class T1fv_4(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t1fv_4"

    class T2fv_4(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t2fv_4"

    class T3fv_4(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t3fv_4"

    class T1fv_8(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t1fv_8"

    class T2fv_8(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t2fv_8"

    class T3fv_8(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t3fv_8"

    class T1fv_16(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t1fv_16"

    class T2fv_16(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t2fv_16"

    class T3fv_16(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t3fv_16"

    class T1fv_32(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t1fv_32"

    class T2fv_32(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t2fv_32"

    class T3fv_32(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t3fv_32" #+BaseCodelets._CODELET_SUFIX;

    class T3fv_32_Buf(T3fv_32):
        _CODELET_INDEX = "1"

    class T1fv_64(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t1fv_64"

    class T2fv_64(BaseCldwCodelets):
        _CODELET_NAME = "fftw_codelet_t2fv_64"

    # Q Codelets
    class Q1fv_8(BaseCldwCodeletsDIF):
        _CODELET_NAME = "fftw_codelet_q1fv_8"

    # RDFT rank0 Codelets
    class Rdft_rank0_tiled(BaseCldCodelets):
        _CODELET_NAME = "fftw_rdft_rank0_register"
        _CODELET_INDEX = "4"
        _CODELET_SUFIX = ""

    # N Codelets
    class N1fv_2(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_2"

    class N2fv_2(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n2fv_2"

    class N1fv_4(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_4"

    class N2fv_4(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n2fv_4"

    class N1fv_8(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_8"

    class N2fv_8(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n2fv_8"

    class N1fv_16(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_16"

    class N2fv_16(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n2fv_16"

    class N1fv_32(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_32"

    class N2fv_32(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n2fv_32"

    class N2fv_32_Buf(N2fv_32):
        _CODELET_INDEX = "1"

    class N1fv_64(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_64" #+_CODELET_SUFIX;

    class N2fv_64(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n2fv_64"

    class N1fv_128(BaseCldCodelets):
        _CODELET_NAME = "fftw_codelet_n1fv_128"

