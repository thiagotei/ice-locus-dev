'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging, sys, copy

from locus.tools.opts.opttoolinterface import AbstractOptTool, AbstractOptToolOpt
from locus.optspace.parameters import Parameter, Integer, PowerOfTwo,\
Permutation, Boolean
from locus.util.misc import readBuffer
from locus.backend.gencache import GenCache
from locus.util.exceptions import OptExecError, NotLegalOptimizationError

log = logging.getLogger("Builtins")

class Builtins(AbstractOptTool):

    def __init__(self):
        pass

    class Altdesc(AbstractOptToolOpt):

        def __init__(self):
            log.debug("Altdesc")

        @staticmethod
        def getToolName():
            return "Builtins"

        @staticmethod
        def getOptName():
            return "Altdesc"

        @staticmethod
        def getParams():
            return {'source' : str}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        def apply(self, srccode, orighashes, params, regiongoalname, args, combid, optid):
            """ Apply the current opt using the params on the code region with
            the name regiongoal. """
            log.debug("start {}".format(params))

            newbuffer = readBuffer(params['source'])

            # Find the hash
            stripebuf, file_hashkey = GenCache.striphashkey(newbuffer, srccode.filetype)

#            srccode.replaceCodeRegion(regiongoalname, stripebuf, hashkey)

            for ind, coreg in enumerate(srccode.annotated()):
                if coreg.label == regiongoalname:
                    label_orig = orighashes[ind][0]
                    hash_orig  = orighashes[ind][1]
                    if coreg.label != label_orig:
                        raise RuntimeError("Labels of the orighash and coreg are "
                                " different. {} != {}.".format(label_orig, coreg.label))
                    #endif

                    if hash_orig == file_hashkey:
                        srccode.replaceCodeRegion(coreg, stripebuf)
                    else:
                        raise RuntimeError("Hash keys do not match label: {}\nsourcecode: "
                                        "{}\naltdesc: {}\n".format(label_orig,
                                        hash_orig, file_hashkey))
                    #endif
                #endif
            #endfor
            return srccode

    class AbortCodeGen(AbstractOptToolOpt):
        """Used when the code generation should be aborted but does not want to
        stop the search."""

        def __init__(self):
            log.debug("Altdesc")

        @staticmethod
        def getToolName():
            return "Builtins"

        @staticmethod
        def getOptName():
            return "Altdesc"

        @staticmethod
        def getParams():
            return {'source' : str}

        @staticmethod
        def getSearchParams():
            return {}

        @staticmethod
        def getSearchTypes():
            return {}

        @classmethod
        def apply(cls, srccode, orighashes, params, regiongoalname, args, combid, optid):
            std = ""
            error = "Code generation aborted, the search continues! "
            retcode = 1
            raise NotLegalOptimizationError("Code gen aborted on "
                    "variant {}!".format(combid),std,error,retcode)
        ##
    ####
