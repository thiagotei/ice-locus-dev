'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import logging

from locus.tools.toolinterface import AbstractTool
from abc import ABCMeta, abstractmethod

log = logging.getLogger("AbstractOptTool")

class AbstractOptTool(AbstractTool, metaclass=ABCMeta):
    """ This is a superclass to all optimization tools.
    """

    def __init__(self, name):
        AbstractTool.__init__(self, name)
        log.debug("AbstractOptTools init")

class AbstractOptToolOpt(object, metaclass=ABCMeta):
    """ This is a meta class for optimizations inner class in the tools
    """

    @abstractmethod
    def apply(self):
        pass

    @abstractmethod
    def getSearchTypes(self):
        pass

    @abstractmethod
    def getParams(self):
        pass

    @abstractmethod
    def getSearchParams(self):
        pass

    @abstractmethod
    def getToolName():
        pass
