'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

#import subprocess32 as subprocess
import logging, sys, time, os, shlex

if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
    print("Using subprocess32")
else:
    import subprocess

log = logging.getLogger("Commands")

STDOUT_FILE_NAME = './.tmp_ice_comp_stdout.txt'
STDERR_FILE_NAME = './.tmp_ice_comp_stderr.txt'

class Commands:
    
    def __init__(self):
        pass

    @staticmethod
    def callCmd(cmd, timeout=None):
        output = None
        error_out = None
        retcode = None
        elapsed = 0.0
        hadtimeout = False

        if cmd is None or not cmd:
            return (output, error_out, retcode, elapsed, hadtimeout)
        #endif

        try:
            # Thiago Nov 11 2019: had to disable the shell creation (shell=False), because 
            # it would kill the shell created but leave the program running in the background.
            cmdlist = shlex.split(cmd)
            log.debug("invoked: {} -- {} timeout: {}".format(cmd, cmdlist, timeout))

            #rename the original file to use the name for the generated ones.
            #p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
            #output, error_out  = p.communicate()

            with open(STDOUT_FILE_NAME, 'w') as fstdout, \
                    open(STDERR_FILE_NAME, 'w') as fstderr:
                t0 = time.time()
                cplproc = subprocess.run(cmdlist, shell=False,
                        #stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                        stdout=fstdout,
                        stderr=fstderr,
                        timeout=timeout)# timeout in seconds.
                t1 = time.time()
            #endwith

        except subprocess.TimeoutExpired as e:
            #t1 = time.time()
            elapsed = e.timeout
            hadtimeout = True
            log.warning("Process ran for too long. {}".format(e.timeout))

        except Exception as e:
            #log.error("{}".format(e))
            raise
        else:
            with open(STDOUT_FILE_NAME, 'r') as f:
                output = f.read()
            with open(STDERR_FILE_NAME, 'r') as f:
                error_out = f.read()
            #error_out = cplproc.stderr
            retcode = cplproc.returncode
            elapsed = t1 - t0
        finally:
            os.remove(STDOUT_FILE_NAME)
            os.remove(STDERR_FILE_NAME)
        #endtry

        return (output, error_out, retcode, elapsed, hadtimeout)

    @staticmethod
    def callCmdDic(cmd, timeout=None):
        stdout, stderr, retcode, elapsed, hadtimeout = Commands.callCmd(cmd, timeout)
        return {'stdout': stdout,
                'stderr': stderr,
                'returncode': retcode,
                'time': elapsed,
                'timeout': hadtimeout}

