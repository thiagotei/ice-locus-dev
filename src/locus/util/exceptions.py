'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import subprocess, logging, sys

log = logging.getLogger(__name__)


class OptimizationError(RuntimeError):
    """Exception to be raised if any error occur during the invocation of 
    the optimization tools."""
    def __init__(self, message, stdout="", stderr="", retcode=""):
        self.message = message
        self.stdout = stdout
        self.stderr = stderr
        self.retcode = retcode

    def __str__(self):
        ret  = self.message
#        ret += "\nSTDOUT:\n"+self.stdout
#        ret += "\nSTDERR:\n"+self.stderr+"\n"
        return ret

class OptExecError(OptimizationError):
    """Error occurred during optimization """
    
    def __str__(self):
        ret  = self.message
        ret += "\nSTDOUT:\n"+self.stdout
        ret += "\nSTDERR:\n"+self.stderr+"\n"
        return ret

class NotLegalOptimizationError(OptimizationError):
    """This is call when an optimization could not be executed"""
    pass

class StopAfterInterruption(RuntimeError):
    """Raised when the search should be stop after some time"""
    pass

class BoundsException(RuntimeError):
    """Exception when the configuration suggested is not between bounds. """
    pass
