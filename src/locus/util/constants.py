'''
@author: Thiago Teixeira
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

ICE_OK = 0
ICE_ERROR = 1

ICE_C_SUFFIX = ['c', 'h']
ICE_CXX_SUFFIX = ['cpp', 'C', 'cc', 'cxx', 'H', 'hh', 'hpp']
ICE_FORTRAN_SUFFIX = ['f90', 'fpp', 'f']

ICE_CODEFILE_C = 0
ICE_CODEFILE_F = 1
ICE_CODEFILE_CXX = 2

ICE_OUTPUT_MODE_STDOUT = 'stdout'
ICE_OUTPUT_MODE_INPLACE = 'inplace'
ICE_OUTPUT_MODE_SUFFIX = 'suffix'

ICE_WORK_DIR_NAME = '.locus'
ICE_WORK_DIR_PLACE = "."
ICE_RUN_DIR_NAME = 'run-'

ICE_CACHE_DIR_NAME = '.locus-cache'

ICE_ROSE_END_MARK = 'endlooptransformation'

# name to refer the top blk in dict
ICE_TOP_BLK_STR = '_topblk_'

# Installation path for the uiuc-compiler-opts
ICE_TOOL_PATH_CMPOPTS = 'ICE_CMPOPTS_PATH'

# Basename for env variables that are printed in the beginning of execution
ICE_ENV_BASENAME = 'ICELOCUS_'

# Run dir path where Locus saves all the information about the running.
LOCUS_RUNDIRPATH = ICE_ENV_BASENAME+'RUNDIRPATH'

ICE_CONFIG_FILENAME = '.locus.yaml'

def stringCodeType(typefile):
    if typefile == ICE_CODEFILE_C:
        strres = "ICE_CODEFILE_C"
    elif typefile == ICE_CODEFILE_CXX:
        strres = "ICE_CODEFILE_CXX"
    elif typefile == ICE_CODEFILE_F:
        strres = "ICE_CODEFILE_F"
    else:
        strres = "undefined"
    return strres

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    YELLOW = '\033[93m'
    WARNING = YELLOW
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Definition for message colors
ICE_CLR_INITSEED = bcolors.YELLOW
ICE_CLR_NEWBEST = bcolors.YELLOW
ICE_CLR_FAIL = bcolors.FAIL
