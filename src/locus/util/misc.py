'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:
import sys, os, yaml, argparse, sys
import logging
from io import StringIO

from locus.util.constants import ICE_CONFIG_FILENAME

log = logging.getLogger("Misc")

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout

def getDynClass(path, class_name):
    try:
        import importlib
        mod = importlib.import_module(path)
        ret = getattr(mod, class_name)
    except AttributeError as e:
        log.error("AttrErr {}".format(e))
        #return False
        raise
    except Exception as e:
        log.error("Loading Tool Class {}. {}".format(class_name, e))
        #return False
        raise
    else:
        return ret

def getToolClass(path, classname):
    # Loading the tool module and class (eg. RoseUiuc, Pips)
    try:
        toolclass = \
                getDynClass(path+classname.lower(),\
                classname)
    except AttributeError as e:
        #attrs = [attr for attr in dir(toolclass) if not \
        #        attr.startswith('_') ]
        #log.error("Attr Error %s. Attrs found are %s.",\
        #        e, attrs)
        log.error("Attr Error %s.".format(e))
        #return False
        raise
    except Exception as e:
        log.error("Loading Opt Class {}. {}".format(classname, e))
        #return False
        raise

    return toolclass

def getOptClass(toolclass, optname):
    try:
        optclass = getattr(toolclass, optname)
    except Exception as e:
        log.error("%s", e)
        raise
    return optclass

def readBuffer(filename):
    with open(filename,'r') as f:
        ret = f.read()
    return ret

def unwindBackup(nameorig, nametarget):
    try:
        os.rename(nameorig, nametarget)
    except Exception as e:
        log.error("{}".format(e))
        raise

def moduleExists(pathname):
    try:
        import imp
        imp.load_source("", pathname)
    except ImportError as e:
        log.exception("{}".format(e))
        return False
    except Exception as e:
        log.exception("{}".format(e))
        return False
    else:
        return True

def functionExists(pathname, func_name):
    try:
        import imp
        mod = imp.load_source("", pathname)
        func = getattr(mod, func_name)
    except AttributeError as e:
        log.exception("{}".format(e))
        return False
    except Exception as e:
        log.exception("{}".format(e))
        return False
    else:
        return func

def getTimingFunc(tfunc):
    import re
    if tfunc: 
        r = re.split(":", tfunc)
        if len(r) != 2:
            log.error("Wrong format - {} - for the timing <file:function>!".format(tfunc))
            return False

        timemodule = r[0]
        timefunc = r[1]
        if not os.path.isfile(timemodule):
            log.error('File - {} - to the timing function is not valid!'.format(timemodule))
            return False

        if moduleExists(timemodule):
            return functionExists(timemodule, timefunc)
    else:
        return False

#
# General function to get time from time command  on linux.
#
def getDefaultTiming(std, error):
    try:
        m = re.split('\s*', error)
        result = 'N/A' 
        if m and len(m) >= 3:
           result = float(m[2]) 
    except Exception as e:
        log.exception(e)
        return None

    return result

def getBasenameFile(path):
    # Extract srcfilename without the extension
    name = os.path.basename(path)
    namenoext = os.path.splitext(name)[0]
    return namenoext
###

def customargparser(parents, args=None):

    confparser = argparse.ArgumentParser(add_help=False,
                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    confparser.add_argument("--config-file", required=False, nargs = '?',
                        default='.locus.yaml', const='.locus.yaml', type=str,
                        help="Config file that may include all the commandline"
                        " arguments. Yaml format.")

    confargs, remaining_argv = confparser.parse_known_args(args)

    argparser = argparse.ArgumentParser(add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        parents=parents+[confparser])

    cfgfilename = confargs.config_file  #ICE_CONFIG_FILENAME
    if cfgfilename and os.path.isfile(cfgfilename):
        with open(cfgfilename, 'r') as f:
            inpcfg = yaml.load(f, Loader=yaml.SafeLoader)
            #print(f'{cfgfilename}: {inpcfg}')
            argparser.set_defaults(**inpcfg)
        #
        #print(f'Loaded arguments form {cfgfilename}!')
        log.info(f'Loaded arguments form {cfgfilename}!')
    #

    args = argparser.parse_args(args)
    return args
###

def addBoolArg(parser, name, default=False, help=None):
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('--' + name, dest=name, action='store_true', 
                       help=help)
    group.add_argument('--no-' + name, dest=name, action='store_false')
    parser.set_defaults(**{name:default})
###
