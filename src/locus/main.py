'''
@author: Thiago Teixeira
'''

import argparse
import logging

from locus.ice import ICE, argparser as iceargparser, init_logging as iceinit_logging
from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser
#from locus.tools.search.opentunertool_locus import OpenTunerTool, \
#                    argparser as tunerargparser
from locus.util.misc import customargparser

log = logging.getLogger(__name__)

def main(searchtobj, xparser, args=None):
    parsers = [iceargparser, optlangargparser, xparser]
    args = customargparser(parsers, args)
    args.rundirpath = iceinit_logging()
    if args.debug:
        logging.root.setLevel(logging.DEBUG)
        #log.setLevel(logging.DEBUG)
#    print "Main running"
    log.debug("Main running!")
    lp = LocusParser(args.optfile, args.debug)
    #optuner = OpenTunerTool(args)
    st = searchtobj(args)
    ice_inst = ICE(lp, st, args)
    ice_inst.run()
#
