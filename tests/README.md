Tests
-------

Run all tests: `python all_tests.py`

Run a specific test: `python test_cparser_sourcecode.py Test_cparser_sourcecode.test_add_label`
