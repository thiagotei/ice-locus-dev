#!/usr/bin/env python
'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import sys, unittest, logging

sys.path[0:0] = ['.', '..']

log = logging.getLogger()
log.setLevel(logging.WARNING)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.CRITICAL)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(funcName)s %(message)s -- %(lineno)d')
ch.setFormatter(formatter)
log.addHandler(ch)


suite = unittest.TestLoader().loadTestsFromNames(
        ['test_opt_roseuiuc']
)

testresult = unittest.TextTestRunner(verbosity=1).run(suite)
sys.exit(0 if testresult.wasSuccessful() else 1)

