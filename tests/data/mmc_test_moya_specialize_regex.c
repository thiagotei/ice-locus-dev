#include <stdio.h>
#include <stdlib.h>

#define matSize 100
#define maxTest 100

/* Column-major order (Same as Fortran). */
//#define ind(i,j) (j)*matSize+i
/* Row-makor order. */
#define ind(i,j) (i)*matSize+j

/* Statically allocate the matrices.  This can improve the compiler's
   ability to optimize operations on these variables.  This will be discussed
   in more detail later in the course. */
double matA[matSize*matSize], matB[matSize*matSize], matC[matSize*matSize];

double mysecond(void);
void dummy(double *a, double *b, double *c);

int main(int argc, char *argv[])
{
    double tStart, tEnd, tLoop, rate, t;
    int    i, j, k, tests;

    /* Initialize the matrics */
    /* Note that this is *not* in the best order with respect to cache;
       this will be discussed later in the course. */
#pragma @ICE block=rep
    for (i=0; i<matSize; i++)
	for (j=0; j<matSize; j++) {
	    matA[ind(i,j)] = 1.0 + i;
	    matB[ind(i,j)] = 1.0 + j;
	    matC[ind(i,j)] = 0.0;
	}
#pragma @ICE endblock

    tLoop = 1.0e10;
    for (tests=0; tests<maxTest; tests++) {
	tStart = mysecond();
#pragma moya specialize 
	for (i=0; i<matSize; i++)
	    for (j=0; j<matSize; j++) {
		for (k=0; k<matSize; k++)
		    matC[ind(i,j)]+= matA[ind(i,k)] * matB[ind(k,j)];
	    }
// endlooptransformation
	tEnd = mysecond();
	t = tEnd - tStart;
	dummy(matA, matB, matC);
	if (t < tLoop) tLoop = t;
	if (matC[ind(0,0)] < 0) {
	    fprintf(stderr, "Failed matC sign test\n");
	}
    }
        
    int    i2, j2, k2, tests2;
    for (tests2=0; tests2<maxTest; tests2++) {
	tStart = mysecond();
#pragma moya specialize 
	for (i2=0; i2<matSize; i2++)
	    for (j2=0; j2<matSize; j2++) {
		for (k2=0; k2<matSize; k2++)
		    matC[ind(i2,j2)] += matA[ind(i2,k2)] * matB[ind(k2,j2)];
	    }
// endlooptransformation
	tEnd = mysecond();
	t = tEnd - tStart;
	dummy(matA, matB, matC);
	if (t < tLoop) tLoop = t;
	if (matC[ind(0,0)] < 0) {
	    fprintf(stderr, "Failed matC sign test\n");
	}
    }

    /* Note that explicit formats are used to limit the number of
       significant digits printed (at most this many digits are significant) */
    printf("Matrix size = %d\n", matSize);
    printf("Time        = %.2e secs\n", tLoop);
    rate = (2.0 * matSize) * matSize * (matSize / tLoop);
    printf("Rate        = %.2e MFLOP/s\n", rate * 1.0e-6);

    return 0;
}
