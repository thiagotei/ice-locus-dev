double matA[100 * 100];
double matB[100 * 100];
double matC[100 * 100];
double mysecond(void);
void dummy(double *a, double *b, double *c);
int main(int argc, char *argv[])
{
  double tStart;
  double tEnd;
  double tLoop;
  double rate;
  double t;
  int i;
  int j;
  int k;
  int tests;
  #pragma @ICE block=rep
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
  {
    matA[(i * 100) + j] = 1.0 + i;
    matB[(i * 100) + j] = 1.0 + j;
    matC[(i * 100) + j] = 0.0;
  }


  #pragma @ICE endblock
  tLoop = 1.0e10;
  for (tests = 0; tests < maxTest; tests++)
  {
    tStart = mysecond();
    #pragma @ICE loop=matmul
    #pragma rose_uiuc 10 4
    for (i = 0; i < 100; i++)
    {
      #pragma parallel for 1
      for (j = 0; j < 100; j++)
      {
        #pragma parallel for 2
        for (k = 0; k < 100; k++)
          matC[(i * 100) + j] += matA[(i * 100) + k] * matB[(k * 100) + j];

      }

    }

    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;
  }

  return 0;
}

