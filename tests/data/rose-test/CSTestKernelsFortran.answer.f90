!
! @file Kernels for testing
!
! @ICE block=simpleReplaceFortran
SUBROUTINE TESTKERNEL(result)
INTEGER :: result
result = 0
WRITE (*) 'Hello PlasCom2 World.'
END SUBROUTINE TESTKERNEL

! @ICE endblock
! 
! 
SUBROUTINE ICEUNROLLFORTRANTESTKERNEL(result)
INTEGER :: result
INTEGER :: iBegin, iEnd, jBegin, jEnd, kBegin, kEnd
INTEGER :: iter_count_1
INTEGER :: fringe_2
iBegin = 0
iEnd = 20
jBegin = 0
jEnd = 10
kBegin = 0
kEnd = 5
result = 0
! @ICE loop=unrollTestFortran
DO i = iBegin, iEnd
DO j = jBegin, jEnd
!iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;
IF (MOD(kEnd + 1 - kBegin,1) .EQ. 0) THEN
iter_count_1 = (kEnd + 1 - kBegin) / 1
ELSE
iter_count_1 = (kEnd + 1 - kBegin) / 1 + 1
END IF
!fringe = iter_count%unroll_factor==0 ? 0:unroll_factor*step
IF (MOD(iter_count_1,3) .EQ. 0) THEN
fringe_2 = 0
ELSE
fringe_2 = 3
END IF
DO k = kBegin, kEnd - fringe_2, 1 * 3
result = result + 1
result = result + 1
result = result + 1
END DO
DO k = k, kEnd, 1
result = result + 1
END DO
END DO
END DO
! @ICE endloop
END SUBROUTINE ICEUNROLLFORTRANTESTKERNEL

! 
! 
SUBROUTINE ICEINTERCHANGEFORTRANTESTKERNEL(result)
INTEGER :: result
INTEGER :: iBegin, iEnd, jBegin, jEnd, kBegin, kEnd
iBegin = 0
iEnd = 20
jBegin = 0
jEnd = 10
kBegin = 0
kEnd = 5
result = 0
! @ICE loop=interchangeTestFortran
DO j = jBegin, jEnd
DO k = kBegin, kEnd
DO i = iBegin, iEnd
result = result + 1
END DO
END DO
END DO
! @ICE endloop
END SUBROUTINE ICEINTERCHANGEFORTRANTESTKERNEL

! 
! 
SUBROUTINE ICETILEFORTRANTESTKERNEL(result)
INTEGER :: result
INTEGER :: iBegin, iEnd, jBegin, jEnd, kBegin, kEnd
INTEGER :: iter_count_1
INTEGER :: fringe_2
iBegin = 0
iEnd = 20
jBegin = 0
jEnd = 10
kBegin = 0
kEnd = 5
result = 0
! @ICE loop=tileTestFortran
DO i = iBegin, iEnd
DO j = jBegin, jEnd
!iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;
IF (MOD(kEnd + 1 - kBegin,1) .EQ. 0) THEN
iter_count_1 = (kEnd + 1 - kBegin) / 1
ELSE
iter_count_1 = (kEnd + 1 - kBegin) / 1 + 1
END IF
!fringe = iter_count%unroll_factor==0 ? 0:unroll_factor*step
IF (MOD(iter_count_1,3) .EQ. 0) THEN
fringe_2 = 0
ELSE
fringe_2 = 3
END IF
DO k = kBegin, kEnd - fringe_2, 1 * 3
result = result + 1
result = result + 1
result = result + 1
END DO
DO k = k, kEnd, 1
result = result + 1
END DO
END DO
END DO
! @ICE endloop
END SUBROUTINE ICETILEFORTRANTESTKERNEL

! 
! 
SUBROUTINE ICESTRIPMINEFORTRANTESTKERNEL(result)
INTEGER :: result
INTEGER :: iBegin, iEnd, jBegin, jEnd, kBegin, kEnd
INTEGER :: lt_var_k_1
iBegin = 0
iEnd = 20
jBegin = 0
jEnd = 10
kBegin = 0
kEnd = 5
result = 0
! @ICE loop=stripMineTestFortran
DO i = iBegin, iEnd
DO j = jBegin, jEnd
DO lt_var_k_1 = FLOOR(REAL(kBegin - 0) / 3) * 3 + 0, FLOOR(REAL(kEnd - 0) / 3) * 3 + 0, 3
DO k = MAX(lt_var_k_1,kBegin), MIN(kEnd,lt_var_k_1 + 3 - 1), 1
result = result + 1
END DO
END DO
END DO
END DO
! @ICE endloop
END SUBROUTINE ICESTRIPMINEFORTRANTESTKERNEL

