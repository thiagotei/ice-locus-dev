#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
double A[2048][2048];
double B[2048][2048];
double C[2048][2048];
void init_array()
{
  int i;
  int j;
  for (i = 0; i < 2048; i++)
  {
    for (j = 0; j < 2048; j++)
    {
      A[i][j] = i + j;
      B[i][j] = (double) (i * j);
      C[i][j] = 0.0;
    }

  }

}

void print_array()
{
  int i;
  int j;
  for (i = 0; i < 2048; i++)
  {
    for (j = 0; j < 2048; j++)
    {
      fprintf(stderr, "%lf ", C[i][j]);
      if ((j % 80) == 79)
        fprintf(stderr, "\n");

    }

    fprintf(stderr, "\n");
  }

}

double rtclock()
{
  struct timeval Tp;
  int stat;
  stat = gettimeofday(&Tp, 0);
  if (stat != 0)
    printf("Error return from gettimeofday: %d", stat);

  return Tp.tv_sec + (Tp.tv_usec * 1.0e-6);
}

double t_start;
double t_end;
int main()
{
  int i;
  int j;
  int k;
  init_array();
  t_start = rtclock();
  ;
  #pragma @ICE loop=matmul
  for (i = 0; i < 2048; i++)
    for (k = 0; k < 2048; k++)
  {
    for (j = 0; j <= 2047; j += 4)
    {
      C[i][j] = (1 * C[i][j]) + ((1 * A[i][k]) * B[k][j]);
      C[i][j + 1] = (((double) 1) * C[i][j + 1]) + ((((double) 1) * A[i][k]) * B[k][j + 1]);
      C[i][j + 2] = (((double) 1) * C[i][j + 2]) + ((((double) 1) * A[i][k]) * B[k][j + 2]);
      C[i][j + 3] = (((double) 1) * C[i][j + 3]) + ((((double) 1) * A[i][k]) * B[k][j + 3]);
    }

  }


  t_end = rtclock();
  ;
  printf("Matrix size = %d | Time = %7.5lf ms\n", 2048, (t_end - t_start) * 1.0e3);
  ;
  if (fopen(".test", "r"))
  {
    print_array();
  }

  return 0;
}

