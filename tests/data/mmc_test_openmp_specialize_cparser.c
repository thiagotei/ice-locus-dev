#include <stdio.h>
#include <stdlib.h>
double matA[100 * 100];
double matB[100 * 100];
double matC[100 * 100];
double mysecond(void);
void dummy(double *a, double *b, double *c);
int main(int argc, char *argv[])
{
  double tStart;
  double tEnd;
  double tLoop;
  double rate;
  double t;
  int i;
  int j;
  int k;
  int tests;
  #pragma @ICE block=rep
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
  {
    matA[(i * 100) + j] = 1.0 + i;
    matB[(i * 100) + j] = 1.0 + j;
    matC[(i * 100) + j] = 0.0;
  }


  #pragma @ICE endblock
  tLoop = 1.0e10;
  for (tests = 0; tests < 100; tests++)
  {
    tStart = mysecond();
    #pragma omp parallel for schedule(static,16)
    for (i = 0; i < 100; i++)
    {
      #pragma omp parallel for schedule(dynamic,32)
      for (j = 0; j < 100; j++)
      {
        for (k = 0; k < 100; k++)
          matC[(i * 100) + j] += matA[(i * 100) + k] * matB[(k * 100) + j];

      }

    }

    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;
    if (matC[(0 * 100) + 0] < 0)
    {
      fprintf(stderr, "Failed matC sign test\n");
    }
  }

  int i2;
  int j2;
  int k2;
  int tests2;
  for (tests2 = 0; tests2 < 100; tests2++)
  {
    tStart = mysecond();
    #pragma omp parallel for schedule(static,16)
    for (i2 = 0; i2 < 100; i2++)
    {
      #pragma omp parallel for schedule(dynamic,32)
      for (j2 = 0; j2 < 100; j2++)
      {
        for (k2 = 0; k2 < 100; k2++)
          matC[(i2 * 100) + j2] += matA[(i2 * 100) + k2] * matB[(k2 * 100) + j2];

      }

    }

    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;
    if (matC[(0 * 100) + 0] < 0)
    {
      fprintf(stderr, "Failed matC sign test\n");
    }
  }

  printf("Matrix size = %d\n", 100);
  printf("Time        = %.2e secs\n", tLoop);
  rate = ((2.0 * 100) * 100) * (100 / tLoop);
  printf("Rate        = %.2e MFLOP/s\n", rate * 1.0e-6);
  return 0;
}

