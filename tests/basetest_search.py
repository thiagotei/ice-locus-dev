'''

@author: thiago
'''

from basetest import BaseTest

class BaseTestSearch(BaseTest):
    pass
####

locuscode1=r'''
CodeReg test {
    b = 100 // 4;
    f1 = integer(2 .. 10 .. 1);
    {
        f2 = poweroftwo(2 .. 16);
        perm = permutation([20,21,22]);
        a = 10 + 5;
    } OR {
        a = 20 + 3;
        f3 = enum(10, 20, 30);
        *Unroll();
    }
    res = 100 * 3;
}
'''
