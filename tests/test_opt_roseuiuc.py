'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import argparse
import re, unittest, sys, logging, itertools, os, shutil
import glob

from locus.ice import ICE, argparser as iceargparser, \
init_logging as iceinit_logging, createWorkingDir


from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser
from locus.util.constants import ICE_TOOL_PATH_CMPOPTS, ICE_OK

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

log = logging.getLogger(__name__)

argparser = argparse.ArgumentParser(add_help=True,
        parents=[iceargparser,
        optlangargparser])

class Test_opt_roseuiuc(unittest.TestCase):

    def basecall(self, srccodepath, locuspath):
        tool_dir = os.getenv(ICE_TOOL_PATH_CMPOPTS, None)
        if tool_dir is None:
            #raise OptExecError(f"{ICE_TOOL_PATH_CMPOPTS} not defined!")
            raise RuntimeError(f"{ICE_TOOL_PATH_CMPOPTS} not defined!")

        args = argparser.parse_args(
                ['-t',locuspath,'-f',srccodepath,'-o','suffix','-u','.ice'])
                #['-t',locuspath,'-f',srccodepath,'-o','suffix','-u','.ice','--debug'])
                #['-t',locuspath,'-f',srccodepath,'--debug'])
        #dirpath = iceinit_logging()
        dirpath = createWorkingDir()
        logging.basicConfig(level=logging.CRITICAL)
        if args.debug:
            logging.root.setLevel(logging.DEBUG)
        log.setLevel(logging.DEBUG)
        #    print "Main running"
        #log.debug("Main running!")
        args.rundirpath = dirpath
        lp = LocusParser(args.optfile, args.debug)
        ice_inst = ICE(lp, None, args)
        iceret = ice_inst.run()
        ret = False
        if iceret == ICE_OK:
            ret = True

        shutil.rmtree(dirpath)
        return ret

    def check_ld_lib_path(self):
        ld_values = os.getenv("LD_LIBRARY_PATH", None)
        #print "ld_values", ld_values
        if ld_values is None or 'jre' not in ld_values:
            #raise RuntimeError("LD_LIBRARY_PATH does not have jre path!")
            found = False
        else:
            found = True
        return found

    def open_result(self, respath):
        codeRes = None
        try:
            f = open(respath, 'r')
        except IOError as err:
            ldfound = self.check_ld_lib_path()
            if not ldfound:
                raise RuntimeError("Could not find ICE result! It may be LD_LIBRARY_PATH that does not have jre path or Rose does not have the Fortran frontend!")
            else:
                raise RuntimeError("Could not find ICE result! It may be Rose does not have the Fortran frontend! Check the jre path on LD_LIBRARY_PATH.")
        else:
            with f:
                codeRes = f.read()
            #endwith
        #endif
        return codeRes
    #endif

    def test_cxx(self):
        ''' '''
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        filename = '/data/rose-test/CSTestKernelsCXX'
        ext = '.cpp'
        fileinput = dir_path+filename+ext
        optinput = dir_path+'/data/rose-test/tuneTestingRose.opt'
        #print "Eita!!!"
        retrun = self.basecall(fileinput, optinput)

        if not retrun:
            self.fail("System could not finish properly.")

        with open(dir_path+filename+'.answer'+ext, 'r') as f:
            codeAnswer = f.read()

        respath = dir_path+filename+'.ice'+ext
        codeRes = self.open_result(respath)

        self.assertEqual(codeRes, codeAnswer)

        if os.path.isfile(respath):
            os.remove(respath)

        tifiles = glob.glob("tmp*.ti")
        if tifiles:
            for dotti in tifiles:
                os.remove(dotti)
        #   #
    #endif

    def test_fortran(self):
        ''' '''
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        filename = '/data/rose-test/CSTestKernelsFortran'
        ext = '.f90'
        fileinput = dir_path+filename+ext
        optinput = dir_path+'/data/rose-test/tuneTestingRose.opt'
        #print "Eita!!!"
        retrun = self.basecall(fileinput, optinput)

        if not retrun:
            self.fail("System could not finish properly.")

        with open(dir_path+filename+'.answer'+ext, 'r') as f:
            codeAnswer = f.read()

        respath = dir_path+filename+'.ice'+ext
        codeRes = self.open_result(respath)

        self.assertEqual(codeRes, codeAnswer)

        if os.path.isfile(respath):
            os.remove(respath)
    #endif

    def test_c(self):
        ''' '''
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        filename = '/data/rose-test/matmul'
        ext = '.c'
        fileinput = dir_path+filename+ext
        optinput = dir_path+'/data/rose-test/matmul.locus'
        retrun = self.basecall(fileinput, optinput)

        if not retrun:
            self.fail("System could not finish properly.")

        with open(dir_path+filename+'.answer'+ext, 'r') as f:
            codeAnswer = f.read()

        respath = dir_path+filename+'.ice'+ext
        codeRes = self.open_result(respath)

        self.assertEqual(codeRes, codeAnswer)

        if os.path.isfile(respath):
            os.remove(respath)
    #endif

if __name__ == '__main__':
    unittest.main()

