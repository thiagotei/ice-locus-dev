#!/usr/bin/env python
'''
@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import sys, unittest, logging

sys.path[0:0] = ['.', '..']

log = logging.getLogger()
log.setLevel(logging.WARNING)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(funcName)s %(message)s -- %(lineno)d')
ch.setFormatter(formatter)
log.addHandler(ch)


suite = unittest.TestLoader().loadTestsFromNames(
        ['test_cparser_sourcecode',
        'test_regex_sourcecode',
        'test_opt_moya',
        'test_opt_openmp',
        'test_locus_parser',
        'test_locus_cfg',
        'test_search_exhaustive'
#        'test_search_choco'
        ]
)

testresult = unittest.TextTestRunner(verbosity=1).run(suite)
sys.exit(0 if testresult.wasSuccessful() else 1)

