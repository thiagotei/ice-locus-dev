'''

@author: thiago
'''

import re, unittest, sys, logging, itertools, os

#import test_locus_parser as tlo

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.locus.locuscfg import LocusCFG
from locus.frontend.optlang.locus.dataflowopts import ReachingDefs,\
        ConstantProp, ConstantFold, DeadCodeElim, combinedCfgAstOpts
from locus.backend.interpreter import interpreter, topblkinterpreter
#from locus.util.misc import Capturing
#import locus.frontend.optlang.locus.locusast as modlocusast
import locus.frontend.optlang.locus.optimizer as optimizer
from locus.backend.genlocus import GenLocus

log = logging.getLogger(__name__)

class Test_locus_cfg(unittest.TestCase):

    def basecall(self, lvl=logging.CRITICAL):
        logging.basicConfig(level=logging.CRITICAL)
        logging.root.setLevel(lvl)
        log.setLevel(lvl)
    ###

    def test_ast2cfg(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex, False)
        locustree.createscope()
        ent = locustree['fftw']
        #ent.show()
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.write(root)
        #LocusCFG.drawcfg(root, "locusfftwex")

        locustree = lp.parse(locusfftwex1, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex1")
        #LocusCFG.write(root)
        self.assertTrue(True)
    ###

    def test_ast2cfg_B(self):
        #self.basecall(logging.DEBUG)
        self.basecall()
        lp = LocusParser()
        locustree = lp.parse(locusfftwex2, False)
        locustree.createscope()
        ent = locustree['fftw']
        #ent.show()
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.write(root)
        #LocusCFG.drawcfg(root, "test ast2cfg B")
        self.assertTrue(True)
    ###

    def test_reachdef(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwexDBG")
        #LocusCFG.write(root)
        ReachingDefs.exec(root)
        #ReachingDefs.write(root)

        #
        # Compare the reaching definitions of the 1st instruction of the last node 
        # before the exit node
        #
        bbdict = LocusCFG.dictfycfg(root)
        answerND13 = {x.unqid for x in bbdict[18].stmtsIN[0]}
        #print(answerND13)
        correctND13 = {3, 9, 25, 66, 20, 36, 41, 12, 63, 6, 46, 31, 58, 52}
        self.assertEqual(correctND13, answerND13)

        answer = {ndid: {x.unqid for x, y in bbdict[ndid].edges} for ndid in bbdict}
        correct = {0: {1}, 1: {2}, 2: {3}, 3: {25, 4}, 25: {26}, 4: {5}, 26: set(), 
                    5: {6}, 6: {10, 7}, 7: {8}, 10: {11}, 8: {9}, 11: {24, 12}, 9: {10}, 
                    12: {13}, 24: {25}, 13: {14}, 14: {18, 15}, 18: {19}, 15: {16}, 
                    19: {20, 23}, 16: {17}, 23: {24}, 20: {21}, 17: {18}, 21: {22}, 22: {23}}
        #print(answer)
        self.assertEqual(correct, answer)
    ###

    def test_constprop(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex1, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex1 BEF")
        #LocusCFG.write(root)

        ReachingDefs.exec(root)
        #ReachingDefs.write(root)

        props = ConstantProp.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex1 AFT")
        #print("props ",props)
        self.assertEqual(props, 8)

        locustree = lp.parse(locusfftwex, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex BEF")
        #LocusCFG.write(root)

        ReachingDefs.exec(root)
        #ReachingDefs.write(root)

        props = ConstantProp.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex AFT")
        #print("props ",props)
        self.assertEqual(props, 7)
    ###

    def test_constfold(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex1, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex")
        #LocusCFG.write(root)

        ReachingDefs.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex1 start")
        #ReachingDefs.write(root)

        props = ConstantProp.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex1 constprop")
        self.assertEqual(props, 8)

        folds = ConstantFold.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex1 constprop constfold")
        self.assertEqual(folds, 10)

        lp = LocusParser()
        locustree = lp.parse(locusfftwex, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex")
        #LocusCFG.write(root)

        ReachingDefs.exec(root)
        #ReachingDefs.write(root)
        #LocusCFG.drawcfg(root, "locusfftwex start")

        props = ConstantProp.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex constprop")
        self.assertEqual(props, 7)

        folds = ConstantFold.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex constprop constfold")
        self.assertEqual(folds, 5)
    ###

    def test_deadcode_A(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex")
        #LocusCFG.write(root)

        ReachingDefs.exec(root)
        #ReachingDefs.write(root)
        #LocusCFG.drawcfg(root, "locusfftwex start")

        props = ConstantProp.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex constprop")
        self.assertEqual(props, 7)

        folds = ConstantFold.exec(root)
        #LocusCFG.drawcfg(root, "test_deadcode locusfftwex_cp_cf")
        self.assertEqual(folds, 5)

        deaths = DeadCodeElim.exec(root)
        #LocusCFG.drawcfg(root, "test_deadcode locusfftwex_cp_cf_dce")
        self.assertEqual(deaths, 2)
    ###

    def test_deadcode_B(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex1, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "locusfftwex")
        #LocusCFG.write(root)

        ReachingDefs.exec(root)
        #ReachingDefs.write(root)
        #LocusCFG.drawcfg(root, "locusfftwex start")

        props = ConstantProp.exec(root)
        #LocusCFG.drawcfg(root, "locusfftwex constprop")
        self.assertEqual(props, 8)

        folds = ConstantFold.exec(root)
        #LocusCFG.drawcfg(root, "test_deadcodeB locusfftwex1_cp_cf")
        self.assertEqual(folds, 10)

        deaths = DeadCodeElim.exec(root)
        #LocusCFG.drawcfg(root, "test_deadcodeB locusfftwex1_cp_cf_dce")
        self.assertEqual(deaths, 2)
    ###

    def test_multiopts(self):
        lp = LocusParser()
        locustree = lp.parse(locusfftwex, False)
        ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "orig_test_multiopts locusfftwex_cp_cf_dce")
        #LocusCFG.write(root)

        rescheck = [[7,5,2],[4,3,2],[3,4,1],[0,0,0]]
        res = []

        it = -1
        while True:
            it += 1
            ReachingDefs.exec(root)
            props = ConstantProp.exec(root)
            folds = ConstantFold.exec(root)
            deaths = DeadCodeElim.exec(root)

            #LocusCFG.drawcfg(root, str(it)+"_test_multiopts locusfftwex_cp_cf_dce")

            res.append([props, folds, deaths])
            #print("props: ", props, "folds: ", folds, "deaths: ", deaths)

            if props == 0 and folds == 0 and deaths == 0:
                break
            #
        ##
        # check results for each iteration
        self.assertEqual(res, rescheck)

        # check number of iterations
        self.assertEqual(it, 3)
    ###

    def test_orblk1(self):
        self.basecall()
        lp = LocusParser()
        locustree = lp.parse(locusORblk1, False)
        ent = locustree['ortst']
        #locustree = lp.parse(locusfftwex1, False)
        #ent = locustree['fftw']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "test_orblk1 locus")
        #LocusCFG.write(root)
    ###

    def test_orblk_reachdef(self):
        lp = LocusParser()
        locustree = lp.parse(locusORblk1, False)
        ent = locustree['ortst']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "test_orblk_reachdef locus")
        #LocusCFG.write(root)
        ReachingDefs.exec(root)
        #ReachingDefs.write(root)

        #
        # Compare the reaching definitions of the 1st instruction of the last node 
        # before the exit node
        #
        bbdict = LocusCFG.dictfycfg(root)
        answerND16 = {x.unqid for x in bbdict[29].stmtsIN[0]}
        #print(answerND16)
        correctND16 = {82, 74, 62, 68, 79, 3, 6, 57}
        self.assertEqual(correctND16, answerND16)

        answer = {ndid: {x.unqid for x, y in bbdict[ndid].edges} for ndid in bbdict}
        correct = {0: {1}, 1: {2}, 2: {3}, 3: {43, 20, 4}, 20: {21}, 4: {5}, 43: {44}, 
                21: {22}, 5: {6}, 44: {45}, 22: {35, 23}, 6: {19, 7}, 45: {58, 46}, 35: {36, 39}, 
                23: {24}, 7: {8}, 19: {62}, 58: {59}, 46: {47}, 36: {37}, 39: {40}, 24: {25}, 
                8: {9}, 62: {63}, 59: {60}, 47: {48}, 37: {38}, 40: {41}, 25: {26, 29}, 
                9: {10, 13}, 63: {64, 67}, 60: {61}, 48: {49, 52}, 38: {42}, 41: {42}, 29: {30}, 
                26: {27}, 10: {11}, 13: {14}, 67: {68}, 64: {65}, 61: {62}, 49: {50}, 52: {53}, 42: {62}, 
                30: {34, 31}, 27: {28}, 11: {12}, 14: {18, 15}, 68: {69}, 65: {66}, 50: {51}, 
                53: {57, 54}, 34: {42}, 31: {32}, 28: {29}, 12: {13}, 18: {19}, 15: {16}, 69: {70}, 
                66: {70}, 51: {52}, 54: {55}, 57: {61}, 32: {33}, 16: {17}, 70: {71}, 55: {56}, 
                33: {34}, 17: {18}, 71: set(), 56: {57}}
        self.assertEqual(correct, answer)
    ###

    def test_orblk_multiopts(self):
        lp = LocusParser()
        locustree = lp.parse(locusORblk1, False)
        ent = locustree['ortst']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "orig_test_or_multiopts cp_cf_dce")
        #LocusCFG.write(root)

        bbdict = LocusCFG.dictfycfg(root)
        answer = {ndid: {x.unqid for x, y in bbdict[ndid].edges} for ndid in bbdict}
        correct = {0: {1}, 1: {2}, 2: {3}, 3: {43, 4, 20}, 4: {5}, 20: {21}, 43: {44}, 
                5: {6}, 21: {22}, 44: {45}, 6: {19, 7}, 22: {35, 23}, 45: {58, 46}, 19: {62}, 
                7: {8}, 35: {36, 39}, 23: {24}, 58: {59}, 46: {47}, 62: {63}, 8: {9}, 39: {40}, 
                36: {37}, 24: {25}, 59: {60}, 47: {48}, 63: {64, 67}, 9: {10, 13}, 40: {41}, 
                37: {38}, 25: {26, 29}, 60: {61}, 48: {49, 52}, 64: {65}, 67: {68}, 10: {11}, 
                13: {14}, 41: {42}, 38: {42}, 26: {27}, 29: {30}, 61: {62}, 52: {53}, 49: {50}, 
                65: {66}, 68: {69}, 11: {12}, 14: {18, 15}, 42: {62}, 27: {28}, 30: {34, 31}, 
                53: {57, 54}, 50: {51}, 66: {70}, 69: {70}, 12: {13}, 18: {19}, 15: {16}, 28: {29}, 
                34: {42}, 31: {32}, 57: {61}, 54: {55}, 51: {52}, 70: {71}, 16: {17}, 32: {33}, 
                55: {56}, 71: set(), 17: {18}, 33: {34}, 56: {57}}
        #print(answer)
        self.assertEqual(correct, answer)

        #rescheck = [[7,5,2],[4,3,2],[3,4,1],[0,0,0]]
        rescheck = [[10,4,1],[7,7,7],[3,3,3],[0,0,0]]
        res = []

        it = -1
        while True:
            it += 1
            ReachingDefs.exec(root)
            props = ConstantProp.exec(root)
            folds = ConstantFold.exec(root)
            deaths = DeadCodeElim.exec(root)

            #LocusCFG.drawcfg(root, str(it)+"_test_or_multiopts cp_cf_dce")

            res.append([props, folds, deaths])
            #print("it: ",it, "props: ", props, "folds: ", folds, "deaths: ", deaths)

            if props == 0 and folds == 0 and deaths == 0:
                ReachingDefs.exec(root)
                break
            #
        ##
        # check results for each iteration
        self.assertEqual(res, rescheck)

        # check number of iterations
        self.assertEqual(it, len(rescheck)-1)

        #LocusCFG.drawcfg(root, "final_test_or_multiopts cp_cf_dce")
        bbdict = LocusCFG.dictfycfg(root)
        #ReachingDefs.write(root)
        answerND8 = {x.unqid for x in bbdict[13].stmtsIN[0]}
        #print(answerND8)
        correctND8 = {31, 34, 14, 26, 6, 9, 3}
        self.assertEqual(correctND8, answerND8)

        answer = {ndid: {x.unqid for x, y in bbdict[ndid].edges} for ndid in bbdict}
        correct = {0: {1}, 1: {2}, 2: {3}, 3: {43, 20, 4}, 20: {21}, 43: {44}, 4: {5}, 
                    21: {23}, 44: {46}, 5: {7}, 23: {24}, 46: {47}, 7: {8}, 24: {29}, 
                    47: {49}, 8: {10}, 29: {34}, 49: {50}, 10: {11}, 34: {42}, 50: {51}, 
                    11: {12}, 42: {62}, 51: {52}, 12: {13}, 62: {64}, 52: {54}, 13: {15}, 
                    64: {65}, 54: {55}, 15: {16}, 65: {66}, 55: {56}, 16: {17}, 66: {70}, 
                    56: {57}, 17: {18}, 70: {71}, 57: {61}, 18: {19}, 71: set(), 61: {62}, 19: {62}}
        #print(answer)
        self.assertEqual(correct, answer)
    ###

    def test_combineastcfg_transf(self):
        """ Avoid making any structural change on the CFG (e.g., dead code elimination),
            so it still usable in the AST level.
            It avoids the implementation of CFG2AST operatio, which I found it dificult.
            What is necessary on CFG is Reaching Definitions.
            The idea here is to ReachDef, ConstProp, ConstFold on CFG but dead code elimination on
            AST level.
        """
        lp = LocusParser()
        locustree = lp.parse(locusORblk1, False)
        ent = locustree['ortst']
        root = LocusCFG.ast2cfg(ent.body)
        #LocusCFG.drawcfg(root, "orig_combineastcfg cp_cf_dce")
        #GenLocus.searchmode(locustree, "orig_omg.locus")

        rescheck = [[10,4,1],[7,7,7],[3,3,3],[0,0,0]]
        res = []

        it = -1
        while True:
            it += 1
            ReachingDefs.exec(root)
            props = ConstantProp.exec(root)
            folds = ConstantFold.exec(root)
            #LocusCFG.drawcfg(root, str(it)+"_combineastcfg cp_cf_dce")
            dcec = optimizer.DeadCodeEliminationConsts(ent.body)
            deaths = dcec.execute() 

            #GenLocus.searchmode(locustree, str(it)+"_omg.locus")

            res.append([props, folds, deaths])
            #print("it: ",it, "props: ", props, "folds: ", folds, "deaths: ", deaths)

            if props == 0 and folds == 0 and deaths == 0:
                break
            #
            root = LocusCFG.ast2cfg(ent.body)
        ##

        # check results for each iteration
        self.assertEqual(res, rescheck)

        # check number of iterations
        self.assertEqual(it, len(rescheck)-1)
    ###

    def test_combineastcfg_problemA(self):
        #self.basecall(logging.DEBUG)
        correctA = {0: {1}, 1: {2}, 2: {3}, 3: {4}, 4: {28, 5}, 28: {29}, 5: {6}, 
                    29: {30}, 6: {7}, 30: {31}, 7: {8}, 31: {32}, 8: {9}, 32: {33}, 
                    9: {10}, 33: {34}, 10: {11}, 34: {35}, 11: {12}, 35: {36}, 
                    12: {16, 19, 13}, 36: {37}, 13: {14}, 19: {20}, 16: {17}, 37: set(), 
                    14: {15}, 20: {21}, 17: {18}, 15: {22}, 21: {22}, 18: {22}, 22: {23}, 
                    23: {24}, 24: {25}, 25: {26}, 26: {27}, 27: {34}}
        correctB = {0: {1}, 1: {2}, 2: {3}, 3: {4}, 4: {8, 11, 5}, 11: {12}, 8: {9}, 5: {6}, 
                    12: {13}, 9: {10}, 6: {7}, 13: {14}, 10: {14}, 7: {14}, 14: {15}, 
                    15: {16}, 16: {17}, 17: {18}, 18: set()}
        correctC = {0: {1}, 1: {2}, 2: {3}, 3: {4}, 4: {5}, 5: {6}, 6: {7}, 7: {8}, 8: {9}, 9: {10}, 10: set()} 
        inps = [("A", problematicA, correctA), 
                ("B", problematicB, correctB), 
                ("C", problematicC, correctC)]

        for name, pr, correct in inps:
            lp = LocusParser()
            locustree = lp.parse(pr, False)
            ent = locustree['fftw']
            root = LocusCFG.ast2cfg(ent.body)
            bbdict = LocusCFG.dictfycfg(root)
            answer = {ndid: {x.unqid for x, y in bbdict[ndid].edges} for ndid in bbdict}
            #print(answer)
            #LocusCFG.drawcfg(root, "orig_problematic"+name+" cp_cf_dce")
            self.assertEqual(answer, correct)
        #endfor
    ###
#endclass

locusORblk2=r'''
OptSeq ortst() {
    n = 1024;
    {
        print "A Block";
    } OR {
        print "B Block";
    } OR {
        print "C Block";
    }
    print "Final";
}
'''

locusORblk1=r'''
OptSeq ortst() {
    n = 1024;
    threshindir = 256;
    {
        newr = 4;
        newm = n / newr;
        if (newm > 1) {
            indirUsed = False;

            if (newm >= threshindir) {
                id = newid;
                newid = id + "4";
                indirUsed = True;
            }

            print "recdft id: "+id+" newid: "+newid;

            if (indirUsed == True) {
                print "IndirEpilogue Used!";

            }
        }

    } OR {
        newr = 8;
        newm = n / newr;
        if (newm > 1) {
            indirUsed = False;

            if (newm >= threshindir) {
                id = newid;
                newid = id + "8";
                indirUsed = True;
            }

            print "recdft id: "+id+" newid: "+newid;

            if (indirUsed == True) {
                print "IndirEpilogue Used!";

            }
        } elif newm == 0 {
            print "Ahhhh elif newm == 0";
        } else {
            print "Ahhhh else Cammon!";
        }

    } OR {
        newr = 2;
        newm = n / newr;
        if (newm > 1) {
            indirUsed = False;

            if (newm >= threshindir) {
                id = newid;
                newid = id + "2";
                indirUsed = True;
            }

            print "recdft id: "+id+" newid: "+newid;

            if (indirUsed == True) {
                print "IndirEpilogue Used!";
            }
        } else {
            print "First else Eita";
        }
    }
    if n == 1024 {
        print "Finished!!";
    } else {
        print "Also Finished!!";
    }
}
'''

locusfftwex2=r'''
OptSeq fftw() {
    threshindir = 32;
    n = 128;
    id = "t";
    vecrnk = 0;
    if (n > 0) {
        newid = id + "1";
        newvecrnk = vecrnk + 1;
    }
    print "recdft id: "+id+" newid: "+newid;
    if (vecrnk > 0) {
        newid = id + "2";
        newvecrnk = vecrnk + 2;
    } else {
        print "First else Eita";
    }
    if n == 1024 {
        print "Finished!!";
    } else {
        print "Also Finished!!";
    }
}
'''
locusfftwex1=r'''
OptSeq fftw() {
    threshindir = 32;
    n = 128;
    id = "t";
    vecrnk = 0;
    if (n > 0) {
        newid = id + "1";
        newvecrnk = vecrnk + 1;
    }
    print "recdft id: "+id+" newid: "+newid;
    if (vecrnk > 0) {
        newid = id + "2";
        newvecrnk = vecrnk + 2;
    } else {
        print "First else Eita";
    }
    print "final id: "+id+" newid: "+newid;
}
'''

locusfftwex=r'''
OptSeq fftw() {
    threshindir = 32;
    n = 128;
    id = "t";
    vecrnk = 0;
    if (n > 0) {
        newid = id + "1";
        newvecrnk = vecrnk + 1;
        if (vecrnk > 1) {
           id = newid;
           newid = id + "2";
        }

        newr = 4;
        newm = n / newr;
        if (newm > 1) {
            indirUsed = False;

            if (newm >= threshindir) {
                id = newid;
                newid = id + "3";
                indirUsed = True;
            }

            print "recdft id: "+id+" newid: "+newid;

            if (indirUsed == True) {
                print "IndirEpilogue Used!";

            }
        }
    }
}
'''

problematicC=r'''
OptSeq fftw()
{
  CallMethodFftw.ProlDftCt (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
  {
    {
      CallMethodFftw.T3fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
    }
  }
  recdft(16, 4, 64, 2, "_t1", "_t12", 2);
  CallMethodFftw.EpilDftCt (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
}
'''

problematicB=r'''
OptSeq fftw()
{
  CallMethodFftw.ProlDftCt (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
  {
    {
      CallMethodFftw.T1fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
    }
    OR
    {
      CallMethodFftw.T2fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
    }
    OR
    {
      CallMethodFftw.T3fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
    }
  }
  recdft(16, 4, 64, 2, "_t1", "_t12", 2);
  CallMethodFftw.EpilDftCt (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
}
'''

problematicA=r'''
OptSeq fftw()
{
  nsuf = 1;
  id = "_t1";
  parid = "_t";
  vecrnk = 1;
  prevn = 256;
  r = 4;
  n = 64;
  {
    {
      newnum = 2;
      {
	newid = "_t12";
	newvecrnk = 2;
	oldid = "_t1";
	oldparid = "_t";
	newr = 4;
	newm = 16;
	{
	  CallMethodFftw.ProlDftCt (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
	  {
	    {
	      CallMethodFftw.T1fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
	    }
	    OR
	    {
	      CallMethodFftw.T2fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
	    }
	    OR
	    {
	      CallMethodFftw.T3fv_4 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
	    }
	  }
	  recdft(16, 4, 64, 2, "_t1", "_t12", 2);
	  CallMethodFftw.EpilDftCt (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
	}
      }
    }
    OR
    {
      newid = "_t12";
      oldid = "_t1";
      oldparid = "_t";
      {
	CallMethodFftw.N1fv_64 (paridarg="_t", idarg="_t1", childid="_t12", buffer="fftreplacement.txt");
      }
    }
  }
}
'''

if __name__ == "__main__":
    unittest.main()


