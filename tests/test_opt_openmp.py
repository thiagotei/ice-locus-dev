'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import os, re, unittest, sys, logging, itertools

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
from locus.frontend.srccode.regex.sourcecode import SourceCode as SourceCodeRegex
from locus.frontend.srccode.cparser.sourcecode import SourceCode as SourceCodeCparser
from locus.util.constants import ICE_CODEFILE_C
from locus.tools.opts.openmp import OpenMP

log = logging.getLogger(__name__)

class Test_opt_openmp(unittest.TestCase):

    def test_openmp_for_regex(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc_regex.c'
        sr = SourceCodeRegex(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        openmp_sp = OpenMP.OMPFor()
        openmp_sp.apply(sr, [], {'loop':'0','schedule':'static', 'chunk':16}, 'matmul', None, 0, 0)

        buf = sr.unparseandwrite(lambda x: x)
        with open(dir_path+'/data/mmc_test_openmp_specialize_regex.c', 'r') as f:
            codeAnswer1 = f.read()

        self.assertEqual(codeAnswer1, buf)

    def test_openmp_parallel_regex(self):
        pass

    def test_openmp_for_cparser(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc.c'
        sr = SourceCodeCparser(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        openmp_sp = OpenMP.OMPFor()
        openmp_sp.apply(sr, [], {'loop':'0','schedule':'static', 'chunk':16}, 'matmul', None, 0, 0)
        openmp_sp.apply(sr, [], {'loop':'0.0','schedule':'dynamic', 'chunk':32}, 'matmul', None, 0, 0)

        buf = sr.unparseandwrite(lambda x: x)
        with open(dir_path+'/data/mmc_test_openmp_specialize_cparser.c', 'r') as f:
            codeAnswer1 = f.read()

        self.assertEqual(codeAnswer1, buf)

if __name__ == '__main__':
    unittest.main()

