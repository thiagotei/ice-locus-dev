import re

def getTimingMatMul(std, error):
    #print std
    pattern = re.compile(r"^ min\s+max.*")
    r = None
    lines_iter = iter(std.splitlines())
    for l in lines_iter:
        #print "Testing",l
        #if l.startswith("\tmin\tmax"):
        mat = pattern.match(l)
        #print mat
        if mat:
            #print "Found!", mat
            r = next(lines_iter)
            break

    result = 'N/A'
    if r:
        m = re.split('\s*', r)
        #print m
        if m and len(m) >= 3:
            result = float(m[2])
    return result


