#include <psycho_4.c_psycho_4_init_line210_1.ptrderefout.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <openssl/md5.h>
#include <psycho_4.c_psycho_4_init_line210_1.h>
#include <math.h>
#include <common.h>
#include <timer.h>
#ifdef DYNAMIC_COUNTERS 
#include <msrapi.h> 
#endif
extern void* getCycleArray(unsigned long long heapSize, int iterations);
extern void* deleteCycleArray(void *cycleArray);
extern uint64_t findMedian(uint64_t *cycleArray, int size);
extern void standardDeviation(uint64_t *cycleArray, int size, uint64_t *mean, uint64_t *stdv);
extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length, int iterations);
extern void* saveCurrentStackToHeap(void* curSP, void* preBottom, long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);
extern void loop();
extern void* dummy(int num, ...);
int i;
double * restrict window;
int main(int argc, char** argv)
{
    int inputIndices[10];
    FILE* pFile_;
    int iterations = 1;
    uint64_t *cycleArray;
    void *oldBrk;
    static char* inputDataFileName[] = {"psycho_4.c_psycho_4_init_line210_1.ptrderefout.hd", "psycho_4.c_psycho_4_init_line210_1.ptrderefout.st", "psycho_4.c_psycho_4_init_line210_1.ptrderefout.gl"};
    static char heapDataFile[256] = {0};
    static char stackDataFile[256] = {0};
    static char globalDataFile[256] = {0};
    static uint64_t tsc_start = 0, tsc_end, loop_cycles = 0;
    static uint64_t min_tsc_cycles = LLONG_MAX, max_tsc_cycles = 0;
    static uint64_t mean_tsc_cycles = 0, median_tsc_cycles, stdv_tsc_cycles;
    if (argc < 3)
    {
        printf("Usage: %s path_to_data_files iterations\n", argv[0]);
        exit(1);
    }
    sprintf (heapDataFile, "%s/%s", argv[1], inputDataFileName[0]);
    sprintf (stackDataFile, "%s/%s", argv[1], inputDataFileName[1]);
    sprintf (globalDataFile, "%s/%s", argv[1], inputDataFileName[2]);
    iterations = atoi(argv[2]);
    for (int it = 0; it < iterations; it++)
    {
        window = (double *) window_;
        if (heapChunkSize > 0)
        {
            if (it < 1)
                oldBrk = mallocMemoryChunk(heapDataFile, minimumHeapAddress, maximumHeapAddress, heapChunkSize);
            else
                resetHeapData(minimumHeapAddress, maximumHeapAddress, heapChunkSize);
        }
        if (it < 1)
            cycleArray = getCycleArray(heapChunkSize, iterations);
        static long long stack_length = 0;
        register void *preRSP __asm__ ("sp");
        register void *rsp;
        void *storedTo;
        if (stackChunkSize > 0)
        {
            rsp = (void *) benStackPtrAddress;
            storedTo = saveCurrentStackToHeap(preRSP, stackBottomAddress, &stack_length);
            char* buffer = loadBenchStackToCurrentStack(stackDataFile, benStackPtrAddress, stackChunkSize);
            memcpy(rsp, buffer, stackChunkSize);
        }

#ifdef DYNAMIC_COUNTERS 
				startMonitoring(it); 
#endif
        tsc_start = rdtsc();

#if defined(ITERATIONS)
        for (int it = 0; it < ITERATIONS; it++)
        {
#endif
            loop();
#if defined(ITERATIONS)
        }
#endif

        tsc_end = rdtsc();
#ifdef DYNAMIC_COUNTERS 
				stopMonitoring(it); 
#endif
        loop_cycles = tsc_end - tsc_start;
        cycleArray[it] = loop_cycles;
        if (it > 0 && loop_cycles < min_tsc_cycles)
            min_tsc_cycles = loop_cycles;
        if (it > 0 && loop_cycles > max_tsc_cycles)
            max_tsc_cycles = loop_cycles;
        if (stackChunkSize > 0)
        {
            if (stack_length > 0)
            {
                void *storeSpValAddr = getSavedAddress(storedTo);
                memcpy(preRSP, storeSpValAddr, stack_length);
                restoreCurrentStack(storedTo, stack_length);
            }
        }
    }
    standardDeviation(cycleArray, iterations, &mean_tsc_cycles, &stdv_tsc_cycles);
    median_tsc_cycles = findMedian(cycleArray, iterations);
    if (heapChunkSize > 0)
        deleteMemoryMapping(oldBrk, maximumHeapAddress, heapChunkSize, iterations);
    else
        deleteCycleArray(cycleArray);
    printf("\n");
    printf("Scalar i: %d\n", i);
    void* ptrCheckSumVal = dummy(1, window);
    printf("The checksum of the addresses of pointer variables is: %p \n", ptrCheckSumVal);
    printf("Simulation time (cycles) stats:\n min\tmax\tmean\tmedian\tstdv\n");
    printf("%"PRIu64"\t%"PRIu64"\t%"PRIu64"\t%"PRIu64"\t%"PRIu64"\t\n", min_tsc_cycles, max_tsc_cycles, mean_tsc_cycles, median_tsc_cycles, stdv_tsc_cycles);
#ifdef DYNAMIC_COUNTERS 
		printPcuData(iterations); 
#endif
    return 0;
}
