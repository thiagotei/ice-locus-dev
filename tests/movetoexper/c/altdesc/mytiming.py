
import re 

def getTiming(std, error):
    r = re.search('.*real.*', error)
    m = []
    if r:
        m = re.split('\s*',r.group())
    result = 'N/A'
    if m and len(m) >= 2:
       result = float(m[1])

    return result


