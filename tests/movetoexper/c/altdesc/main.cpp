#include <iostream>

int main(int argc, char* argv[]){

  int ans=0;

#pragma @ICE block=Loop
  for(int i=0; i < 10000; i++){
    ans+=i;  
  }
#pragma @ICE endblock

  std::cout << "answer = " << ans << std::endl;

  return 0;
}
