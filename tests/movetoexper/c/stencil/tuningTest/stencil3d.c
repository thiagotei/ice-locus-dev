#include <stdio.h>

#define ADDR(f,s,t,h,l) ((f*h*l)+(s*l)+t)

void stencil3d(double *A, double *B, 
                const int niter, 
                const size_t x, const size_t y, const size_t z, 
                const double C0, const double C1) {
    int t, i, j, k = 0;

#pragma @ICE loop=stencil
    for (t = 0; t < niter; t++) {
        for(i = 1; i < x-1; i++ ) {
            for(j = 1; j < y-1; j++) {
                for(k = 1; k < z-1; k++) {
                    B[ADDR(i,j,k,y,z)] = C0 * A[ADDR(i,j,k,y,z)] + C1 * (
                            A[ADDR((i+1),j,k,y,z)] + A[ADDR((i-1),j,k,y,z)] +
                            A[ADDR(i,(j+1),k,y,z)] + A[ADDR(i,(j-1),k,y,z)] +
                            A[ADDR(i,j,(k+1),y,z)] + A[ADDR(i,j,(k-1),y,z)]
                    );
                }
            }
        }
        double * tmp = A;
        A = B;
        B = tmp;
    }
#pragma @ICE endloop
}
