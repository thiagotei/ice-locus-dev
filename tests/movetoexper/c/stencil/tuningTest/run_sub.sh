#!/bin/bash

if [ -z ${SLURM_JOBID+x} ];then
        ice -d 2 -t tuning.yaml -r 1000 -f ./mytiming.py:getTimingStencil -i -g stencil3d.c
else
        ice -a $SLURM_JOBID  -d 2 -t tuning.yaml -r 1000 -f ./mytiming.py:getTimingStencil -i -g stencil3d.c
fi
