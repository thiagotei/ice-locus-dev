#include <stdio.h>

#define ADDR(f,s,t,h,l) ((f*h*l)+(s*l)+t)

void stencil3d(double *A, double *B, 
                const int niter, 
                const size_t x, const size_t y, const size_t z, 
                const double C0, const double C1) {
    int t, i, j, k = 0;
    for (t = 0; t < niter; t++) {
        for(i = 1; i < x-1; i++ ) {
            for(j = 1; j < y-1; j++) {
                for(k = 1; k < z-1; k++) {
                    B[ADDR(i,j,k,y,z)] = C0 * A[ADDR(i,j,k,y,z)] + C1 * (
                            A[ADDR((i+1),j,k,y,z)] + A[ADDR((i-1),j,k,y,z)] +
                            A[ADDR(i,(j+1),k,y,z)] + A[ADDR(i,(j-1),k,y,z)] +
                            A[ADDR(i,j,(k+1),y,z)] + A[ADDR(i,j,(k-1),y,z)]
                    );
#ifdef DEBUG_IT
                    printf("A[%d %d %d %d] %lf ", i, j, k,  ADDR(i,j,k,y,z),A[ADDR(i,j,k,y,z)]);
                    printf("A[%d %d %d %d] %lf ", i+1, j, k,  ADDR((i+1),j,k,y,z),A[ADDR((i+1),j,k,y,z)]);
                    printf("A[%d %d %d %d] %lf ", i-1, j, k,  ADDR((i-1),j,k,y,z),A[ADDR((i-1),j,k,y,z)]);
                    printf("A[%d %d %d %d] %lf ", i, j+1, k,  ADDR(i,(j+1),k,y,z),A[ADDR(i,(j+1),k,y,z)]);
                    printf("A[%d %d %d %d] %lf ", i, j-1, k,  ADDR(i,(j-1),k,y,z),A[ADDR(i,(j-1),k,y,z)]);
                    printf("A[%d %d %d %d] %lf ", i, j, k+1,  ADDR(i,j,(k+1),y,z),A[ADDR(i,j,(k+1),y,z)]);
                    printf("A[%d %d %d %d] %lf \n", i, j, k-1,ADDR(i,j,(k-1),y,z),A[ADDR(i,j,(k-1),y,z)]);
#endif
                }
            }
        }
        double * tmp = A;
        A = B;
        B = tmp;
    }
}
