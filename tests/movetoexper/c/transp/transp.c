#define ind(_i,_j,_bx) ((_i)+(_bx)*(_j))

/* Transpose a(n,m) into b(m,n).
   a(i,j) is in a(i+j*n); that is, in column-major order
 */
void trans(int n, int m, double *a, double *b)
{
    int i, j;

#pragma @ICE loop=trans
    for (i=0; i<n; i++) {
        for (j=0; j<m; j++) {
            a[ind(i,j,n)] = b[ind(j,i,m)];
        }
    }
}

