#!/usr/bin/env python

import argparse
import matplotlib.pyplot as plt
from sys import stdout,exit
import re


def mean(data):
    """Return the sample arithmetic mean of data."""
    n = len(data)
    if n < 1:
        raise ValueError('mean requires at least one data point')
    return sum(data)/n # in Python 2 use sum(data)/float(n)

def _ss(data):
    """Return sum of square deviations of sequence data."""
    c = mean(data)
    ss = sum((x-c)**2 for x in data)
    return ss

def stddev(data, ddof=0):
    """Calculates the population standard deviation
    by default; specify ddof=1 to compute the sample
    standard deviation."""
    n = len(data)
    if n < 2:
        raise ValueError('variance requires at least two data points')
    ss = _ss(data)
    pvar = ss/(n-ddof)
    return pvar**0.5

def miner(exprs, pos, input):
    assert(len(exprs) == len(pos)), "Position and expression dont have the same size."
    status = 0
    found_number = [[] for x in range(len(exprs))]
    for line in input:
#   print line        
        if status % 100 == 0:
            stdout.write("\r%d lines read" % status)
            stdout.flush()
        for e in range(len(exprs)):
            #res1 = exprs[e].findall(line)
            res1 = exprs[e].search(line)
           # print res1
            if(res1):
             #   print res1
             #   for r in res1:
                my_r = re.split(' ',line)
#       print my_r
                f_p = []
                for p in pos[e]:
#                    print p
                    #found_number[e].append(float(my_r.pop(1)))                 
                    #print field
                    #print field[1], " ", field[0]
                    f_p.append(p[1](my_r[p[0]]))
                found_number[e].append(f_p)
        status = status +1
    stdout.write("\nEnd {} lines read.\n".format(status))
    # Found number has list of list of list. The first list holds a list to each pattern, 
    # second list holds a list to each line result for each pattern,
    # the third list hold a position for each field on the line (defined in the pos variable).
    return found_number



parser = argparse.ArgumentParser(description="Overtime stats")
#parser.add_argument('--infile', type=argparse.FileType('r'),
parser.add_argument('infile', type=str,
                    help='input file required from the ice.')
parser.add_argument('--title', type=str, default=None, required=False, help='optional title to the graph')
parser.add_argument('--ymax', type=float, default=None, required=False, help='max optional value y axis')
parser.add_argument('--ymin', type=float, default=None, required=False, help='min optional value y axis')
parser.add_argument('--cc', type=str, default=None, required=False, help='compilers to show on the graph. comma separated')
parser.add_argument('--fig', type=str, default='.pdf', required=False, help='by defining the extension the figure type can be changed.')

re_qr2bi = re.compile("Baseline result metric compiler")
re_tests = re.compile("\[OpenTunerTool\] Run cfg_id:")

list_exprs = []
pos = []

list_exprs.append(re_qr2bi)
list_exprs.append(re_tests)
pos.append([(6,str),(8,str),(9,float)])
pos.append([(3,int),(5,float),(7,str)])

args = parser.parse_args()
with open(args.infile, 'r') as inp:
    the_results = miner(list_exprs, pos, inp)


list_baseline = the_results[0]
list_time_results = the_results[1]

# results per compiler
cc_results = {}

cc_pos = 2
time_pos = 1
if args.cc: 
    cc_allowed = {x.strip() for x in args.cc.split(',')}
    print("cc_allowed:", cc_allowed)

# one serie for each compiler
for res in list_time_results:
    if args.cc and res[cc_pos] not in cc_allowed:
        continue

    if res[cc_pos] not in cc_results:
        cc_results[res[cc_pos]] = []

    cc_results[res[cc_pos]].append(res[time_pos])

#max_tests_each = {}
max_tests = 0
for res, times in cc_results.items():
#    if res not in max_tests_each:
#        max_tests_each[res] = len(times) 

    if len(times) > max_tests:
        max_tests = len(times)

y_total_min = float("inf")
y_total_max = float("-inf")

cc_best_variant = {}
print("Stats:")
for res, times in cc_results.items():
    mean_ = mean(times)
    stddev_ = stddev(times, ddof=1)
    print(res," min: ", min(times)," max: ", max(times), " mean: ", mean_, " stddev: ", stddev_)
    if mean_+(2*stddev_) > y_total_max:
        y_total_max = mean_+(2*stddev_)
    if mean_-(2*stddev_) < y_total_min:
        y_total_min = mean_-(1*stddev_)

    print(res, " ", len(times))
    if res not in cc_best_variant:
        cc_best_variant[res] = []
    min_ = float("inf") 
    for t in times:
        if t < min_:
            min_ = t
        cc_best_variant[res].append(min_)

x = list(range(1,max_tests+1))
#print max_tests_each

#print the_results
#print list_time_results
print(list_baseline)
#print cc_results
#print cc_best_variant

#print x

#exit(0)
#plt.fig(1)
#for res, times in cc_results.iteritems():
for res, times in cc_best_variant.items():
    #plt.plot(x, times, marker='o', linestyle='--', label=res)
    x_each = list(range(1,len(times)+1))
    plt.plot(x_each, times, marker='o', fillstyle='none', linestyle='--', label=res, markevery=15)
    #mean_ = mean(times)
    #stddev_ = stddev(times, ddof=1)
    #print res," min: ", min(times)," max: ", max(times), " mean: ", mean_, " stddev: ", stddev_
    #if mean_+(2*stddev_) > y_total_max:
    #    y_total_max = mean_+(2*stddev_)
    #if mean_-(2*stddev_) < y_total_min:
    #    y_total_min = mean_-(1*stddev_)
    
print("Y Min: ", y_total_min, " Max: ",y_total_max)

for b in list_baseline:
    if args.cc and b[0] not in cc_allowed:
        continue
    if b[1] == '-O3':
        times = [b[2] for i in range(1, max_tests+1)]
        plt.plot(x,times,linestyle='--', label=b[0]+" "+b[1])
      
plt.xlabel("Variants Executed")
plt.ylabel("Execution time (sec)")
plt.xlim([0.5,max_tests+0.5])
y_min = 0.0
y_max = 10.0
if args.ymin:
    y_min = args.ymin
else:
    y_min = y_total_min 
if args.ymax:
    y_max = args.ymax
else:
    y_max = y_total_max
plt.ylim([y_min,y_max])
#plt.axhline(y=5)
#plt.text(10.1,0,'blah',rotation=90)
#plt.text(5.1,1,'blah')
if args.title:
    plt.title(args.title)
else:
    plt.title(args.infile)
plt.legend()
#plt.show()
plt.savefig(args.infile+args.fig)
plt.close()




# print stats
# max min lowest 

