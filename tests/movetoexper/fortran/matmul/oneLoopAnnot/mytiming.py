import sys, re

def getTimingMatMul(std, error):
    try:
        r = re.search('.*Time.*',std)
        m = []
        if r:
            m = re.split('\s*', r.group())
        result = 'N/A'
        if m and len(m) >= 4:
            result = float(m[3])
    except Exception as e:
        sys.stderr.write("[getTimingMatMul] Error {}.\n".format(e))

    return result


