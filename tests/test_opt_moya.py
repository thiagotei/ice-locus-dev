'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import os, re, unittest, sys, logging, itertools

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
from locus.frontend.srccode.regex.sourcecode import SourceCode as SourceCodeRegex
from locus.frontend.srccode.cparser.sourcecode import SourceCode as SourceCodeCparser
from locus.util.constants import ICE_CODEFILE_C
from locus.tools.opts.moya import Moya

log = logging.getLogger(__name__)

class Test_opt_moya(unittest.TestCase):

    def test_moya_specialize_regex(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc_regex.c'
        sr = SourceCodeRegex(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        moya_sp = Moya.Specialize()
        moya_sp.apply(sr, [], {}, 'matmul', None, 0, 0)

        buf = sr.unparseandwrite(lambda x: x)
        with open(dir_path+'/data/mmc_test_moya_specialize_regex.c', 'r') as f:
            codeAnswer1 = f.read()

        self.assertEqual(codeAnswer1, buf)
        #import difflib
        #s = difflib.SequenceMatcher(None, codeAnswer1, buf)
        #for block in s.get_matching_blocks():
        #    print block
        #log.debug("%s",buf)
        #log.debug("%s",codeAnswer1)

    def test_moya_declare_regex(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc_regex.c'
        sr = SourceCodeRegex(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        moya_sp = Moya.Declare()
        moya_sp.apply(sr, [], {}, 'matmul', None, 0, 0)

        buf = sr.unparseandwrite(lambda x: x)
        with open(dir_path+'/data/mmc_test_moya_declare_regex.c', 'r') as f:
            codeAnswer1 = f.read()

        self.assertEqual(codeAnswer1, buf)
        #log.debug(buf)

    def test_moya_specialize_cparser(self):
        pass

if __name__ == '__main__':
    unittest.main()

