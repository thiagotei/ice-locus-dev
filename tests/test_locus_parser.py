'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import re, unittest, sys, logging, itertools, os

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.frontend.optlang.locus.locusvisitor import OptionalStmtVisitor,\
        OrBlockVisitor, OrStmtVisitor, OrStmtAndParentVisitor, GenericVisitor, GenericVisitorWScope
from locus.backend.interpreter import interpreter,topblkinterpreter
from locus.util.misc import Capturing
import locus.frontend.optlang.locus.locusast as modlocusast
import locus.frontend.optlang.locus.optimizer as optimizer

log = logging.getLogger(__name__)

class Test_locus_parser(unittest.TestCase):

    def test_parse_code(self):
        lp = LocusParser()
#        locustree = lp.parse(locuscode1, True)
        locustree = lp.parse(locuscode1, False)

        listmk = modlocusast.ListMaker([modlocusast.Constant(int,0),modlocusast.Constant(int,1),
                    modlocusast.Constant(int,2)])
        assign =        modlocusast.ID("flag")
        assign = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL, assign,
                listmk)
        checktopblk = modlocusast.Block()
        checktopblk.stmts.append(assign)
        checktree = modlocusast.LocusAST(topblk=checktopblk)

        argsTiling = [modlocusast.Argument(modlocusast.ID("loop"),modlocusast.Constant(str,"0.0.0")),
                    modlocusast.Argument(modlocusast.ID("factor"),modlocusast.Constant(int,2))]
        funcTiling = modlocusast.FuncCall(modlocusast.ID("Pips.Tiling"),argsTiling)
        retTiling = modlocusast.Return([modlocusast.Constant(str, "2D")])
        block2D = modlocusast.Block([funcTiling, retTiling])
        optseq2d = modlocusast.OptSeqDef(modlocusast.ID("Tile2D"), block2D)
        func = modlocusast.FuncCall(modlocusast.ID("Tile2D"))
        blockcs = modlocusast.Block([func])
        codeseq = modlocusast.CodeRegDef(modlocusast.ID("matmul"),blockcs)
        checktree[codeseq.id.name] = codeseq
        checktree[optseq2d.id.name] = optseq2d

#        print "== checktree",checktree
#        for name, node   in  checktree:
#            print node
#        print "== parsedtree",locustree
#        for name, node   in  locustree:
#            print node
        compret = modlocusast.compareLocusASTs(checktopblk,locustree.topblk)
        self.assertTrue(compret)

        compret = False
        for symid, symrootA in checktree.items():
#            print "A:",symid,type(symrootA)
            if symid in locustree:
                symrootB = locustree[symid]
#                print "B:",symid,type(symrootB)
                compret = modlocusast.compareLocusASTs(symrootA, symrootB)#symrootA == symrootB
                if not compret:
                    break
            else:
                compret = False
                break

        self.assertTrue(compret)

    def test_ast_orblockvisitor(self):
        lp = LocusParser()
        locustree = lp.parse(locuscode2, False)
#        for name, node in locustree:
#            print node

        node = locustree["Tile2D"]
        visitor = OrBlockVisitor()
        visitor.visit(node)

        self.assertEqual(len(visitor.values), 2)

    def test_ast_orstmtvisitor(self):
        lp = LocusParser()
        locustree = lp.parse(locuscode3, False)
#        for name, node in locustree:
#            print node

        node = locustree["Tile2D"]
        visitor = OrStmtVisitor()
        visitor.visit(node)

        self.assertEqual(len(visitor.values), 3)

    def test_ast_optionalstmt(self):
        lp = LocusParser()
        locustree = lp.parse(locusoptional1, False)

#        for name, node in locustree:
#            print node

        node = locustree["matmul"]
        visitor = OptionalStmtVisitor()
        visitor.visit(node)

        node = locustree["Tile2D"]
        visitor2 = OptionalStmtVisitor()
        visitor2.visit(node)

        self.assertEqual(len(visitor.values)+len(visitor2.values), 3)

    def test_ast_selectorstmt(self):
        lp = LocusParser()
        locustree = lp.parse(locuscode3, False)
        checklocustree = lp.parse(checklocuscode3, False)

        node = locustree["Tile2D"]
        visitor = OrStmtAndParentVisitor()
        visitor.visit(node)

        for p, n in visitor.values:
            p.replace(n, n[1])

        symrootA = checklocustree['Tile2D']
        symrootB = locustree['Tile2D']
        compret = modlocusast.compareLocusASTs(symrootA, symrootB)#symrootA == symrootB

        self.assertTrue(compret)

    def test_replace_myindex(self):
        # There was problem when replacing constant of NoneType
        # the list.index function would replace by the 1st constant none found,
        # since the __eq__ operator on Constant in overload and only compares the value, not
        # the node id. therefore I created a new index function to consider always the node id.
        # this test tries that.

        # Testing replace on Block
        noneNode1 = modlocusast.Constant(type(None), None)
        assA = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL,
                modlocusast.ID("a"),
                modlocusast.SearchPowerOfTwo(modlocusast.RangeExpr(modlocusast.Constant(int,2),modlocusast.Constant(int,1024))))
        noneNode2 = modlocusast.Constant(type(None), None)
        blockA = modlocusast.Block([noneNode1, assA, noneNode2])

        func = modlocusast.FuncCall(modlocusast.ID("Pips.Tiling"),None)
        blockA.replace(noneNode2, func)

        blockA_ids_check = [noneNode1.unqid, assA.unqid, func.unqid]
        blockA_ids = [n.unqid for n in blockA]
        self.assertEqual(blockA_ids_check, blockA_ids)

#        block1 = modlocusast.Block([noneNode1])
#        block2 = modlocusast.Block([assA])
#        block3 = modlocusast.Block([noneNode2])
#        block4 = modlocusast.Block([func])
#        orblockB = modlocusast.OrBlock([block1, block2, block3])
#        orblockB.replace(block3, block4)
#        orblockB_ids_check = [block1.id, block2.id, block4.id]
#        orblockB_ids = [n.id for n in orblockB]
#        self.assertEqual(orblockB_ids_check, orblockB_ids)

        # Testing replace on OrStmt
        funcB = modlocusast.FuncCall(modlocusast.ID("Rose.Interchange"),None)
        orstmtA = modlocusast.OrStmt([noneNode1, func, noneNode2])
        orstmtA.replace(noneNode2, funcB)
        orstmtA_ids_check = [noneNode1.unqid, func.unqid, funcB.unqid]
        orstmtA_ids = [n.unqid for n in orstmtA]
        self.assertEqual(orstmtA_ids_check, orstmtA_ids)

        # Testing replace on ListMaker
        lmakerA = modlocusast.ListMaker([noneNode1, func, noneNode2])
        lmakerA.replace(noneNode2, funcB)
        lmakerA_ids_check = [noneNode1.unqid, func.unqid, funcB.unqid]
        lmakerA_ids = [n.unqid for n in lmakerA]
        self.assertEqual(lmakerA_ids_check, lmakerA_ids)

    def test_searchable_types(self):
        lp = LocusParser()
        locustree = lp.parse(locussearchable1, True)

        argsTiling =[modlocusast.Argument(modlocusast.ID("factor"),
            modlocusast.SearchInteger(modlocusast.RangeExpr(
                modlocusast.Constant(int, 2),modlocusast.Constant(int,10))))]
        func = modlocusast.FuncCall(modlocusast.ID("Pips.Tiling"),argsTiling)
        assA = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL,
                modlocusast.ID("a"),
                modlocusast.SearchPowerOfTwo(modlocusast.RangeExpr(modlocusast.Constant(int,2),modlocusast.Constant(int,1024))))
        assB = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL,
                modlocusast.ID("b"),
                modlocusast.SearchEnum([modlocusast.Constant(str,"O1"),modlocusast.Constant(str,"O2"),
                    modlocusast.Constant(str,"O3")]))
        permA = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL,
                    modlocusast.ID("li"),
                    modlocusast.ListMaker([modlocusast.Constant(int,1),
                        modlocusast.Constant(int,2),
                        modlocusast.Constant(int,3)]))
        assP = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL,
                modlocusast.ID("p"), 
                modlocusast.SearchPerm(modlocusast.ID("li")))
        blockcs = modlocusast.Block([func, assA, assB, permA, assP])
        codeseq = modlocusast.CodeRegDef(modlocusast.ID("jungle"),blockcs)
        #assign = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL,
        glbvar  = modlocusast.ID("globalvar")
        searchnum = modlocusast.SearchEnum([modlocusast.Constant(int,1),modlocusast.Constant(int,2),
                    modlocusast.Constant(int,3),modlocusast.Constant(int,4)])
        assGlb = modlocusast.AssignStmt(modlocusast.AssignStmt.EQUAL, glbvar,
                searchnum)
        blkGlb = modlocusast.Block()
        blkGlb.stmts.append(assGlb)
        #print "%%%"
        #codeseq.show()
        #locustree[codeseq.id.name].show()
        compret = modlocusast.compareLocusASTs(codeseq, locustree[codeseq.id.name] )
        self.assertTrue(compret)
        compret = modlocusast.compareLocusASTs(blkGlb,
            locustree.topblk )
        self.assertTrue(compret)

    def test_scopes(self):
        lp = LocusParser()
        locustree = lp.parse(locuscode2, False)
        locustree.createscope()

#        print type(locustree),locustree.scope

        # For each CodeReg or OptSeq
#        for ent in locustree.entities():
        ent = locustree['Tile2D']
        checklist=[None, 0, 0, 2, 2, 0]
        scopecheck=[['blah','bleh','z'],['i','j'],['blue'],['a'],['j'],['c']]
#        print ent.id.name,":"
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        i = 0
        for _, node in visitor.values:
#            print "\t{",node.scope, "}", "node:",repr(node), "scope.parent:", repr(node.scope.parent)
            #Checking the scope
            currscope = sorted([x for x in node.scope])
#            print currscope
            self.assertEqual(scopecheck[i],currscope)
            #Checking parents of the scope
            self.assertEqual(node.scope.parent,
                    visitor.values[checklist[i]][1].scope if checklist[i] is
                    not None else locustree.topblk.scope)
            i += 1

    def test_interpreter_op(self):
        lp = LocusParser()
        locustree = lp.parse(locusbinaryop1, False)
        locustree.createscope()

        scopecheck = {"g_val": modlocusast.Constant(int,5034), 
                "g_i": modlocusast.Constant(int,13), 
                "g_h": modlocusast.Constant(int,65), 
                "g_p": modlocusast.Constant(int,5000), 
                #"g_t": None, 
                "g_t": modlocusast.Constant(bool, False), 
                "j"  : modlocusast.Constant(int,18),
                "g_z": modlocusast.Constant(int,5)}
        ent = locustree['matmul']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        #print "@@@@ Interpreting topblk..."
        topblkinterpreter(locustree.topblk)
        #print "@@@@ Interpreting codegen..."
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        #if1scope = visitor.values[1][1].scope
        #if2scope = visitor.values[2][1].scope
        topscope = locustree.topblk.scope
        #print "@@@@ ",topscope
        for key in scopecheck:
            #print key
            if key in topscope:
                #print scopecheck[key], topscope[key]
                self.assertEqual(scopecheck[key], topscope[key])
            else:
                #print key
                # did not find the proper key
                self.assertTrue(False)
            #
        #
    #enddef

    def test_block_scop(self):
        """Test block changing the scope when entering a new block."""
        lp = LocusParser()
        locustree = lp.parse(locusblkscop1, False)
        locustree.createscope()

        scopecheck = {"g_i": modlocusast.Constant(int,100),
                      "g_j": modlocusast.Constant(int,118),
                      "g_h": modlocusast.Constant(int,110),
                      "g_k": modlocusast.Constant(int,120)}

        ent = locustree['eita']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        #blkintscope = visitor.values[1][1].scope
        topscope = locustree.topblk.scope
        for key in scopecheck:
            #if key in intscope:
            #    self.assertEqual(scopecheck[key], intscope[key])
            #elif key in blkintscope:
            #    self.assertEqual(scopecheck[key], blkintscope[key])
            if key in topscope:
                self.assertEqual(scopecheck[key], topscope[key])
            else:
                # did not find the proper key
                self.assertTrue(False)
    #enddef

    def test_recursion(self):
        """ test a simple recursion """
        lp = LocusParser()
        locustree = lp.parse(locusrec1)
        locustree.createscope()

        scopecheck = {"g_xal": modlocusast.Constant(int,2)}

        ent = locustree['trec']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        for key in scopecheck:
            #if key in intscope:
            if key in topscope:
                #print key, topscope[key]
                self.assertEqual(scopecheck[key], topscope[key])
        #   #
    #enddef

    def test_recursion_fib(self):
        """ test a fibonacci recursion """
        lp = LocusParser()
        locustree = lp.parse(locusrec2)
        locustree.createscope()

        scopecheck = {"g_xal": modlocusast.Constant(int,34)}

        ent = locustree['trecfib']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        for key in scopecheck:
            #if key in intscope:
            if key in topscope:
                #print key, topscope[key]
                self.assertEqual(scopecheck[key], topscope[key])
        #   #
    #enddef

    def test_conditional_search(self):
        lp = LocusParser()
        locustree = lp.parse(condvar, False)
        locustree.createscope()
        mintI = modlocusast.Constant(int,2)
        mintK = modlocusast.Constant(int,4)
        mintJ = modlocusast.Constant(int,6)
        mintI_2 = modlocusast.Constant(int,7)
        mintK_2 = modlocusast.Constant(int,8)
        mintJ_2 = modlocusast.Constant(int,9)
        scopecheck = {"g_tileI": mintI,
                "g_tileK": mintK,
                "g_tileJ": mintJ,
                "g_tileI_2": mintI_2,
                "g_tileK_2": mintK_2,
                "g_tileJ_2": mintJ_2}
        ent = locustree['matmol']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        for key in scopecheck:
            #if key in intscope:
            if key in topscope:
                #self.assertEqual(scopecheck[key], intscope[key])
                self.assertEqual(scopecheck[key], topscope[key])
            else:
                # did not find the proper key
                self.assertTrue(False)

    def test_max_usedef(self):
        ''' This teh calc of some func (e.g. max, min) on some searchable
        variable to estimate its value. It is used on Opentuner for searchable
        variables that are conditioned on other searchable varibles.'''

        lp = LocusParser()
        locustree = lp.parse(condvarExpr, False)
        locustree.createscope()
        self.searchnodes = {}
        for name, ent in locustree.items():
            log.debug("name: %s",name)
            for ty in modlocusast.SearchPowerOfTwo,modlocusast.SearchEnum:
                visitor = GenericVisitorWScope(ty)
                visitor.visit(ent)
                dictid = {n.unqid:(p,n,s) for p,n,s in visitor.values}
                self.searchnodes.update(dictid)
            #endfor ty
        #endfor optuni

        # Get the vars on the top blk
        for ty in modlocusast.SearchPowerOfTwo,modlocusast.SearchEnum:
            visitor = GenericVisitorWScope(ty)
            visitor.visit(locustree.topblk)
            dictid = {n.unqid:(p,n,s) for p,n,s in visitor.values}
            self.searchnodes.update(dictid)
        #endfor ty

#        dictcheck = {6: (12, 16),#
#                14: (7, 30),#
#                21: (2, 512), # tileI
#                31: (6, 8), # tileI_2
#                41: (28, 33),  #tileI_3
#                47: (4, 512), #tileK
#                54: (6, 512), # tileJ
#                62: (33, 512),
#                70: (2, 1024), #tileK_2
#                78: (33, 519)} #tileJ_2
        dictcheck = [
                (12, 16),#
                (7, 30),#
                (2, 512), # tileI
                (6, 8), # tileI_2
                (28, 33),  #tileI_3
                (4, 512), #tileK
                (6, 512), # tileJ
                (33, 512),
                (2, 1024), #tileK_2
                (33, 519)] #tileJ_2

        dictfound = {}

        for key, value in self.searchnodes.items():
            _, node, nscope = value
#            print "=== start key:", key, node
            paramkey = str(node.unqid)
            if isinstance(node, modlocusast.SearchPowerOfTwo):
#                print "* poftwo min"
                _min = optimizer.calcFuncExprVal(node, nscope, min)
#                print "* poftwo max"
                _max = optimizer.calcFuncExprVal(node, nscope, max)
            elif isinstance(node, modlocusast.SearchEnum):
#                print "* enum min"
                _min = optimizer.calcFuncExprVal(node, nscope, min)
#                print "* enum max"
                _max = optimizer.calcFuncExprVal(node, nscope, max)
            else:
                print("test_locus_parser/test_max_usedef  Searchable type not recognized. ")
            #endif

            #print key,": _min", _min, type(_min) , " _max", _max, type(_max)
            #print key,": _min", _min, " _max", _max
            dictfound[key] = (_min.value, _max.value)
            #dictfound.append( (_min.value, _max.value) )
        #endfor
        #print dictcheck, len(dictcheck)
        #print dictfound
        listfound = [value for (key, value) in sorted(dictfound.items())]
        #print listfound, len(listfound)
        self.assertEqual(listfound, dictcheck)
    #enddef

    def test_interpreter_for(self):
        lp = LocusParser()
        locustree = lp.parse(locusfor1, False)
        locustree.createscope()
#        print locustree.scope

        scopecheck = {"globalvar": modlocusast.Constant(int,23)}
        ent = locustree['jungle']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
#        print "values", visitor.values
        intscope = visitor.values[0][1].scope

        #print "intscope parent",intscope.parent
        #print "intscope",intscope

        key = "globalvar"
        self.assertEqual(scopecheck[key], locustree.topblk.scope[key])

    def test_list_access(self):
        """This test list access and print command."""

        lp = LocusParser()
        locustree = lp.parse(locuslistacc1, False)
        locustree.createscope()

        scopecheck = {"g_res": modlocusast.Constant(str,"4 opa 3")}
        ent = locustree['jundle']
        #ent.show()
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        with Capturing() as output:
            interpreter(ent)
        #this test the print stmt. but i removed because
        #if there is anything being printed out this error will show up and
        # might confuse me in the future.
        #refoutput = ['\"Print worked!! 4 opa 3\"']
        #self.assertEqual(refoutput,output)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        #print "intscope",intscope
        key = "g_res";
        #print(key,":",topscope[key])
        self.assertEqual(scopecheck[key], topscope[key])

    def test_tuple_assign(self):
        """This test tuple assignement"""
        lp = LocusParser()
        locustree = lp.parse(locustupleass1, True)
        locustree.createscope()
        ent = locustree['jundle']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #print visitor.values[0][1].scope
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        #print intscope
        scopecheck = {"g_a": modlocusast.Constant(int,10),
                      "g_b": modlocusast.Constant(int,25),
                      "g_h": modlocusast.TupleMaker((modlocusast.Constant(int,111),
                              modlocusast.Constant(int,222))),
                      "g_p": modlocusast.Constant(int,222),
                      "g_c": modlocusast.Constant(int,15)}
        #print("scopecheck:",scopecheck)
        #print("topscope:",topscope)
        for key in scopecheck:
            self.assertEqual(scopecheck[key],topscope[key])

    def test_seq_call(self):
        """Test seq call """
        lp = LocusParser()
        locustree = lp.parse(locusseqcall1, True)
        locustree.createscope()
        ent = locustree['seqtest']
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        #print intscope
        blist = []
        clist = []
        dlist = []
        for i in range(3,8):
            blist.append(modlocusast.Constant(int, i))
        for i in range(3,11,2):
            clist.append(modlocusast.Constant(int, i))
        for i in range(4,19,3):
            dlist.append(modlocusast.Constant(int, i))
        scopecheck = {"g_b": modlocusast.ListMaker(blist),
                      "g_c": modlocusast.ListMaker(clist),
                      "g_d": modlocusast.ListMaker(dlist)}
        #print intscope
        #for key in "c","b","d":
        #    print key,intscope[key]
        for key in scopecheck:
            #print "scopecheck",key,scopecheck[key],topscope[key]
            self.assertEqual(scopecheck[key],topscope[key])

    def test_if(self):
        lp = LocusParser()
        locustree = lp.parse(locusiftest, False)
        locustree.createscope()
        ent = locustree['blah']
        #ent.show()
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        #print intscope
        scopecheck = {"g_a": modlocusast.Constant(int, 1)}
        key = "g_a"
        #self.assertEqual(scopecheck[key], intscope[key])
        self.assertEqual(scopecheck[key], topscope[key])

    def test_if_elif(self):
        lp = LocusParser()
        locustree = lp.parse(locuselseiftest, False)
        locustree.createscope()
        ent = locustree['blah']
        #print ent
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        #print intscope
        scopecheck = {"g_a": modlocusast.Constant(int, 20)}
        key = "g_a"
        #print intscope[key]
        self.assertEqual(scopecheck[key], topscope[key])

    def test_if_elif_else(self):
        lp = LocusParser()
        locustree = lp.parse(locuselseiftest2, False)
        locustree.createscope()
        ent = locustree['blah']
        #print ent
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        #intscope = visitor.values[0][1].scope
        topscope = locustree.topblk.scope
        #print intscope
        scopecheck = {"g_a": modlocusast.Constant(int, 30)}
        key = "g_a"
        #print topscope[key]
        self.assertEqual(scopecheck[key], topscope[key])

    def test_getenv(self):
        lp = LocusParser()
        locustree = lp.parse(locusgetenvtest, False)
        locustree.createscope()
        ent = locustree['blah']
        keyval = '2304'
        os.environ['ICELOCUS_TEST'] = keyval
        #print "env found?",os.getenv('ICELOCUS_TEST')
        visitor = GenericVisitor(modlocusast.Block)
        visitor.visit(ent)
        topblkinterpreter(locustree.topblk)
        interpreter(ent)
        key = 'l_a'
        topscope = locustree.topblk.scope
        #print(topscope)
        scopecheck = {key: modlocusast.Constant(str, keyval),
                      'l_b': modlocusast.Constant(int, int(keyval)),
                      'l_c': modlocusast.Constant(int, 14),
                      'l_d': modlocusast.Constant(int, 14),
                      'l_e': modlocusast.Constant(int, 17),
                      'l_f': modlocusast.Constant(int, 17),
                      'l_t': modlocusast.Constant(float, 17.0),
                      'l_z': modlocusast.Constant(int, float(keyval)),
                      'l_g': modlocusast.Constant(int, str(None))}
        #print(topscope)
        #self.assertEqual(scopecheck[key], topscope[key])
        self.assertDictEqual(scopecheck, topscope.table)
        os.environ.pop('ICELOCUS_TEST')
    #enddef

    def test_div(self):
        lp = LocusParser()
        locustree = lp.parse(locustestdiv, False)
        locustree.createscope()
        topblkinterpreter(locustree.topblk)
        topscope = locustree.topblk.scope
        scopecheck = {'a': modlocusast.Constant(float, 1.0),
                    'b': modlocusast.Constant(float, 2.0),
                    'a2': modlocusast.Constant(int, 1),
                    'b2': modlocusast.Constant(int, 2),
                    'c': modlocusast.Constant(float, 0.5),
                    'c2': modlocusast.Constant(float, 0.5),
                    'd': modlocusast.Constant(float, 0.0),
                    'd2': modlocusast.Constant(int, 0)}
        self.assertDictEqual(scopecheck, topscope.table)
        #print(topscope)
    ###

    def test_stringescape(self):
        lp = LocusParser()
        locustree = lp.parse(locusescape, False)
        locustree.createscope()
        topblkinterpreter(locustree.topblk)
        #locustree.topblk.showCode()
        topscope = locustree.topblk.scope
        key = 'sedchange'
        answerkey = modlocusast.Constant(str, r"val=2048; sed -e \"s/define M 256/define M ${val}/g\" -e \"s/define N 256/define N ${val}/g\" -e \"s/define K 256/define K ${val}/g\" globals.h > globals.tmp.h;")
        #print(topscope[key], type(topscope[key]),'\n'+str(answerkey))
        self.assertEqual(topscope[key], answerkey)
    ###

    def test_optsetdecor(self):
        lp = LocusParser()
        locustree = lp.parse(locusoptseqdecor, False)
        locustree.createscope()
        #locustree.show()
        #locustree['ops1'].showCode()
        #locustree['ops1'].show()
        self.assertEqual(locustree['ops1'].decor, modlocusast.Decorator('exprec'))
        self.assertEqual(locustree['ops2'].decor, None)
    ###

    def test_emptylist(self):
        lp = LocusParser()
        locustree = lp.parse(locusemptylist, False)
        locustree.createscope()
        topblkinterpreter(locustree.topblk)
        topscope = locustree.topblk.scope
        ent = locustree['ah']
        interpreter(ent)
        answer= {'b': modlocusast.Constant(int,1), 
                'c': modlocusast.Constant(int,3), 
                'o': modlocusast.Constant(int,4)}
        #print(answer)
        #print(topscope)
        self.assertDictEqual(answer, topscope.table)
    ###
####


locusoptseqdecor=r'''
@exprec
OptSeq ops1 (x, b) {
    return x + b;
}
OptSeq ops2 () {
    return x + b;
}
'''

locusemptylist=r'''b=1000;
c=2000;
o=3000;
OptSeq doit (anlst) {
    anlst =  anlst + ["ops"];
    #print "Optseq doit: "+str(anlst);
    return anlst;
}

CodeReg ah {
    newlst = [] + ["leaf"];
    b = len(newlst);
    lst = ["what"];
    lstC = lst + ["va va", "be be"];
    #print str(lstC);
    c = len(lstC);
    optlst = ["mah"];
    optlst = optlst +["upa"];
    {
        optlst = optlst;
        {
            optlst = optlst + ["cow"];
        }
    }
    optlst = doit(optlst);
    #print "Codereg ah: "+str(optlst);
    o = len(optlst);
}
'''

locusescape=r'''
matshapestr="2048";
sedchange = "val="+matshapestr+"; sed -e \"s/define M 256/define M ${val}/g\" -e \"s/define N 256/define N ${val}/g\" -e \"s/define K 256/define K ${val}/g\" globals.h > globals.tmp.h;";
'''

locustestdiv=r'''a = 1.0;
b = 2.0;
c = a / b;
d = a // b; 
a2 = 1;
b2 = 2;
c2 = a2 / b2;
d2 = a2 // b2;
'''

locusgetenvtest=r'''l_a = 10;
l_b = 22;
l_c = 3821;
l_d = 1728;
l_e = 9999;
l_f = 8888;
l_g = "ops";
l_t = "haha";
l_z = "eita";
CodeReg blah {
    l_a = getenv("ICELOCUS_TEST");
    l_z = float(l_a);
    l_b = int(l_a);
    l_c = getenv("ICELOCUS_VAR_DO_NOT_EXIST", default=14);
    l_d = int(l_c);
    l_e = getenv("ICELOCUS_VAR_DO_NOT_EXIST_2", 17);
    l_f = int(l_e);
    l_t = float(l_e);
    l_g = getenv("ICELOCUS_VAR_DO_NOT_EXIST_3");
}
'''

locuselseiftest=r'''g_a = False;
CodeReg blah {
    None;
    a = 1;
    b = True;
    if (b && a > 1)  {
        a  = 10;
    } elif (a == 1) {
        a = 20;
    } else {
        a = 30;
    }
    #print "Value of a:"+str(a);
    g_a = a;
}
'''

locuselseiftest2=r'''g_a = False;
CodeReg blah {
    a = 0;
    b = True;
    if (b && a > 1)  {
        a  = 10;
    } elif (a == 1) {
        a = 20;
    } else {
        a = 30;
    }
    #print "Value of a:"+str(a);
    g_a = a;
}
'''

locusiftest=r'''g_a = False;
CodeReg blah {
    a = 1;
    b = True;
    if (b && a > 1)  {
        a  = 10;
    }
    if (True && False){
        #print "MyWo";
        a = 15;
    }
    if (False && True){
        #print "MyCrazy!!";
        a = 20;
    }
    if (False || False){
        a = 30;
    }
    g_a = a;
}
'''

locusseqcall1=r'''g_b = False;
g_c = False;
g_d = False;
CodeReg seqtest {
    b = seq(3,8);
    c = seq(3,11,2);
    t1,t2,t3 = 4,19,3;
    d = seq(t1,t2,t3);
    g_b = b;
    g_c = c;
    g_d = d;
}
'''

locustupleass1=r'''g_a = False;
g_b = False;
g_c = False;
g_h = False;
g_p = False;
gl = 15;
CodeReg jundle {
    a,b,c = 10,12+13,gl;
    h = (111,222);
    p = h[1];
    g_a = a;
    g_b = b;
    g_c = c;
    g_p = p;
    g_h = h;
}
'''

locuslistacc1=r'''g_res = False;
CodeReg jundle {
    a = [1,2,3];
    b = a[0];
    res = str(b+a[2])+" opa "+str(len(a));
    print "Print worked!! "+res;
    g_res = res;
}
'''

locusbinaryop1=r'''
j = 10;
valid = False;
g_i = False;
g_val = False;
g_h = False;
g_t = False;
g_p = False;
g_z = False;
OptSeq Tiling(loop, mat)
{
    totile = loop;
    vixe = mat;
    return totile+vixe;
}
CodeReg matmul {
    i = j+3;
    j = 18;
    h = i*5;
    z = h % 6;
    p = 27;
    if h == 65 && z == 5 {
        p = 5000;
    }# else {
    #    p = 27;
    #}
    val = Tiling(p, 34);
    if valid {
        t = 100;
        g_t = t;
    }
    g_i = i;
    g_val = val;
    g_h = h;
    g_p = p;
    g_z = z;
}
'''

locusrec1=r'''nl=32; #3;
g_xal = False;
OptSeq recrec(v1) {
    #print "running recrec "+str(v1);
    if (v1 < 4) {
        #print "end v1 "+str(v1);
        return v1; 
    }
    ya = recrec(v1/2);
    #ya = v1;
    return ya;
}
CodeReg trec {
    xal = recrec(nl);
    g_xal = xal;
}
'''

locusrec2=r'''nl=9; #3;
g_xal = False;
OptSeq fib(v1) {
    #print "running recrec "+str(v1);
    if (v1 <= 1) {
        #print "end v1 "+str(v1);
        return v1; 
    }
    ya = fib(v1-1) + fib(v1-2);
    #ya = v1;
    return ya;
}
CodeReg trecfib {
    xal = fib(nl);
    g_xal = xal;
}
'''

locusblkscop1=r'''
g_i = False;
g_j = False;
g_h = False;
g_k = False;
CodeReg eita {
i = 100;
j = 18 +i;
{
    h = 10 + i;
    k = 2 + j;
    g_h = h;
    g_k = k;
}
g_i = i;
g_j = j;
}
'''

locusfor1=r'''
globalvar = 1;
CodeReg jungle {
    for(a=1; a<5; a = a+1) {
        #print globalvar + 3 + a;
        globalvar = globalvar + 3 + a;
    }
}'''

locussearchable1=r'''
globalvar = enum(1,2,3,4);
CodeReg jungle {
    Pips.Tiling(factor=integer(2 .. 10));
    a = poweroftwo(2 .. 1024);
    b = enum("O1", "O2", "O3");
    li = [1,2,3];
    p = permutation(li);
}'''

locusoptional1=r'''
OptSeq Tile2D () {
    *Pips.Tiling(loop="0.0.0", factor=integer(2 .. 10));
}
CodeReg matmul {
    *Tile2D(); i = *t +1;
}'''

locuscode1=r'''
flag = [0, 1, 2];
OptSeq Tile2D () {
    Pips.Tiling(loop="0.0.0", factor=2);
    return "2D";
}
CodeReg matmul {
    Tile2D();
#    i = flag +2;
}'''

locuscode2=r'''
flag1002 = [0, 1, 2];
lunch = [0, 1, 2];
OptSeq Tile2D (blah, bleh) {
    Pips.Tiling(loop="0.0.0", factor=2+h);
    {
        i = H + 2;
        j = flat * 3;
    }
    { blue = red + green; {a = b * 10;} OR { j = z + 1;} } OR { c = d * 20; }
    z = x * w;
    return "2D";
}
CodeReg matmul {
    Tile2D();
}'''

locuscode3=r'''
flag = [0, 1, 2];
OptSeq Tile2D () {
    Pips.Tiling(loop="0.0.0", factor=2);
    {
        i = H + 2;
        j = Pips.Unroll() OR flat * 3;
    }
#    { a = b * 10 OR j = z + 1; } OR { c = d * 20; }
    { a = b * 10 OR z + 1; } OR { c = d * 20; }
    RoseUiuc.stripmine() OR Moya.runtime();
    return "2D";
}
CodeReg matmul {
    Tile2D();
}'''

checklocuscode3=r'''
flag = [0, 1, 2];
OptSeq Tile2D () {
    Pips.Tiling(loop="0.0.0", factor=2);
    {
        i = H + 2;
        j = flat * 3;
    }
    { j = z + 1; } OR { c = d * 20; }
    Moya.runtime();
    return "2D";
}
CodeReg matmul {
    Tile2D();
}'''

condvar=r'''
g_tileI = False;
g_tileK = False;
g_tileJ = False;
g_tileI_2 = False;
g_tileK_2 = False;
g_tileJ_2 = False;
CodeReg matmol {
    tileI = poweroftwo(2 .. 512);
    tileK = integer(4 .. 512);
    tileJ = enum(6,128,256,512);
    tileI_2 = poweroftwo(7 .. tileI);
    tileK_2 = poweroftwo(8 .. tileK);
    tileJ_2 = poweroftwo(9 .. tikeJ);
    g_tileI = tileI;
    g_tileK = tileK;
    g_tileJ = tileJ;
    g_tileI_2 = tileI_2;
    g_tileK_2 = tileK_2;
    g_tileJ_2 = tileJ_2;
}
'''

condvarExpr=r'''
x=14+poweroftwo(12 .. 16);
CodeReg matmol {
    y = x + enum(7,9,x);
    tileI = poweroftwo(2 .. 512);
    tileI_2 = poweroftwo(2+4 .. 3+5);
    tileI_3 = poweroftwo(2+x .. 3+x);
    tileK = integer(4 .. 512);
    tileJ = enum(6,128,256,512);
    tileI_2 = poweroftwo(7+x .. tileI);
    tileK_2 = poweroftwo(tileI .. tileK+tileJ);
    tileJ_2 = poweroftwo(y .. tileJ+7);
}
'''

condvarComplex=r'''
x = 14;
OptSeq tile (tileK, tileJ) {
    tileK_2 = poweroftwo(8 .. tileK);
    tileJ_2 = poweroftwo(9 .. tikeJ);
    return tileK_2, tikeJ_2
}
CodeReg matmal {
    tileI = poweroftwo(2 .. 512);
    tileK = integer(4 .. 512);
    tileJ = enum(6,128,256,512);
    tileI_2 = poweroftwo(7 .. tileI+);
    tileK_2, tileJ_2 = tile(tileK, tileJ);
}
'''
if __name__ == "__main__":
    unittest.main()


