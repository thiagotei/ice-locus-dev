'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import re, unittest, sys, logging, itertools, os

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.frontend.srccode.cparser.coderegion import CodeRegion
import locus.frontend.srccode.cparser.sourcecode as icecparser
from locus.util.constants import ICE_CODEFILE_C
from pycparser import c_ast

log = logging.getLogger(__name__)

code1=r'''
double matA[100*100], matB[100*100], matC[100*100];

double mysecond(void);
void dummy(double *a, double *b, double *c);

int main(int argc, char *argv[])
{
    double tStart, tEnd, tLoop, rate, t;
    int    i, j, k, tests;

#pragma @ICE block=rep
    for (i=0; i<100; i++)
        for (j=0; j<100; j++) {
            matA[(i)*100+j] = 1.0 + i;
            matB[(i)*100+j] = 1.0 + j;
            matC[(i)*100+j] = 0.0;
        }
#pragma @ICE endblock

    tLoop = 1.0e10;
    for (tests=0; tests<maxTest; tests++) {
        tStart = mysecond();
#pragma @ICE loop=matmul
        for (i=0; i<100; i++)
            for (j=0; j<100; j++) {
                for (k=0; k<100; k++)
                    matC[(i)*100+j]+= matA[(i)*100+k] * matB[(k)*100+j];
                }
        tEnd = mysecond();
        t = tEnd - tStart;
        dummy(matA, matB, matC);
        if (t < tLoop) tLoop = t;
    }
        
    return 0;
}
'''

code2=r'''double matA[100 * 100];
double matB[100 * 100];
double matC[100 * 100];
double mysecond(void);
void dummy(double *a, double *b, double *c);
int main(int argc, char *argv[])
{
  double tStart;
  double tEnd;
  double tLoop;
  double rate;
  double t;
  int i;
  int j;
  int k;
  int tests;
  #pragma moya all blah
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
  {
    matA[(i * 100) + j] = 1.0 + i;
    matB[(i * 100) + j] = 1.0 + j;
    matC[(i * 100) + j] = 0.0;
  }


  tLoop = 1.0e10;
  for (tests = 0; tests < maxTest; tests++)
  {
    tStart = mysecond();
    #pragma rose_uiuc 10 4
    for (i = 0; i < 100; i++)
      for (j = 0; j < 100; j++)
    {
      for (k = 0; k < 100; k++)
        matC[(i * 100) + j] += matA[(i * 100) + k] * matB[(k * 100) + j];

    }


    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;
  }

  return 0;
}

'''

code3=r'''#include <stdio.h>
#include <stdlib.h>
double matA[100 * 100];
double matB[100 * 100];
double matC[100 * 100];
double mysecond(void);
void dummy(double *a, double *b, double *c);
int main(int argc, char *argv[])
{
  double tStart;
  double tEnd;
  double tLoop;
  double rate;
  double t;
  int i;
  int j;
  int k;
  int tests;
  #pragma @ICE block=rep
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
  {
    matA[(i * 100) + j] = 1.0 + i;
    matB[(i * 100) + j] = 1.0 + j;
    matC[(i * 100) + j] = 0.0;
  }


  #pragma @ICE endblock
  tLoop = 1.0e10;
  for (tests = 0; tests < 100; tests++)
  {
    tStart = mysecond();
    #pragma @ICE loop=matmul
    for (i = 0; i < 100; i++)
      for (j = 0; j < 100; j++)
    {
      for (k = 0; k < 100; k++)
        matC[(i * 100) + j] += matA[(i * 100) + k] * matB[(k * 100) + j];

    }


    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;
    if (matC[(0 * 100) + 0] < 0)
    {
      fprintf(stderr, "Failed matC sign test\n");
    }
  }

  int i2;
  int j2;
  int k2;
  int tests2;
  for (tests2 = 0; tests2 < 100; tests2++)
  {
    tStart = mysecond();
    #pragma @ICE loop=matmul
    for (i2 = 0; i2 < 100; i2++)
      for (j2 = 0; j2 < 100; j2++)
    {
      for (k2 = 0; k2 < 100; k2++)
        matC[(i2 * 100) + j2] += matA[(i2 * 100) + k2] * matB[(k2 * 100) + j2];

    }


    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;
    if (matC[(0 * 100) + 0] < 0)
    {
      fprintf(stderr, "Failed matC sign test\n");
    }
  }

  printf("Matrix size = %d\n", 100);
  printf("Time        = %.2e secs\n", tLoop);
  rate = ((2.0 * 100) * 100) * (100 / tLoop);
  printf("Rate        = %.2e MFLOP/s\n", rate * 1.0e-6);
  return 0;
}

'''

class Test_cparser_sourcecode(unittest.TestCase):

    def test_parse_pragma(self):
        string = "@ICE loop = matmul"
        res = icecparser.parsePragma(string)
        self.assertEqual(res, (CodeRegion.LOOP , 'matmul'))

        string = "@ICE block=urbana"
        res = icecparser.parsePragma(string)
        self.assertEqual(res, (CodeRegion.BLOCK, 'urbana'))

        string = "@ICE endblock"
        res = icecparser.parsePragma(string)
        self.assertEqual(res, (icecparser.ENDBLOCK , None))

    def test_gen_coderegs(self):
        sr =  icecparser.SourceCode('mmc_fake.c', ICE_CODEFILE_C)
        sr.parseandtraverse(code1)
#        sr =  SourceCode('tests/refact/mmc.c', ICE_CODEFILE_C)
        #sr.readandparse()
    
        srcheck = []
        srcheck.append(CodeRegion('rep', c_ast.Pragma('none'),
                                    c_ast.Pragma('none'),
                                    c_ast.For(None, None, None, None), 
                                    c_ast.For(None, None, None, None),
                                    c_ast.Compound([]), CodeRegion.BLOCK))
        srcheck.append(CodeRegion('matmul', c_ast.Pragma('none'), None,
                                    c_ast.For(None, None, None, None), None,
                                    c_ast.Compound([]), CodeRegion.LOOP))


#        log.debug("CodeRegions:")
        for cr1,cr2 in zip(sr,srcheck):
            self.assertEqual(Test_cparser_sourcecode.comparecoderegions(cr1,cr2),
                True)
           # log.debug(cr1)

    @staticmethod
    def comparecoderegions(cr1, cr2):
        if type(cr1.parent) != type(cr2.parent) or \
           type(cr1.startnode) != type(cr2.startnode) or \
           type(cr1.endnode) != type(cr2.endnode) or \
           type(cr1.pragmabegin) != type(cr2.pragmabegin) or \
           type(cr1.pragmaend) != type(cr2.pragmaend) or \
           cr1.label != cr2.label or \
           cr1.type != cr2.type:
            return False
        else:
            return True

    def test_replace_pragmas(self):
        sr =  icecparser.SourceCode('mmc_fake.c', ICE_CODEFILE_C)
        sr.parseandtraverse(code1)

        # test replacing a loop code region.
        target = 'matmul'
        sr.replaceCoregWithPragma(target, 'rose_uiuc 10 4')
        #sr.unparseandwrite(sys.stdout.write)

        # test replacing a block code region.
        target = 'rep'
        sr.replaceCoregWithPragma(target, 'moya all blah')
        result = sr.unparseandwrite(lambda x: x)
        self.assertEqual(code2, result)

    def test_add_pragmas(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)

        sr =  icecparser.SourceCode('mmc_fake.c', ICE_CODEFILE_C)
        sr.parseandtraverse(code1)

        # test replacing a loop code region.
        target = 'matmul'
        sr.addPragmaLoop(target, 'rose_uiuc 10 4')
        #sr.unparseandwrite(sys.stdout.write)

        # test replacing a block code region.
        target = 'matmul'
        sr.addPragmaLoop(target, 'parallel for 1', "0.0")

        target = 'matmul'
        sr.addPragmaLoop(target, 'parallel for 2', "0.0.0")

        result = sr.unparseandwrite(lambda x: x)
        with open(dir_path+'/data/mmc_test_add_pragmas_cparser.c' , 'r') as f:
            codeAnswer = f.read()

#        log.info(result)
        self.assertEqual(codeAnswer, result)

    # Test getting the includes, adding function mark
    # and generate the tmp file that will be parsed.
    def test_get_includes(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc.c'
        fileoutput = dir_path+"/data/ice_mmc.c"
        checkincludes = ["#include <stdio.h>","#include <stdlib.h>"]

        sr =  icecparser.SourceCode(fileinput, ICE_CODEFILE_C)
        filetouse = sr.saveincludes(fileinput, fileoutput, sr.includes)

        # remove the file created
        if os.path.isfile(fileoutput):
            os.remove(fileoutput)

        self.assertEqual(filetouse, fileoutput)
        self.assertEqual(checkincludes, sr.includes)

    # Test reading, saving the include and 
    # and generate the tmp file and parse it
    # to AST.
    def test_read_and_write(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc.c'

        sr = icecparser.SourceCode(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        res = sr.unparseandwrite(lambda x: x)
        #log.debug(res)
        self.assertEqual(code3, res)

    def test_add_label(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc.c'

        sr = icecparser.SourceCode(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        worked = sr.addLabelLoop('matmul', 'pipslabel', "0.0")
        worked = sr.addLabelLoop('matmul', 'otherlabel', "0.0.0")

        res = sr.unparseandwrite(lambda x: x)
        with open(dir_path+'/data/mmc_test_add_label_cparser.c' , 'r') as f:
            codeAnswer = f.read()

#        log.info("add_label:\n%s",res)
        self.assertEqual(codeAnswer, res)

    def test_replace_pragma_with_label(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc.c'

        sr = icecparser.SourceCode(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        # test replacing a block code region.
        target = 'matmul'
        sr.replaceCoregWithLabel(target, 'coreglabel', pos="0")
        result = sr.unparseandwrite(lambda x: x)
#        log.info(result)
        with open(dir_path+'/data/mmc_test_replace_pragma_with_label_cparser.c' , 'r') as f:
            codeAnswer = f.read()

        self.assertEqual(codeAnswer, result)

    def test_replace_label_with_pragma(self):
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        fileinput = dir_path+'/data/mmc_test_replace_pragma_with_label_cparser.c'

        sr = icecparser.SourceCode(fileinput, ICE_CODEFILE_C)
        sr.readandparse()

        sr.replaceLabelWithCoreg('coreglabel', 'matmul')
        result = sr.unparseandwrite(lambda x: x)
#        log.info(result)
        with open(dir_path+'/data/mmc_test_replace_label_with_pragma_cparser.c' , 'r') as f:
            codeAnswer = f.read()

        self.assertEqual(codeAnswer, result)

if __name__ == '__main__':
    unittest.main()

