'''

@author: thiago
'''

import re, unittest, sys, logging, itertools, os
import chocolate as choco
import argparse

#import test_locus_parser as tlo

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.ice import ICE, argparser as iceargparser
from locus.frontend.optlang.optlanginterface import argparser as optlangargparser
from locus.frontend.optlang.locus.locusparser import LocusParser
from locus.tools.search.chocoopttool_locus import ChocoOptTool
from locus.tools.search.searchtoolinterface import argparser as absstparser
import basetest_search as baseS

log = logging.getLogger(__name__)

argparser = argparse.ArgumentParser(add_help=True,
        parents=[iceargparser,
        optlangargparser,
        absstparser])

class Test_search_choco(unittest.TestCase):

    def basecall(self, srccodepath="./blah.locus", locuspath="./blah.c", lvl=logging.CRITICAL):
        logging.basicConfig(level=lvl)
        logging.root.setLevel(lvl)
        log.setLevel(lvl)
        args = argparser.parse_args(
                ['-t',locuspath,'-f',srccodepath,'-o','suffix','-u','.ice'])
                #[])
        return args
    ###

    def test_convonly(self):
        args = self.basecall(lvl=logging.CRITICAL)#(lvl=logging.INFO)
        lp = LocusParser()
        locustree = lp.parse(baseS.locuscode1, False)
        optuner = ChocoOptTool(args)
        parsedsrcfiles = None
        coregorighashes = None
        optuner.convertOptUni(locustree, parsedsrcfiles, coregorighashes)
        #print(optuner.space)
        conn = choco.SQLiteConnection("sqlite:///choco.db")
        sampler = choco.Random(conn, optuner.space, clear_db=True)
        try:
            for i in range(3):
                token, params = sampler.next()
                #print(token, sorted(params.items()))



                sampler.update(token, 10)
            ##
        except StopIteration as e:
            log.info("All space traversed!")

        os.remove('./choco.db')
        os.remove('./choco.db.lock')
        log.info("Finished")
    ###

####

if __name__ == "__main__":
    unittest.main()


