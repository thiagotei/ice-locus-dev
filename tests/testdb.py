#/usr/bin/env python

import sys, argparse, statistics as stat

if '.' not in sys.path:
    sys.path.insert(0,'.')

if '..' not in sys.path:
    sys.path.insert(0,'..')

import locus.tools.search.resultsdb.models as m
from locus.tools.search.resultsdb.connect import connect

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")

args=parser.parse_args()

def dbnameparse(dbname):
    if '://' not in dbname:
        newdbname = 'sqlite:///' + dbname
    #
    return newdbname
###

def getBestConfBySpace():
    dbname = dbnameparse(args.i)
    engine, session = connect(dbname, True)

#    locs = session.query(m.LocusFile)

#    for l in locs:
#        print(f"LocusFile: {l}")
#        for s in l.searches:
#            print(f"  {s}")
#            for v in s.variants:
#                print(f"    {v}")
#            #
#            for e in s.envvars:
#                str_vals = ",".join(str(x) for x in e.values)
#                print(f"    {e} => {str_vals}")
#            #
#        #
#    #
#
#    join1 = session.query(m.Search,m.EnvVar, m.EnvValue).filter(m.EnvVar.id == m.EnvValue.envvarid)
#    print(f"Eita:")
#    for s, evar, evalue in join1:
#        print(f"  {s} | {evar} | {evalue}")
#    #

    print("######################\n################\n\n")

    #tgthash= '659dec92309add86d0ab703b3389a3a015920f70bdcda665b5bcce851a141d9d'
    tgthash= '659dec92309add86d0ab703b3389a3a015920f70bdcda665b5bcce851a141d9d'
    tgtenvvalues = {'ICELOCUS_MATSHAPE_M':'1024', 'ICELOCUS_MATSHAPE_N':'1024', 
            'ICELOCUS_MTRANSP_STOP': '32', 'ICELOCUS_CXX':'g++', 'ICELOCUS_CC':'gcc'}
    #x = session.query(m.Search).filter(m.Search.locusfile.hash == tgthash, m.Search.envvalues == tgtenvvalues)
    #x = session.query(m.Search).filter(m.Search.locusfile.hash == tgthash).all()
    #x = session.query(m.Search).join(m.Search.locusfile).filter(m.LocusFile.hash == tgthash).all()
    matchedsearches = []
    #x = session.query(m.Search).filter(m.LocusFile.hash == tgthash)
    x = session.query(m.Search).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
    #for e in x:
    #    print(f"Search: {e.id}")

    #return

    for e in x:
        print(f"Search: {e.id}")
        searchmatched = True 
        for evalue in e.envvalues:
            envvar = evalue.envvar
            #print(f" {envvar.name} {type(envvar.name)} {tgtenvvalues[envvar.name]}")
            if envvar.name in tgtenvvalues and evalue.value == tgtenvvalues[envvar.name]:
                print(f"Matches: {envvar.name} {evalue.value}")
            else:
                print(f"It does not so abort! {envvar.name} {evalue.value}")
                searchmatched = False
                break
            #
        #
        if searchmatched:
            print(f"Searched {e.id} matches!")
            matchedsearches.append(e)
        #
        #print(f"Opa X: {e}")
    #

#    confsbest = {}
#    for ms in matchedsearches:
#        print(f"{ms.id}...")
#        for v in ms.variants:
#            emetrics = [e.metric for e in v.experiments]
#            metric = stat.median(emetrics)
#
#            if v.configurationid not in confsbest:
#                print(f"Adding confid {v.configurationid} to confsbest.")
#                confsbest[v.configurationid] = metric
#            else:
#                if metric < confsbest[v.configurationid]:
#                    print(f"Found better metric for conf:  {v.configurationid}"
#                            f" search: {ms.id} variant: {v.id} metric: {metric}")
#                    confsbest[v.configurationid] = metric
#                else:
#                    print(f"Found worse metric for conf:  {v.configurationid}"
#                            f" search: {ms.id} variant: {v.id} metric: {metric}")
#                #
#            #
#        #
#    #
#
#    for confid in confsbest:
#        print(f"Looking for conf: {confid}")
#        conf = session.query(m.Configuration).filter(confid == m.Configuration.id).one()
#        data = conf.data
#        print(f"Conf {conf.id} data: {data}")
#    #

    print("######################\n################\n\n")
    bestsofar = float('inf')
    confobj = None
    allconfs = []
    for ms in matchedsearches:
        for v in ms.variants:
            emetrics = [e.metric for e in v.experiments]
            metric = stat.median(emetrics)
        #
            if metric < bestsofar:
                bestsofar = metric
                confobj = (v.id, v.configurationid)
            ##
            print(f"Appending varid: {v.id} confid: {v.configurationid} metric: {metric}")
            allconfs.append((v.id, v.configurationid, metric))
    ##

    for vid, confid, metric in sorted(allconfs, key=lambda x: x[2])[:2]:
        print(f"Looking for conf: {confid}")
        conf = session.query(m.Configuration).filter(confid == m.Configuration.id).one()
        data = conf.data
        print(f"Conf {conf.id} data: {data} varid: {vid} metric: {metric}")

    session.close()
###


if __name__ == '__main__':
    getBestConfBySpace()
#
