'''

@author: thiago
'''
# vim: set expandtab:tabstop=4:softtabstop=4:shiftwidth=4:textwidth=80:smarttab:autoindent:

import re, unittest, sys, logging, itertools

if '.' not in sys.path:
    sys.path.insert(0,'.')
if '..' not in sys.path:
    sys.path.insert(0, '..')

from locus.frontend.srccode.regex.coderegionannot import CodeRegionAnnot
import locus.frontend.srccode.regex.sourcecode as icecparser
from locus.util.constants import ICE_CODEFILE_C

log = logging.getLogger(__name__)

code1=r'''
double matA[100*100], matB[100*100], matC[100*100];

double mysecond(void);
void dummy(double *a, double *b, double *c);

int main(int argc, char *argv[])
{
    double tStart, tEnd, tLoop, rate, t;
    int    i, j, k, tests;

#pragma @ICE block=rep
    for (i=0; i<100; i++)
        for (j=0; j<100; j++) {
            matA[(i)*100+j] = 1.0 + i;
            matB[(i)*100+j] = 1.0 + j;
            matC[(i)*100+j] = 0.0;
        }
#pragma @ICE endblock

    tLoop = 1.0e10;
    for (tests=0; tests<maxTest; tests++) {
        tStart = mysecond();
#pragma @ICE loop=matmul
        for (i=0; i<100; i++)
            for (j=0; j<100; j++) {
                for (k=0; k<100; k++)
                    matC[(i)*100+j]+= matA[(i)*100+k] * matB[(k)*100+j];
                }
        tEnd = mysecond();
        t = tEnd - tStart;
        dummy(matA, matB, matC);
        if (t < tLoop) tLoop = t;
    }
        
    return 0;
}
'''

code2=r'''double matA[100 * 100];
double matB[100 * 100];
double matC[100 * 100];
double mysecond(void);
void dummy(double *a, double *b, double *c);
int main(int argc, char *argv[])
{
  double tStart;
  double tEnd;
  double tLoop;
  double rate;
  double t;
  int i;
  int j;
  int k;
  int tests;
  #pragma moya all blah
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
  {
    matA[(i * 100) + j] = 1.0 + i;
    matB[(i * 100) + j] = 1.0 + j;
    matC[(i * 100) + j] = 0.0;
  }


  tLoop = 1.0e10;
  for (tests = 0; tests < maxTest; tests++)
  {
    tStart = mysecond();
    #pragma rose_uiuc 10 4
    for (i = 0; i < 100; i++)
      for (j = 0; j < 100; j++)
    {
      for (k = 0; k < 100; k++)
        matC[(i * 100) + j] += matA[(i * 100) + k] * matB[(k * 100) + j];

    }


    tEnd = mysecond();
    t = tEnd - tStart;
    dummy(matA, matB, matC);
    if (t < tLoop)
      tLoop = t;

  }

  return 0;
}

'''

class Test_regex_sourcecode(unittest.TestCase):

    def test_replace_pragmas(self):
        pass

if __name__ == '__main__':
    unittest.main()

