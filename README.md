Locus
=========

Locus is a system for program optimization that is able to 
use multiple code optimization tools and search techniques.
It also includes a language for optimization spaces.

More information [on the wiki](https://bitbucket.org/thiagotei/ice-locus-dev/wiki/Home).

Installation
-------

1. Download the source code: `git clone https://bitbucket.org/thiagotei/ice-locus-dev.git <locus path>`
2. Install the virtual environment tool: `pip install virtualenv`
3. Create a Virtual Environment (Locus versions >=1.4 are Python3): `virtualenv <env path> --python=python3`
4. Activate the Virtual Environment:  `source <env path>/bin/activate`
5. Go to the Locus source directory: `cd <locus path>`
7. Install Locus in the environment: `pip install .` (or for develop `pip install -e .`)
8. Check if you see Locus binaries in the environment: `locus --help`

Do not forget to activate the environment when using the system!  

Checking Installation
-------

1. `cd <locus path>/tests`
2. `python all_tests.py`


Locus Language
-------
Please look at the [wiki](https://bitbucket.org/thiagotei/ice-locus-dev/wiki/Home) or the following paper for more information about the syntax of the language :

Locus: A System and a Language for Program Optimization. Thiago S. F. X. Teixeira, Corinne Ancourt, David Padua, William Gropp. CGO, 2019, Washington DC, USA.

Metric Evaluation
-------

The implementation of the function that gets the metric out of the stdout, stderr, or a file must accept two 
arguments that are the stdout and stderr resulted from the execution command. The function must also return a
float type number. An output example and a function to parse it are shown below.
This is necessary only when empirical search is used.

This is the output of the program in the stdout:
```
Matrix size = 10
Time        = 1.91e-06 secs
Rate        = 1.05e+03 MFLOP/s
```
The code in the mytiming.py file returns the value `1.91e-06` from the output above. The function code is:
```
#!python
# This is mytiming.py
import re

def getTiming(std, error):
    m = re.split('\s*', error)
    result = 'N/A'
    if m and len(m) >= 3:
       result = float(m[2])

    return result
```
Command line example using the function implemented:
```
locus -t ${OPT} -f ${SRCFILE} --tfunc mytiming.py:getTiming 
```

Annotations on the Code
-------

The loops blocks of code are marked differently depending on the language (and parser). 
We recommend the use on C codes (C++ and Fortran have limited support).

Blocks:

* C:  
    `#pragma @ICE block=<id>` and `#pragma @ICE endblock` 

Loops:  

* C:  
    `#pragma @ICE loop=<id>` 

#### C Code Example
```
#!c
void example(int n, int * prefix) {

#pragma @ICE block=example1
        for(int i=0; i < n; i++) {
                *prefix  = *prefix - n;
        }   
#pragma @ICE endblock

#pragma @ICE loop=l2
        for(int i=1; i <n ; i++) {
                *prefix = *prefix *i;     
        }   
}
```

Citation
-------

Plase cite the following paper:

@inproceedings{Teixeira:2019:LSL:3314872.3314898,
 author = {Teixeira, Thiago S. F. X. and Ancourt, Corinne and Padua, David and Gropp, William},
 title = {Locus: A System and a Language for Program Optimization},
 booktitle = {Proceedings of the 2019 IEEE/ACM International Symposium on Code Generation and Optimization},
 series = {CGO 2019},
 year = {2019},
 isbn = {978-1-7281-1436-1},
 location = {Washington, DC, USA},
 pages = {217--228},
 numpages = {12},
 url = {http://dl.acm.org/citation.cfm?id=3314872.3314898},
 acmid = {3314898},
 publisher = {IEEE Press},
 address = {Piscataway, NJ, USA},
 keywords = {code generation, compilers, domain-specific language, optimization},
} 


